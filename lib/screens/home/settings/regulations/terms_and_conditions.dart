import 'package:flutter/material.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

class TermsAndConditionsPage extends StatefulWidget {
  TermsAndConditionsPage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _TermsAndConditionsPageState createState() => _TermsAndConditionsPageState();
}

class _TermsAndConditionsPageState extends State<TermsAndConditionsPage> {
  //Booleano usato per attivare lo spinner
  bool loading = false;

  static const String title = "Termini & Condizioni\n";

  static const String generalCondition =
      "Scaricando o utilizzando l'app, questi termini si applicheranno automaticamente: assicurati quindi di leggerli attentamente prima di utilizzare l'app. Non è consentito copiare o modificare in alcun modo l'app, qualsiasi parte dell'app o i nostri marchi. Non puoi tentare di estrarre il codice sorgente dell'app e non dovresti nemmeno provare a tradurre l'app in altre lingue o creare versioni derivate. L'app stessa, e tutti i marchi, i diritti d'autore, i diritti sui database e altri diritti di proprietà intellettuale ad essa correlati, appartengono ancora a ZampOn.\n \nZampOn si impegna a garantire che l'app sia il più utile ed efficiente possibile. Per tale motivo, ci riserviamo il diritto di apportare modifiche all'app o di addebitarne i servizi, in qualsiasi momento e per qualsiasi motivo. Non ti addebiteremo mai l'app o i suoi servizi senza chiarirti esattamente per cosa stai pagando.\n \nL'app ZampOn memorizza ed elabora i dati personali che ci hai fornito, al fine di fornire il nostro Servizio. È tua responsabilità proteggere il tuo telefono e l'accesso all'app. Ti consigliamo quindi di non eseguire il jailbreak o il root del tuo telefono, che è il processo di rimozione delle restrizioni e limitazioni software imposte dal sistema operativo ufficiale del tuo dispositivo. Potrebbe rendere il tuo telefono vulnerabile a malware/virus/programmi dannosi, compromettere le funzionalità di sicurezza del tuo telefono e potrebbe significare che l'app ZampOn non funzionerà correttamente o del tutto.\n n\L'app utilizza servizi di terze parti che dichiarano i propri Termini e Condizioni.\n n\Collegamento a Termini e condizioni di fornitori di servizi di terze parti utilizzati dall'app\n \nServizi di Google Play\nGoogle Analytics per Firebase\nFirebase Crashlytics\nFacebook\n \nDovresti essere consapevole del fatto che ci sono alcune cose di cui ZampOn non si assumerà la responsabilità. Alcune funzioni dell'app richiedono che l'app disponga di una connessione Internet attiva. La connessione può essere Wi-Fi o fornita dal tuo gestore di rete mobile, ma ZampOn non può assumersi la responsabilità del mancato funzionamento dell'app se non hai accesso al Wi-Fi e non hai nessuno dei tuoi margini di dati restanti.\n \nSe stai utilizzando l'app al di fuori di un'area con Wi-Fi, dovresti ricordare che i termini del contratto con il tuo gestore di rete mobile continueranno ad applicarsi. Di conseguenza, il tuo gestore di telefonia mobile potrebbe addebitarti il costo dei dati per la durata della connessione durante l'accesso all'app o altri costi di terze parti. Utilizzando l'app, ti assumi la responsabilità di tali addebiti, inclusi i costi per i dati in roaming, se utilizzi l'app al di fuori del tuo territorio (ad esempio regione o paese) senza disattivare il roaming dati. Se non sei il pagatore della bolletta per il dispositivo su cui stai utilizzando l'app, tieni presente che presupponiamo che tu abbia ricevuto l'autorizzazione dal pagatore della bolletta per l'utilizzo dell'app.\n \nAllo stesso modo, ZampOn non può sempre assumersi la responsabilità del modo in cui utilizzi l'app, ovvero devi assicurarti che il tuo dispositivo rimanga carico: se la batteria si esaurisce e non puoi accenderlo per usufruire del servizio, ZampOn non può accettare la responsabilità.\n \nPer quanto riguarda la responsabilità di ZampOn per l'utilizzo dell'app, quando si utilizza l'app, è importante tenere presente che, sebbene ci sforziamo di garantire che sia aggiornata e corretta in ogni momento, ci affidiamo a terze parti per fornire informazioni a noi in modo che possiamo metterle a tua disposizione. ZampOn non si assume alcuna responsabilità per eventuali perdite, dirette o indirette, che si verificano a causa dell'affidamento completo a questa funzionalità dell'app.\n \nAd un certo punto, potremmo voler aggiornare l'app. L'app è attualmente disponibile su Android: i requisiti per il sistema (e per eventuali sistemi aggiuntivi a cui decidiamo di estendere la disponibilità dell'app) potrebbero cambiare e dovrai scaricare gli aggiornamenti se desideri continuare a utilizzare l'app. ZampOn non promette che aggiornerà sempre l'app in modo che sia rilevante per te e/o funzioni con la versione Android che hai installato sul tuo dispositivo. Tuttavia, prometti di accettare sempre gli aggiornamenti dell'applicazione quando ti vengono offerti, potremmo anche voler interrompere la fornitura dell'app e potremmo interromperne l'uso in qualsiasi momento senza darti preavviso di cessazione. Salvo diversa indicazione da parte nostra, in caso di risoluzione,\n \n(a) i diritti e le licenze a te concessi in questi termini cesseranno; \n(b) devi interrompere l'utilizzo dell'app e (se necessario) eliminarla dal tuo dispositivo.\n";

  static const String changesThisTermsConditionsTitle =
      "Modifiche ai presenti Termini e condizioni\n";

  static const String changesThisTermsConditions =
      "Potremmo aggiornare i nostri Termini e condizioni di volta in volta. Pertanto, si consiglia di rivedere periodicamente questa pagina per eventuali modifiche. Ti avviseremo di eventuali modifiche pubblicando i nuovi Termini e Condizioni su questa pagina.\n \nQuesti termini e condizioni sono in vigore dal 22-09-2021\n";

  static const String contactUsTitle = "Contattaci\n";

  static const String contactUs =
      "Se hai domande o suggerimenti sulla nostra Informativa sulla privacy, non esitare a contattarci a:\n \ninfo.zampon@gmail.com\n\n\n\n\n\n";

  static const String empty = "";

  Widget _title() {
    return Text(title,
        style: informationTitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _generalCondition() {
    return Text(generalCondition,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _changesThisTermsConditionsTitle() {
    return Text(changesThisTermsConditionsTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _changesThisTermsConditions() {
    return Text(changesThisTermsConditions,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _contactUsTitle() {
    return Text(contactUsTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _contactUs() {
    return Text(contactUs,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _filler() {
    return Text(empty,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _fillerTwo() {
    return Text(empty,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  @override
  Widget build(BuildContext context) {
    //lista dei widget che saranno incolonnati nella pagina
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_title());
    columnElementList.add(_generalCondition());
    columnElementList.add(_changesThisTermsConditionsTitle());
    columnElementList.add(_changesThisTermsConditions());
    columnElementList.add(_contactUsTitle());
    columnElementList.add(_contactUs());
    columnElementList.add(_filler());
    columnElementList.add(_fillerTwo());

    return loading
        ? Loading()
        : genericPage(context, true,
            columnElementList: columnElementList,
            appbar: true,
            trasparent: false,
            noTitle: false);
  }
}
