import 'package:flutter/material.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

class PrivacyPolicyPage extends StatefulWidget {
  PrivacyPolicyPage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _PrivacyPolicyPageState createState() => _PrivacyPolicyPageState();
}

class _PrivacyPolicyPageState extends State<PrivacyPolicyPage> {
  //Booleano usato per attivare lo spinner
  bool loading = false;

  static const String title = "Privacy Policy\n";

  static const String generalCondition =
      "Il team ha creato l'app ZampOn come app gratuita. Questo SERVIZIO è fornito da ZampOn gratuitamente ed è destinato all'uso così com'è.\n \nQuesta pagina viene utilizzata per informare i visitatori in merito alle nostre politiche con la raccolta, l'uso e la divulgazione di informazioni personali se qualcuno decide di utilizzare il nostro servizio.\n \nSe scegli di utilizzare il nostro Servizio, accetti la raccolta e l'utilizzo delle informazioni in relazione a questa politica. Le informazioni personali che raccogliamo vengono utilizzate per fornire e migliorare il servizio. Non utilizzeremo né condivideremo le tue informazioni con nessuno se non come descritto nella presente Informativa sulla privacy.\n \nI termini utilizzati nella presente Informativa sulla privacy hanno lo stesso significato dei nostri Termini e condizioni, accessibili su ZampOn se non diversamente definito nella presente Informativa sulla privacy.\n";

  static const String collectionAndUseTitle =
      "Raccolta e utilizzo delle informazioni\n";

  static const String collectionAndUse =
      "Per una migliore esperienza, durante l'utilizzo del nostro Servizio, potremmo richiederti di fornirci alcune informazioni di identificazione personale. Le informazioni che richiediamo verranno da noi conservate e utilizzate come descritto nella presente informativa sulla privacy.\n \nL'app utilizza servizi di terze parti che possono raccogliere informazioni utilizzate per identificarti.\n \nLink all'informativa sulla privacy dei fornitori di servizi terzi utilizzati dall'app\n \nServizi di Google Play\nGoogle Analytics per Firebase\nFirebase Crashlytics\nFacebook\n";

  static const String logDataTitle = "Log Data\n";

  static const String logData =
      "Desideriamo informarti che ogni volta che utilizzi il nostro Servizio, in caso di errore nell'app raccogliamo dati e informazioni (tramite prodotti di terze parti) sul tuo telefono chiamati Log Data.\n \nQuesti dati di registro possono includere informazioni come l'indirizzo del protocollo Internet (“IP”) del dispositivo, il nome del dispositivo, la versione del sistema operativo, la configurazione dell'app durante l'utilizzo del nostro Servizio, l'ora e la data di utilizzo del Servizio e altre statistiche.\n";

  static const String cookiesTitle = "Cookies\n";

  static const String cookies =
      "I cookie sono file con una piccola quantità di dati che vengono comunemente utilizzati come identificatori univoci anonimi. Questi vengono inviati al tuo browser dai siti Web che visiti e vengono archiviati nella memoria interna del tuo dispositivo.\n \nQuesto Servizio non utilizza questi “cookie“ in modo esplicito. Tuttavia, l'app potrebbe utilizzare codici e librerie di terze parti che utilizzano “cookie“ per raccogliere informazioni e migliorare i propri servizi. Hai la possibilità di accettare o rifiutare questi cookie e sapere quando un cookie viene inviato al tuo dispositivo. Se scegli di rifiutare i nostri cookie, potresti non essere in grado di utilizzare alcune parti di questo Servizio.\n";

  static const String serviceProvidersTitle = "Service Providers\n";

  static const String serviceProviders =
      "Possiamo assumere società e individui di terze parti per i seguenti motivi:\n \nPer facilitare il nostro Servizio;\nPer fornire il Servizio per nostro conto;\nPer eseguire lavori relativi al Servizio;\nPer aiutarci ad analizzare come viene utilizzato il nostro Servizio.\n \nDesideriamo informare gli utenti di questo Servizio che queste terze parti hanno accesso alle tue Informazioni Personali. Il motivo è svolgere i compiti loro assegnati per nostro conto. Tuttavia, sono obbligati a non divulgare o utilizzare le informazioni per nessun altro scopo.\n";

  static const String securityTitle = "Sicurezza\n";

  static const String security =
      "Apprezziamo la tua fiducia nel fornirci le tue informazioni personali, quindi ci sforziamo di utilizzare mezzi commercialmente accettabili per proteggerle. Ma ricorda che nessun metodo di trasmissione su Internet o metodo di archiviazione elettronica è sicuro e affidabile al 100% e non possiamo garantirne la sicurezza assoluta.\n";

  static const String linksOtherSitesTitle = "Collegamenti ad altri siti\n";

  static const String linksOtherSites =
      "Questo Servizio può contenere collegamenti ad altri siti. Se fai clic su un collegamento di terze parti, verrai indirizzato a quel sito. Tieni presente che questi siti esterni non sono gestiti da noi. Pertanto, ti consigliamo vivamente di rivedere l'Informativa sulla privacy di questi siti Web. Non abbiamo alcun controllo e non ci assumiamo alcuna responsabilità per il contenuto, le politiche sulla privacy o le pratiche di siti o servizi di terze parti.\n";

  static const String childrensPrivacyTitle = "Privacy dei bambini\n";

  static const String childrensPrivacy =
      "Questi Servizi non si rivolgono a persone di età inferiore ai 13 anni. Non raccogliamo consapevolmente informazioni di identificazione personale da bambini di età inferiore ai 13 anni. Nel caso in cui scoprissimo che un bambino di età inferiore ai 13 anni ci ha fornito informazioni personali, le cancelliamo immediatamente dai nostri server. Se sei un genitore o tutore e sei consapevole che tuo figlio ci ha fornito informazioni personali, ti preghiamo di contattarci in modo che saremo in grado di intraprendere le azioni necessarie.\n";

  static const String changesThisPolicyTitle =
      "Modifiche alla presente Informativa sulla privacy\n";

  static const String changesThisPolicy =
      "Potremmo aggiornare la nostra Informativa sulla privacy di volta in volta. Pertanto, si consiglia di rivedere periodicamente questa pagina per eventuali modifiche. Ti avviseremo di eventuali modifiche pubblicando la nuova Privacy Policy in questa pagina.\n \nQuesta politica è in vigore dal 22-09-2021\n";

  static const String contactUsTitle = "Contattaci\n";

  static const String contactUs =
      "Se hai domande o suggerimenti sulla nostra Informativa sulla privacy, non esitare a contattarci all'indirizzo:\n \ninfo.zampon@gmail.com\n\n\n\n\n\n";

  static const String empty = "";

  Widget _title() {
    return Text(title,
        style: informationTitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _generalCondition() {
    return Text(generalCondition,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _collectionAndUseTitle() {
    return Text(collectionAndUseTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _collectionAndUse() {
    return Text(collectionAndUse,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _logDataTitle() {
    return Text(logDataTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _logData() {
    return Text(logData,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _cookiesTitle() {
    return Text(cookiesTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _cookies() {
    return Text(cookies,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _serviceProvidersTitle() {
    return Text(serviceProvidersTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _serviceProviders() {
    return Text(serviceProviders,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _securityTitle() {
    return Text(securityTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _security() {
    return Text(security,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _linksOtherSitesTitle() {
    return Text(linksOtherSitesTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _linksOtherSites() {
    return Text(linksOtherSites,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _childrensPrivacyTitle() {
    return Text(childrensPrivacyTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _childrensPrivacy() {
    return Text(childrensPrivacy,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _changesThisPolicyTitle() {
    return Text(changesThisPolicyTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _changesThisPolicy() {
    return Text(changesThisPolicy,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _contactUsTitle() {
    return Text(contactUsTitle,
        style: informationSubtitelStyle(context), textAlign: TextAlign.left);
  }

  Widget _contactUs() {
    return Text(contactUs,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _filler() {
    return Text(empty,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  Widget _fillerTwo() {
    return Text(empty,
        style: informationBodyStyle(context), textAlign: TextAlign.left);
  }

  @override
  Widget build(BuildContext context) {
    //lista dei widget che saranno incolonnati nella pagina
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_title());
    columnElementList.add(_generalCondition());
    columnElementList.add(_collectionAndUseTitle());
    columnElementList.add(_collectionAndUse());
    columnElementList.add(_logDataTitle());
    columnElementList.add(_logData());
    columnElementList.add(_cookiesTitle());
    columnElementList.add(_cookies());
    columnElementList.add(_serviceProvidersTitle());
    columnElementList.add(_serviceProviders());
    columnElementList.add(_securityTitle());
    columnElementList.add(_security());
    columnElementList.add(_linksOtherSitesTitle());
    columnElementList.add(_linksOtherSites());
    columnElementList.add(_childrensPrivacyTitle());
    columnElementList.add(_childrensPrivacy());
    columnElementList.add(_changesThisPolicyTitle());
    columnElementList.add(_changesThisPolicy());
    columnElementList.add(_contactUsTitle());
    columnElementList.add(_contactUs());
    columnElementList.add(_filler());
    columnElementList.add(_fillerTwo());

    return loading
        ? Loading()
        : genericPage(context, true,
            columnElementList: columnElementList,
            appbar: true,
            trasparent: false,
            noTitle: false);
  }
}
