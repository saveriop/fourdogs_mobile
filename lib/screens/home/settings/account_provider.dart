import 'package:flutter/material.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/settings/account_summary.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:provider/provider.dart';

class AccountProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final UserData? user = Provider.of<UserData?>(context);
    if (user != null) {
      return AccountSummary(user: user);
    } else {
      return Loading();
    }
  }
}
