import 'package:flutter/material.dart';
import 'package:zampon/screens/home/newAds/steps/informative_text.dart';
import 'regulations/terms_and_conditions.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/screens/home/settings/supportUs/support_ZampOn_page.dart';
import 'package:zampon/screens/home/settings/userProfilePage/personal_profile_stream_provider.dart';
import 'package:zampon/screens/home/settings/myAds/my_ads.dart';
import 'package:zampon/screens/home/settings/regulations/privacy_policy.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';

class AccountSummary extends StatefulWidget {
  final UserData user;

  AccountSummary({required this.user});

  @override
  _AccountSummaryState createState() => _AccountSummaryState();
}

class _AccountSummaryState extends State<AccountSummary> {
  Widget _personalProfile() {
    return accountNavigationButton(
      context,
      Icon(
        CustomIcons.i42_profile_uman_icon,
        color: customStrongLiliac,
        size: MediaQuery.of(context).size.height * 0.04,
      ),
      AppLocalizations.of(context)!.personalProfile,
      () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PersonalProfileStreamProvider(
                      userUid: this.widget.user.uid,
                      calledUid: this.widget.user.uid,
                    )));
      },
    );
  }

  ///Tasto per creare un nuovo annuncio
  Widget _newAds() {
    return accountNavigationButton(
      context,
      Icon(
        CustomIcons.i14_create_advert_icon,
        color: customStrongLiliac,
        size: MediaQuery.of(context).size.height * 0.04,
      ),
      AppLocalizations.of(context)!.postAnAd,
      () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => InformativeText(
                      user: this.widget.user,
                    )));
      },
    );
  }

  ///Tasto i miei annunci
  Widget _myAds() {
    return accountNavigationButton(
      context,
      Icon(
        CustomIcons.i40_my_alerts,
        color: customStrongLiliac,
        size: MediaQuery.of(context).size.height * 0.04,
      ),
      AppLocalizations.of(context)!.yourAds,
      () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MyAds(userData: this.widget.user)));
      },
    );
  }

  ///Tasto Supportaci
  Widget _supportZampOn() {
    return accountNavigationButton(
      context,
      Icon(
        CustomIcons.i38_donation_icon,
        color: customStrongLiliac,
        size: MediaQuery.of(context).size.height * 0.04,
      ),
      AppLocalizations.of(context)!.supportZampOn,
      () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => SupportZampOnPage()));
      },
    );
  }

  Widget _privacyPolicy() {
    return accountNavigationButton(
      context,
      Icon(
        CustomIcons.i41_privacy_policy,
        color: customStrongLiliac,
        size: MediaQuery.of(context).size.height * 0.04,
      ),
      AppLocalizations.of(context)!.privacyPolicy,
      () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => PrivacyPolicyPage()));
      },
    );
  }

  Widget _termsAndCondition() {
    return accountNavigationButton(
      context,
      Icon(
        CustomIcons.i43_terms_and_conditions,
        color: customStrongLiliac,
        size: MediaQuery.of(context).size.height * 0.04,
      ),
      AppLocalizations.of(context)!.termsConditions,
      () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => TermsAndConditionsPage()));
      },
    );
  }

  Widget _logOut(AuthService _auth) {
    return accountNavigationButton(
        context,
        Icon(
          CustomIcons.i39_exit_icon,
          color: customStrongLiliac,
          size: MediaQuery.of(context).size.height * 0.04,
        ),
        AppLocalizations.of(context)!.exitProfile, () async {
      await _auth.singOut();
    });
  }

  ///Costruisce il sottomenù contenete tutte le varie opzioni
  @override
  Widget build(BuildContext context) {
    final AuthService auth = AuthService();
    return CustomScrollView(slivers: <Widget>[
      SliverToBoxAdapter(
        child: Container(
          //height: MediaQuery.of(context).size.height * 0.3,
          //width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter, end: Alignment.bottomCenter,
                  // Add one stop for each color
                  // Values should increase from 0.0 to 1.0
                  //stops: [0.5, 0.9],
                  colors: [customStrongLiliac, customPaleLiliac])),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              _personalProfile(),
              _newAds(),
              _myAds(),
              _supportZampOn(),
              _privacyPolicy(),
              _termsAndCondition(),
              _logOut(auth),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.4,
              ),
            ],
          ),
        ),
      ),
    ]);
  }
}
