import 'package:flutter/material.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/settings/account_provider.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:provider/provider.dart';

class AccountStreamProvider extends StatelessWidget {
  final UserData userData;

  AccountStreamProvider({required this.userData});

  @override
  Widget build(BuildContext context) {
    return StreamProvider<UserData?>.value(
      value: DatabaseServiceUser(uid: this.userData.uid).userData,
      initialData: null,
      child: AccountProvider(),
    );
  }
}
