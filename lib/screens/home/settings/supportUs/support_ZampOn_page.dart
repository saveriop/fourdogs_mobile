import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';

//import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/logo_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SupportZampOnPage extends StatefulWidget {
  SupportZampOnPage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _SupportZampOnPageState createState() => _SupportZampOnPageState();
}

class _SupportZampOnPageState extends State<SupportZampOnPage> {
  bool loading = false;
  bool whoWeAreIsVisible = false;
  bool goalsIsVisible = false;

  //Logo con nome
  Widget _logo() {
    return Padding(
      padding: EdgeInsets.only(
          //top: MediaQuery.of(context).size.height * 0.1,
          ),
      child: noPaddingLogoAndName(context),
    );
  }

  //Linea che divide la pagina
  Widget _divider() {
    return mediumDivider(context);
  }

  //Bottone per abilitare il testo who we are
  Widget _whoWeAreButton() {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.02,
      ),
      child: buttonWithRightIcon(
        context,
        AppLocalizations.of(context)!.whoWeAre,
        !whoWeAreIsVisible
            ? Icon(Icons.keyboard_arrow_right)
            : Icon(Icons.keyboard_arrow_down),
        () => setState(() => whoWeAreIsVisible = !whoWeAreIsVisible),
        width: MediaQuery.of(context).size.width * 0.85,
      ),
    );
  }

  //testo who we are
  static const String whoWeAre =
      "Il team di ZampOn è composto da persone ed in quanto tali ognuno ha la sua storia, le sue passioni, il suo obiettivo.\n\nCiascuno di noi è diverso dall’altro ma quello che ci unisce è l’amore per gli animali, il valore del rispetto e dell’uguaglianza.\n\nSe ti stai chiedendo se siamo più team cane o team gatto tranquilli abbiamo entrambe le fazioni all’interno del team!\n\nSiamo semplicemente amanti degli animali e grazie ai nostri obiettivi comuni vogliamo trasmettere il valore dell’adozione. Sappiamo tutti quanto amore, compagnia e riconoscenza possa dare un amico a quattro zampe ed è per questo che offriamo questo strumento per aiutare tutti i nostri amici umani ad adottare in modo semplice, chiaro.";

  Widget _whoWeAre() {
    return Visibility(
      child: Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.02,
        ),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.85,
          child: Text(whoWeAre,
              style: informationSubtitelStyle(context),
              textAlign: TextAlign.center),
        ),
      ),
      maintainSize: false,
      maintainAnimation: true,
      maintainState: true,
      visible: whoWeAreIsVisible,
    );
  }

  //Bottone per abilitare il testo goals
  Widget _goalsButton() {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.02,
        bottom: MediaQuery.of(context).size.height * 0.02,
      ),
      child: buttonWithRightIcon(
        context,
        AppLocalizations.of(context)!.ourGoals,
        !goalsIsVisible
            ? Icon(Icons.keyboard_arrow_right)
            : Icon(Icons.keyboard_arrow_down),
        () => setState(() => goalsIsVisible = !goalsIsVisible),
        width: MediaQuery.of(context).size.width * 0.85,
      ),
    );
  }

  //testo goals
  static const String goals =
      "Per ZampOn adottare significa ridare nuova vita ad un animale il quale conosce le carezze e l’amore umano solo dalle fredde sbarre di un canile/gattile dunque\n\n-Il nostro obiettivo principale è quello di offrire uno strumento gratuito per agevolare l’adozione di animali domestici senza farti perdere nel caos degli annunci online e sui social.\n\nL’obiettivo del cuore?\n\n-L’obiettivo più importante sarà vedere quante vite animali lasceranno le gabbie per appropriarsi di un comodo divano e magari il prossimo divano occupato sarà il tuo!";

  Widget _goals() {
    return Visibility(
      child: Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.02,
        ),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.85,
          child: Text(goals,
              style: informationSubtitelStyle(context),
              textAlign: TextAlign.center),
        ),
      ),
      maintainSize: false,
      maintainAnimation: true,
      maintainState: true,
      visible: goalsIsVisible,
    );
  }

  //Linea che divide la pagina solo se si apre la sezione goals
  Widget _goalsDivider() {
    return Visibility(
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * 0.02,
        ),
        child: mediumDivider(context),
      ),
      maintainSize: false,
      maintainAnimation: true,
      maintainState: true,
      visible: goalsIsVisible,
    );
  }

  //Primo testo
  static const String supportMethods =
      "Se hai compreso i nostri obbiettivi e apprezzi quello che facciamo,\npuoi supportarci con un offerta libera al seguente IBAN:\n";

  Widget _supportMethods() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      child: Text(supportMethods,
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center),
    );
  }

  //Secondo testo
  static const String transferCausal =
      "Puoi inserire 'Contributo Volontario' come causale.\n";

  Widget _transferCausal() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      child: Text(transferCausal,
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center),
    );
  }

  //Secondo testo
  static const String iban = "IT22B0303241340010000010329";

  Widget _iban() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      child: Column(children: [
        Text(iban,
            style: informationSubtitelStyle(context),
            textAlign: TextAlign.center),
        IconButton(
          icon: Icon(
            CustomIcons.i13_bottone_copia,
            size: MediaQuery.of(context).size.height * 0.04,
            color: customWhite,
          ),
          onPressed: () {
            final snackBar = SnackBar(
              content: Text(AppLocalizations.of(context)!.copied),
              action: SnackBarAction(
                label: iban,
                onPressed: () {},
              ),
            );
            Clipboard.setData(ClipboardData(text: iban)).then((value) {
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            });
          },
        ),
      ]),
    );
  }

  /*Terzo testo
  static const String watchTheVideo =
      "Oppure guardando un Video Ad di 30 secondi selezionando il pulsante sottostante\n";*/

  //Quarto testo
  static const String thanks =
      "Ogni tipo di supporto è enormemente apprezzato dal Team ZampOn!\n";

  Widget _thanks() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      child: Text(thanks,
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_logo());
    columnElementList.add(_divider());
    columnElementList.add(_whoWeAreButton());
    columnElementList.add(_whoWeAre());
    columnElementList.add(_goalsButton());
    columnElementList.add(_goals());
    columnElementList.add(_goalsDivider());
    columnElementList.add(_supportMethods());
    columnElementList.add(_iban());
    columnElementList.add(_transferCausal());
    columnElementList.add(_thanks());

    return loading
        ? Loading()
        : genericPage(
            context,
            true,
            columnElementList: columnElementList,
            appbar: true,
            trasparent: false,
            noTitle: false,
          );
  }
}
