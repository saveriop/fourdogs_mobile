import 'package:flutter/material.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/settings/userProfilePage/personal_profile_provider.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:provider/provider.dart';

class PersonalProfileStreamProvider extends StatelessWidget {
  final String userUid;
  final String calledUid;

  PersonalProfileStreamProvider(
      {required this.userUid, required this.calledUid});

  @override
  Widget build(BuildContext context) {
    return StreamProvider<UserData>.value(
      value: DatabaseServiceUser(uid: this.calledUid).userData,
      //initialData: null,
      initialData: UserData(uid: "", locationFilter: ""),
      child: PersonalProfileProvider(calledUid: this.userUid),
    );
  }
}
