import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/User/user_counter.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/modify_bio.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/modify_mobilePhone.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/modify_password.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/modify_username.dart';
import 'package:zampon/services/ws_rest/user_info.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/user_avatar.dart';

class PersonalProfile extends StatefulWidget {
  final UserData user;
  final bool isLoggedUser;

  PersonalProfile({required this.user, required this.isLoggedUser});

  @override
  _PersonalProfileState createState() => _PersonalProfileState();
}

class _PersonalProfileState extends State<PersonalProfile> {
  UserCounter? userCounters;

  _fetchData(String uid) async {
    ///questo metodo richiamerà un end point per ottenere i contatori
    userCounters = await UserInfo().getUserCounter(uid);
    return userCounters;
  }

  ///definizione tasto modifica
  Widget _modifyButton({VoidCallback? onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.04,
        width: MediaQuery.of(context).size.width * 0.065,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: customWhite,
          border: Border.all(color: customPaleLiliac),
        ),
        child: Icon(
          CustomIcons.i30_edit_icon,
          size: MediaQuery.of(context).size.height * 0.025,
          color: customStrongLiliac,
        ),
      ),
    );
  }

  ///Username + tasto modifica
  Widget _username(BuildContext context, String? username, String uid) {
    List<Widget> rowElementList = <Widget>[];
    rowElementList.add(
      Text(username ?? "",
          style: informationTitelStyle(context), textAlign: TextAlign.center),
    );

    ///il tasto modifica è visibile solo per l'utente loggato
    if (this.widget.isLoggedUser) {
      rowElementList.add(SizedBox(
        width: MediaQuery.of(context).size.width * 0.03,
      ));
      rowElementList.add(_modifyButton(onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ModifyUsername(username: username ?? "", uid: uid)));
      }));
    }

    return _row(rowElementList, mainAxisAlignment: MainAxisAlignment.center);
  }

  ///foto Profilo
  Widget _avatar(String uid) {
    return UserAvatar(
      userUid: uid,
      personalUser: true,
      isLoggedUser: this.widget.isLoggedUser,
      profileScreen: true,
    );
  }

  ///Numero di telefono + tasto modifica
  Widget _mobilePhone(String? mobilePhone, String uid) {
    List<Widget> rowElementList = <Widget>[];
    rowElementList.add(
      IconButton(
          onPressed: () {},
          icon: Icon(CustomIcons.i10_telephone_icon, color: customWhite)),
    );
    rowElementList.add(
      Text(mobilePhone ?? "",
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center),
    );
    if (this.widget.isLoggedUser) {
      rowElementList.add(SizedBox(
        width: MediaQuery.of(context).size.height * 0.03,
      ));

      rowElementList.add(_modifyButton(onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ModifyMobilePhone(mobilePhone: mobilePhone ?? "", uid: uid)));
      }));
    }
    return _row(
      rowElementList,
    );
  }

  /// Tasto modifica password
  Widget _modifyPassword() {
    List<Widget> rowElementList = <Widget>[];
    rowElementList.add(
      IconButton(
          onPressed: () {},
          icon: Icon(CustomIcons.i31_reset_password_icon, color: customWhite)),
    );
    rowElementList.add(
      Text(AppLocalizations.of(context)!.modifyPassword,
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center),
    );
    if (this.widget.isLoggedUser) {
      rowElementList.add(SizedBox(
        width: MediaQuery.of(context).size.height * 0.03,
      ));

      rowElementList.add(_modifyButton(onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ModifyPassword()));
      }));
    }
    return _row(
      rowElementList,
    );
  }

  /// Email utente
  Widget _email(String? email) {
    List<Widget> rowElementList = <Widget>[];
    rowElementList.add(IconButton(
        onPressed: () {},
        icon: Icon(CustomIcons.i17_email_icon, color: customWhite)));
    rowElementList.add(Text(email ?? "",
        style: informationSubtitelStyle(context), textAlign: TextAlign.center));
    return _row(
      rowElementList,
    );
  }

  ///Linea che divide la pagina
  Widget _divider() {
    return largeDivider(context);
  }

  ///Numero degli annunci di adozione creati
  Widget _adsAdoptionNumber() {
    String adoptionCounter =
        userCounters != null ? userCounters!.adoption.toString() : "0";
    List<Widget> rowElementList = <Widget>[];
    rowElementList.add(IconButton(
        onPressed: () {},
        icon: Icon(CustomIcons.i1_adoption_icon, color: customWhite)));
    if (userCounters != null) {
      rowElementList.add(Text(
          AppLocalizations.of(context)!.ppAdoptionAd + adoptionCounter,
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center));
    }
    return _row(
      rowElementList,
    );
  }

  /*
   *  Numero degli annunci di sparizione creati 
   */
  Widget _adsLostNumber() {
    String counter =
        userCounters != null ? userCounters!.disappear.toString() : "0";
    List<Widget> rowElementList = <Widget>[];
    rowElementList.add(IconButton(
        onPressed: () {},
        icon: Icon(CustomIcons.i4_lost_found_pet, color: customWhite)));
    if (userCounters != null) {
      rowElementList.add(Text(AppLocalizations.of(context)!.ppLostAd + counter,
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center));
    }
    return _row(
      rowElementList,
    );
  }

  ///Numero degli annunci dei trovati creati
  Widget _adsFoundNumber() {
    String counter = userCounters != null ? userCounters!.found.toString() : "0";
    List<Widget> rowElementList = <Widget>[];
    rowElementList.add(IconButton(
        onPressed: () {},
        icon: Icon(CustomIcons.i3_found_pet_icon, color: customWhite)));
    if (userCounters != null) {
      rowElementList.add(Text(AppLocalizations.of(context)!.ppFoundAd + counter,
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center));
    }
    return _row(
      rowElementList,
    );
  }

  ///Titolo biografia
  Widget _bioTitle(String? bio, String uid) {
    List<Widget> rowElementList = <Widget>[];
    rowElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.05,
    ));
    rowElementList.add(
      Container(
        padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width * 0.04,
        ),
        child: Text(AppLocalizations.of(context)!.biography,
            style: informationTitelStyle(context), textAlign: TextAlign.center),
      ),
    );
    if (this.widget.isLoggedUser) {
      rowElementList.add(SizedBox(
        width: MediaQuery.of(context).size.width * 0.03,
      ));
      rowElementList.add(_modifyButton(onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ModifyBio(bio: bio ?? "", uid: uid)));
      }));
    }
    return _row(
      rowElementList,
    );
  }

  ///Corpo della biografia
  Widget _bioBody(String? bio) {
    List<Widget> rowElementList = <Widget>[];
    rowElementList.add(
      Flexible(
        child: Container(
          padding: EdgeInsets.only(
            left: MediaQuery.of(context).size.width * 0.04,
          ),
          child: Text(bio ?? AppLocalizations.of(context)!.ppBio,
              style: informationSubtitelStyle(context),
              textAlign: TextAlign.left),
        ),
      ),
    );
    return _row(
      rowElementList,
    );
  }

  ///Riga generica necessaria per costruire oggetti generici
  Widget _row(List<Widget> rowElementList,
      {MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start}) {
    return Row(
      mainAxisAlignment: mainAxisAlignment,
      children: List.generate(rowElementList.length, (index) {
        return rowElementList[index];
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList;
    return FutureBuilder(
            future: _fetchData(this.widget.user.uid),
            builder: (context, snapshot) {
              if (snapshot.data == null) {
                return Loading();
              } else {
                columnElementList = <Widget>[];
                columnElementList.add(_username(
                    context, this.widget.user.username, this.widget.user.uid));
                columnElementList.add(SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ));
                columnElementList.add(_avatar(this.widget.user.uid));
                columnElementList.add(SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ));

                ///il numero di telefono, email e modifica password
                ///sono visibili solo all'utente proprietario dell'account
                if (this.widget.isLoggedUser) {
                  columnElementList.add(_mobilePhone(
                      this.widget.user.mobilePhone!, this.widget.user.uid));

                  ///se l'utente non ha effettuato l'accesso tramite sso potrà modificare password
                  if (!this.widget.user.sso!) {
                    columnElementList.add(_modifyPassword());
                  }
                  columnElementList.add(_email(this.widget.user.email));
                }
                columnElementList.add(_divider());
                columnElementList.add(_adsAdoptionNumber());
                columnElementList.add(_adsLostNumber());
                columnElementList.add(_adsFoundNumber());
                columnElementList.add(_divider());
                columnElementList
                    .add(_bioTitle(this.widget.user.bio, this.widget.user.uid));
                columnElementList.add(_bioBody(this.widget.user.bio));
                return genericPage(context, true,
                        columnElementList: columnElementList,
                        appbar: true,
                        trasparent: false,
                        noTitle: false,
                        neverScroll: false);
              }
            });
  }
}
