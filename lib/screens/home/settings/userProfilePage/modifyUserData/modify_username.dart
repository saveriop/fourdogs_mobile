import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/feedback_dialog.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

class ModifyUsername extends StatefulWidget {
  final String username;
  final String uid;

  ModifyUsername({required this.username, required this.uid});

  @override
  _ModifyUsernameState createState() => _ModifyUsernameState();
}

class _ModifyUsernameState extends State<ModifyUsername> {
  String newUsername = "";
  bool loading = false;

  ///Titolo della pagina
  Widget _title() {
    return Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.05,
        ),
        child: subTitle(
          context,
          AppLocalizations.of(context)!.modifyUsername,
          icon: Icon(
            CustomIcons.i42_profile_uman_icon,
            color: customWhite,
            size: MediaQuery.of(context).size.height * 0.04,
          ),
          thereIsIcon: true,
        ));
  }

  Widget _currentUsername(BuildContext context) {
    return noIconTextFormFieldCustom(
      context,
      initialValue: this.widget.username,
      enabled: false,
      hintText: '',
    );
  }

  Widget _newUsername(BuildContext context) {
    return noIconTextFormFieldCustom(context,
        initialValue: newUsername,
        hintText: '',
        onChanged: (val) {
      setState(() {
        newUsername = val!;
      });
    });
  }

  Widget _confirmButton(BuildContext context) {
    return confirmationButton(context, AppLocalizations.of(context)!.confirm,
        () async {
      if (newUsername.isNotEmpty && this.widget.username != newUsername) {
        setState(() {
          loading = true;
        });
        await DatabaseServiceUser(uid: this.widget.uid)
            .updateField(DatabaseServiceUser.USERNAME_LABEL, newUsername);
        setState(() {
          loading = false;
        });
        feedBackDialog(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_title());
    columnElementList.add(divider(context));
    columnElementList.add(textPagination(
      context,
      "Attuale username",
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_currentUsername(context));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(textPagination(
      context,
      "Inserisci il nuovo username",
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_newUsername(context));
        columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.01,
    ));
        columnElementList.add(divider(context));
        columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_confirmButton(context));
    return loading
        ? Loading()
        : genericPage(context, true,
            columnElementList: columnElementList,
            appbar: true,
            trasparent: false,
            noTitle: false,
            neverScroll: true);
  }
}
