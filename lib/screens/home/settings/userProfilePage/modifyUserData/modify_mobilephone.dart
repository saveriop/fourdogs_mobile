import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/feedback_dialog.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

class ModifyMobilePhone extends StatefulWidget {
  final String mobilePhone;
  final String uid;

  ModifyMobilePhone({required this.mobilePhone, required this.uid});

  @override
  _ModifyMobilePhoneState createState() => _ModifyMobilePhoneState();
}

class _ModifyMobilePhoneState extends State<ModifyMobilePhone> {
  String newMobilePhone = "";
  bool loading = false;

  ///Titolo della pagina
  Widget _title() {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        child: subTitle(
          context,
          AppLocalizations.of(context)!.modifyPhone,
          icon: Icon(CustomIcons.i10_telephone_icon, color: customWhite),
          thereIsIcon: true,
        ));
  }

  Widget _currentMobilePhone(BuildContext context) {
    return noIconTextFormFieldCustom(
      context,
      initialValue: this.widget.mobilePhone,
      enabled: false,
      hintText: '',
    );
  }

  Widget _newMobilePhone(BuildContext context) {
    return noIconTextFormFieldCustom(context,
        keyboardType: TextInputType.phone,
        initialValue: newMobilePhone,
        hintText: '',
        onChanged: (val) {
          setState(() {
            newMobilePhone = val!;
        });
    });
  }

  Widget _confirmButton(BuildContext context) {
    return confirmationButton(context, AppLocalizations.of(context)!.confirm,
        () async {
      if (newMobilePhone.isNotEmpty &&
          this.widget.mobilePhone != newMobilePhone) {
        setState(() {
          loading = true;
        });
        await DatabaseServiceUser(uid: this.widget.uid).updateField(
            DatabaseServiceUser.MOBILE_PHONE_LABEL, newMobilePhone);
        setState(() {
          loading = false;
        });
        feedBackDialog(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];
    //columnElementList.add(SizedBox( height: MediaQuery.of(context).size.height * 0.07, ));
    columnElementList.add(_title());
    columnElementList.add(divider(context));
    columnElementList.add(textPagination(
      context,
      "Attuale numero di telefono",
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_currentMobilePhone(context));
    columnElementList.add(textPagination(
      context,
      "Inserisci il nuovo numero di telefono",
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_newMobilePhone(context));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.01,
    ));
    columnElementList.add(divider(context));
        columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_confirmButton(context));
    return loading
        ? Loading()
        : genericPage(context, true,
            columnElementList: columnElementList,
            appbar: true,
            trasparent: false,
            noTitle: false,
            neverScroll: true);
  }
}
