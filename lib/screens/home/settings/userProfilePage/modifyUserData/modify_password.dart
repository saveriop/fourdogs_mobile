import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/feedback_dialog.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

class ModifyPassword extends StatefulWidget {
  @override
  _ModifyPasswordState createState() => _ModifyPasswordState();
}

class _ModifyPasswordState extends State<ModifyPassword> {
  bool loading = false;
  String oldPassword = "";
  String newPassword = "";

  ///Form di inserimento password attuale
  Widget _currentPassword(BuildContext context) {
    return noIconTextFormFieldCustom(context,
        keyboardType: TextInputType.visiblePassword,
        obscureText: true,
        labelText: AppLocalizations.of(context)!.oldPassword,
        hintText: '',
        onChanged: (val) {
      setState(() {
        oldPassword = val!;
      });
    });
  }

  ///Form di inserimento nuova password
  Widget _newPassword(BuildContext context) {
    return noIconTextFormFieldCustom(context,
        keyboardType: TextInputType.visiblePassword,
        obscureText: true,
        labelText: AppLocalizations.of(context)!.newPassword,
        hintText: '',
        onChanged: (val) {
      setState(() {
        newPassword = val!;
      });
    });
  }

  ///Tasto conferma
  ///Solo se la nuova e la vecchia password sono valorizzate e diverse viene effettuato l'aggiornamento
  Widget _confirmButton(BuildContext context) {
    return confirmationButton(context, AppLocalizations.of(context)!.confirm,
        () async {
      if (oldPassword.isNotEmpty &&
          newPassword.isNotEmpty &&
          oldPassword != newPassword) {
        setState(() {
          loading = true;
        });
        new AuthService().changePassword(oldPassword, newPassword);
        setState(() {
          loading = false;
        });
        feedBackDialog(context);
      }
    });
  }

  ///Sottotitolo della pagina
  Widget _title() {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        child: subTitle(
          context,
          AppLocalizations.of(context)!.modifyPassword,
          icon: Icon(CustomIcons.i31_reset_password_icon, color: customWhite),
          thereIsIcon: true,
        ));
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.07,
    ));
    columnElementList.add(_title());
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_currentPassword(context));
    columnElementList.add(_newPassword(context));
    columnElementList.add(_confirmButton(context));
    return loading
        ? Loading()
        : genericPage(context, true,
            columnElementList: columnElementList,
            appbar: true,
            trasparent: false,
            noTitle: false,
            neverScroll: true);
  }
}
