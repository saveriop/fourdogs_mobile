import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zampon/assets/custom_icons.dart';

import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/feedback_dialog.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

class ModifyBio extends StatefulWidget {
  final String bio;
  final String uid;

  ModifyBio({required this.bio, required this.uid});

  @override
  _ModifyBioState createState() => _ModifyBioState();
}

class _ModifyBioState extends State<ModifyBio> {
  String newBio = "";
  bool loading = false;

  ///Titolo della pagina
  Widget _title() {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        child: subTitle(
          context,
          AppLocalizations.of(context)!.biography,
          icon: FaIcon(CustomIcons.i16_description_icon, color: customWhite),
          thereIsIcon: true,
        ));
  }

  Widget _newBio(context) {
    return textFormFieldCustom(
      context,
      initialValue: this.widget.bio,
      keyboardType: TextInputType.multiline,
      textCapitalization: TextCapitalization.sentences,
      maxLength: 256,
      maxLines: null,
      minLines: 10,
      noIcon: true,
      labelText: AppLocalizations.of(context)!.biography,
      //hintText: AppLocalizations.of(context).presentation,
      onChanged: (value) {
        newBio = value!;
      },
    );
  }

  Widget _confirmButton(BuildContext context) {
    return confirmationButton(context, AppLocalizations.of(context)!.modify,
        () async {
      if (this.widget.bio != newBio && newBio.isNotEmpty) {
        setState(() {
          loading = true;
        });
        await DatabaseServiceUser(uid: this.widget.uid)
            .updateField(DatabaseServiceUser.BIO_LABEL, newBio);
        setState(() {
          loading = false;
        });
        feedBackDialog(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.07,
    ));
    columnElementList.add(_title());
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_newBio(context));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_confirmButton(context));
    return genericPage(context, true,
        columnElementList: columnElementList,
        appbar: true,
        trasparent: false,
        noTitle: false,
        neverScroll: true);
  }
}
