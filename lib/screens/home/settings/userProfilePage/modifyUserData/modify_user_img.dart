import 'dart:io';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/avatar_selection.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/avatar_utility.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/feedback_dialog.dart';
import 'package:zampon/services/firebase/storage/storage_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

class ModifyUserImg extends StatefulWidget {
  final String uid;
  final String photoUrl;

  ModifyUserImg({required this.uid, required this.photoUrl});

  @override
  _ModifyUserImgState createState() => _ModifyUserImgState();
}

class _ModifyUserImgState extends State<ModifyUserImg> {
  String? photoPath;

  Future<Null> _cropImage() async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(
        cropStyle: CropStyle.circle,
        sourcePath: photoPath!,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        ///Nel caso in cui si accede alla galleria oppure alla fotocamera e non si scatta/sceglie alcuna foto e si torna indietro l'eccezione viene catturata
        ).catchError((error, stackTrace) => null);
    if (croppedFile != null) {
      setState(() {
        photoPath = croppedFile.path;
      });
    }
  }

  @override
  void initState() {
    photoPath = this.widget.photoUrl;
    super.initState();
  }

  //Apre la galleria e permette di selezionare una foto
  Future _getImageFromGalley() async {
    final ImagePicker _picker = ImagePicker();
    final pickedFile = await _picker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 50,
        maxWidth: 800,
        maxHeight: 600);
    setState(() => pickedFile != null
        ? photoPath = pickedFile.path
        : photoPath = photoPath);
  }

  //permette di scattare una foto
  Future _getImageFromCamera() async {
    final ImagePicker _picker = ImagePicker();
    final pickedFile = await _picker.pickImage(
        source: ImageSource.camera,
        imageQuality: 50,
        maxWidth: 800,
        maxHeight: 600);
    setState(() => pickedFile != null
        ? photoPath = pickedFile.path
        : photoPath = photoPath);
  }

  //Elemento grigli per immagini caricate dall'utente
  Widget _photoProfile() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      height: MediaQuery.of(context).size.width * 0.5,
      //height: MediaQuery.of(context).size.height * 0.25,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: customWhite,
        border: Border.all(
          color: customWhite, 
          width: MediaQuery.of(context).size.width * 0.01,
        )
      ),
      child: Stack(children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: ClipOval(child: showImage(photoPath!)),
        ),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_photoProfile());
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(Text(
      AppLocalizations.of(context)!.selectImmProfOption,
      textAlign: TextAlign.center,
      style: TextStyle(
          fontSize: MediaQuery.of(context).size.width * 0.045,
          color: customWhite,
          fontWeight: FontWeight.bold),
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(loginButtonWithIcon(
        context,
        FaIcon(
          CustomIcons.i34_load_picture_icon,
          color: customStrongLiliac,
          size: MediaQuery.of(context).size.height * 0.04,
        ),
        AppLocalizations.of(context)!.addNewPhoto, () {
      _getImageFromGalley().then((value) => _cropImage());
    }));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.005,
    ));
    columnElementList.add(loginButtonWithIcon(
        context,
        FaIcon(
          CustomIcons.i36_shot_picture_icon,
          color: customStrongLiliac,
          size: MediaQuery.of(context).size.height * 0.04,
        ),
        AppLocalizations.of(context)!.makeNewPhoto, () {
      _getImageFromCamera().then((value) => _cropImage());
    }));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.005,
    ));
    columnElementList.add(loginButtonWithIcon(
        context,
        FaIcon(
          CustomIcons.i35_select_avatar_icon,
          color: customStrongLiliac,
          size: MediaQuery.of(context).size.height * 0.04,
        ),
        AppLocalizations.of(context)!.avatarSelect, () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => AvatarSelection(
                    uid: this.widget.uid,
                    photoUrl: this.widget.photoUrl,
                  )));
    }));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.005,
    ));
    columnElementList.add(confirmationButton(
        context, AppLocalizations.of(context)!.confirm, () async {
      File image = File(photoPath!);
      new StorageDp().updateProfileImg(image, this.widget.uid);
      feedBackDialog(context);
    }));
    return genericPage(context, true,
        columnElementList: columnElementList,
        appbar: true,
        trasparent: false,
        noTitle: false,
        neverScroll: true);
  }
}
