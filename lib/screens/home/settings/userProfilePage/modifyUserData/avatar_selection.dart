import 'dart:io';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/avatar_utility.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/feedback_dialog.dart';
import 'package:zampon/services/firebase/storage/storage_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

class AvatarSelection extends StatefulWidget {
  final String uid;
  final String photoUrl;

  AvatarSelection({required this.uid, required this.photoUrl});

  @override
  _AvatarSelectionState createState() => _AvatarSelectionState();
}

class _AvatarSelectionState extends State<AvatarSelection> {
  //path della foto da salvare
  String? photoPath;
  //lista foto avatar
  List<String> avatarUrlListRow1 = [
    "assets/images/avatar/cat_avatar.png",
    "assets/images/avatar/guinea_pig_avatar.png",
    "assets/images/avatar/parrot_avatar.png",
  ];
  List<String> avatarUrlListRow2 = [
    "assets/images/avatar/rabbit_avatar.png",
    "assets/images/avatar/shiba_avatar.png",
    "assets/images/avatar/turtle_avatar.png",
  ];

  @override
  void initState() {
    photoPath = this.widget.photoUrl;
    super.initState();
  }

  //Elemento grigli per immagini caricate dall'utente
  Widget _photoProfile() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      height: MediaQuery.of(context).size.width * 0.5,
      decoration: BoxDecoration(
        color: customWhite,
        border: Border.all(
          width: MediaQuery.of(context).size.width * 0.01,
          color: customWhite, // Set border color
        ),
        shape: BoxShape.circle,
        //borderRadius: circularBorderRadius(context), // Set rounded corner radius
      ),
      child: Stack(children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: ClipOval(child: showImage(photoPath!)),
        ),
      ]),
    );
  }

  Widget _selectableAvatar(String avatarUrl) {
    return GestureDetector(
      onTap: () {
        setState(() => photoPath = avatarUrl);
      },
      child: Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.01),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.29,
          height: MediaQuery.of(context).size.width * 0.29,
          decoration: BoxDecoration(
            color: customWhite,
            border: Border.all(
                color: customWhite, // Set border color
                width: MediaQuery.of(context).size.height *
                    0.002), // Set border width
           shape: BoxShape.circle,
           // borderRadius: circularBorderRadius(context), // Set rounded corner radius
          ),
          child: ClipOval(
            child: Image.asset(
              avatarUrl,
              fit: BoxFit.cover,
              alignment: Alignment.center,
              height: double.infinity,
              width: double.infinity,
            ),
          ),
        ),
      ),
    );
  }

  Widget _avatarRow() {
    return Container(
      child: SingleChildScrollView(
        child: Column(children: [
          Row(
            children: List.generate(
              avatarUrlListRow1.length,
              (index) {
                return Flexible(
                    child: _selectableAvatar(avatarUrlListRow1[index]));
              },
            ),
          ),
          Row(
            children: List.generate(
              avatarUrlListRow2.length,
              (index) {
                return Flexible(
                    child: _selectableAvatar(avatarUrlListRow2[index]));
              },
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.02),
          _confirmButton(context)
        ]),
      ),
    );
  }

  /*
   * Tasto di conferma
   */
  Widget _confirmButton(BuildContext context) {
    return confirmationButton(context, AppLocalizations.of(context)!.confirm,
        () async {
      final Directory systemTempDir = Directory.systemTemp;
      final byteData = await rootBundle.load(photoPath!);
      final file = File('${systemTempDir.path}/' + this.widget.uid);
      await file.writeAsBytes(byteData.buffer
          .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
      new StorageDp().updateProfileImg(file, this.widget.uid);
      Navigator.pop(context);
      Navigator.pop(context);
      feedBackDialog(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    //lista dei widget che saranno incolonnati nella pagina
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_photoProfile());
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(Text(
      AppLocalizations.of(context)!.avatarSelection,
      style: TextStyle(
          fontSize: MediaQuery.of(context).size.width * 0.045,
          color: customWhite,
          fontWeight: FontWeight.bold),
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_avatarRow());
    return genericSliversPage(context,
        columnElementList: columnElementList,
        wrapInSingleChildScrollView: true);
  }
}
