import 'dart:io';

import 'package:flutter/material.dart';

  //Restituisce l'immagine da mostrare in griglia
  Widget showImage(String photoPath) {
    if (photoPath.startsWith("assets")) {
      return Image.asset(
        photoPath,
        alignment: Alignment.center,
        height: double.infinity,
        width: double.infinity,
        fit: BoxFit.cover,
      );
    } else if (photoPath.startsWith("http")) {
      return Image.network(
        photoPath,
        alignment: Alignment.center,
        height: double.infinity,
        width: double.infinity,
        fit: BoxFit.cover,
      );
    } else {
      return Image.file(
        File(photoPath),
        fit: BoxFit.cover,
        height: double.infinity,
        width: double.infinity,
        alignment: Alignment.center,
      );
    }
  }