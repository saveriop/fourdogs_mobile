import 'package:flutter/material.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

/*
   *  AlertDialog feedback operazione completata
   */
void feedBackDialog(
  BuildContext context,
) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: AlertDialog(
            titlePadding: EdgeInsets.only(),
            backgroundColor: Colors.transparent,
            title: Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.width * 0.07,
                  bottom: MediaQuery.of(context).size.height * 0.03,
                  left: MediaQuery.of(context).size.width * 0.03,
                  right: MediaQuery.of(context).size.height * 0.015),
              decoration: BoxDecoration(
                borderRadius: mediumBorderRadius(context),
                color: customStrongLiliac,
                border: Border.all(
                  color: customWhite,
                ),
              ),
              child: Column(children: <Widget>[
                Text(AppLocalizations.of(context)!.dataUpdated,
                    style: informationSubtitelStyle(context),
                    textAlign: TextAlign.center),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
                _confirmButtonShowDialog(
                  context,
                )
              ]),
            )),
      );
    },
  );
}

Widget _confirmButtonShowDialog(BuildContext context) {
  return confirmationButton(context, AppLocalizations.of(context)!.confirm,
      () async {
    Navigator.pop(context);
    Navigator.pop(context);
  });
}
