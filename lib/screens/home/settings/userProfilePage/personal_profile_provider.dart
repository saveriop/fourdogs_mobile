import 'package:flutter/material.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/settings/userProfilePage/personal_profile.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:provider/provider.dart';

class PersonalProfileProvider extends StatelessWidget {
  final String calledUid;

  PersonalProfileProvider({required this.calledUid});

  @override
  Widget build(BuildContext context) {
    final UserData? user = Provider.of<UserData?>(context);
    if (user != null) {
      //se l'uid dell'utente chiamate uguale all'uid del profilo utente allora la pagina profilo sarà modificabile
      //altrimenti sarà in sola lettura
      return PersonalProfile(
          user: user, isLoggedUser: user.uid == calledUid ? true : false);
    } else {
      return Loading();
    }
  }
}
