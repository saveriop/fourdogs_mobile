import 'package:flutter/material.dart';
import 'package:async/async.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/closed_ad.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/services/firebase/storage/storage_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/utility.dart';
import 'package:zampon/shared/widgetModel/info_box_model.dart';

class MyClosedAdCard extends StatefulWidget {
  final AdClosedElements adClosedElements;
  final UserData userData;

  MyClosedAdCard({required this.adClosedElements, required this.userData});

  @override
  _MyClosedAdCardState createState() => _MyClosedAdCardState();
}

class _MyClosedAdCardState extends State<MyClosedAdCard> {
  ///Memorizzazione asincrono per l'oggetto Future relativo alle immagini
  final AsyncMemoizer _memoizer = AsyncMemoizer();

  ///Fetch img from store
  _fetchData() {
    return this._memoizer.runOnce(() async {
      return await new StorageDp()
          .getPetImgReference(this.widget.adClosedElements.id, "_0");
    });
  }

  ///Widget dedicato alla visualizzazione dell'immagine di un singolo annuncio
  Widget _image() {
    return FutureBuilder(
        future: this._fetchData(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('none');
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text(
                  '${snapshot.error}',
                  style: TextStyle(color: Colors.red),
                );
              }
          }
          return Container(
            child: Stack(children: [
              Positioned.fill(
                child: Align(
                  child: Container(
                    color: customWhite,
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.1),
                    color: customStrongLiliac,
                  ),
                ),
              ),
              Positioned.fill(
                child: CachedNetworkImage(
                  fit: BoxFit.fitWidth,
                  imageUrl: snapshot.data.toString(),
                  imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                    borderRadius: lightBorderRadius(context),
                  )),
                  placeholder: (context, url) => Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ]),
          );
        });
  }

  ///Icona cuore in alto a sinistra dell'immagine
  Widget _favorite() {
    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.025),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.05,
          width: MediaQuery.of(context).size.width * 0.1,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: !this
                    .widget
                    .userData
                    .favoriteAds!
                    .contains(this.widget.adClosedElements.id)
                ? Colors.white
                : Colors.red,
          ),
          child: Icon(
            CustomIcons.i37_favorite_icon,
            size: MediaQuery.of(context).size.height * 0.04,
            color: !this
                    .widget
                    .userData
                    .favoriteAds!
                    .contains(this.widget.adClosedElements.id)
                ? customStrongLiliac
                : Colors.white,
          ),
        ),
      ),
    );
  }

  /// restituisce un widget Align contenete la fascia colorata che sarà posizionata
  /// in alto a sinistra dell'img.
  /// Necessita l'adCategory per differenziare le immagini da mostrare a video
  Widget _ribbon(AdCategory adCategory) {
    AssetImage assetImage = AssetImage('assets/images/ribbon_adozione.png');
    if (adCategory == AdCategory.Adoption) {
      assetImage = AssetImage('assets/images/ribbon_adozione.png');
    } else if (adCategory == AdCategory.Disappear) {
      assetImage = AssetImage('assets/images/ribbon_scomparso.png');
    } else if (adCategory == AdCategory.Found) {
      assetImage = AssetImage('assets/images/ribbon_avvistato.png');
    }
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
          padding: EdgeInsets.only(),
          child: Image(
            image: assetImage,
            height: MediaQuery.of(context).size.height * 0.12,
          )),
    );
  }

  ///Icona che indica se l'annuncio è di adozione oppure di scomparsa
  ///Visibile solo nella sezione scomparsi
  Widget _iconType() {
    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.025),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.05,
          width: MediaQuery.of(context).size.width * 0.1,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: customStrongLiliac,
          ),
          child: Icon(
            this.widget.adClosedElements.adCategory == AdCategory.Adoption
                ? Icons.house
                : Icons.query_stats,
            size: MediaQuery.of(context).size.height * 0.04,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  ///Questo metodo applica il timbro in base alla categoria
  Widget _isClosed(AdCategory adCategory) {
    //applica il timbro solo per gli annunci chiusi
    AssetImage assetImage;
    switch (adCategory) {
      case AdCategory.Adoption:
        assetImage = AssetImage('assets/images/timbro_adottato.png');
        break;
      case AdCategory.Found:
      case AdCategory.Disappear:
        assetImage = AssetImage('assets/images/timbro_trovato.png');
        break;
      default:
        assetImage = AssetImage('assets/images/timbro_adottato.png');
        break;
    }
    return Align(
      alignment: Alignment.center,
      child: Padding(
          padding: EdgeInsets.only(),
          child: Image(
            image: assetImage,
            height: MediaQuery.of(context).size.height * 0.48,
          )),
    );
  }

  Widget _squareImage(BuildContext context) {
    return Flexible(
      child: Stack(
        children: [
          _image(),

          ///se si tratta di un annuncio di adozione appare l'icona dei favoriti
          ///se si tratta di un annuncio di scomparsa mostra l'icona di adozione
          this.widget.adClosedElements.adCategory == AdCategory.Adoption
              ? _favorite()
              : _iconType(),
          _ribbon(this.widget.adClosedElements.adCategory!),

          ///se l'annuncio è chiuso sull'img viene applicato il timbro
          _isClosed(this.widget.adClosedElements.adCategory!),
        ],
      ),
    );
  }

  ///Nome dell'annuncio
  Widget _adName() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
      ),
      child: Text(
        this.widget.adClosedElements.petName!,
        style: TextStyle(
          color: customWhite,
          fontSize: MediaQuery.of(context).size.height * 0.03,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  ///Riquadri con le informazione dell'animale
  Widget _petInformation() {
    ///Nel caso in cui sia anno che mese siano valorizzati con 0 l'età si dichiara sconosciuta
    String age = "";
    if (this.widget.adClosedElements.petAge!.years == 0 &&
        this.widget.adClosedElements.petAge!.months == 0) {
      age = AppLocalizations.of(context)!.unknownAge;
    } else {
      age =
          "${this.widget.adClosedElements.petAge!.years} ${AppLocalizations.of(context)!.years} ${this.widget.adClosedElements.petAge!.months} ${AppLocalizations.of(context)!.months}";
    }
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.01,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ///pet category. E.g.: Cane, gatto, altro
          petInfoLabel(
              context,
              getPetLabel(context, this.widget.adClosedElements.petCategory),
              getPetTypeMap(context)[getPetLabel(
                  context, this.widget.adClosedElements.petCategory)]!),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.048,
          ),

          ///pet gender. e.g. : maschio, femmina
          petInfoLabel(
              context,
              getGenderLabel(context, this.widget.adClosedElements.petGender),
              CustomIcons.i24_icona_maschio),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.05,
          ),

          ///pet years
          petAgeInfoLabel(context, age, CustomIcons.i25_pet_age_icon),
        ],
      ),
    );
  }

  //Luogo dell'annuncio
  Widget _adLocation() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.001,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(
            CustomIcons.i26_pet_location,
            color: customWhite,
            size: MediaQuery.of(context).size.height * 0.03,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.02,
          ),
          Text(
            this.widget.adClosedElements.adLocation!.region! +
                "," +
                this.widget.adClosedElements.adLocation!.province! +
                "," +
                this.widget.adClosedElements.adLocation!.city!,
            style: TextStyle(
              color: customWhite,
              fontSize: MediaQuery.of(context).size.height * 0.02,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  ///Data chiusura annuncio
  Widget _adClosedDate() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.005,
      ),
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(
              CustomIcons.i15_calendar_icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.03,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.02,
            ),
            Text(
              AppLocalizations.of(context)!.closedAdDate,
              style: TextStyle(
                color: customWhite,
                fontSize: MediaQuery.of(context).size.height * 0.02,
              ),
            ),
            Text(
              this.widget.adClosedElements.closedDate!,
              style: TextStyle(
                color: customWhite,
                fontSize: MediaQuery.of(context).size.height * 0.02,
              ),
            ),
          ]),
    );
  }

  ///Data di pubblicazione dell'annuncio
  Widget _adDate() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.015,
      ),
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(
              CustomIcons.i15_calendar_icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.03,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.02,
            ),
            Text(
              AppLocalizations.of(context)!.adDate,
              style: TextStyle(
                color: customWhite,
                fontSize: MediaQuery.of(context).size.height * 0.02,
              ),
            ),
            Text(
              this.widget.adClosedElements.adDate!,
              style: TextStyle(
                color: customWhite,
                fontSize: MediaQuery.of(context).size.height * 0.02,
              ),
            ),
          ]),
    );
  }

  Widget _bottomTextSide() {
    return Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.01),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _adName(),
              _petInformation(),
              _adLocation(),
              _adDate(),
              _adClosedDate()
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: adDecoration(context),
      margin:
          EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.015),
      width: MediaQuery.of(context).size.width * 0.015,
      child: Column(
        children: <Widget>[
          _squareImage(context),
          _bottomTextSide(),
        ],
      ),
    );
  }
}
