import 'dart:async';
import 'package:flutter/material.dart';

import 'package:zampon/model/closed_ad.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/settings/myAds/myClosedAds/my_closed_ad_card.dart';
import 'package:zampon/services/ws_rest/closed_ads.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';

class MyClosedAdListPagination extends StatefulWidget {
  final UserData user;

  MyClosedAdListPagination({required this.user});

  @override
  _MyClosedAdListPaginationState createState() =>
      _MyClosedAdListPaginationState();
}

class _MyClosedAdListPaginationState extends State<MyClosedAdListPagination> {
  ///indice di pagina. Valore default 0
  int pageIndex = 0;

  ///dimensione di ogni pagina. Valore default 5
  int pageSize = 10;
  List<Future<List<AdClosedElements>>> futures = [];
  List<AdClosedElements> listAds = [];
  late Stream<List<AdClosedElements>> _futuresStream;

  ///controller del refresh widget
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  /// Variabile che gestisce il loading.
  /// Quando l'utente clicca bottoni asincroni, per far si che non vengano innescati più volte
  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;

  ///Metodo invocato quando sono stato scrollati tutti gli elementi a video.
  ///Richiede dei nuovi elementi paginati dal Back-End andando ad alimentare la lista inizializzata nel initState
  ///Ad ogni chiamata verifica se gli elementi della lista sono aumentati e nel caso incrementa l'indice di paginazione
  void _onLoading() async {
    int length = listAds.length;

    ///richiedo una nuova pagina tramite una chiamata http
    listAds = (await new ClosedAds().nextPageAds(
        uid: this.widget.user.uid,
        pageIndex: pageIndex,
        pageSize: pageSize,
        listAds: listAds))!;

    ///se la lunghezza non è variata significa che non ci sono altri nuovi annunci
    ///pertanto non occorre incrementare il contatore della paginazione
    if (length < listAds.length) {
      pageIndex++;
    }

    /// keep time to reload widget
    await Future.delayed(Duration(milliseconds: 350));

    /// if failed,use loadFailed(),if no data return,use LoadNodata()
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  ///All'init del Widget viene effettuata la chiamata HTTP verso il Back-end per ottenere i primi elementi paginati
  ///Dal oggetto Future prodotto viene generato lo Stream fondamentale per il funzionamento dello  StreamBuilder
  @override
  void initState() {
    super.initState();

    ///Alimentazione dell'oggetto Future contenente la lista degli annunci
    ///Questi annunci sono il risultato della prima chiamata HTTP
    futures.add(new ClosedAds().initPageAds(
        pageIndex: pageIndex, pageSize: pageSize, uid: this.widget.user.uid));

    ///Popola per la prima volta la lista degli elementi da visualizzare
    futures.forEach((element) {
      element.then((value) => listAds = value);
    });

    ///incremento la l'indicatore delle pagine
    pageIndex++;

    ///Genera lo stream basato sugli elementi ricevuti in output dal Back-end
    ///Sulla base di questo Stream lavorerà lo StreamBuilder definito nel metodo Build
    _futuresStream = Stream<List<AdClosedElements>>.fromFutures(futures);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<AdClosedElements>>(
      initialData: [],
      stream: _futuresStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data!.length > 0) {
            return SmartRefresher(
              controller: _refreshController,
              enablePullDown: false,
              enablePullUp: true,
              onLoading: _onLoading,
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverToBoxAdapter(
                    child: Column(children: [
                      Container(
                        color: customWhite,
                        height: MediaQuery.of(context).size.height * 0.01,
                      ),
                    ]),
                  ),
                  SliverGrid(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1, //numero di colonne della griglia
                      childAspectRatio: 1,
                      crossAxisSpacing: 1,
                      mainAxisSpacing: 8,
                    ),
                    delegate: SliverChildBuilderDelegate((context, index) {
                      return MyClosedAdCard(
                        adClosedElements: snapshot.data![index],
                        userData: this.widget.user,
                      );
                    }, childCount: snapshot.data!.length),
                  ),
                ],
              ),
            );
          } else {
            return CustomScrollView(slivers: <Widget>[
              SliverToBoxAdapter(
                  child: Column(children: [
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.8,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            // Add one stop for each color
                            // Values should increase from 0.0 to 1.0
                            stops: [0.5, 0.9],
                            colors: [customStrongLiliac, customPaleLiliac])),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'assets/images/logo_miei_annunci_vuoto.png',
                            height: MediaQuery.of(context).size.height * 0.7,
                            width: MediaQuery.of(context).size.width * 0.7,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ]))
            ]);
          }
        } else {
          return Loading();
        }
      },
    );
  }
}
