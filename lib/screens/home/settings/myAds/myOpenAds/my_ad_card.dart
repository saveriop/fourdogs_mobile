import 'package:flutter/material.dart';
import 'package:async/async.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';

import 'package:zampon/model/ad.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/adList/adDetail/ad_detail.dart';
import 'package:zampon/screens/home/newAds/add_new_ad.dart';
import 'package:zampon/screens/home/settings/myAds/myOpenAds/my_ads_dialog.dart';
import 'package:zampon/services/firebase/storage/storage_dp.dart';
import 'package:zampon/services/ws_rest/ads.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/utility.dart';
import 'package:zampon/shared/widgetModel/info_box_model.dart';

class MyAdCard extends StatefulWidget {
  final AdElements adElements;
  final UserData userData;

  MyAdCard({required this.adElements, required this.userData});

  @override
  _MyAdCardState createState() => _MyAdCardState();
}

class _MyAdCardState extends State<MyAdCard> {
  ///Memorizzazione asincrono per l'oggetto Future relativo alle immagini
  final AsyncMemoizer _memoizer = AsyncMemoizer();

  /// Variabile che gestisce il loading.
  /// Quando l'utente clicca bottoni asincroni, per far si che non vengano innescati più volte
  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;

  ///Fetch img from store
  _fetchData() {
    return this._memoizer.runOnce(() async {
      return await new StorageDp()
          .getPetImgReference(this.widget.adElements.id, "_0");
    });
  }

  ///Widget dedicato alla visualizzazione dell'immagine di un singolo annuncio
  Widget _image() {
    return FutureBuilder(
        future: this._fetchData(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('none');
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text(
                  '${snapshot.error}',
                  style: TextStyle(color: Colors.red),
                );
              }
          }
          return Container(
            child: Stack(children: [
              Positioned.fill(
                child: Align(
                  child: Container(
                    color: customWhite,
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.1),
                    color: customStrongLiliac,
                  ),
                ),
              ),
              Positioned.fill(
                child: CachedNetworkImage(
                  fit: BoxFit.fitWidth,
                  imageUrl: snapshot.data.toString(),
                  imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                    borderRadius: lightBorderRadius(context),
                  )),
                  placeholder: (context, url) => Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ]),
          );
        });
  }

  ///Tasto modifica posizionato in alto a destra
  Widget _modify(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        ///reperisco tutte le immagini dell'annuncio prima di accedere alla modifica
        List<String> photoList = [];
        StorageDp storage = new StorageDp();
        for (int i = 0; i < this.widget.adElements.photoNumber!; i++) {
          photoList.add(await storage.recoverImagesFromDb(
              this.widget.adElements.id!, "_" + i.toString()));
        }
        setState(() => loading = true);
        this.widget.adElements.adDescription = await Ads()
            .getDescription(this.widget.adElements.id!, cached: false);
        Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AddNewAd(
                        userData: this.widget.userData,
                        adElements: this.widget.adElements,
                        photoList: photoList,
                        isEditAd: true)))
            .then((value) => setState(() => loading = false));
      },
      child: Align(
        alignment: Alignment.topRight,
        child: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.025),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.05,
            width: MediaQuery.of(context).size.width * 0.1,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: Icon(
              CustomIcons.i30_edit_icon,
              size: MediaQuery.of(context).size.height * 0.04,
              color: customStrongLiliac,
            ),
          ),
        ),
      ),
    );
  }

  //Tasto elimina posizionato in alto a sinistra
  Widget _delete(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        closeMyAdDialog(context, this.widget.adElements);
      },
      child: Align(
        alignment: Alignment.topLeft,
        child: Padding(
          padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.025),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.05,
            width: MediaQuery.of(context).size.width * 0.1,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: Icon(
              CustomIcons.i9_delete_icon,
              size: MediaQuery.of(context).size.height * 0.04,
              color: customStrongLiliac,
            ),
          ),
        ),
      ),
    );
  }

  Widget _squareImage(BuildContext context) {
    return Flexible(
      child: Stack(
        children: [
          _image(),
          Column(children: [
            _delete(context),
          ]),
          _modify(context)
        ],
      ),
    );
  }

  ///Nome dell'annuncio
  Widget _adName() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
      ),
      child: Text(
        this.widget.adElements.petName,
        style: TextStyle(
          color: customWhite,
          fontSize: MediaQuery.of(context).size.height * 0.03,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  ///Riquadri con le informazione dell'animale
  Widget _petInformation() {
    ///Nel caso in cui sia anno che mese siano valorizzati con 0 l'età si dichiara sconosciuta
    String age = "";
    if (this.widget.adElements.petAge!.years == 0 &&
        this.widget.adElements.petAge!.months == 0) {
      age = AppLocalizations.of(context)!.unknownAge;
    } else {
      age =
          "${this.widget.adElements.petAge!.years} ${AppLocalizations.of(context)!.years} ${this.widget.adElements.petAge!.months} ${AppLocalizations.of(context)!.months}";
    }

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.01,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ///pet category. E.g.: Cane, gatto, altro
          petInfoLabel(
              context,
              getPetLabel(context, this.widget.adElements.petCategory),
              getPetTypeMap(context)[
                  getPetLabel(context, this.widget.adElements.petCategory)]!),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.048,
          ),

          ///pet gender. e.g. : maschio, femmina
          petInfoLabel(
              context,
              getGenderLabel(context, this.widget.adElements.petGender!),
              CustomIcons.i24_icona_maschio),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.05,
          ),

          ///pet years
          petAgeInfoLabel(context, age, CustomIcons.i25_pet_age_icon),
        ],
      ),
    );
  }

  //Luogo dell'annuncio
  Widget _adLocation() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.001,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(
            CustomIcons.i26_pet_location,
            color: customWhite,
            size: MediaQuery.of(context).size.height * 0.03,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.02,
          ),
          Text(
            this.widget.adElements.adLocation!.region! +
                "," +
                this.widget.adElements.adLocation!.province! +
                "," +
                this.widget.adElements.adLocation!.city!,
            style: TextStyle(
              color: customWhite,
              fontSize: MediaQuery.of(context).size.height * 0.02,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  ///Data di pubblicazione dell'annuncio
  Widget _adDate() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.015,
      ),
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(
              CustomIcons.i15_calendar_icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.03,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.02,
            ),
            Text(
              AppLocalizations.of(context)!.adDate,
              style: TextStyle(
                color: customWhite,
                fontSize: MediaQuery.of(context).size.height * 0.02,
              ),
            ),
            Text(
              this.widget.adElements.adDate!,
              style: TextStyle(
                color: customWhite,
                fontSize: MediaQuery.of(context).size.height * 0.02,
              ),
            ),
          ]),
    );
  }

  Widget _bottomTextSide() {
    return Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.01),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _adName(),
              _petInformation(),
              _adLocation(),
              _adDate(),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        setState(() => loading = true);
        this.widget.adElements.adDescription =
            await Ads().getDescription(this.widget.adElements.id!);
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => AdDetail(
                    adElements: this.widget.adElements,
                    userData: this.widget.userData,
                  )),
        ).then((value) => setState(() => loading = false));
      },
      child: Container(
        decoration: adDecoration(context),
        margin:
            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.015),
        width: MediaQuery.of(context).size.width * 0.015,
        child: Column(
          children: <Widget>[
            _squareImage(context),
            _bottomTextSide(),
          ],
        ),
      ),
    );
  }
}
