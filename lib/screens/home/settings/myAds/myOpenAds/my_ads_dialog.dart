import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/services/ws_rest/ads.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/pop_up_model.dart';

/// AlertDialog feedback operazione completata
/// Compare solo come conferma di chiusura annuncio
void feedBackDialog(
  BuildContext context,
) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return confirmPopUp(
          context,
          AppLocalizations.of(context)!.operationComplete,
          AppLocalizations.of(context)!.deleteFeedback);
    },
  );
}

///AlertDialog dedicata alla chiusura dell'annuncio
void closeMyAdDialog(BuildContext context, AdElements adElements) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return popUpCustom(
          context,
          CustomIcons.i9_delete_icon,
          AppLocalizations.of(context)!.closingAd,
          AppLocalizations.of(context)!.closeMyAdDisappearDialog,
          _closeReasonButton(
              context,
              adElements.adCategory == AdCategory.Adoption
                  ? AppLocalizations.of(context)!.adopted
                  : AppLocalizations.of(context)!.found,
              0,
              adElements,
              adElements.contact!.userUid!,
              adElements.id!),
          _closeReasonButton(context, AppLocalizations.of(context)!.otherReasons,
              1, adElements, adElements.contact!.userUid!, adElements.id!),
          iconColor: Colors.green[300]);
    },
  );
}

//Tasto contenente la ragione della chiusura
ElevatedButton _closeReasonButton(BuildContext context, String reasonLabel,
    int indexReason, AdElements adElements, String uid, String id) {
  return ElevatedButton(
    style: ElevatedButton.styleFrom(
      fixedSize: Size(
        MediaQuery.of(context).size.width * 0.8,
        MediaQuery.of(context).size.height * 0.065,
      ),
      backgroundColor : customWhite,
      foregroundColor : customStrongLiliac,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: customWhite,
        ),
        borderRadius: mediumBorderRadius(context),
      ),
    ),
    onPressed: () async {
      if (indexReason == 0) {
        //chiusura dell'annuncio con timbro
        new Ads().closeAd(adElements, reasonLabel);
      } else {
        //Eliminazione definitiva tramite chiamata HTTP al back-end
        Ads().definitiveCancellation(id, uid);
        Navigator.of(context).pop();
        feedBackDialog(context);
      }
      Navigator.of(context).pop();
      feedBackDialog(context);
    },
    child: Text(reasonLabel,
        style: TextStyle(
            color: customStrongLiliac,
            fontSize: MediaQuery.of(context).size.height * 0.025)),
  );
}
