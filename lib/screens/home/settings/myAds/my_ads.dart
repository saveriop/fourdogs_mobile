import 'package:flutter/material.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/user.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:zampon/screens/home/settings/myAds/myOpenAds/my_ad_list_pagination.dart';
import 'package:zampon/screens/home/settings/myAds/myClosedAds/my_closed_ad_list_pagination.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/title_model.dart';

class MyAds extends StatefulWidget {
  final UserData userData;

  MyAds({required this.userData});

  @override
  _MyAdsState createState() => _MyAdsState();
}

class _MyAdsState extends State<MyAds> {
  bool openAds = true;
  String title = "";

  @override
  void initState() {
    super.initState();
  }

  ///Sottomenù dei filtri
  ///Anche essendo un sottomenù questo widget è scrollabile come un qualsiasi annuncio
  Widget _openAds() {
    return Container(
        decoration: BoxDecoration(
          color: customStrongLiliac,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            TextButton.icon(
              icon: Icon(
                CustomIcons.i28_icona_annunci_aperti,
                color: customWhite,
              ),
              label: Text(
                AppLocalizations.of(context)!.openAds,
                style: TextStyle(
                    decoration: openAds
                        ? TextDecoration.underline
                        : TextDecoration.none,
                    decorationThickness: 2,
                    fontWeight: FontWeight.bold,
                    fontSize: MediaQuery.of(context).size.height * 0.02,
                    color: customWhite),
              ),
              onPressed: () {
                setState(() {
                  openAds = true;
                });
              },
            ),
          ],
        ));
  }

  Widget _closedAds() {
    return Container(
        decoration: BoxDecoration(
          color: customStrongLiliac,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            TextButton.icon(
              icon: Icon(
                CustomIcons.i29_icona_annunci_chiusi,
                color: customWhite,
              ),
              label: Text(
                AppLocalizations.of(context)!.closedAds,
                style: TextStyle(
                    decoration: !openAds
                        ? TextDecoration.underline
                        : TextDecoration.none,
                    decorationThickness: 2,
                    fontWeight: FontWeight.bold,
                    fontSize: MediaQuery.of(context).size.height * 0.02,
                    color: customWhite),
              ),
              onPressed: () {
                setState(() {
                  openAds = false;
                });
              },
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return genericSliversPage(context,
        backgroundcolor: customWhite,
        columnElementList: <Widget>[
          pageName(
              context,
              openAds
                  ? AppLocalizations.of(context)!.myAds
                  : AppLocalizations.of(context)!.myClosedAds),
          Container(
            decoration: BoxDecoration(
              color: customStrongLiliac,
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [_openAds(), _closedAds()]),
          ),
          Expanded(
              child: openAds
                  ? MyAdListPagination(
                      user: this.widget.userData,
                    )
                  : MyClosedAdListPagination(
                      user: this.widget.userData,
                    ))
        ]);
  }
}
