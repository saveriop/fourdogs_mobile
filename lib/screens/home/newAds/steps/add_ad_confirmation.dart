import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/logo_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import '../../../wrapper.dart';

class AddAdConfirmation extends StatefulWidget {
  final String? title;
  final bool isEditAd;

  AddAdConfirmation({Key? key, this.title, required this.isEditAd}) : super(key: key);

  @override
  _AddAdConfirmationState createState() => _AddAdConfirmationState();
}

class _AddAdConfirmationState extends State<AddAdConfirmation> {
  bool loading = false;

  //Logo con nome
  Widget _logo() {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.03,
      ),
      child: noPaddingLogoAndName(context),
    );
  }

  Widget _confirmationMessage() {
    String confirmationMessage = this.widget.isEditAd
        ? AppLocalizations.of(context)!.editConfirm
        : AppLocalizations.of(context)!.publicConfirm;

    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.05,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.85,
        child: Text(confirmationMessage,
            style: informationTitelStyle(context), textAlign: TextAlign.center),
      ),
    );
  }

  //Icona
  Widget _addAdIcon() {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.05,
      ),
      child: Container(
        child: Icon(
          CustomIcons.i50_icona_spunta,
          color: customWhite,
          size: MediaQuery.of(context).size.width * 0.08,
        ),
      ),
    );
  }

  //Bottone conferma
  Widget _confirmButton() {
    return confirmationButton(context, AppLocalizations.of(context)!.confirm,
        () async {
      Navigator.of(context)..pop();
      Navigator.of(context).pop();
      Navigator.of(context).pop();

      Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (BuildContext context) => new Wrapper()));
    });
  }

  @override
  Widget build(BuildContext context) {
    ///Questo widget è generato dal metodo generico genericPage()
    ///Pertanto nel metodo build() si alimenta una lista di Widget con tutti gli oggetti da mettere a video
    List<Widget> columnElementList = <Widget>[];

    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.10,
    ));
    columnElementList.add(_logo());
    columnElementList.add(_confirmationMessage());
    columnElementList.add(_addAdIcon());
    columnElementList.add(_confirmButton());

    return loading
        ? Loading()

        ///Il Widget WillPopScope serve per inibire il tasto indietro del sistema
        : WillPopScope(
            onWillPop: () async => false,
            child: genericPage(
              context,
              false,
              columnElementList: columnElementList,
              appbar: false,
              trasparent: false,
              noTitle: false,
            ),
          );
  }
}
