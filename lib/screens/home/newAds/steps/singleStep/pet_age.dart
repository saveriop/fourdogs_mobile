import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/model/age.dart';
import 'package:zampon/shared/constants.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

final GlobalKey<FormState> formkey = GlobalKey<FormState>();
bool unknowAgeformkey = false;

GlobalKey<FormState> petAgeformVal() {
  return formkey;
}

///Sezione per selezionare l'età del pet
Widget petAge({
  BuildContext? context,
  bool? unknowAge,
  Function(bool?)? unknowAgeChecked,
  AdElements? adElements,
  bool? isEdit,
}) {
  unknowAgeformkey = unknowAge!;
  return Form(
    key: formkey,
    child: Padding(
        padding: EdgeInsets.only(),
        child: Column(children: [
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context!).size.height * 0.01),
            child: largeDivider(context),
          ),
          subTitle(
            context,
            AppLocalizations.of(context)!.age,
            icon: FaIcon(
              FontAwesomeIcons.asterisk,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.015,
            ),
            thereIsIcon: true,
            iconOnLeft: false,
          ),
          Padding(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.01,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  child: Container(
                    child: textFormFieldCustom(
                      context,
                      readOnly: unknowAge ? true : false,
                      isResettable: true,
                      initialValue:
                          isEdit! ? adElements!.petAge!.years.toString() : "",
                      keyboardType: TextInputType.number,
                      icon: Icon(
                        CustomIcons.i25_pet_age_icon,
                        color: customWhite,
                      ),
                      labelText: AppLocalizations.of(context)!.years,
                      hintText: AppLocalizations.of(context)!.years,
                      validator: validateAge,
                      onChanged: (value) {
                        if (adElements!.petAge != null) {
                          isNumeric(value!)
                              ? adElements.petAge!.years = int.parse(value)
                              : adElements.petAge!.years = 0;
                        } else {
                          adElements.petAge = Age(months: 0, years: 0);
                          isNumeric(value!)
                              ? adElements.petAge!.years = int.parse(value)
                              : adElements.petAge!.years = 0;
                        }
                      },
                    ),
                    height: MediaQuery.of(context).size.height * 0.08,
                    width: MediaQuery.of(context).size.width * 0.4,
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.04,
                ),
                Flexible(
                  child: Container(
                    child: textFormFieldCustom(
                      context,
                      readOnly: unknowAge ? true : false,
                      isResettable: true,
                      initialValue:
                          isEdit ? adElements!.petAge!.months.toString() : "",
                      keyboardType: TextInputType.number,
                      labelText: AppLocalizations.of(context)!.months,
                      hintText: AppLocalizations.of(context)!.months,
                      validator: validateMonth,
                      onChanged: (value) {
                        if (adElements!.petAge != null) {
                          isNumeric(value!)
                              ? adElements.petAge!.months = int.parse(value)
                              : adElements.petAge!.months = 0;
                        } else {
                          adElements.petAge = Age(years: 0, months: 0);
                          isNumeric(value!)
                              ? adElements.petAge!.months = int.parse(value)
                              : adElements.petAge!.months = 0;
                        }
                      },
                    ),
                    height: MediaQuery.of(context).size.height * 0.08,
                    width: MediaQuery.of(context).size.width * 0.3,
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                padding: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Text(
                  AppLocalizations.of(context)!.dontKnow,
                  style: TextStyle(
                    color: customWhite,
                    fontWeight: FontWeight.bold,
                    fontSize: MediaQuery.of(context).size.width * 0.055,
                  ),
                ),
              ),
              Theme(
                data: Theme.of(context)
                    .copyWith(unselectedWidgetColor: customWhite),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                  width: MediaQuery.of(context).size.width * 0.06,
                  child: Checkbox(
                    activeColor: customPaleLiliac,
                    value: unknowAge,
                    onChanged: unknowAgeChecked,
                    tristate: false,
                  ),
                ),
              ),
            ]),
          ),
          textPagination(context, AppLocalizations.of(context)!.selectOption),
        ])),
  );
}
