import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';

import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/checkbox_model.dart';

//Sezione per selezionare il genere del pet
Widget petSize({
  BuildContext? context,
  bool? isSmall,
  bool? isMedium,
  bool? isLarge,
  Function(bool?)? isSmallChecked,
  Function(bool?)? isMediumChecked,
  Function(bool?)? isLargeChecked,
}) {
  return Padding(
      padding: EdgeInsets.only(),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context!).size.height * 0.01),
          child: largeDivider(context),
        ),
        subTitle(
          context,
          AppLocalizations.of(context)!.size,
          icon: FaIcon(
            FontAwesomeIcons.asterisk,
            color: customWhite,
            size: MediaQuery.of(context).size.height * 0.015,
          ),
          thereIsIcon: true,
          iconOnLeft: false,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.fSmall,
          isSmall!,
          isSmallChecked!,
          therIsIcon: true,
          icon: FaIcon(CustomIcons.i53_small_size_icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.03),
          subtext: AppLocalizations.of(context)!.max12Kg,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.fMedium,
          isMedium!,
          isMediumChecked!,
          therIsIcon: true,
          icon: FaIcon(CustomIcons.i52_medium_size_icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.03),
          subtext: AppLocalizations.of(context)!.max30Kg,
          topPadding: MediaQuery.of(context).size.height * 0.02,
          bottomPadding: MediaQuery.of(context).size.height * 0.02,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.fLarge,
          isLarge!,
          isLargeChecked!,
          therIsIcon: true,
          icon: FaIcon(CustomIcons.i51_large_size_icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.045),
          subtext: AppLocalizations.of(context)!.from30Kg,
        ),
        textPagination(context, AppLocalizations.of(context)!.selectOption),
      ]));
}
