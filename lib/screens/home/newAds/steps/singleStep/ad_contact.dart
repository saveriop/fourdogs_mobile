import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

final GlobalKey<FormState> formkey = GlobalKey<FormState>();

GlobalKey<FormState> adContactformVal() {
  return formkey;
}

//Sezione contatti annuncio
Widget adContact({BuildContext? context, AdElements? adElements}) {
  return Form(
    key: formkey,
    child: Padding(
        padding: EdgeInsets.only(),
        child: Column(children: [
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context!).size.height * 0.01),
            child: largeDivider(context),
          ),
          subTitle(
            context,
            AppLocalizations.of(context)!.contacts,
            icon: Icon(CustomIcons.i50_icona_spunta, color: customWhite),
          ),

          ///input box numero di telefono
          Container(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.05,
              right: MediaQuery.of(context).size.width * 0.1,
              top: MediaQuery.of(context).size.width * 0.02,
              bottom: MediaQuery.of(context).size.width * 0.02,
            ),
            height: MediaQuery.of(context).size.height * 0.11,
            child: textFormFieldCustom(
              context,
              maxLength: 11,
              icon: Icon(
                CustomIcons.i10_telephone_icon,
                color: customWhite,
              ),
              labelText: AppLocalizations.of(context)!.phoneNumber,
              keyboardType: TextInputType.phone,
              initialValue: adElements!.contact!.mobilePhoneForAd,
              validator: validateMobile,
              onChanged: (value) {
                adElements.contact!.mobilePhoneForAd = value;
              },
            ),
          ),

          ///input box email
          Container(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.05,
              right: MediaQuery.of(context).size.width * 0.1,
              top: MediaQuery.of(context).size.width * 0.02,
              bottom: MediaQuery.of(context).size.width * 0.02,
            ),
            height: MediaQuery.of(context).size.height * 0.11,
            child: Focus(
              child: textFormFieldCustom(
                context,
                maxLength: 64,
                icon: Icon(
                  CustomIcons.i17_email_icon,
                  color: customWhite,
                ),
                labelText: AppLocalizations.of(context)!.emailAddress,
                keyboardType: TextInputType.emailAddress,
                initialValue: adElements.contact!.emailForAd,
                validator: validateEmail,
                onChanged: (value) {
                  adElements.contact!.emailForAd = value;
                },
              ),
            ),
          ),
          textPagination(
              context, AppLocalizations.of(context)!.necessaryInformation),
        ])),
  );
}
