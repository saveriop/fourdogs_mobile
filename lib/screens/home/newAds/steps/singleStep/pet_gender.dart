import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/shared/constants.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/shared/widgetModel/checkbox_model.dart';

//Sezione per selezionare il genere del pet
Widget petGender({
  BuildContext? context,
  bool? isMale,
  bool? isFemale,
  bool? isOtherGen,
  Function(bool?)? maleChecked,
  Function(bool?)? femaleChecked,
  Function(bool?)? otherGenChecked,
}) {
  return Padding(
      padding: EdgeInsets.only(),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context!).size.height * 0.01),
          child: largeDivider(context),
        ),
        subTitle(
          context,
          AppLocalizations.of(context)!.gender,
          icon: FaIcon(
            FontAwesomeIcons.asterisk,
            color: customWhite,
            size: MediaQuery.of(context).size.height * 0.015,
          ),
          thereIsIcon: true,
          iconOnLeft: false,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.male,
          isMale!,
          maleChecked!,
          therIsIcon: true,
          icon: Icon(CustomIcons.i24_icona_maschio,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.035),
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.female,
          isFemale!,
          femaleChecked!,
          therIsIcon: true,
          icon: Icon(CustomIcons.i21_icona_femmina,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.04),
          topPadding: MediaQuery.of(context).size.height * 0.02,
          bottomPadding: MediaQuery.of(context).size.height * 0.02,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.unknown,
          isOtherGen!,
          otherGenChecked!,
          therIsIcon: true,
          icon: Icon(CustomIcons.i23_icona_genere_sconosciuto,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.03),
        ),
        textPagination(context, AppLocalizations.of(context)!.selectOption),
      ]));
}
