import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

final GlobalKey<FormState> formkey = GlobalKey<FormState>();

GlobalKey<FormState> adInformationformVal() {
  return formkey;
}

//Sezione Informazioni annuncio
Widget adInfo(BuildContext context, AdElements adElements) {
  return Form(
    key: formkey,
    child: Padding(
        padding: EdgeInsets.only(),
        child: Column(children: [
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height * 0.01),
            child: largeDivider(context),
          ),
          subTitle(
            context,
            AppLocalizations.of(context)!.information,
            icon: Icon(CustomIcons.i50_icona_spunta, color: customWhite),
          ),
          //Sezione Nome Annuncio
          Container(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.05,
              right: MediaQuery.of(context).size.width * 0.1,
              top: MediaQuery.of(context).size.width * 0.02,
              bottom: MediaQuery.of(context).size.width * 0.00029,
            ),
            height: MediaQuery.of(context).size.height * 0.11,
            child: textFormFieldCustom(
              context,
              maxLength: 28,
              initialValue: adElements.petName,
              icon: Icon(
                CustomIcons.i27_pet_name_icon,
                color: customWhite,
              ),
              labelText: AppLocalizations.of(context)!.insertName,
              //hintText: AppLocalizations.of(context).insertName,
              validator: validateName,
              onChanged: (value) {
                adElements.petName = value!;
              },
            ),
          ),

          //CITTA'
          Container(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.05,
              right: MediaQuery.of(context).size.width * 0.1,
              top: MediaQuery.of(context).size.width * 0.02,
              bottom: MediaQuery.of(context).size.width * 0.00002,
            ),
            height: MediaQuery.of(context).size.height * 0.08,
            child: textFormFieldForItalianCity(context, adElements,
                icon: Icon(
                  CustomIcons.i26_pet_location,
                  color: customWhite,
                ),
                labelText: AppLocalizations.of(context)!.insertPlace),
          ),
          textPagination(
              context, AppLocalizations.of(context)!.necessaryInformation),
        ])),
  );
}
