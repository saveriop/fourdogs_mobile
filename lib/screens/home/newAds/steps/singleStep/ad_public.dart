import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/model/age.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/newAds/steps/singleStep/ad_contact.dart';
import 'package:zampon/screens/home/newAds/steps/singleStep/ad_description.dart';
import 'package:zampon/screens/home/newAds/steps/singleStep/pet_age.dart';
import 'package:zampon/services/firebase/storage/storage_dp.dart';
import 'package:zampon/services/ws_rest/ads.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/screens/home/newAds/steps/singleStep/ad_information.dart';

import '../add_ad_confirmation.dart';

Widget _public(BuildContext context) {
  return Container(
    width: MediaQuery.of(context).size.width * 0.85,
    child: Text(AppLocalizations.of(context)!.beforePublicate,
        style: informationSubtitelStyle(context), textAlign: TextAlign.center),
  );
}

Widget _errorMessage(BuildContext context) {
  return Container(
    width: MediaQuery.of(context).size.width * 0.85,
    child: Text(AppLocalizations.of(context)!.genericFormError,
        style: errorSubtitelStyle(context), textAlign: TextAlign.center),
  );
}

///Metodo valida i dati del form di compilazione
///Se tutti i dati obbligatori sono stati compilati
///e se è stata caricata almeno una foto per l'annuncio
///solo allora l'annuncio viene registrato
///Se tutti i dati sono corretti restituisce true
_validateForm(List<String> photoList, Function updateErrorMessage) {
  GlobalKey<FormState> adInformationformkey = adInformationformVal();
  GlobalKey<FormState> petAgeformKey = petAgeformVal();
  GlobalKey<FormState> adContactformkey = adContactformVal();
  GlobalKey<FormState> adDescriptionformkey = adDescriptionformVal();
  if (adInformationformkey.currentState!.validate() &&
      adDescriptionformkey.currentState!.validate() &&
      adContactformkey.currentState!.validate() &&
      photoList.isNotEmpty &&
      (unknowAgeformkey || petAgeformKey.currentState!.validate())) {
    return true;
  } else {
    ///Nel caso in cui la validazione non venga superata viene richiamato il metodo updateErrorMessage()
    ///Il metodo attiva una boolean per attivare il messaggio di errore generico
    updateErrorMessage();
    return false;
  }
}

///Questo metodo viene richiamato solo se si accede nella modalità di creazione annuncio
///Valida i dati inseriti dall'utente e nel caso in cui la validizione si concluda con successo
///effettua le dovute chiamate HTTP per salvare l'annuncio presso il Back-end e
///salvare le immagini nello storage
_createAd({
  BuildContext? context,
  List<String>? photoList,
  AdElements? adElements,
  Function? updateErrorMessage,
  Function? loaderOn,
  Function? loaderOff,
}) async {
  ///Se tutti i dati obbligatori sono stati compilati
  ///e se è stata caricata almeno una foto per l'annuncio
  ///solo allora l'annuncio viene registrato
  if (_validateForm(photoList!, updateErrorMessage!)) {
    loaderOn!();

    ///viene effettuata una chiamata HTTP POST per la registrazione dell'annuncio
    String id = await Ads().createAd(adElements!);

    ///Se la risposta al Back-End ha esito positivo (http-code 200)
    ///si riceverà in risposta l'Id dell'annuncio
    ///Questo Id sarà utilizzato per salvare le immagini nello store
    if (id.isNotEmpty) {
      int i = 0;
      StorageDp storageDp = new StorageDp();
      photoList.forEach((element) async {
        File image = File(element);
        storageDp.insertPetImg(image, id + "_" + i.toString(), id);
        i++;
      });
      Navigator.push(
        context!,
        MaterialPageRoute(
            builder: (context) => AddAdConfirmation(
                  isEditAd: false,
                )),
      ).then((value) => loaderOff!());
    }
  }
}

///Questo metodo viene richiamato solo se si accede nella modalità di modifica annuncio
///Valida i dati inseriti dall'utente e nel caso in cui la validizione si concluda con successo
///effettua le dovute chiamate HTTP per salvare l'annuncio presso il Back-end e
///salvare le immagini nello storage
_modifyAd(
    {BuildContext? context,
    List<String>? photoList,
    AdElements? adElements,
    Function? updateErrorMessage}) async {
  ///Se tutti i dati obbligatori sono stati compilati
  ///e se è stata caricata almeno una foto per l'annuncio
  ///solo allora l'annuncio viene modificato
  if (_validateForm(photoList!, updateErrorMessage!)) {
    int? status = await Ads().modifyAd(adElements!);

    ///se la richiesta di aggiornamento è terminata con successo
    ///il metodo restituirà HTTP CODE 201
    ///Solo in quel caso aggiorno le immagini nello storage
    if (status == 201) {
      String id = adElements.id!;
      int i = 0;
      StorageDp storageDp = new StorageDp();
      photoList.forEach((element) async {
        File image = File(element);
        storageDp.insertPetImg(image, id + "_" + i.toString(), id);
        i++;
      });
      Navigator.push(
        context!,
        MaterialPageRoute(
            builder: (context) => AddAdConfirmation(
                  isEditAd: true,
                )),
      );
    }
  }
}

///Sezione con descrizione tasto di conferma
Widget adPublic({
  BuildContext? context,
  AdElements? adElements,
  UserData? userData,
  bool? isEditAd,
  bool? formErrorMessage,
  List<String>? photoList,
  Function? updateErrorMessage,
  Function? loaderOn,
  Function? loaderOff,
}) {
  return Padding(
      padding: EdgeInsets.only(),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context!).size.height * 0.01),
          child: largeDivider(context),
        ),

        ///Titolo
        subTitle(
          context,
          AppLocalizations.of(context)!.information,
          icon: Icon(CustomIcons.i50_icona_spunta, color: customWhite),
        ),
        Container(
          padding: EdgeInsets.only(
            left: MediaQuery.of(context).size.width * 0.05,
            right: MediaQuery.of(context).size.width * 0.1,
            top: MediaQuery.of(context).size.width * 0.02,
            bottom: MediaQuery.of(context).size.width * 0.02,
          ),
          child: _public(context),
        ),
        Padding(
          padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.02,
              bottom: MediaQuery.of(context).size.height * 0.03),
          child: Container(
            child:
                Image(image: AssetImage('assets/logo/small_logo_no_name.png')),
          ),
        ),

        ///Messaggio di errore da mostrare a video in caso di mancata compilazione dei campi obbligatori
        formErrorMessage!
            ? Container(
                padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.05,
                  right: MediaQuery.of(context).size.width * 0.1,
                  top: MediaQuery.of(context).size.width * 0.02,
                  bottom: MediaQuery.of(context).size.width * 0.02,
                ),
                child: _errorMessage(context))
            : Container(),

        ///Tasto conferma
        ///Prima di registrare/modificare l'annuncio effettua la validazione dei campi obbligatori
        confirmationButton(context, AppLocalizations.of(context)!.post,
            () async {
          await DefaultCacheManager().emptyCache();
          if (adElements!.petAge == null) {
            adElements.petAge = Age(months: 0, years: 0);
          } else if (adElements.petAge!.years == null ||
              adElements.petAge!.months == null) {
            adElements.petAge!.years =
                adElements.petAge!.years == null ? 0 : adElements.petAge!.years;
            adElements.petAge!.months =
                adElements.petAge!.months == null ? 0 : adElements.petAge!.months;
          }
          if (isEditAd!) {
            _modifyAd(
                context: context,
                photoList: photoList,
                adElements: adElements,
                updateErrorMessage: updateErrorMessage);
          } else {
            _createAd(
              context: context,
              photoList: photoList,
              adElements: adElements,
              updateErrorMessage: updateErrorMessage,
              loaderOn: loaderOn,
              loaderOff: loaderOff,
            );
          }
        }),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.05,
        )
      ]));
}
