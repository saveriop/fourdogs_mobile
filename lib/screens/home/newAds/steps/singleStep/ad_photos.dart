import 'dart:io';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/shared/constants.dart';

XFile? imageFile;
ImagePicker _picker = ImagePicker();

//Sezione Foto annuncio
Widget adPhoto(
    {BuildContext? context,
    AdElements? adElements,
    List<String>? photoList,
    Function? updatePhoto}) {
  //size container della galleria di foto
  double containerSize;
  if (!photoList!.asMap().containsKey(1)) {
    containerSize = MediaQuery.of(context!).size.height * 0.22;
  } else if (photoList.asMap().containsKey(1) &&
      !photoList.asMap().containsKey(4)) {
    containerSize = MediaQuery.of(context!).size.height * 0.38;
  } else {
    containerSize = MediaQuery.of(context!).size.height * 0.54;
  }
  return Padding(
      padding: EdgeInsets.only(),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height * 0.01),
          child: largeDivider(context),
        ),
        subTitle(
          context,
          AppLocalizations.of(context)!.adPhotoPt1 +
              photoList.length.toString() +
              AppLocalizations.of(context)!.adPhotoPt2,
          icon: FaIcon(
            FontAwesomeIcons.asterisk,
            color: customWhite,
            size: MediaQuery.of(context).size.height * 0.015,
          ),
          thereIsIcon: true,
          iconOnLeft: false,
        ),
        Container(
          //se la lista di foto è di lunghezza almeno 3, allora il container diventa più grande
          height: containerSize,
          width: MediaQuery.of(context).size.width * 0.9,
          color: Colors.transparent,
          child: Column(children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            Row(
              children: [
                _adPhotoButtonFromGallery(
                    context, updatePhoto!, photoList.length),
                _adPhotoButtonFromCamera(
                    context, updatePhoto, photoList.length),
                _viewImages(context, 0, updatePhoto, photoList),
              ],
            ),
            Row(children: [
              _viewImages(context, 1, updatePhoto, photoList),
              _viewImages(context, 2, updatePhoto, photoList),
              _viewImages(context, 3, updatePhoto, photoList),
            ]),
            Row(children: [
              _viewImages(context, 4, updatePhoto, photoList),
              _viewImages(context, 5, updatePhoto, photoList),
            ]),
          ]),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.05,
        ),
        textPagination(context, AppLocalizations.of(context)!.addPhotoGuid),
      ]));
}

Future<String> _cropImage(String photoPath) async {
  CroppedFile? croppedFile = await ImageCropper()
      .cropImage(
        cropStyle: CropStyle.rectangle,
        sourcePath: photoPath,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        /*
      androidUiSettings: AndroidUiSettings(
          toolbarTitle: "Ritaglia l'immagine",
          toolbarColor: customStrongLiliac,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false),
          */
        /*
      iosUiSettings: IOSUiSettings(
        title: "Ritaglia l'immagine",
      )*/
        ///Nel caso in cui si accede alla galleria oppure alla fotocamera e non si scatta/sceglie alcuna foto e si torna indietro l'eccezione viene catturata
      )
      .catchError((error, stackTrace) => null);
  if (croppedFile != null) {
    return croppedFile.path;
  }
  return photoPath;
}

///Widget che contiene il bottone per il caricamento tramite galleria
Widget _adPhotoButtonFromGallery(
    BuildContext context, Function updatePhoto, int photoNumber) {
  return IconButton(
    onPressed: () {
      if (photoNumber <= 5) {
        _openGallery(context, updatePhoto);
      }
    },
    icon: Icon(CustomIcons.i20_gallery_icon,
        color: photoNumber <= 5 ? customWhite : customStrongGrey),
    iconSize: MediaQuery.of(context).size.width * 0.27,
    padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.01,
        vertical: MediaQuery.of(context).size.height * 0.004),
  );
}

///Widget che contiene il bottone per il caricamento tramite camera
Widget _adPhotoButtonFromCamera(
    BuildContext context, Function updatePhoto, int photoNumber) {
  return IconButton(
    onPressed: () {
      if (photoNumber <= 5) {
        _openCamera(context, updatePhoto);
      }
    },
    icon: Icon(CustomIcons.i44_photo_icon,
        color: photoNumber <= 5 ? customWhite : customStrongGrey),
    iconSize: MediaQuery.of(context).size.width * 0.27,
    padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.01,
        vertical: MediaQuery.of(context).size.height * 0.004),
  );
}

///Funzione per il widget del caricamento tramite galleria
Future _openGallery(BuildContext context, Function updatePhoto) async {
  imageFile = await _picker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 50,
      maxWidth: 800,
      maxHeight: 600);

  ///questo controllo serve per evitare il null-point
  ///nel caso in cui l'utente non selezioni la foto e torni indietro
  if (imageFile != null) {
    String filePath = await _cropImage(imageFile!.path);
    //updatePhoto(imageFile.path, true);
    updatePhoto(filePath, true);
  }
}

///Funzione per il widget del caricamento tramite fotocamera
Future _openCamera(BuildContext context, Function updatePhoto) async {
  imageFile = await _picker.pickImage(
      source: ImageSource.camera,
      imageQuality: 50,
      maxWidth: 800,
      maxHeight: 600);

  ///questo controllo serve per evitare il null-point
  ///nel caso in cui l'utente non selezioni la foto e torni indietro
  if (imageFile != null) {
    updatePhoto(imageFile!.path, true);
  }
}

///Restituisce l'immagine da mostrare in griglia
Widget _getImage(BuildContext context, String url) {
  if (url.startsWith("assets")) {
    return Image.asset(
      url,
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width * 0.27,
      height: MediaQuery.of(context).size.width * 0.27,
      fit: BoxFit.cover,
    );
  } else if (url.startsWith("http")) {
    return Image.network(
      url,
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width * 0.27,
      height: MediaQuery.of(context).size.width * 0.27,
      fit: BoxFit.cover,
    );
  } else {
    return Image.file(
      File(url),
      fit: BoxFit.cover,
      width: MediaQuery.of(context).size.width * 0.27,
      height: MediaQuery.of(context).size.width * 0.27,
      alignment: Alignment.center,
    );
  }
}

//Contenitore per ogni singola immagine caricata della lista
Widget _viewImages(BuildContext context, int index, Function updatePhoto,
    List<String> photoList) {
  if (photoList.asMap().containsKey(index)) {
    return Container(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.01,
            vertical: MediaQuery.of(context).size.height * 0.004),
        child: Stack(children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: _getImage(context, photoList[index]),
          ),
          Positioned.fill(
            child: IconButton(
              alignment: Alignment.topRight,
              icon: Icon(Icons.cancel),
              color: Colors.red,
              iconSize: MediaQuery.of(context).size.height * 0.03,
              disabledColor: Colors.yellow,
              onPressed: () {
                updatePhoto(photoList[index], false);
              },
            ),
          ),
        ]));
  } else
    return Container();
}
