import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/shared/constants.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/shared/widgetModel/checkbox_model.dart';

//Sezione per la categoria di annuncio
Widget adCategory({
  BuildContext? context,
  bool? isAdoption,
  bool? isDisappear,
  bool? isFound,
  Function(bool?)? adoptionChecked,
  Function(bool?)? disappearChecked,
  Function(bool?)? foundChecked,
}) {
  return Padding(
      padding: EdgeInsets.only(),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context!).size.height * 0.01),
          child: largeDivider(context),
        ),
        subTitle(
          context,
          AppLocalizations.of(context)!.adCategory,
          icon: FaIcon(
            FontAwesomeIcons.asterisk,
            color: customWhite,
            size: MediaQuery.of(context).size.height * 0.015,
          ),
          thereIsIcon: true,
          iconOnLeft: false,
          bottomPadding: MediaQuery.of(context).size.height * 0.01,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.adoption,
          isAdoption!,
          adoptionChecked!,
          therIsIcon: true,
          icon: FaIcon(CustomIcons.i1_adoption_icon, color: customWhite),
          bottomPadding: MediaQuery.of(context).size.height * 0.02,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.disappearCategory,
          isDisappear!,
          disappearChecked!,
          therIsIcon: true,
          icon: FaIcon(CustomIcons.i4_lost_found_pet, color: customWhite),
          bottomPadding: MediaQuery.of(context).size.height * 0.02,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.foundCategory,
          isFound!,
          foundChecked!,
          therIsIcon: true,
          icon: FaIcon(CustomIcons.i3_found_pet_icon, color: customWhite),
        ),
        textPagination(context, AppLocalizations.of(context)!.selectOption),
      ]));
}
