import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/shared/constants.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/shared/widgetModel/checkbox_model.dart';

//Sezione per selezionare la specie del pet
Widget petSpecies({
  BuildContext? context,
  bool? isDog,
  bool? isCat,
  bool? isOther,
  Function(bool?)? dogChecked,
  Function(bool?)? catChecked,
  Function(bool?)? otherChecked,
}) {
  return Padding(
      padding: EdgeInsets.only(),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context!).size.height * 0.01),
          child: largeDivider(context),
        ),
        subTitle(
          context,
          AppLocalizations.of(context)!.species,
          icon: FaIcon(
            FontAwesomeIcons.asterisk,
            color: customWhite,
            size: MediaQuery.of(context).size.height * 0.015,
          ),
          thereIsIcon: true,
          iconOnLeft: false,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.dog,
          isDog!,
          dogChecked!,
          therIsIcon: true,
          icon: FaIcon(CustomIcons.i48_dog_type_icon, color: customWhite),
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.cat,
          isCat!,
          catChecked!,
          therIsIcon: true,
          icon: FaIcon(CustomIcons.i47_cat_type_icon, color: customWhite),
          topPadding: MediaQuery.of(context).size.height * 0.02,
          bottomPadding: MediaQuery.of(context).size.height * 0.02,
        ),
        newAdCheckbox(
          context,
          AppLocalizations.of(context)!.otherPet,
          isOther!,
          otherChecked!,
          therIsIcon: true,
          icon: FaIcon(CustomIcons.i49_other_type_icon, color: customWhite),
        ),
        textPagination(context, AppLocalizations.of(context)!.selectOption),
      ]));
}
