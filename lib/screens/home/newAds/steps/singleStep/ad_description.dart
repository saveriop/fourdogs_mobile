import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

final GlobalKey<FormState> formkey = GlobalKey<FormState>();

GlobalKey<FormState> adDescriptionformVal() {
  return formkey;
}

//Inserimento descrizione dell'annuncio
Widget adDescription({BuildContext? context, AdElements? adElements}) {
  return Form(
    key: formkey,
    child: Padding(
        padding: EdgeInsets.only(),
        child: Column(children: [
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context!).size.height * 0.01),
            child: largeDivider(context),
          ),
          subTitle(
            context,
            AppLocalizations.of(context)!.description,
            icon: Icon(CustomIcons.i50_icona_spunta, color: customWhite),
            bottomPadding: MediaQuery.of(context).size.height * 0.01,
          ),
          Container(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.05,
              right: MediaQuery.of(context).size.width * 0.1,
            ),
            child: textFormFieldCustom(context,
                initialValue: adElements!.adDescription,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.sentences,
                maxLength: 560,
                maxLines: null,
                minLines: 6,
                labelText: AppLocalizations.of(context)!.description,
                validator: validateDescription,
                icon: Icon(
                  CustomIcons.i16_description_icon,
                  color: customWhite,
                ), onChanged: (value) {
              adElements.adDescription = value;
            }, fontSize: MediaQuery.of(context).size.width * 0.04),
          )
        ])),
  );
}
