import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/newAds/add_new_ad.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';

import 'package:zampon/shared/widgetModel/logo_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

class InformativeText extends StatefulWidget {
  final UserData user;

  InformativeText({required this.user});

  @override
  State<InformativeText> createState() => _InformativeTextState();
}

class _InformativeTextState extends State<InformativeText> {

  //Logo con nome
  Widget _logo() {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.03,
      ),
      child: noPaddingLogoAndName(context),
    );
  }

  Widget _warningMessageTitle() {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.00005,
        bottom: MediaQuery.of(context).size.height * 0.015,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.85,
        child: Text(AppLocalizations.of(context)!.adsWarningTitle,
            style: informationTitelStyle(context), textAlign: TextAlign.center),
      ),
    );
  }

  Widget _warningMessage() {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.01,
        left: MediaQuery.of(context).size.width * 0.05,
        right: MediaQuery.of(context).size.width * 0.05,
      ),
      child: Container(
        child: Text(
          AppLocalizations.of(context)!.adsWarning,
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _banMessage() {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.005,
        left: MediaQuery.of(context).size.width * 0.05,
        right: MediaQuery.of(context).size.width * 0.05,
      ),
      child: Container(
        child: Text(
          AppLocalizations.of(context)!.banWarning,
          style: informationSubtitelStyle(context),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  //Bottone conferma
  Widget _confirmButton() {
    return confirmationButton(context, AppLocalizations.of(context)!.understand,
        () async {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => AddNewAd(
                  userData: this.widget.user,
                  adElements: initAdElement(),
                  photoList: [],
                  isEditAd: false)));
    });
  }

  @override
  Widget build(BuildContext context) {
    ///Questo widget è generato dal metodo generico genericPage()
    ///Pertanto nel metodo build() si alimenta una lista di Widget con tutti gli oggetti da mettere a video
    List<Widget> columnElementList = <Widget>[];

    columnElementList.add(_warningMessageTitle());
    columnElementList.add(_warningMessage());
    columnElementList.add(_banMessage());
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.03,
    ));
    columnElementList.add(_logo());
    columnElementList.add(_confirmButton());

    return genericPage(context, true,
        columnElementList: columnElementList,
        appbar: false,
        trasparent: false,
        noTitle: false,
        backButtonColor: customWhite);
  }
}
