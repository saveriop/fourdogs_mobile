import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';

import 'package:zampon/model/ad.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/enum/gender.dart';
import 'package:zampon/model/enum/pet_category.dart';
import 'package:zampon/model/enum/pet_size.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'steps/singleStep/ad_category.dart';
import 'steps/singleStep/ad_contact.dart';
import 'steps/singleStep/ad_description.dart';
import 'steps/singleStep/ad_information.dart';
import 'steps/singleStep/ad_photos.dart';
import 'steps/singleStep/ad_public.dart';
import 'steps/singleStep/pet_age.dart';
import 'steps/singleStep/pet_gender.dart';
import 'steps/singleStep/pet_size.dart';
import 'steps/singleStep/pet_species.dart';

///Questo Widget viene utilizzato sia per la creazione annuncio che per la modifica
///Parametri in input obbligatori:
///userData -> bean dell'utente che effettua l'operazione
///adElements -> bean dell'annuncio. In caso di creazione sarà inizializzato con dei valori standard. In caso di modifica sarà valorizzato con i valori dell'annuncio
///photoList -> Lista di URL delle foto dell'annuncio. In caso di creazione la lista sarà vuota. In caso di modifica sarà popolata con la lista dei link https di firebase
///isEditAd -> se è valorizzata con TRUE allora indica che si è acceduti in modalità modifica, altrimenti in creazione
class AddNewAd extends StatefulWidget {
  final UserData userData;
  final AdElements adElements;
  final List<String> photoList;
  final bool isEditAd;

  AddNewAd(
      {required this.userData,
      required this.adElements,
      required this.photoList,
      required this.isEditAd});

  @override
  _AddNewAdState createState() => _AddNewAdState();
}

class _AddNewAdState extends State<AddNewAd> {
  /// Variabile che gestisce il loading.
  /// Quando l'utente clicca bottoni asincroni, per far si che non vengano innescati più volte
  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;

  ///booleana che controlla la visualizzazione del messaggio di errore per i campi obbligatori
  bool formErrorMessage = false;

  ///Bool per la Categoria
  bool isAdoption = true;
  bool isDisappear = false;
  bool isFound = false;

  ///Bool per la specie
  bool isDog = true;
  bool isCat = false;
  bool isOther = false;

  ///Bool per il genere
  bool isMale = true;
  bool isFemale = false;
  bool isOtherGen = false;

  ///Bool per l'età
  bool unknowAge = false;

  ///Bool per la taglia
  bool isSmall = false;
  bool isMedium = true;
  bool isLarge = false;

  ///Sottotitolo della pagina
  Widget _subTitle() {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        child: subTitle(
          context,
          this.widget.isEditAd
              ? AppLocalizations.of(context)!.editAd
              : AppLocalizations.of(context)!.createYourAd,
          icon: FaIcon(CustomIcons.i5_icona_annuncio, color: customWhite),
          thereIsIcon: true,
        ));
  }

  ///I contatti dell'annuncio (email e numero di telefono) vengono
  ///inizializzati con i dati dell'utente che sta creando l'annuncio
  void _initContact() {
    this.widget.adElements.contact!.emailForAd =
        this.widget.adElements.contact!.emailForAd!.isEmpty
            ? this.widget.userData.email
            : this.widget.adElements.contact!.emailForAd;
    this.widget.adElements.contact!.mobilePhoneForAd =
        this.widget.adElements.contact!.mobilePhoneForAd!.isEmpty
            ? this.widget.userData.mobilePhone
            : this.widget.adElements.contact!.mobilePhoneForAd;
  }

  ///inizializzazione delle checkbox
  _initCheckBoxValue() {
    switch (this.widget.adElements.adCategory) {
      case AdCategory.Adoption:
        setState(() {
          isAdoption = true;
          isDisappear = false;
          isFound = false;
        });
        break;
      case AdCategory.Disappear:
        setState(() {
          isAdoption = false;
          isDisappear = true;
          isFound = false;
        });
        break;
      case AdCategory.Found:
        setState(() {
          isAdoption = false;
          isDisappear = false;
          isFound = true;
        });
        break;
      default:
        setState(() {
          isAdoption = true;
          isDisappear = false;
          isFound = false;
        });
        break;
    }
    switch (this.widget.adElements.petGender) {
      case PetGender.MALE:
        setState(() {
          isMale = true;
          isFemale = false;
          isOtherGen = false;
        });
        break;
      case PetGender.FEMALE:
        setState(() {
          isMale = false;
          isFemale = true;
          isOtherGen = false;
        });
        break;
      case PetGender.OTHER:
        setState(() {
          isMale = false;
          isFemale = false;
          isOtherGen = true;
        });
        break;
      default:
        setState(() {
          isMale = true;
          isFemale = false;
          isOtherGen = false;
        });
        break;
    }
    switch (this.widget.adElements.petCategory) {
      case PetCategory.DOG:
        setState(() {
          isDog = true;
          isCat = false;
          isOther = false;
        });
        break;
      case PetCategory.CAT:
        setState(() {
          isDog = false;
          isCat = true;
          isOther = false;
        });
        break;
      case PetCategory.OTHER:
        setState(() {
          isDog = false;
          isCat = false;
          isOther = true;
        });
        break;
      default:
        setState(() {
          isDog = true;
          isCat = false;
          isOther = false;
        });
        break;
    }
    switch (this.widget.adElements.petSize) {
      case PetSize.SMALL:
        setState(() {
          isSmall = true;
          isMedium = false;
          isLarge = false;
        });
        break;
      case PetSize.MEDIUM:
        setState(() {
          isSmall = false;
          isMedium = true;
          isLarge = false;
        });
        break;
      case PetSize.BIG:
        setState(() {
          isSmall = false;
          isMedium = false;
          isLarge = true;
        });
        break;
      default:
        setState(() {
          isSmall = true;
          isMedium = false;
          isLarge = false;
        });
        break;
    }
  }

  @override
  void initState() {
    super.initState();

    ///inizializzazione dei dati del contatto
    _initContact();

    ///inizializzazione dell'uid del creatore dell'annuncio
    this.widget.adElements.contact!.userUid = this.widget.userData.uid;

    ///inizializzazione dei valori delle checkbox
    ///Richiamare questo metodo è indispensabile se si accede tramite la modifica
    _initCheckBoxValue();
  }

  @override
  void dispose() {
    ///Durante la chiusura del widget, sia per la creazione che per l'uscita
    ///In qualsiasi caso vengono svutati i seguente array:
    ///List<XFile> contenete la lista delle immagini caricate durante la sessione utente
    ///this.widget.photoList lista delle immagini che sarebberò salvate nello store
    this.widget.photoList.clear();

    super.dispose();
  }

  ///Questo widget è generato dal metodo generico genericSliversPage()
  ///Pertanto nel metodo build() si alimenta una lista di Widget con tutti gli oggetti da mettere a video
  ///Essendo un form occorre prevedere la validazione dei dati in input.
  ///Tutti i widget che necessitano la validazione espongono un metodo pubblico che restituisce il proprio GlobalKey<FormState>
  ///Quando il tasto di conferma viene premuto occorre validare tutti i GlobalKey.
  ///Essendo che il form è impostato tutto su una pagina scrollabile occorre mostrare un messaggio di errore generico
  ///vicino al tasto 'Pubblica' quando una validazione del GlobalKey non viene superata.
  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];

    ///Titolo
    columnElementList.add(_subTitle());

    ///Sezione Categoria Annuncio
    columnElementList.add(adCategory(
      context: context,
      isAdoption: isAdoption,
      isDisappear: isDisappear,
      isFound: isFound,
      adoptionChecked: (bool? value) {
        setState(() {
          isAdoption = (isAdoption ? true : value)!;
          isDisappear = false;
          isFound = false;
          this.widget.adElements.adCategory = AdCategory.Adoption;
        });
      },
      disappearChecked: (bool? value) {
        setState(() {
          isAdoption = false;
          isDisappear = (isDisappear ? true : value)!;
          isFound = false;
          this.widget.adElements.adCategory = AdCategory.Disappear;
        });
      },
      foundChecked: (bool? value) {
        setState(() {
          isAdoption = false;
          isDisappear = false;
          isFound = (isFound ? true : value)!;
          this.widget.adElements.adCategory = AdCategory.Found;
        });
      },
    ));

    ///Specie dell'Animale
    columnElementList.add(petSpecies(
      context: context,
      isDog: isDog,
      isCat: isCat,
      isOther: isOther,
      dogChecked: (bool? value) {
        setState(() {
          isDog = (isDog ? true : value)!;
          isCat = false;
          isOther = false;
          this.widget.adElements.petCategory = PetCategory.DOG;
        });
      },
      catChecked: (bool? value) {
        setState(() {
          isDog = false;
          isCat = (isCat ? true : value)!;
          isOther = false;
          this.widget.adElements.petCategory = PetCategory.CAT;
        });
      },
      otherChecked: (bool? value) {
        setState(() {
          isDog = false;
          isCat = false;
          isOther = (isOther ? true : value)!;
          this.widget.adElements.petCategory = PetCategory.OTHER;
        });
      },
    ));

    ///Sezione Foto Annuncio
    columnElementList.add(adPhoto(
        context: context,
        adElements: this.widget.adElements,
        photoList: this.widget.photoList,
        updatePhoto: (String value, bool add) {
          setState(() {
            if (add) {
              ///aggiungi una foto dalla lista
              this.widget.photoList.add(value);
              this.widget.adElements.photoNumber = this.widget.photoList.length;
            } else {
              ///rimuovi una foto dalla lista
              this.widget.photoList.remove(value);
              this.widget.adElements.photoNumber = this.widget.photoList.length;
            }
          });
        }));

    ///Sezione Informazioni Annuncio: Nome e località
    columnElementList.add(adInfo(context, this.widget.adElements));

    ///Sezione Genere dell'Animale
    columnElementList.add(petGender(
      context: context,
      isMale: isMale,
      isFemale: isFemale,
      isOtherGen: isOtherGen,
      maleChecked: (bool? value) {
        setState(() {
          isMale = (isMale ? true : value)!;
          isFemale = false;
          isOtherGen = false;
          this.widget.adElements.petGender = PetGender.MALE;
        });
      },
      femaleChecked: (bool? value) {
        setState(() {
          isMale = false;
          isFemale = (isFemale ? true : value)!;
          isOtherGen = false;
          this.widget.adElements.petGender = PetGender.FEMALE;
        });
      },
      otherGenChecked: (bool? value) {
        setState(() {
          isMale = false;
          isFemale = false;
          isOtherGen = (isOtherGen ? true : value)!;
          this.widget.adElements.petGender = PetGender.OTHER;
        });
      },
    ));

    ///Sezione Anni dell'Animale
    columnElementList.add(petAge(
        isEdit: this.widget.isEditAd,
        context: context,
        unknowAge: unknowAge,
        adElements: this.widget.adElements,
        unknowAgeChecked: (bool? value) {
          setState(() {
            unknowAge = value!;
          });
        }));

    ///Sezione Taglia dell'Animale
    columnElementList.add(petSize(
      context: context,
      isSmall: isSmall,
      isMedium: isMedium,
      isLarge: isLarge,
      isSmallChecked: (bool? value) {
        setState(() {
          isSmall = (isSmall ? true : value)!;
          isMedium = false;
          isLarge = false;
          this.widget.adElements.petSize = PetSize.SMALL;
        });
      },
      isMediumChecked: (bool? value) {
        setState(() {
          isSmall = false;
          isMedium = (isMedium ? true : value)!;
          isLarge = false;
          this.widget.adElements.petSize = PetSize.MEDIUM;
        });
      },
      isLargeChecked: (bool? value) {
        setState(() {
          isSmall = false;
          isMedium = false;
          isLarge = (isLarge ? true : value)!;
          this.widget.adElements.petSize = PetSize.BIG;
        });
      },
    ));

    ///Sezione Descrizione Annuncio
    columnElementList.add(
        adDescription(context: context, adElements: this.widget.adElements));
    columnElementList.add(textPagination(
        context, AppLocalizations.of(context)!.descriptionWarning));

    ///Sezione Contatti
    columnElementList
        .add(adContact(context: context, adElements: this.widget.adElements));

    ///Bottone per pubblicare l'annuncio
    columnElementList.add(adPublic(
        context: context,
        adElements: this.widget.adElements,
        userData: this.widget.userData,
        isEditAd: this.widget.isEditAd,
        formErrorMessage: formErrorMessage,
        photoList: this.widget.photoList,
        loaderOn: () {
          setState(() => loading = true);
        },
        loaderOff: () {
          setState(() => loading = false);
        },
        updateErrorMessage: () {
          setState(() {
            formErrorMessage = true;
          });
        }));

    return loading
        ? Loading()
        : genericSliversPage(
            context,
            columnElementList: columnElementList,
            wrapInSingleChildScrollView: true,
          );
  }
}
