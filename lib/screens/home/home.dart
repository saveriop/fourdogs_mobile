import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/main.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/adList/ad_list_pagination.dart';
import 'package:zampon/screens/home/newAds/steps/informative_text.dart';
import 'package:zampon/screens/home/settings/account_stream_provider.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/app_bar_model.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';

import '../../services/firebase/firestone/user_dp.dart';

class Home extends StatefulWidget {
  //Indica il tab di partenza
  final int tabIndex;

  Home({
    required this.tabIndex,
  });

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late ScrollController _scrollViewController;

  bool _isVisible = true;

  //StreamSubscription _connectionChangeStream;
  bool isOffline = false;

  @override
  void initState() {
    initUniLinks().then((value) => this.setState(() {
          MyApp.link = value;
        }));

    super.initState();

    ///se il token JWT dell'utente è scaduto effettua il refresh
    if (AuthService.isExpired) {
      AuthService().refreshJwt();
      AuthService.isExpired = false;
    }

    ///Inizializzazione token JWT
    AuthService().initJwt();

    ///gestione della visibilità del tasto per aggiungere annunci. Sarà visibile solo sui primi due TAB
    ///Inizializzazione del TabController inizializzando la lunghezza degli elementi
    _tabController = TabController(vsync: this, length: 4);
    _tabController.addListener(() {
      setState(() {
        _isVisible = false;
        if (_tabController.index < 2) {
          _isVisible = true;
        }
      });
    });
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _scrollViewController.addListener(() {
      setState(() {
        _isVisible = false;
        if (_tabController.index < 2) {
          _isVisible = true;
        }

        ///Il tasto di creazione annunci sarà visibile se l'utente scrolla verso l'alto
        /// e se si trova in uno dei primi due Tab
        _isVisible = _scrollViewController.position.userScrollDirection ==
                ScrollDirection.forward &&
            _tabController.index < 2;
      });
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollViewController.dispose();

    super.dispose();
  }

  ///Metodo che restituisce
  TabBar _navigationBar() {
    return TabBar(
      controller: _tabController,
      indicatorColor: customStrongLiliac,
      unselectedLabelColor: customStrongLiliac,
      labelColor: customStrongLiliac,
      indicatorPadding:
          EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.003),
      indicator: UnderlineTabIndicator(
          borderSide: BorderSide(
              width: MediaQuery.of(context).size.width * 0.005,
              color: customStrongLiliac),
          insets: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.045,
              right: MediaQuery.of(context).size.width * 0.030)),
      tabs: <Tab>[
        Tab(
          height: MediaQuery.of(context).size.height * 0.065,
          child: IconButton(
            icon: Icon(
              CustomIcons.i1_adoption_icon,
              size: MediaQuery.of(context).size.height * 0.045,
              color: customStrongLiliac,
            ),
            onPressed: () {
              _isVisible = true;
              _tabController.index = 0;
              _scrollViewController
                  .jumpTo(_scrollViewController.position.maxScrollExtent - 150);
            },
          ),
        ),
        Tab(
          child: IconButton(
            icon: Icon(
              CustomIcons.i2_animale_smarrito_trovato,
              size: MediaQuery.of(context).size.height * 0.045,
              color: customStrongLiliac,
            ),
            onPressed: () {
              _isVisible = true;
              _tabController.index = 1;
              _scrollViewController
                  .jumpTo(_scrollViewController.position.maxScrollExtent - 150);
            },
          ),
        ),
        Tab(
          child: IconButton(
            icon: Icon(
              CustomIcons.i37_favorite_icon,
              size: MediaQuery.of(context).size.height * 0.045,
              color: customStrongLiliac,
            ),
            onPressed: () {
              _isVisible = false;
              _tabController.index = 2;
              _scrollViewController
                  .jumpTo(_scrollViewController.position.maxScrollExtent - 150);
            },
          ),
        ),
        Tab(
          child: IconButton(
            icon: Icon(
              CustomIcons.i33_icona_opzioni,
              size: MediaQuery.of(context).size.height * 0.045,
              color: customStrongLiliac,
            ),
            onPressed: () {
              _isVisible = false;
              _tabController.index = 3;
              _scrollViewController
                  .jumpTo(_scrollViewController.position.maxScrollExtent - 150);
            },
          ),
        ),
      ],
    );
  }

  ///  Tasto a scomparsa per la creazione di un annuncio
  Widget _newAdButton(UserData user) {
    return Container(
      child: Offstage(
          offstage: !_isVisible,
          child: ElevatedButton(
            child: Icon(
              CustomIcons.i14_create_advert_icon,
              size: MediaQuery.of(context).size.height * 0.05,
            ),
            style: ButtonStyle(
              side: MaterialStateBorderSide.resolveWith((states) {
                return const BorderSide(color: customWhite);
              }),
              shape: MaterialStateProperty.all(CircleBorder()),
              padding: MaterialStateProperty.all(EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.025,
                  bottom: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.03,
                  right: MediaQuery.of(context).size.width * 0.03)),
              backgroundColor: MaterialStateProperty.all(customStrongLiliac),
            ),
            onPressed: () async {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => InformativeText(
                            user: user,
                          )));
            },
          )),
    );
  }

  ///reperisce l'oggetto UserData completo
  _fetchData(String uid) async {
    ///questo metodo richiamerà un end point per ottenere i contatori
    return await DatabaseServiceUser().findCurrentUser(uid);
  }

  ///Contiene tutti i widget visibili nella home:
  ///-Tab controller
  ///-Card degli annunci
  ///-Tasto crea nuovo annuncio
  Widget _getHomePage(
      UserData user, String? pageNameText, AssetImage? pageNameImg) {
    return DefaultTabController(
        length: 5,
        initialIndex: this.widget.tabIndex,
        child: Scaffold(
          backgroundColor: customWhite,
          body: Stack(children: [
            NestedScrollView(
              controller: _scrollViewController,
              headerSliverBuilder: (context, value) {
                return [
                  sliverAppBar(
                    context, false, pageNameText: pageNameText,
                    pageNameImg: pageNameImg,
                    navigatorBar: _navigationBar(),
                    home: true,
                    //animation: animation
                  )
                ];
              },
              body: Column(children: [
                Expanded(
                  child: TabBarView(
                    controller: _tabController,
                    children: [
                      Column(children: <Widget>[
                        Expanded(
                          child: AdListPagination(
                            adCategory: AdCategory.Adoption,
                            user: user,
                          ),
                        ),
                      ]),
                      Column(children: <Widget>[
                        Expanded(
                          child: AdListPagination(
                            adCategory: AdCategory.Disappear,
                            user: user,
                          ),
                        ),
                      ]),
                      Column(children: <Widget>[
                        Expanded(
                          child: AdListPagination(
                            adCategory: AdCategory.Favorites,
                            user: user,
                          ),
                        ),
                      ]),
                      AccountStreamProvider(userData: user),
                    ],
                  ),
                ),
              ]),
            ),

            ///tasto per creazione annunci
            Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height * 0.04,
                  right: MediaQuery.of(context).size.width * 0.08),
              child: Align(
                  alignment: Alignment.bottomRight, child: _newAdButton(user)),
            ),
          ]),
        ));
  }

  @override
  Widget build(BuildContext context) {
    final UserData? user = Provider.of<UserData?>(context);

    String? pageNameText;
    if (_tabController.index == 0) {
      pageNameText = AppLocalizations.of(context)!.pageNameAdoption;
    } else if (_tabController.index == 1) {
      pageNameText = AppLocalizations.of(context)!.pageNameDisapp;
    } else if (_tabController.index == 2) {
      pageNameText = AppLocalizations.of(context)!.pageNameFavourite;
    } else if (_tabController.index == 3) {
      pageNameText = AppLocalizations.of(context)!.pageNamePersAre;
    }

    AssetImage? pageNameImg;
    if (_tabController.index == 0) {
      pageNameImg = AssetImage('assets/images/title/ZampOnAdozioni.png');
    } else if (_tabController.index == 1) {
      pageNameImg = AssetImage('assets/images/title/ZampOnScomparsi.png');
    } else if (_tabController.index == 2) {
      pageNameImg = AssetImage('assets/images/title/ZampOnPreferiti.png');
    } else if (_tabController.index == 3) {
      pageNameImg = AssetImage('assets/images/title/ZampOnOpzioni.png');
    }

    return FutureBuilder(
        future: _fetchData(user!.uid),
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return Loading();
          } else {
            return _getHomePage(
                snapshot.data as UserData, pageNameText, pageNameImg);
          }
        });

/*
    return (user == null || AuthService.jwt == null)
        ? Loading(isOffline: isOffline)

        ///All'aperture dell'app l'oggetto UserData non è popolato al 100 % ma è presente solo lo uid
        ///di conseguenza è necessario rileggere l'oggetto
        : FutureBuilder(
            future: _fetchData(user.uid),
            builder: (context, snapshot) {
              if (snapshot.data == null) {
                return Loading();
              } else {
                return _getHomePage(
                    snapshot.data as UserData, pageNameText, pageNameImg);
              }
            });
  */
  }
}
