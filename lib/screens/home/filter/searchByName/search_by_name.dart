import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/main.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/services/ws_rest/ads.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

class SearchByName extends StatefulWidget {
  final String uid;

  SearchByName({required this.uid});

  @override
  _SearchByNameState createState() => _SearchByNameState();
}

class _SearchByNameState extends State<SearchByName> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  String error = "";

  static String _info = "La ricerca per Nome avviene effettuata solo su base nazionale, pertanto i risultati non possono essere filtrati tramite regioni, province o comuni. Mentre il filtro 'Ricerca per Nome' è attivo, tutti gli altri filtri di ricerca sono temporaneamente disabilitati.";

  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;

  //Autocomplete per la selezione del comune , Provincia, Regione
  String _displayForOption(String option) => option;

  Widget textFormFieldForInput(
    BuildContext context,
    List<String> autocompleteList, {
    bool noIcon = false,
    String? hintText,
    String? labelText,
    Widget? icon,
  }) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.00029),
      child: Autocomplete<String>(
        initialValue: TextEditingValue(text: MyApp.selectedName),
        optionsBuilder: (TextEditingValue textEditingValue) {
          if (textEditingValue.text == '') {
            return const Iterable<String>.empty();
          }
          return autocompleteList.where((element) => element
              .toLowerCase()
              .startsWith(textEditingValue.text.toLowerCase()));
        },
        displayStringForOption: _displayForOption,
        fieldViewBuilder: (BuildContext context,
            TextEditingController fieldTextEditingController,
            FocusNode fieldFocusNode,
            VoidCallback onFieldSubmitted) {
          return TextFormField(
            validator: (val) {
              return validateNameInput(val!, autocompleteList);
            },
            controller: fieldTextEditingController,
            focusNode: fieldFocusNode,
            style: TextStyle(color: customStrongLiliac),
            decoration: noIcon
                ? textFormFieldCustomDecorationNoIcon(
                    context,
                    hintText: hintText!,
                    labelText: labelText!,
                  )
                : textFormFieldCustomDecoration(context,
                    hintText: hintText, labelText: labelText, icon: icon),
          );
        },
        onSelected: (String selection) {
          MyApp.selectedName = selection;
        },
      ),
    );
  }

  ///Titolo della pagina
  Widget _title(String text) {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
        child: subTitle(
          context,
          text,
          icon: Icon(CustomIcons.ricerca_per_nome, color: customWhite),
          thereIsIcon: true,
        ));
  }

  Widget _divider() {
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
      child: largeDivider(context),
    );
  }

  //messaggio di errore
  Widget _loginErrorMessage() {
    return Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.02,
        ),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.65,
          child: Text(
            error,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.045,
                color: Colors.red,
                fontWeight: FontWeight.bold),
          ),
        ));
  }

  Widget _searchForm() {
    return Form(
        key: _formkey,
        child: Padding(
            padding: EdgeInsets.only(),
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.024,
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.1,
                    top: MediaQuery.of(context).size.width * 0.02,
                    bottom: MediaQuery.of(context).size.width * 0.002,
                  ),
                  height: MediaQuery.of(context).size.height * 0.11,
                  child: textFormFieldForInput(
                      context, MyApp.allPetNames /*this.widget.citiesList*/,
                      icon: Icon(
                        CustomIcons.i27_pet_name_icon,
                        color: customWhite,
                      ),
                      labelText:
                          AppLocalizations.of(context)!.searchByNameLabelName),
                ),
                textPagination(
                    context, AppLocalizations.of(context)!.searchByNameSubtitle),
              ],
            )));
  }

  ///reperisco la lista dei nomi di tutti gli annunci che serviranno ad effettuare la ricerca per nome
  _fetchData() async {
    if(MyApp.allPetNames.isEmpty){
      MyApp.allPetNames = await Ads().getAllPetName();
    }
    return MyApp.allPetNames;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_title(AppLocalizations.of(context)!.searchByNameTitle));
    columnElementList.add(_divider());
    columnElementList.add(_searchForm());
    columnElementList.add(
      SizedBox(height: MediaQuery.of(context).size.height * 0.024,)
      );
    columnElementList.add(_divider());
    columnElementList.add(
      SizedBox(height: MediaQuery.of(context).size.height * 0.024,)
      );
    columnElementList.add(
      confirmationButton(
        context, 
        AppLocalizations.of(context)!.confirm,
         () async {
      if (_formkey.currentState!.validate()) {
        setState(() => loading = true);
                    await DefaultCacheManager().emptyCache();
            new DatabaseServiceUser(uid: this.widget.uid).updateField(
                DatabaseServiceUser.LOCATION_FILTER_LABEL,
                ITALY);
        Navigator.of(context)
            .pushNamedAndRemoveUntil(
                '/wrapper', (Route<dynamic> route) => false)
            .then((value) {
          setState(() => loading = false);
          
        });
      } else {
        setState(() {
          error = AppLocalizations.of(context)!.searchErrorMessage;
          loading = false;
        });
      }
    }));
    columnElementList.add(_loginErrorMessage());
    columnElementList.add(
      SizedBox(height: MediaQuery.of(context).size.height * 0.0004,)
      );

      if(MyApp.selectedName.isNotEmpty){
        columnElementList.add(
      confirmationButton(
        context,
        AppLocalizations.of(context)!.searchRemuveNameLabel,
        (){
          setState(() {
            loading = true;
            MyApp.selectedName = "";
          });
        Navigator.of(context)
            .pushNamedAndRemoveUntil(
                '/wrapper', (Route<dynamic> route) => false)
            .then((value) {
          setState(() => loading = false);
        }
      );})
    );
    
      }
    columnElementList.add(warningTextPagination(context, _info),);

    return FutureBuilder(
        future: _fetchData(),
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return Loading();
          } else {
            return genericSliversPage(
              context,
              columnElementList: columnElementList,
              wrapInSingleChildScrollView: true,
            );
          }
        });
  }
}
