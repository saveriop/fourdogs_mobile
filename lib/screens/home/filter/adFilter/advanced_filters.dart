import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/main.dart';
import 'package:zampon/screens/home/newAds/steps/singleStep/pet_gender.dart';
import 'package:zampon/screens/home/newAds/steps/singleStep/pet_size.dart';
import 'package:zampon/screens/home/newAds/steps/singleStep/pet_species.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

class AdvancedFilters extends StatefulWidget {
  @override
  _AdvancedFiltersState createState() => _AdvancedFiltersState();
}

class _AdvancedFiltersState extends State<AdvancedFilters> {
  /// Variabile che gestisce il loading.
  /// Quando l'utente clicca bottoni asincroni, per far si che non vengano innescati più volte
  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;

  bool isFirstTime = true;

  ///Bool per la specie
  bool isDog = MyApp.filter.filterSpecies.isDog;
  bool isCat = MyApp.filter.filterSpecies.isCat;
  bool isOther = MyApp.filter.filterSpecies.isOther;

  ///Bool per il genere
  bool isMale = MyApp.filter.filterGender.isMale;
  bool isFemale = MyApp.filter.filterGender.isFemale;
  bool isOtherGen = MyApp.filter.filterGender.isOtherGen;

  ///Bool per l'età
  bool unknowAge = true;

  ///Bool per la taglia
  bool isSmall = MyApp.filter.filterSize.isSmall;
  bool isMedium = MyApp.filter.filterSize.isMedium;
  bool isLarge = MyApp.filter.filterSize.isLarge;

  ///Sottotitolo della pagina
  Widget _subTitle() {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        child: subTitle(
          context,
          AppLocalizations.of(context)!.applySearchFilters,
          icon: Icon(
            CustomIcons.i19_filter_icon,
            color: customWhite,
          ),
          thereIsIcon: true,
        ));
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];

    ///Solo se sono stati attivati i filtri apparirà il tasto di rimozione filtri
    if (MyApp.filter.isFilterApplied()) {
      columnElementList
          .add(SizedBox(height: MediaQuery.of(context).size.height * 0.03));
      columnElementList.add(confirmationButton(
          context, AppLocalizations.of(context)!.removeFilters, () async {
        setState(() => loading = true);
        //svuoto la cache
        await DefaultCacheManager().emptyCache();

        ///resetto tutti i filtri
        MyApp.filter.filterGender.isFemale = false;
        MyApp.filter.filterGender.isMale = false;
        MyApp.filter.filterGender.isOtherGen = false;
        MyApp.filter.filterSpecies.isDog = false;
        MyApp.filter.filterSpecies.isCat = false;
        MyApp.filter.filterSpecies.isOther = false;
        MyApp.filter.filterSize.isSmall = false;
        MyApp.filter.filterSize.isMedium = false;
        MyApp.filter.filterSize.isLarge = false;
        Navigator.of(context)
            .pushNamedAndRemoveUntil(
                '/wrapper', (Route<dynamic> route) => false)
            .then((value) {
          setState(() => loading = false);
        });
      }));
    }
    columnElementList.add(_subTitle());
    columnElementList.add(petSpecies(
      context: context,
      isDog: isDog,
      isCat: isCat,
      isOther: isOther,
      dogChecked: (bool? value) {
        setState(() {
          isDog = value!;
        });
      },
      catChecked: (bool? value) {
        setState(() {
          isCat = value!;
        });
      },
      otherChecked: (bool? value) {
        setState(() {
          isOther = value!;
        });
      },
    ));
    columnElementList.add(petGender(
      context: context,
      isMale: isMale,
      isFemale: isFemale,
      isOtherGen: isOtherGen,
      maleChecked: (bool? value) {
        setState(() {
          isMale = value!;
        });
      },
      femaleChecked: (bool? value) {
        setState(() {
          isFemale = value!;
        });
      },
      otherGenChecked: (bool? value) {
        setState(() {
          isOtherGen = value!;
        });
      },
    ));
    columnElementList.add(petSize(
      context: context,
      isSmall: isSmall,
      isMedium: isMedium,
      isLarge: isLarge,
      isSmallChecked: (bool? value) {
        setState(() {
          isSmall = value!;
        });
      },
      isMediumChecked: (bool? value) {
        setState(() {
          isMedium = value!;
        });
      },
      isLargeChecked: (bool? value) {
        setState(() {
          isLarge = value!;
        });
      },
    ));

    columnElementList.add(confirmationButton(
        context, AppLocalizations.of(context)!.confirm, () async {
      setState(() => loading = true);
      //svuoto la cache
      await DefaultCacheManager().emptyCache();
      isFirstTime = false;

      ///aggiorno filtri categoria
      MyApp.filter.filterGender.isFemale = isFemale;
      MyApp.filter.filterGender.isMale = isMale;
      MyApp.filter.filterGender.isOtherGen = isOtherGen;

      ///aggiorno filtri specie
      MyApp.filter.filterSpecies.isDog = isDog;
      MyApp.filter.filterSpecies.isCat = isCat;
      MyApp.filter.filterSpecies.isOther = isOther;

      ///aggiorno filtri taglia
      MyApp.filter.filterSize.isSmall = isSmall;
      MyApp.filter.filterSize.isMedium = isMedium;
      MyApp.filter.filterSize.isLarge = isLarge;
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/wrapper', (Route<dynamic> route) => false)
          .then((value) {
        setState(() => loading = false);
      });
    }));

    return genericSliversPage(
      context,
      columnElementList: columnElementList,
      wrapInSingleChildScrollView: true,
    );
  }
}
