import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/location/province.dart';
import 'package:zampon/model/location/region.dart';
import 'package:zampon/screens/home/filter/location/Location_province_filter.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/services/ws_rest/location.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

class LocationFilters extends StatefulWidget {
  final List<Region> regionList;
  final String uid;

  LocationFilters({required this.regionList, required this.uid});

  @override
  _LocationFiltersState createState() => _LocationFiltersState();
}

class _LocationFiltersState extends State<LocationFilters> {
  /// Variabile che gestisce il loading.
  /// Quando l'utente clicca bottoni asincroni, per far si che non vengano innescati più volte
  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;
  bool isEnabled = false;

  ///Titolo della pagina
  Widget _title() {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
        child: subTitle(
          context,
          AppLocalizations.of(context)!.researchArea,
          icon: Icon(CustomIcons.i6_city_icon, color: customWhite),
          thereIsIcon: true,
        ));
  }

  Widget _divider() {
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
      child: largeDivider(context),
    );
  }

  Widget _buildRegion(BuildContext context, Region region) {
    isEnabled =
        region.adoption! + region.disappear! + region.found! > 0 ? true : false;
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.001),
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        borderOnForeground: false,
        child: ListTile(
          tileColor: Colors.transparent,
          contentPadding: EdgeInsets.only(),
          visualDensity: VisualDensity(
              horizontal: 0,
              vertical: -MediaQuery.of(context).size.height * 0.0001),
          dense: true,
          enabled: isEnabled,
          leading: Padding(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.03,
              right: MediaQuery.of(context).size.width * 0.03,
            ),
            child: Icon(
              CustomIcons.i7_region_icon,
              color: isEnabled ? customWhite : Colors.grey[400],
            ),
          ),
          title: Text(
              region.name! +
                  " (" +
                  (region.adoption! + region.disappear! + region.found!)
                      .toString() +
                  ")",
              style: TextStyle(
                  color: isEnabled ? customWhite : Colors.grey[400],
                  fontSize: MediaQuery.of(context).size.height * 0.025,
                  fontWeight: FontWeight.bold)),
          subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                Text(
                  AppLocalizations.of(context)!.adoptions +
                      region.adoption.toString() +
                      AppLocalizations.of(context)!.disappears +
                      region.disappear.toString() +
                      AppLocalizations.of(context)!.foundLabel +
                      region.found.toString(),
                  style: TextStyle(
                      wordSpacing: MediaQuery.of(context).size.height * 0.005,
                      color: isEnabled ? customWhite : Colors.grey[400],
                      fontWeight: FontWeight.bold),
                ),
                thinDivider(context)
              ]),
          onTap: () async {
            if(ITALY == region.name){
                          setState(() => loading = true);
            await DefaultCacheManager().emptyCache();
            new DatabaseServiceUser(uid: this.widget.uid).updateField(
                DatabaseServiceUser.LOCATION_FILTER_LABEL,
                region.name);
            Navigator.of(context)
                .pushNamedAndRemoveUntil(
                    '/wrapper', (Route<dynamic> route) => false)
                .then((value) {
              setState(() => loading = false);
            });
            } else {
            setState(() => loading = true);
            List<Province> provinceList =
                await Locations().getProvincesByRegion(region.name!);
            Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LocationProvinceFilter(
                            region: region,
                            provinceList: provinceList,
                            uid: this.widget.uid)))
                .then((value) => setState(() => loading = false));
            }
          },
        ),
      ),
    );
  }

  ///Costruisce un oggetto di tipo Region ma con i valori di tutta ITALIA
  /// per permettere all'utente di effettuare la ricerca per tutta ITALIA
  Region _getItalyValue(){
    Region italy = Region(name: ITALY, adoption: 0, disappear: 0, found: 0);
    this.widget.regionList.forEach((element) {
        italy.adoption = italy.adoption! + element.adoption!;
        italy.disappear = italy.disappear! + element.disappear!;
        italy.found = italy.found! + element.found!;
    });
    return italy;
  }

  @override
  Widget build(BuildContext context) {
    ///ordina le regioni in base al numero di annunci
    this.widget.regionList.sort((b, a) =>
        (a.adoption! + a.disappear!).compareTo(b.adoption! + b.disappear!));
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_title());
    columnElementList.add(_divider());
    columnElementList.add(subTitle(
      context,
      AppLocalizations.of(context)!.italyResearchArea,
      icon: Icon(CustomIcons.i6_city_icon, color: customWhite),
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_buildRegion(context, _getItalyValue()));
    columnElementList.add(_divider());
    columnElementList.add(subTitle(
      context,
      AppLocalizations.of(context)!.researchRegionArea,
      icon: Icon(CustomIcons.i6_city_icon, color: customWhite),
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));

    ///Per ogni regione viene realizzata una riga
    this.widget.regionList.forEach((element) {
      columnElementList.add(_buildRegion(context, element));
    });
    return loading
        ? Loading()
        : genericSliversPage(
            context,
            columnElementList: columnElementList,
            wrapInSingleChildScrollView: true,
          );
  }
}
