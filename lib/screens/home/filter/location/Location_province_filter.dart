import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/location/province.dart';
import 'package:zampon/model/location/region.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/services/ws_rest/location.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

import 'location_city_filter.dart';

class LocationProvinceFilter extends StatefulWidget {
  final Region region;
  final List<Province> provinceList;
  final String uid;

  LocationProvinceFilter(
      {required this.region, required this.provinceList, required this.uid});

  @override
  _LocationProvinceFilterState createState() => _LocationProvinceFilterState();
}

class _LocationProvinceFilterState extends State<LocationProvinceFilter> {
  /// Variabile che gestisce il loading.
  /// Quando l'utente clicca bottoni asincroni, per far si che non vengano innescati più volte
  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;
  bool isEnabled = false;

  ///Titolo della pagina
  Widget _title(String text) {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
        child: subTitle(
          context,
          text,
          icon: Icon(CustomIcons.i6_city_icon, color: customWhite),
          thereIsIcon: true,
        ));
  }

  Widget _divider() {
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
      child: largeDivider(context),
    );
  }

  ///Costruisce la riga dedicata alla regione
  Widget _buildRegion(BuildContext context, Region region) {
    isEnabled =
        region.adoption! + region.disappear! + region.found! > 0 ? true : false;
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.003),
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        borderOnForeground: false,
        child: ListTile(
          tileColor: Colors.transparent,
          contentPadding: EdgeInsets.only(),
          visualDensity: VisualDensity(
              horizontal: 0,
              vertical: -MediaQuery.of(context).size.height * 0.001),
          dense: true,
          enabled: isEnabled,
          leading: Padding(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.03,
              right: MediaQuery.of(context).size.width * 0.03,
            ),
            child: Icon(
              CustomIcons.i7_region_icon,
              color: isEnabled ? customWhite : Colors.grey[400],
            ),
          ),
          title: Text(
              region.name! +
                  " (" +
                  (region.adoption! + region.disappear! + region.found!)
                      .toString() +
                  ")",
              style: TextStyle(
                  color: isEnabled ? customWhite : Colors.grey[400],
                  fontSize: MediaQuery.of(context).size.height * 0.025,
                  fontWeight: FontWeight.bold)),
          subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                Text(
                  AppLocalizations.of(context)!.adoptions +
                      region.adoption.toString() +
                      AppLocalizations.of(context)!.disappears +
                      region.disappear.toString() +
                      AppLocalizations.of(context)!.foundLabel +
                      region.found.toString(),
                  style: TextStyle(
                      wordSpacing: MediaQuery.of(context).size.height * 0.005,
                      color: isEnabled ? customWhite : Colors.grey[400],
                      fontWeight: FontWeight.bold),
                ),
                thinDivider(context)
              ]),
          onTap: () async {
            setState(() => loading = true);
            await DefaultCacheManager().emptyCache();
            new DatabaseServiceUser(uid: this.widget.uid).updateField(
                DatabaseServiceUser.LOCATION_FILTER_LABEL,
                this.widget.region.name);
            Navigator.of(context)
                .pushNamedAndRemoveUntil(
                    '/wrapper', (Route<dynamic> route) => false)
                .then((value) {
              setState(() => loading = false);
            });
          },
        ),
      ),
    );
  }

  ///Costruisce la riga dedicata alla provincia
  Widget _buildProvince(BuildContext context, Province province) {
    isEnabled = province.adoption! + province.disappear! + province.found! > 0
        ? true
        : false;
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.003),
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        borderOnForeground: false,
        child: ListTile(
          tileColor: Colors.transparent,
          contentPadding: EdgeInsets.only(),
          visualDensity: VisualDensity(
              horizontal: 0,
              vertical: MediaQuery.of(context).size.height * 0.001),
          dense: true,
          enabled: isEnabled,
          leading: Padding(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.03,
              right: MediaQuery.of(context).size.width * 0.03,
            ),
            child: Text(
              province.province!,
              style: TextStyle(
                  color: isEnabled ? customWhite : Colors.grey[400],
                  fontSize: MediaQuery.of(context).size.height * 0.025,
                  fontWeight: FontWeight.bold),
            ),
          ),
          title: Text(
              province.name! +
                  " (" +
                  (province.adoption! + province.disappear! + province.found!)
                      .toString() +
                  ")",
              style: TextStyle(
                  color: isEnabled ? customWhite : Colors.grey[400],
                  fontSize: MediaQuery.of(context).size.height * 0.025,
                  fontWeight: FontWeight.bold)),
          subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                Text(
                  AppLocalizations.of(context)!.adoptions +
                      province.adoption.toString() +
                      AppLocalizations.of(context)!.disappears +
                      province.disappear.toString() +
                      AppLocalizations.of(context)!.foundLabel +
                      province.found.toString(),
                  style: TextStyle(
                      wordSpacing: MediaQuery.of(context).size.height * 0.005,
                      color: isEnabled ? customWhite : Colors.grey[400],
                      fontWeight: FontWeight.bold),
                ),
                thinDivider(context)
              ]),
          onTap: () async {
            setState(() => loading = true);
            await DefaultCacheManager().emptyCache();
            new DatabaseServiceUser(uid: this.widget.uid).updateField(
                DatabaseServiceUser.LOCATION_FILTER_LABEL,
                this.widget.region.name! + "(" + province.province! + ")");
            Navigator.of(context)
                .pushNamedAndRemoveUntil(
                    '/wrapper', (Route<dynamic> route) => false)
                .then((value) {
              setState(() => loading = false);
            });
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    ///ordina le regioni in base al numero di annunci
    this.widget.provinceList.sort((b, a) =>
        (a.adoption! + a.disappear!).compareTo(b.adoption! + b.disappear!));
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_title(
      AppLocalizations.of(context)!.researchArea,
    ));
    //--------------------------------------------------
    columnElementList.add(_divider());
    columnElementList.add(subTitle(
      context,
      AppLocalizations.of(context)!.searchForRegion,
      icon: Icon(CustomIcons.i6_city_icon, color: customWhite),
    ));
    columnElementList.add(_buildRegion(context, this.widget.region));
    //--------------------------------------------------
    columnElementList.add(_divider());
    columnElementList.add(subTitle(
      context,
      AppLocalizations.of(context)!.searchForCity,
      icon: Icon(CustomIcons.i6_city_icon, color: customWhite),
    ));
    columnElementList.add(SizedBox(height: MediaQuery.of(context).size.height * 0.016,));
    columnElementList.add(
      confirmationButton(context,
       "Ricerca",
       () async {
          setState(() => loading = true);
         List<String> citiesList =
                await Locations().getCitiesNameByRegion(this.widget.region.name!);
            Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LocationCityFilter(
                            citiesList: citiesList,
                            region: this.widget.region.name!,
                            uid: this.widget.uid)))
                .then((value) => setState(() => loading = false));
       })
      );
    //--------------------------------------------------
    columnElementList.add(_divider());
    columnElementList.add(subTitle(
      context,
      AppLocalizations.of(context)!.searchForProvince,
      icon: Icon(CustomIcons.i6_city_icon, color: customWhite),
    ));
    this.widget.provinceList.forEach((element) {
      columnElementList.add(_buildProvince(context, element));
    });

    return genericSliversPage(
      context,
      columnElementList: columnElementList,
      wrapInSingleChildScrollView: true,
    );
  }
}
