import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

class LocationCityFilter extends StatefulWidget {
  final List<String> citiesList;
  final String region;
  final String uid;

  LocationCityFilter(
      {required this.citiesList, required this.region, required this.uid});

  @override
  _LocationCityFilterState createState() => _LocationCityFilterState();
}

class _LocationCityFilterState extends State<LocationCityFilter> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  String city = "";
  String error = "";

  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;

  //Autocomplete per la selezione del comune , Provincia, Regione
  String _displayForOption(String option) => option;

  Widget textFormFieldForInputCity(
    BuildContext context,
    List<String> autocompleteList, {
    bool noIcon = false,
    String? hintText,
    String? labelText,
    Widget? icon,
  }) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.00029),
      child: Autocomplete<String>(
        initialValue: TextEditingValue(text: city),
        optionsBuilder: (TextEditingValue textEditingValue) {
          if (textEditingValue.text == '') {
            return const Iterable<String>.empty();
          }
          return autocompleteList.where((element) => element
              .toLowerCase()
              .startsWith(textEditingValue.text.toLowerCase()));
        },
        displayStringForOption: _displayForOption,
        fieldViewBuilder: (BuildContext context,
            TextEditingController fieldTextEditingController,
            FocusNode fieldFocusNode,
            VoidCallback onFieldSubmitted) {
          //fieldTextEditingController = TextEditingController(text: Location.initialValue(this.widget.adElements.adLocation));
          return TextFormField(
            //initialValue: Location.initialValue(this.widget.adElements.adLocation),
            validator: (val) {
              return validateLocationInput(val!, autocompleteList);
            },
            controller: fieldTextEditingController,
            focusNode: fieldFocusNode,
            style: TextStyle(color: customStrongLiliac),
            decoration: noIcon
                ? textFormFieldCustomDecorationNoIcon(
                    context,
                    hintText: hintText!,
                    labelText: labelText!,
                  )
                : textFormFieldCustomDecoration(context,
                    hintText: hintText, labelText: labelText, icon: icon),
          );
        },
        onSelected: (String selection) {
          city = selection;
        },
      ),
    );
  }

  ///Titolo della pagina
  Widget _title(String text) {
    return Padding(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
        child: subTitle(
          context,
          text,
          icon: Icon(CustomIcons.i6_city_icon, color: customWhite),
          thereIsIcon: true,
        ));
  }

  //messaggio di errore
  Widget _loginErrorMessage() {
    return Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.02,
        ),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.65,
          child: Text(
            error,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.045,
                color: Colors.red,
                fontWeight: FontWeight.bold),
          ),
        ));
  }

  Widget _searchForm() {
    return Form(
        key: _formkey,
        child: Padding(
            padding: EdgeInsets.only(),
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.04,
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.1,
                    top: MediaQuery.of(context).size.width * 0.02,
                    bottom: MediaQuery.of(context).size.width * 0.02,
                  ),
                  height: MediaQuery.of(context).size.height * 0.11,
                  child:
                      textFormFieldForInputCity(context, this.widget.citiesList,
                          icon: Icon(
                            CustomIcons.i26_pet_location,
                            color: customWhite,
                          ),
                          labelText: AppLocalizations.of(context)!.insertPlace),
                ),
                textPagination(context,
                    "*Saranno visualizzati nei suggerimenti solo \n i comuni  possedenti almeno un annuncio."),
              ],
            )));
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_title(
      AppLocalizations.of(context)!.researchArea + this.widget.region,
    ));
    columnElementList.add(divider(context));
    columnElementList.add(subTitle(
      context,
      AppLocalizations.of(context)!.searchForCity,
      icon: Icon(CustomIcons.i6_city_icon, color: customWhite),
    ));
    columnElementList.add(_searchForm());
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.08,
    ));
    columnElementList.add(confirmationButton(
        context, AppLocalizations.of(context)!.confirm, () async {
      if (_formkey.currentState!.validate()) {
        setState(() => loading = true);
        await DefaultCacheManager().emptyCache();
            new DatabaseServiceUser(uid: this.widget.uid).updateField(
                DatabaseServiceUser.LOCATION_FILTER_LABEL,
                "[" + city +"]");

        Navigator.of(context)
            .pushNamedAndRemoveUntil(
                '/wrapper', (Route<dynamic> route) => false)
            .then((value) {
          setState(() => loading = false);
        });
      } else {
        setState(() {
          error = "Ricerca non eseguibile";
          loading = false;
        });
      }
    }));
    columnElementList.add(_loginErrorMessage());

    return genericSliversPage(
      context,
      columnElementList: columnElementList,
      wrapInSingleChildScrollView: true,
    );
  }
}
