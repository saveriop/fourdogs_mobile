import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/main.dart';
import 'package:zampon/model/location/region.dart';
import 'package:zampon/screens/home/filter/location/location_filters.dart';
import 'package:zampon/screens/home/filter/searchByName/search_by_name.dart';
import 'package:zampon/services/ws_rest/location.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/screens/home/adList/ad_card.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/screens/home/filter/adFilter/advanced_filters.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/services/ws_rest/ads.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';

class AdListPagination extends StatefulWidget {
  final AdCategory adCategory;
  final UserData user;

  AdListPagination({required this.adCategory, required this.user});

  @override
  _AdListPaginationState createState() => _AdListPaginationState();
}

class _AdListPaginationState extends State<AdListPagination> {

 double _getfilterHeight(context){
    return MediaQuery.of(context).size.height * 0.050;
 }

  double _getfilterWidth(context){
    return MediaQuery.of(context).size.width * 0.352;
 }

   double _getfilterWidthSearchName(context){
    return MediaQuery.of(context).size.width * 0.442;
 }


  ///indice regione di ricerca. Valore default PUGLIA
  String? region;

  bool filterBarStatus = false;

  ///indice provincia di ricerca. Valore default ""
  String? province = "";

  ///indice di pagina. Valore default 0
  int pageIndex = 0;

  ///Lista dei favoriti
  List<String> favoriteList = [];

  ///dimensione di ogni pagina. Valore default 10
  int pageSize = 10;
  List<Future<List<AdElements>>> futures = [];
  List<AdElements> listAds = [];
  //Stream<List<AdElementsNew>> _futuresStream;
  StreamController<List<AdElements>> _futuresStream = new BehaviorSubject();

  ///controller del refresh widget
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  /// Variabile che gestisce il loading.
  /// Quando l'utente clicca bottoni asincroni, per far si che non vengano innescati più volte
  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;

  ///Metodo invocato quando sono stato scrollati tutti gli elementi a video.
  ///Richiede dei nuovi elementi paginati dal Back-End andando ad alimentare la lista inizializzata nel initState
  ///Ad ogni chiamata verifica se gli elementi della lista sono aumentati e nel caso incrementa l'indice di paginazione
  void _onLoading() async {
    int length = listAds.length;

    ///richiedo una nuova pagina tramite una chiamata http
    if (MyApp.selectedName.isNotEmpty &&
        !(this.widget.adCategory == AdCategory.Favorites)) {
      //DO NOTHING
      //futures.add( new Ads().findByName(MyApp.selectedName));
    } else if (MyApp.filter.isFilterApplied() &&
        !(this.widget.adCategory == AdCategory.Favorites)) {
      listAds = (await new Ads().nextPagePageAdvanceFilter(
          province: province,
          region: region,
          pageIndex: pageIndex,
          pageSize: pageSize,
          adCategory: this.widget.adCategory.toString().split('.').last,
          listAds: listAds,
          city: (region != null && region!.startsWith('[')) ? region : null))!;
    } else if (region != null && region!.startsWith('[')) {
      listAds = (await new Ads().nextPageAdsCity(
          city: region,
          pageIndex: pageIndex,
          pageSize: pageSize,
          favoriteList: favoriteList,
          adCategory: this.widget.adCategory.toString().split('.').last,
          listAds: listAds))!;
    } else {
      listAds = (await new Ads().nextPageAds(
          province: province,
          region: region,
          pageIndex: pageIndex,
          pageSize: pageSize,
          favoriteList: favoriteList,
          adCategory: this.widget.adCategory.toString().split('.').last,
          listAds: listAds))!;
    }

    ///se la lunghezza non è variata significa che non ci sono altri nuovi annunci
    ///pertanto non occorre incrementare il contatore della paginazione
    if (length < listAds.length) {
      pageIndex++;
    }

    /// keep time to reload widget
    await Future.delayed(Duration(milliseconds: 350));

    /// if failed,use loadFailed(),if no data return,use LoadNodata()
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  ///Vengono alimentati i campi Regioni e Provincia
  ///Questi valori saranno utilizzati per reperire gli annunci in base alla località richiesta
  _getLocation() {
    ///In base all'utente vengono calcolati i parametri di locazione: regione e provincia
    String? locationFilterByRegion = this.widget.user.locationFilter;
    String? locationFilterByProvince;

    //verifico se applicare il filtro per provincia
    if (locationFilterByRegion!.indexOf("(") > -1) {
      locationFilterByRegion = locationFilterByRegion.substring(
          0, locationFilterByRegion.indexOf("("));
      locationFilterByProvince = this.widget.user.locationFilter!.substring(
          this.widget.user.locationFilter!.indexOf("(") + 1,
          this.widget.user.locationFilter!.indexOf("(") + 3);
    }
    region = locationFilterByRegion;
    province = locationFilterByProvince;
  }

  ///Popola la lista degli annunci salvati come favoriti
  _getFavorites() {
    favoriteList = this.widget.user.favoriteAds ?? [];
  }

  ///All'init del Widget viene effettuata la chiamata HTTP verso il Back-end per ottenere i primi elementi paginati
  ///Dal oggetto Future prodotto viene generato lo Stream fondamentale per il funzionamento dello  StreamBuilder
  @override
  void initState() {
    super.initState();

    ///popolamento regione e provincia
    _getLocation();

    ///popolamento lista favoriti
    _getFavorites();

    ///Alimentazione dell'oggetto Future contenente la lista degli annunci
    ///Questi annunci sono il risultato della prima chiamata HTTP
    ///Se sono applicati i filtri e non si vuole visualizzare i favoriti viene richiamato il metodo initPageAdvanceFilter
    ///Se attiva la ricerca per nome viene richiamato il metodo findByName
    if (MyApp.selectedName.isNotEmpty &&
        !(this.widget.adCategory == AdCategory.Favorites)) {
      futures.add(new Ads().findByName(MyApp.selectedName));
    } else if (MyApp.filter.isFilterApplied() &&
        !(this.widget.adCategory == AdCategory.Favorites)) {
      futures.add(new Ads().initPageAdvanceFilter(
          province: province,
          region: region,
          pageIndex: pageIndex,
          pageSize: pageSize,
          adCategory: this.widget.adCategory.toString().split('.').last,
          city: (region != null && region!.startsWith('[')) ? region : null),);
    } else if (region != null && region!.startsWith('[')) {
      futures.add(new Ads().initPageAdsCity(
          city: region,
          pageIndex: pageIndex,
          pageSize: pageSize,
          favoriteList: favoriteList,
          adCategory: this.widget.adCategory.toString().split('.').last));
    } else {
      futures.add(new Ads().initPageAds(
          province: province,
          region: region,
          pageIndex: pageIndex,
          pageSize: pageSize,
          favoriteList: favoriteList,
          adCategory: this.widget.adCategory.toString().split('.').last));
    }

    ///Popola per la prima volta la lista degli elementi da visualizzare
    futures.forEach((element) {
      element.then((value) {
        listAds = value;

        ///valido solo per la ricerca per nome
        if (MyApp.selectedName.isNotEmpty &&
            !(this.widget.adCategory == AdCategory.Favorites)) {
          if (this.widget.adCategory == AdCategory.Adoption) {
            listAds.removeWhere(
                (elem) => elem.adCategory != this.widget.adCategory);
          } else {
            listAds
                .removeWhere((elem) => elem.adCategory == AdCategory.Adoption);
          }
        }
      });
    });

    ///incremento la l'indicatore delle pagine
    pageIndex++;

    ///Genera lo stream basato sugli elementi ricevuti in output dal Back-end
    ///Sulla base di questo Stream lavorerà lo StreamBuilder definito nel metodo Build
    _futuresStream.addStream(Stream<List<AdElements>>.fromFutures(futures));
    //_futuresStream = Stream<List<AdElementsNew>>.fromFutures(futures);
  }

  Widget _searchByNameFilter() {
    return Container(
      height: _getfilterHeight(context),
      width:  _getfilterWidthSearchName(context),
      decoration: BoxDecoration(
        color: customWhite,
        borderRadius: heavyBorderRadius(context),
      ),
      child: TextButton.icon(
        icon: Icon(
          MyApp.selectedName.isEmpty
          ? CustomIcons.ricerca_per_nome
          : CustomIcons.elimina_nome,
          color: customStrongLiliac,
        ),
        label: Text(
          MyApp.selectedName.isEmpty
              ? AppLocalizations.of(context)!.searchByName
              : AppLocalizations.of(context)!.searchByNameRemuve,
          style: TextStyle(
              //fontWeight: FontWeight.bold,
              fontSize: MediaQuery.of(context).size.height * 0.018,
              overflow: TextOverflow.ellipsis,
              color: customStrongLiliac),
        ),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      SearchByName(uid: this.widget.user.uid)));
          setState(() {});
        },
      ),
    );
  }

  Widget _advanceFilter() {
    return Container(
     // height: MediaQuery.of(context).size.height * 0.055,
      height: _getfilterHeight(context),
      width:  _getfilterWidth(context),
      decoration: BoxDecoration(
        color: customWhite,
        borderRadius: heavyBorderRadius(context),
      ),
      child: TextButton.icon(
        icon: MyApp.filter.isFilterApplied()
            ? Icon(
                CustomIcons.i18_cancel_filter_icon,
                color: customStrongLiliac,
              )
            : Icon(
                CustomIcons.i19_filter_icon,
                color: customStrongLiliac,
              ),
        label: Text(
          MyApp.filter.isFilterApplied()
              ? AppLocalizations.of(context)!.remuveAdvancedSearch
              : AppLocalizations.of(context)!.advancedSearch,
          style: TextStyle(
              //fontWeight: FontWeight.bold,
              fontSize: MediaQuery.of(context).size.height * 0.018,
              overflow: TextOverflow.ellipsis,
              color: customStrongLiliac),
        ),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => AdvancedFilters()));
          setState(() {});
        },
      ),
    );
  }

  Widget _locationFilter(String? location) {
    return Container(
      height: _getfilterHeight(context),
      width:  _getfilterWidth(context),
      decoration: BoxDecoration(
        color: customWhite,
        borderRadius: heavyBorderRadius(context),
      ),
      child: TextButton.icon(
        icon: Icon(CustomIcons.i6_city_icon, color: customStrongLiliac),
        label: Text(
          //AppLocalizations.of(context).researchArea,
          convertLabelToNameRegion(location),
          style: TextStyle(
              //fontWeight: FontWeight.bold,
              fontSize: MediaQuery.of(context).size.height * 0.018,
              overflow: TextOverflow.ellipsis,
              color: customStrongLiliac),
        ),
        onPressed: () async {
          setState(() => loading = true);
          List<Region> regionList = await Locations().getRegions();
          Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LocationFilters(
                          regionList: regionList, uid: this.widget.user.uid)))
              .then((value) => setState(() => loading = false));
        },
      ),
    );
  }

  Widget _forwardArrowButton() {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          filterBarStatus = true;
        });
      },
      child: Icon(Icons.arrow_forward_ios, color: customStrongLiliac),
      style: ElevatedButton.styleFrom(
        shape: CircleBorder(),
        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.01),
        backgroundColor: customWhite, // <-- Button color
        foregroundColor: customStrongLiliac, // <-- Splash color
      ),
    );
  }

  Widget _backwardArrowButton() {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          filterBarStatus = false;
        });
      },
      child: Icon(Icons.arrow_forward_ios, color: customStrongLiliac, textDirection: TextDirection.rtl),
      style: ElevatedButton.styleFrom(
        shape: CircleBorder(),
        //padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.01),
        backgroundColor: MyApp.selectedName.isNotEmpty
            ? customStrongGrey
            : customWhite, // <-- Button color
        foregroundColor: customStrongLiliac, // <-- Splash color
      ),
    );
  }

  Widget _filterBar2() {
    return Container(
      decoration: BoxDecoration(
        color: customStrongLiliac,
      ),
      child: Column(children: [
        Container(
          padding: EdgeInsets.only(
            left: MediaQuery.of(context).size.width * 0.00015,
            top: MediaQuery.of(context).size.height * 0.0055,
            bottom: MediaQuery.of(context).size.height * 0.0055,
            right: MediaQuery.of(context).size.width * 0.35,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _backwardArrowButton(),
              _searchByNameFilter(),
            ],
          ),
        ),
      ]),
    );
  }

  ///Sottomenù dei filtri
  ///Anche essendo un sottomenù questo widget è scrollabile come un qualsiasi annuncio
  Widget _filterBar1() {
    String? location = region;
    location = province != null ? (region! + ' (' + province! + ')') : region;
    return Container(
        decoration: BoxDecoration(
          color: customStrongLiliac,
        ),
        child: Column(
          children: [
            //searchBar(context),
            Container(
              padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.015,
                top: MediaQuery.of(context).size.height * 0.0055,
                bottom: MediaQuery.of(context).size.height * 0.0055,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _advanceFilter(),
                  _locationFilter(location),
                  _forwardArrowButton(),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _warning(String asset) {
    return Image.asset(
      asset,
      //height: MediaQuery.of(context).size.height * 0.8,
      //width: MediaQuery.of(context).size.width * 0.8,
    );
  }

  Widget _getFilter(){
    if(this.widget.adCategory == AdCategory.Favorites){
      return Container();
    } else {
      if(filterBarStatus || MyApp.selectedName.isNotEmpty){
        return _filterBar2();
      } else {
        return _filterBar1();
      }
    }
  } 

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : StreamBuilder<List<AdElements>>(
            initialData: [],
            stream: _futuresStream.stream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data!.length > 0) {
                  return SmartRefresher(
                    controller: _refreshController,
                    enablePullDown: false,
                    enablePullUp: true,
                    onLoading: _onLoading,
                    child: CustomScrollView(
                      slivers: <Widget>[
                        SliverToBoxAdapter(
                          child: Column(children: [
                            _getFilter(),
                            Container(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                          ]),
                        ),
                        SliverGrid(
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 1, //numero di colonne della griglia
                            childAspectRatio: 1,
                            crossAxisSpacing: 1,
                            mainAxisSpacing: 10,
                          ),
                          delegate:
                              SliverChildBuilderDelegate((context, index) {
                            return AdCard(
                              adElements: snapshot.data![index],
                              userData: this.widget.user,
                            );
                          }, childCount: snapshot.data!.length),
                        ),
                      ],
                    ),
                  );
                } else {
                  String asset;
                  if (MyApp.filter.isFilterApplied() || this.widget.adCategory != AdCategory.Favorites) {
                    asset = 'assets/images/logo_filtro_vuoto.png';
                  } else {
                    asset = 'assets/images/logo_preferiti_vuoto.png';
                  }
                  return CustomScrollView(slivers: <Widget>[
                    SliverToBoxAdapter(
                        child: Column(children: [
                      filterBarStatus || MyApp.selectedName.isNotEmpty
                          ? _filterBar2()
                          : _filterBar1(),
                      Center(
                        child: Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  // Add one stop for each color
                                  // Values should increase from 0.0 to 1.0
                                  stops: [
                                0.5,
                                0.9
                              ],
                                  colors: [
                                customStrongLiliac,
                                customPaleLiliac
                              ])),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              _warning(asset),
                            ],
                          ),
                        ),
                      ),
                    ]))
                  ]);
                }
              } else {
                return Loading();
              }
            },
          );
  }
}
