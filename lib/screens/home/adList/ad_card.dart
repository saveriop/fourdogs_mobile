import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:async/async.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';

import 'package:zampon/model/ad.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/adList/adDetail/ad_detail.dart';
import 'package:zampon/services/firebase/storage/storage_dp.dart';
import 'package:zampon/services/ws_rest/ads.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/utility.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/info_box_model.dart';

class AdCard extends StatefulWidget {
  final AdElements adElements;
  final UserData userData;

  AdCard({required this.adElements, required this.userData});

  @override
  _AdCardState createState() => _AdCardState();
}

class _AdCardState extends State<AdCard> {
  ///Memorizzazione asincrono per l'oggetto Future relativo alle immagini
  final AsyncMemoizer _memoizer = AsyncMemoizer();

  /// Variabile che gestisce il loading.
  /// Quando l'utente clicca bottoni asincroni, per far si che non vengano innescati più volte
  /// a schermo viene mostrato uno spinner. Quest'ultimo è gestito da questa booleana
  bool loading = false;

  _fetchData() {
    return this._memoizer.runOnce(() async {
      return await new StorageDp()
          .getPetImgReference(this.widget.adElements.id, "_0");
    });
  }

  Widget _image() {
    return FutureBuilder(
        future: this._fetchData(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('none');
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text(
                  '${snapshot.error}',
                  style: TextStyle(color: Colors.red),
                );
              }
          }
          return Container(
            child: Stack(children: [
              Positioned.fill(
                child: Align(
                  child: Container(
                    color: customWhite,
                  ),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * 0.05,
                    ),
                    color: this.widget.adElements.isClosed!
                        ? isClosedColor
                        : customStrongLiliac,
                  ),
                ),
              ),
              Positioned.fill(
                child: CachedNetworkImage(
                  fit: BoxFit.fitWidth,
                  //nel momento in cui viene aggiunto un nuovo annuncio,
                  //prima di scaricarsi l'img utilizza l'ultima in memoria se a TRUE
                  useOldImageOnUrlChange: true,
                  imageUrl: snapshot.data.toString(),
                  imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0.65, 0.9],
                        colors: this.widget.adElements.isClosed!
                            ? [isClosedColor, isClosedColor]
                            : [customStrongLiliac, customPaleLiliac]),
                    image: DecorationImage(
                        /*
                        colorFilter: this.widget.adElements.isClosed
                            ? ColorFilter.mode(
                                Colors.grey,
                                BlendMode.saturation,
                              )
                            : null,
                            */
                        scale: 1.0,
                        filterQuality: FilterQuality.high,
                        image: imageProvider,
                        fit: BoxFit.cover),
                    borderRadius: lightBorderRadius(context),
                  )),
                  placeholder: (context, url) => Center(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ]),
          );
        });
  }

  ///Icona cuore in alto a sinistra dell'immagine
  Widget _favorite() {
    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.025),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.05,
          width: MediaQuery.of(context).size.width * 0.1,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: !this.widget.userData.favoriteAds!.contains(this.widget.adElements.id)
                ? Colors.white
                : Colors.red,
          ),
          child: Icon(
            CustomIcons.i37_favorite_icon,
            size: MediaQuery.of(context).size.height * 0.04,
            color: !this.widget.userData.favoriteAds!.contains(this.widget.adElements.id)
                ? customStrongLiliac
                : Colors.white,
          ),
        ),
      ),
    );
  }

  /// restituisce un widget Align contenete la fascia colorata che sarà posizionata
  /// in alto a sinistra dell'img.
  /// Necessita l'adCategory per differenziare le immagini da mostrare a video
  Widget _ribbon(AdCategory? adCategory) {
    AssetImage assetImage = AssetImage('assets/images/ribbon_adozione.png');
    if (adCategory == AdCategory.Adoption) {
      assetImage = AssetImage('assets/images/ribbon_adozione.png');
    } else if (adCategory == AdCategory.Disappear) {
      assetImage = AssetImage('assets/images/ribbon_scomparso.png');
    } else if (adCategory == AdCategory.Found) {
      assetImage = AssetImage('assets/images/ribbon_avvistato.png');
    }
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
          padding: EdgeInsets.only(),
          child: Image(
            image: assetImage,
            height: MediaQuery.of(context).size.height * 0.12,
          )),
    );
  }

  ///Questo metodo verifica che l'annuncio sia stato chiuso o meno.
  ///In tal caso gli applica il timbro in base alla categoria
  Widget _isClosed(AdCategory? adCategory) {
    //applica il timbro solo per gli annunci chiusi
    if (this.widget.adElements.isClosed!) {
      AssetImage? assetImage;
      switch (adCategory) {
        case AdCategory.Adoption:
          assetImage = AssetImage('assets/images/timbro_adottato.png');
          break;
        case AdCategory.Found:
        case AdCategory.Disappear:
          assetImage = AssetImage('assets/images/timbro_trovato.png');
          break;
        default:
          assetImage = null;
          break;
      }
      return Align(
        alignment: Alignment.center,
        child: Padding(
            padding: EdgeInsets.only(),
            child: Image(
              image: assetImage!,
              height: MediaQuery.of(context).size.height * 0.48,
            )),
      );
    } else {
      //nel caso in cui l'annuncio sia aperto restituisce un Container vuoto
      return Container();
    }
  }

  ///Flexible che contine la foto + le sue icone sovrapposte
  Widget _squareImage() {
    return Flexible(
      child: Stack(
        children: [
          _image(),
          this.widget.adElements.adCategory == AdCategory.Adoption
              ? _favorite()
              : Container(),
          _ribbon(this.widget.adElements.adCategory),
          _isClosed(this.widget.adElements.adCategory),
        ],
      ),
    );
  }

  ///Nome dell'annuncio
  Widget _adName() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
      ),
      child: Text(
        this.widget.adElements.petName,
        style: TextStyle(
          color: customWhite,
          fontSize: MediaQuery.of(context).size.height * 0.03,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  ///Riquadri con le informazione dell'animale
  Widget _petInformation() {
    String age = "";
    if (this.widget.adElements.petAge!.years == 0 &&
        this.widget.adElements.petAge!.months == 0) {
      age = AppLocalizations.of(context)!.unknownAge;
    } else {
      age =
          "${this.widget.adElements.petAge!.years} ${AppLocalizations.of(context)!.years} ${this.widget.adElements.petAge!.months} ${AppLocalizations.of(context)!.months}";
    }
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.01,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ///pet category. E.g.: Cane, gatto, altro
          petInfoLabel(
              context,
              getPetLabel(context, this.widget.adElements.petCategory),
              getPetTypeMap(context)[
                  getPetLabel(context, this.widget.adElements.petCategory)]!,
              isClosed: this.widget.adElements.isClosed!),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.048,
          ),

          ///pet gender. e.g. : maschio, femmina
          petInfoLabel(
              context,
              getGenderLabel(context, this.widget.adElements.petGender!),
              getGenderMap(
                  context)[this.widget.adElements.petGender.toString()]!,
              isClosed: this.widget.adElements.isClosed!),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.05,
          ),

          ///pet years
          petAgeInfoLabel(context, age, CustomIcons.i25_pet_age_icon,
              isClosed: this.widget.adElements.isClosed!),
        ],
      ),
    );
  }

  //Luogo dell'annuncio
  Widget _adLocation() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.001,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(
            CustomIcons.i26_pet_location,
            color: customWhite,
            size: MediaQuery.of(context).size.height * 0.03,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.02,
          ),
          Text(
            convertLabelToNameRegion(this.widget.adElements.adLocation!.region) +
                "," +
                this.widget.adElements.adLocation!.province! +
                "," +
                this.widget.adElements.adLocation!.city!,
            style: TextStyle(
              color: customWhite,
              fontSize: MediaQuery.of(context).size.height * 0.02,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  //BUILD
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        //è possibile accedere al dettaglio dell'annuncio solo se non è stato chiuso
        if (!this.widget.adElements.isClosed!) {
          setState(() => loading = true);
          this.widget.adElements.adDescription =
              await Ads().getDescription(this.widget.adElements.id!);
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AdDetail(
                      adElements: this.widget.adElements,
                      userData: this.widget.userData,
                    )),
          ).then((value) => setState(() => loading = false));
        }
      },
      child: Container(
        decoration:
            adDecoration(context, isClosed: this.widget.adElements.isClosed!),
        //margin: EdgeInsets.only( bottom: MediaQuery.of(context).size.height * 0.015,),
        width: MediaQuery.of(context).size.width * 0.015,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _squareImage(),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            _adName(),
            _petInformation(),
            _adLocation(),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            //_adDate(),
          ],
        ),
      ),
    );
  }
}
