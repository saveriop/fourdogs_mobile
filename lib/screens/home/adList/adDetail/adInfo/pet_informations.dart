import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';

import 'package:zampon/model/ad.dart';
import 'package:zampon/model/age.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/utility.dart';

class PetInformations extends StatelessWidget {
  final AdElements adElements;
  final BuildContext context;

  const PetInformations({required this.adElements, required this.context});

  ///Box conteneti genere, sesso e taglia
  Widget _buildPetFeature(BuildContext context, String value, IconData icon) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.07,
      width: MediaQuery.of(context).size.width * 0.43,
      decoration: BoxDecoration(
        color: customWhite,
        borderRadius: mediumBorderRadius(context),
        border: Border.all(
          color: customStrongLiliac,
        ),
      ),
      child: Stack(alignment: AlignmentDirectional.center, children: <Widget>[
        FractionalTranslation(
          translation: Offset(-0.50, -0.42),
          child: circleContainer(context, icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.042),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              value,
              style: TextStyle(
                color: customStrongLiliac,
                fontSize: MediaQuery.of(context).size.width * 0.045,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ]),
    );
  }

  ///Box conteneti età
  Widget _buildPetAgeFeature(
      BuildContext context, String value, String feature, IconData icon) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.07,
      width: MediaQuery.of(context).size.width * 0.43,
      decoration: BoxDecoration(
        color: customWhite,
        borderRadius: mediumBorderRadius(context),
        border: Border.all(
          color: customStrongLiliac,
        ),
      ),
      child: Stack(alignment: AlignmentDirectional.center, children: <Widget>[
        FractionalTranslation(
          translation: Offset(-0.50, -0.42),
          child: circleContainer(context, icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.04),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              value,
              style: TextStyle(
                color: customStrongLiliac,
                fontSize: MediaQuery.of(context).size.width * 0.045,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              feature,
              style: TextStyle(
                color: customStrongLiliac,
                fontSize: MediaQuery.of(context).size.width * 0.045,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ]),
    );
  }

  ///cerchio contenete l'icona dell'informazione
  Widget circleContainer(BuildContext context, IconData icon,
      {Color color = Colors.white, double size = 20}) {
    return Container(
        alignment: Alignment.center,
        child: InkWell(
            child: Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: color,
          ),
          child: Icon(
            icon,
            //grandezza delle icone con le info del pet nel dettagllio annuncio
            size: MediaQuery.of(context).size.height * 0.023,
            color: customStrongLiliac,
          ),
          alignment: Alignment.center,
        )));
  }

  @override
  Widget build(BuildContext context) {
    String year = "";
    String months = "";
    if ("0" == Age.petYears(adElements.petAge) &&
        "0" == Age.petMonths(adElements.petAge)) {
      year = "Età";
      months = "sconosciuta";
    } else {
      year = AppLocalizations.of(context)!.years +
          ": " +
          Age.petYears(adElements.petAge);
      months = AppLocalizations.of(context)!.months +
          ": " +
          Age.petMonths(adElements.petAge);
    }
    return Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.02,
        ),
        child: Column(
          children: [
            Row(
              children: [
                ///SPECIE: CANE, GATTO, ALTRO
                _buildPetFeature(
                    context,
                    getPetLabel(context, adElements.petCategory!),
                    //"",
                    getPetTypeMap(
                        context)[getPetLabel(context, adElements.petCategory)]!),
                Expanded(
                  child: Container(),
                ),

                ///SESSO
                _buildPetFeature(
                    context,
                    getGenderLabel(context, adElements.petGender!),
                    //AppLocalizations.of(context).gender,
                    getGenderMap(context)[adElements.petGender.toString()]!),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            Row(
              children: [
                ///ETA'
                _buildPetAgeFeature(
                    context, year, months, CustomIcons.i25_pet_age_icon),
                Expanded(
                  child: Container(),
                ),

                ///TAGLIA
                _buildPetFeature(
                    context,
                    getSizeLabel(context, adElements.petSize),
                    //AppLocalizations.of(context).size,
                    getPetSizeIconMap(context)[adElements.petSize.toString()]!),
              ],
            ),
          ],
        ));
  }
}
