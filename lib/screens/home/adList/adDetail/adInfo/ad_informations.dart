import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/location.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';

class AdInformations extends StatefulWidget {
  final AdElements adElements;
  final BuildContext context;
  final UserData userData;

  const AdInformations(
      {required this.adElements,
      required this.context,
      required this.userData});

  @override
  _AdInformationsState createState() => _AdInformationsState();
}

class _AdInformationsState extends State<AdInformations> {
  late Color favoriteShapeColor;
  late Color favoriteIconColor;

  @override
  void initState() {
    ///In base alla lista dei preferiti viene effettuata la colorazione dell'icona del cuore
    super.initState();
    if (this.widget.userData.favoriteAds != null) {
      favoriteShapeColor =
          !this.widget.userData.favoriteAds!.contains(this.widget.adElements.id)
              ? Colors.white
              : Colors.red;
      favoriteIconColor =
          !this.widget.userData.favoriteAds!.contains(this.widget.adElements.id)
              ? customStrongLiliac
              : Colors.white;
    } else {
      favoriteShapeColor = Colors.white;
      favoriteIconColor = customStrongLiliac;
    }
  }

  ///Ad name
  Widget _adName() {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.01,
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        Text(
          this.widget.adElements.petName,
          style: TextStyle(
            color: customWhite,
            fontWeight: FontWeight.bold,
            fontSize: MediaQuery.of(context).size.width * 0.07,
          ),
        ),
      ]),
    );
  }

  ///Location and Date costructor
  Widget _adLocationAndDate(IconData icon, String text) {
    return Row(
      children: [
        Icon(
          icon,
          color: customWhite,
          size: MediaQuery.of(context).size.width * 0.05,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.01,
        ),
        Text(
          text,
          style: TextStyle(
            color: customWhite,
            fontSize: MediaQuery.of(context).size.width * 0.035,
          ),
        ),
      ],
    );
  }

  ///Shared Button
  Widget _sharedButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        fixedSize: Size(
          MediaQuery.of(context).size.height * 0.07,
          MediaQuery.of(context).size.height * 0.07,
        ),
        backgroundColor: customWhite,
        foregroundColor: customGreen,
        shape: CircleBorder(
          //RoundedRectangleBorder(
          side: BorderSide(
            color: customWhite,
          ),
          //borderRadius: mediumBorderRadius(context),
        ),
      ),
      onPressed: () async {
        _share(context);
      },
      child: Icon(
        CustomIcons.i12_share_icon,
        size: MediaQuery.of(context).size.height * 0.045,
      ),
    );
  }

  Future<void> _share(BuildContext context) async {
    ///QUESTA PORZIONE DI CODICE SERVE PER SALVARE IN LOCALE L'IMG DIRETTAMENTE DALLO STORE DI FIREBASE
    ///ADESSO NON SERVE MA PER IL VECCHIO CONDIVIDI SERVIVA
    ///NON ELIMINARE
    //StorageDp storageDp = StorageDp();
    //String imgStr =await storageDp.recoverImagesFromDb(this.widget.adElements.id, "_0");
    //Uri imgURL = Uri.parse(imgStr);
    //Response response = await get(imgURL);
    //final documentDirectory = (await getExternalStorageDirectory()).path;
    //File imgFile =new File('$documentDirectory/${this.widget.adElements.petName}.png');
    //imgFile.writeAsBytesSync(response.bodyBytes);
    //final RenderBox box = context.findRenderObject();
    ///List<String> list = [];
    //list.add('$documentDirectory/${this.widget.adElements.petName}.png');

    FlutterShareMe().shareToSystem(
        msg: "${this.widget.adElements.petName}" "\n" " " +
            "${this.widget.adElements.adDescription}" "\n" +
            "\n" +
            _createDynamicLinkFromWebDomain());
  }

  ///CREAZIONE VECCHIO DEEPLINK
  ///creazione del deeplink
  ///ogni link è composto dall'url https://zampon.page.link
  ///da un prefisso "adDetail"
  ///e le variabili ?id= valore dell'ID ANNUNCIO
/*
  _createDynamicLink() {
    String id = this.widget.adElements.id;
    return 'https://zampon.page.link/adDetail?id=$id';
  }
*/
  _createDynamicLinkFromWebDomain() {
    String? id = this.widget.adElements.id;
    return 'http://www.zampon.net?id=$id';
  }

  ///Al click sul tasto del cuore l'utente salverà l'annuncio tra i suoi preferiti
  Widget _favoriteButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        fixedSize: Size(
          MediaQuery.of(context).size.height * 0.07,
          MediaQuery.of(context).size.height * 0.07,
        ),
        backgroundColor: favoriteShapeColor,
        foregroundColor: customStrongLiliac,
        shape: CircleBorder(
          //RoundedRectangleBorder(
          side: BorderSide(
            color: favoriteShapeColor,
          ),
          //borderRadius: mediumBorderRadius(context),
        ),
      ),
      onPressed: () async {
        DatabaseServiceUser databaseServiceUserDp =
            new DatabaseServiceUser(uid: this.widget.userData.uid);
        //cancellazione cache
        await DefaultCacheManager().emptyCache();

        ///se l'annuncio non è presente tra i preferiti viene salvato
        if (!this
            .widget
            .userData
            .favoriteAds!
            .contains(this.widget.adElements.id)) {
          this
              .widget
              .userData
              .favoriteAds!
              .add(this.widget.adElements.id ?? "");
          setState(() {
            favoriteShapeColor = Colors.red;
            favoriteIconColor = Colors.white;
            databaseServiceUserDp.updateField(
                DatabaseServiceUser.FAVORITE_ADS_LABEL,
                this.widget.userData.favoriteAds);
          });
        } else {
          ///se l'annuncio  è presente tra i preferiti viene rimosso
          this.widget.userData.favoriteAds!.remove(this.widget.adElements.id);
          setState(() {
            favoriteShapeColor = Colors.white;
            favoriteIconColor = customStrongLiliac;
            databaseServiceUserDp.updateField(
                DatabaseServiceUser.FAVORITE_ADS_LABEL,
                this.widget.userData.favoriteAds);
          });
        }
      },
      child: Icon(
        CustomIcons.i37_favorite_icon,
        size: MediaQuery.of(context).size.height * 0.045,
        color: favoriteIconColor,
      ),
    );
  }

  ///Row thet aligng Ad location, date and shared button
  Widget _adInformationAndShare(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.01,
        bottom: MediaQuery.of(context).size.height * 0.01,
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _adLocationAndDate(
              CustomIcons.i26_pet_location,
              convertLabelToNameRegion(
                  Location.locationStr(this.widget.adElements.adLocation)),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            _adLocationAndDate(
              CustomIcons.i15_calendar_icon,
              AppLocalizations.of(context)!.adDate +
                  this.widget.adElements.adDate!,
            ),
          ],
        ),
        _sharedButton(),
        this.widget.adElements.adCategory == AdCategory.Adoption
            ? _favoriteButton()
            : Text(""),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [_adName(), _adInformationAndShare(context)]);
  }
}
