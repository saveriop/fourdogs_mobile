import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/settings/userProfilePage/personal_profile_stream_provider.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/user_avatar.dart';

class UserInformations extends StatelessWidget {
  final UserData userData;
  final AdElements adElements;
  final BuildContext context;

  const UserInformations({
    required this.userData,
    required this.adElements,
    required this.context,
  });

  /*
   *  Metodo che permette di telefonare oppure inviare la mail direttamente dalla label dell'annuncio 
   */
  void _manageCallEmailByLabel(String label, String? labelValue) {
    if (label == AppLocalizations.of(context)!.phoneNumber &&
        labelValue != null &&
        labelValue.isNotEmpty) {
      UrlLauncher.launch('tel:' + labelValue);
    } else {
      UrlLauncher.launch('mailto:' + labelValue!);
    }
  }

  _whatappMessage(
    String? labelValue,
    IconData icon,
  ) {
    String whatsappMessage =
        "Ciao, ho letto su ZampOn l'annuncio di " +
            this.adElements.petName +
            " e vorrei ricevere più informazioni in merito. \n Grazie e a presto!";
    return Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.000002),
        child: confirmationButton(
          context,
          "WhatsApp",
          (() async {
            await UrlLauncher.launch(
                "https://wa.me/$labelValue?text=$whatsappMessage");
          }),
          width: MediaQuery.of(context).size.width * 0.9,
          icon: icon,
        ));
  }

  //Singola box che contiene il numero di telefono o l'email
  _userContacts(IconData icon, String label, String? labelValue) {
    return Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.000002),
        child: confirmationButton(context, labelValue != null ? labelValue : "",
            (() {
          _manageCallEmailByLabel(label, labelValue);
        }), width: MediaQuery.of(context).size.width * 0.9, icon: icon));
  }

  //Row con l'immagine dell'utente e la dicitura 'Creato da:' con l'username
  Widget _userNameAndAvatar() {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PersonalProfileStreamProvider(
                    userUid: this.userData.uid,
                    calledUid: this.adElements.contact!.userUid!)));
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              GestureDetector(
                  child: UserAvatar(
                      userUid: this.adElements.contact!.userUid!,
                      calledUid: this.userData.uid)),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.02,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context)!.createdBy,
                    style: TextStyle(
                      color: customWhite,
                      fontSize: MediaQuery.of(context).size.width * 0.05,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.006,
                  ),
                  Container(
                    child: FutureBuilder(
                        future: DatabaseServiceUser()
                            .findUserName(adElements.contact!.userUid!),
                        builder: (context, snapshot) {
                          if (snapshot.data == null) {
                            return Text('');
                          } else {
                            return Text(
                              snapshot.data.toString(),
                              style: TextStyle(
                                color: customWhite,
                                fontSize:
                                    MediaQuery.of(context).size.height * 0.02,
                              ),
                            );
                          }
                        }),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.01,
      ),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.01,
        ),
        _userContacts(
            CustomIcons.i10_telephone_icon,
            AppLocalizations.of(context)!.phoneNumber,
            adElements.contact!.mobilePhoneForAd!),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.015,
        ),
        _userContacts(
            CustomIcons.i17_email_icon,
            AppLocalizations.of(context)!.email,
            adElements.contact!.emailForAd!),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.015,
        ),
        //FontAwesomeIcons.
        _whatappMessage("+39" + adElements.contact!.mobilePhoneForAd!,
            FontAwesomeIcons.whatsapp),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.015,
        ),
        _userNameAndAvatar(),
      ]),
    );
  }
}
