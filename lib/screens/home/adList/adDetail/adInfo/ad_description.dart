import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:zampon/shared/constants.dart';

class AdDescription extends StatelessWidget {
  final String adDescription;
  final BuildContext context;

  const AdDescription({
    required this.adDescription,
    required this.context,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: MediaQuery.of(context).size.height * 0.02,
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Expanded(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              AppLocalizations.of(context)!.description,
              style: TextStyle(
                color: customWhite,
                fontWeight: FontWeight.bold,
                fontSize: MediaQuery.of(context).size.width * 0.06,
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.006,
            ),
            Text(
              this.adDescription,
              style: TextStyle(
                color: customWhite,
                fontSize: MediaQuery.of(context).size.width * 0.04,
              ),
            ),
          ]),
        )
      ]),
    );
  }
}
