import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/reported_ad.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/services/ws_rest/reported.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/pop_up_model.dart';

class AdReport extends StatelessWidget {
  final String id;
  final BuildContext context;
  final UserData user;

  const AdReport(
      {required this.id, required this.context, required this.user});

  ///Tasto contenente la ragione della segnalazione
  ElevatedButton _closeReasonButton(
      BuildContext context, String reasonLabel, int reasonCode) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        fixedSize: Size(
          MediaQuery.of(context).size.width * 0.8,
          MediaQuery.of(context).size.height * 0.065,
        ),
        backgroundColor : customWhite,
        foregroundColor : customStrongLiliac,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: customWhite,
          ),
          borderRadius: mediumBorderRadius(context),
        ),
      ),
      onPressed: () async {
        Reported().createAd(ReportedAd(
          id: this.id,
          saleAd: (reasonCode == 0) ? true : false,
          inappropriateContent: (reasonCode == 1) ? true : false,
        ));
        Navigator.of(context).pop();
        _feedBackDialog(context);
      },
      child: Text(reasonLabel,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: customStrongLiliac,
              fontSize: MediaQuery.of(context).size.height * 0.025)),
    );
  }

  ///AlertDialog feedback operazione completata
  void _feedBackDialog(
    BuildContext context,
  ) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return confirmPopUp(
            context,
            AppLocalizations.of(context)!.operationComplete,
            AppLocalizations.of(context)!.reportedFeedback);
      },
    );
  }

  ///AlertDialog dedicata alla chiusura dell'annuncio di adozione
  void _reportAdDialog(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return popUpCustom(
            context,
            CustomIcons.i45_report_icon,
            AppLocalizations.of(context)!.reportTitle,
            AppLocalizations.of(context)!.reasonReport,
            _closeReasonButton(
                context, AppLocalizations.of(context)!.saleAnnouncement, 0),
            _closeReasonButton(
                context, AppLocalizations.of(context)!.inappropriateContent, 1),
            iconColor: Colors.yellow);
      },
    );
  }

  Widget _warningButton({IconData? icon, String? labelValue}) {
    return Padding(
      padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.02),
      child: ElevatedButton(
        onPressed: () {
          _reportAdDialog(context);
        },
        style: elevationButtonStyleAdDetail(context),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.06,
          width: MediaQuery.of(context).size.width,

          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                icon,
                color: Colors.yellow[700],
                size: MediaQuery.of(context).size.height * 0.03,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.01,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.02,
              ),
              Text(
                labelValue != null ? labelValue : "",
                style: TextStyle(
                  color: customStrongLiliac,
                  fontSize: MediaQuery.of(context).size.width * 0.04,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.025,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //return _warningButton();
    return _warningButton(
        icon: CustomIcons.i45_report_icon,
        labelValue: AppLocalizations.of(context)!.reportLabel);
  }
}
