import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:photo_view/photo_view.dart';

import 'package:zampon/services/firebase/storage/storage_dp.dart';

class PetSingleImageFullScreen extends StatefulWidget {
  final String id;
  final String imageNumber;
  PetSingleImageFullScreen({required this.id, required this.imageNumber});

  @override
  _PetSingleImageFullScreenState createState() =>
      _PetSingleImageFullScreenState();
}

class _PetSingleImageFullScreenState extends State<PetSingleImageFullScreen> {
  @override
  Widget build(BuildContext context) {
    StorageDp _storageDp = StorageDp();

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
              child: Stack(children: [
            FutureBuilder(
                future: _storageDp.recoverImagesFromDb(
                    this.widget.id, this.widget.imageNumber),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return Text('none');
                    case ConnectionState.waiting:
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    case ConnectionState.active:
                      return Text('');
                    case ConnectionState.done:
                      if (snapshot.hasError) {
                        return Text(
                          '${snapshot.error}',
                          style: TextStyle(color: Colors.red),
                        );
                      }
                  }
                  return PhotoView(
                    imageProvider: CachedNetworkImageProvider(
                      snapshot.data.toString(),
                    ),
                  );
                }),
          ])),
        ],
      ),
    );
  }
}
