import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import 'package:zampon/screens/home/adList/adDetail/petImages/pet_gallery_single_image_full_screen.dart';

class ImgGalleryFullScreen extends StatefulWidget {
  final String id;
  final int photoNumber;
  final int currentIndex;
  ImgGalleryFullScreen(
      {required this.id,
      required this.photoNumber,
      required this.currentIndex});

  @override
  _ImgGalleryFullScreenState createState() => _ImgGalleryFullScreenState();
}

class _ImgGalleryFullScreenState extends State<ImgGalleryFullScreen> {
  int _currentIndex = 0;

  @override
  void initState() {
    _currentIndex = this.widget.currentIndex;
    super.initState();
  }

  //Generate the list for dhe image carusel in the ad detail
  List _generateImagesList() {
    List cardList = [];
    for (var i = 0; i < this.widget.photoNumber; i++) {
      cardList.add(
        PetSingleImageFullScreen(
          id: this.widget.id,
          imageNumber: "_$i",
        ),
      );
    }
    return cardList;
  }

  @override
  Widget build(BuildContext context) {
    List cardList = _generateImagesList();
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        color: Colors.black,
        child: GestureDetector(
          // A pointer that was previously in contact with the screen with a primary button and moving vertically is no longer
          // in contact with the screen and was moving at a specific velocity when it stopped contacting the screen.
          onVerticalDragEnd: (onVerticalDragDown) {
            Navigator.pop(context);
          },
          child: CarouselSlider(
            options: CarouselOptions(
              height: double.infinity,
              enlargeStrategy: CenterPageEnlargeStrategy.height,
              viewportFraction: 1.0,
              initialPage: _currentIndex,
              enableInfiniteScroll: false,
              autoPlay: false,
              onPageChanged: (index, reason) {
                setState(() {
                  _currentIndex = index;
                });
              },
            ),
            items: cardList.map((card) {
              return Builder(builder: (BuildContext context) {
                return Card(
                  color: Colors.black,
                  child: card,
                );
              });
            }).toList(),
          ),
        ),
      ),
    );
  }
}
