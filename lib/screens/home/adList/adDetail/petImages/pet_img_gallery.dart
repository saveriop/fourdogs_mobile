import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'package:zampon/model/ad.dart';
import 'package:zampon/screens/home/adList/adDetail/petImages/img_gallery_full_screen.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/screens/home/adList/adDetail/petImages/pet_gallery_single_image.dart';

class PetImgGallery extends StatefulWidget {
  final AdElements adElements;
  PetImgGallery({required this.adElements});

  @override
  _PetImgGalleryState createState() => _PetImgGalleryState();
}

class _PetImgGalleryState extends State<PetImgGallery> {
  int _currentIndex = 0;

  //Generate the list for dhe image carusel in the ad detail
  List _generateImagesList(AdElements adElements) {
    List cardList = [];
    for (var i = 0; i < adElements.photoNumber!; i++) {
      cardList.add(
        PetSingleImage(
          id: adElements.id!,
          imageNumber: "_$i",
        ),
      );
    }
    return cardList;
  }

  @override
  Widget build(BuildContext context) {
    List cardList = _generateImagesList(this.widget.adElements);
      return Container(
          decoration: BoxDecoration(
            borderRadius: lightBorderRadius(context),
          ),
          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.515,
          child: Stack(
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ImgGalleryFullScreen(
                        id: this.widget.adElements.id!,
                        photoNumber: this.widget.adElements.photoNumber!,
                        currentIndex: _currentIndex);
                  }));
                },
                child: CarouselSlider(
                  options: CarouselOptions(
                    height: MediaQuery.of(context).size.height * 0.50,
                    viewportFraction: 1.0,
                    initialPage: 0,
                    enableInfiniteScroll: false,
                    autoPlay: true,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _currentIndex = index;
                      });
                    },
                  ),
                  items:  cardList.map((card) {
                    return Builder(builder: (BuildContext context) {
                      return Container(
                        height: MediaQuery.of(context).size.height * 0.500,
                        decoration: BoxDecoration(borderRadius: lightBorderRadius(context),),
                       child: card
                     );
                    });
                  }).toList(),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: buildIndicator(),
              ),
            ],
          ));
  }

  Widget buildIndicator() => AnimatedSmoothIndicator(
        activeIndex: _currentIndex,
        count: this.widget.adElements.photoNumber!,
        effect: ScrollingDotsEffect(
          activeDotScale: 1,
          dotColor: customWhite.withOpacity(0.3),
          activeDotColor: customWhite,
        ),
      );
}
