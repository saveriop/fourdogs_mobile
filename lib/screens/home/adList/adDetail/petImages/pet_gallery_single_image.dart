import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:zampon/services/firebase/storage/storage_dp.dart';
import 'package:zampon/shared/constants.dart';

class PetSingleImage extends StatefulWidget {
  final String id;
  final String imageNumber;
  PetSingleImage({required this.id, required this.imageNumber});

  @override
  _PetSingleImageState createState() => _PetSingleImageState();
}

class _PetSingleImageState extends State<PetSingleImage> {
  @override
  Widget build(BuildContext context) {
    StorageDp _storageDp = StorageDp();

    return Container(
      height: 500,
      child: 

            FutureBuilder(
                future: _storageDp.recoverImagesFromDb(
                    this.widget.id, this.widget.imageNumber),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return Text('none');
                    case ConnectionState.waiting:
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    case ConnectionState.active:
                      return Text('');
                    case ConnectionState.done:
                      if (snapshot.hasError) {
                        return Text(
                          '${snapshot.error}',
                          style: TextStyle(color: Colors.red),
                        );
                      }
                  }
                  return CachedNetworkImage(
                    useOldImageOnUrlChange: true,
                    imageUrl: snapshot.data.toString(),
                    imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider,
                          scale: 1.0,
                          filterQuality : FilterQuality.high,
                          fit: BoxFit.cover),
                      borderRadius:lightBorderRadius(context), //Radius of carusel images
                    )
                    ),
                    placeholder: (context, url) => Center(
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  );
                }),
    );
  }
}
