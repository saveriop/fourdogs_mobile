import 'package:flutter/material.dart';
import 'package:zampon/main.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/home/adList/adDetail/adInfo/ad_description.dart';
import 'package:zampon/screens/home/adList/adDetail/adInfo/ad_informations.dart';
import 'package:zampon/screens/home/adList/adDetail/adInfo/ad_report.dart';
import 'package:zampon/screens/home/adList/adDetail/adInfo/pet_informations.dart';
import 'package:zampon/screens/home/adList/adDetail/adInfo/user_informations.dart';
import 'package:zampon/screens/home/adList/adDetail/petImages/pet_img_gallery.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/app_bar_model.dart';

class AdDetail extends StatefulWidget {
  final AdElements adElements;
  final UserData userData;

  AdDetail({required this.adElements, required this.userData});

  @override
  _AdDetailState createState() => _AdDetailState();
}

class _AdDetailState extends State<AdDetail> {
  @override
  void initState() {
    ///Vengono resettate le variabili utilizzate per la navigazione tramite deeplink
    MyApp.isLinked = false;
    MyApp.link = "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //Galleria di immagini
    PetImgGallery carouselGallery = PetImgGallery(
      adElements: this.widget.adElements,
    );

    //Informazioni sull'annuncio (Nome, luogo, data e tasto condividi e favoriti)
    AdInformations adInformations = AdInformations(
      adElements: this.widget.adElements,
      context: context,
      userData: this.widget.userData,
    );

    //Informazioni sul pet (sesso, taglia, anni)
    PetInformations petInformations = PetInformations(
      adElements: this.widget.adElements,
      context: context,
    );

    //Informazioni sull'utente che ha pubblicato l'annuncio (numero di telefono, email, username e immagine)
    UserInformations userInformations = UserInformations(
      adElements: this.widget.adElements,
      userData: this.widget.userData,
      context: context,
    );

    //Dettaglio dell'annuncio (descrizione)
    AdDescription adDescription = AdDescription(
      adDescription: this.widget.adElements.adDescription!,
      context: context,
    );

    //Segnala annuncio
    AdReport adReport = AdReport(
      id: this.widget.adElements.id!,
      context: context,
      user: this.widget.userData,
    );

    return Scaffold(
      extendBody: true,
      appBar: customAppBar(context),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  // Add one stop for each color
                  // Values should increase from 0.0 to 1.0
                  stops: [
                0.7,
                0.9
              ],
                  colors: [
                customStrongLiliac,
                customPaleLiliac,
              ])),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              carouselGallery,
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.055,
                ),
                child: Column(
                  children: [
                    adInformations,
                    petInformations,
                    userInformations,
                    adDescription,
                    adReport
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
