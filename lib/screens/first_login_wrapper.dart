import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:zampon/model/user.dart';
import 'package:zampon/screens/authenticate/firstLoginImg/first_login_img.dart';
import 'package:zampon/screens/home/home.dart';

import '../main.dart';

class FirstLoginWrapper extends StatelessWidget {
  final int tabIndex;

  FirstLoginWrapper({required this.tabIndex});

  @override
  Widget build(BuildContext context) {
    ///Se l'utente non ha ancora selezionato l'area di ricerca (primo login)
    ///sarà indirizzato alla selezione della locazione altrimenti accederà direttamente alla HOME
    final UserData? user = Provider.of<UserData?>(context);
    if (user!.checkIflocationFilterIsNull() ||
        user.locationFilter!.isNotEmpty) {
      ///gestione del deeplink
      ///Sia nel caso in cui l'app fosse già aperta
      ///che nel caso in cui l'app fosse chiusa
      ///la gestione dell'apertura app e navigazione in un annuncio specifico è demandata
      ///dai metodi richiamati in questo if
      if (MyApp.isLinked) {
        handleIncomingLinks(context, user);
        uniLinksNavigate(context, user, MyApp.link);
      }

      return Home(
        tabIndex: tabIndex,
      );

      ///E' il primo accesso, ma poichè avviene attraverso SSO l'utente possiede già un'immagine di profilo
   // } else if (user.sso) {
      ///carico in memoria il JSON con le Regione e le Province
     /* AdsLocation location = populates();
      return FirstLoginLocationFilters(
        location: location,
        uid: user.uid,
      );*/

      ///E' il primo accesso, ma poichè avviene attraverso email l'utente non possiede un'immagine di profilo, pertanto dovrà caricarla
    } else {
      return FirstLoginImg(uid: user.uid);
    }
  }
}
