import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/logo_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';
import 'forgot_password.dart';

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final AuthService _auth = AuthService();
  //Serve per effettuare la validazione dei campi in input
  //Si associa al widget Form per
  final _formkey = GlobalKey<FormState>();

  //Booleano usato per attivare lo spinner
  bool loading = false;

  //Text field state
  String email = "";
  String password = "";
  String error = "";

  //Logo con nome
  Widget _logo() {
    return Padding(
      padding: EdgeInsets.only(),
      child: logoAndNameRegistration(context),
    );
  }

  //Viene costruito il form per il login

  //Costruisce i form Email E Password
  Widget _entryField(String title, {bool isPassword = false}) {
    return noIconTextFormFieldCustom(
      context,
      obscureText: isPassword,
      hintText: title,
      validator: (val) {
        if (isPassword) {
          return val!.length < 6
              ? AppLocalizations.of(context)!.passwordLength
              : null;
        } else {
          return val!.isEmpty
              ? AppLocalizations.of(context)!.enterThe + ' $title'
              : null;
        }
      },
      onChanged: (val) {
        setState(() {
          if (isPassword) {
            password = val!;
          } else {
            email = val!;
          }
        });
      }, 
    );
  }

  Widget _userInformationWidget() {
    return Column(
      children: <Widget>[
        _entryField(AppLocalizations.of(context)!.insertHereYourEmail),
        _entryField(AppLocalizations.of(context)!.insertHereYourPass,
            isPassword: true),
      ],
    );
  }

  //Costruisce il tasto per inviare le credenziali e loggarsi
  Widget _submitButton() {
    return confirmationButton(
      context,
      AppLocalizations.of(context)!.login,
      () async {
        if (_formkey.currentState!.validate()) {
          setState(() => loading = true);
          dynamic result =
              await _auth.signInWithEmailAndPassword(email, password);
          //Se il risultato è corretto, lo StreamProvider<User> definito nel main.dart e utilizzato nella
          //classe wrapper.dart verificherà che è presente un utente e permetterà l'accesso alla Home
          if (result == null) {
            setState(() {
              error = AppLocalizations.of(context)!.incorrectCredentials;
              loading = false;
            });
          } else {
            //Se la risposta è positiva ritorno nel Wrapper che troverà
            //l'oggetto User valorizzato e mi rimanderà alla Home
            Navigator.of(context)
                .pushNamedAndRemoveUntil(
                    '/wrapper', (Route<dynamic> route) => false)
                .then((value) {
              setState(() => loading = false);
            });
          }
        }
      },
    );
  }

  //Mette insieme i campi email, password e il tasto per loggare e crea un form
  Widget _formCredential() {
    return Container(
      child: Form(
          key: _formkey,
          child: Column(children: <Widget>[
            _userInformationWidget(),
            _submitButton(),
          ])),
    );
  }

  Widget _forgotPassword() {
    return Container(
        alignment: Alignment.center,
        child: TextButton(
          style: ButtonStyle(
            overlayColor: MaterialStateProperty.all(Colors.transparent),
          ),
          child: Text(AppLocalizations.of(context)!.forgotPassword,
              style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width * 0.035,
                  color: customWhite)),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ForgotPassword()));
          },
        ));
  }

  //messaggio di errore
  Widget _loginErrorMessage() {
    return Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.02,
        ),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.65,
          child: Text(
            error,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.045,
                color: Colors.red,
                fontWeight: FontWeight.bold),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    //lista dei widget che saranno incolonnati nella pagina
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_logo());
    columnElementList.add(_formCredential());
    columnElementList.add(_loginErrorMessage());
    columnElementList.add(_forgotPassword());

    return loading
        ? Loading()
        : genericPage(context, true,
            backButtonColor: customWhite,
            columnElementList: columnElementList,
            appbar: false,
            trasparent: false,
            noTitle: false);
  }
}
