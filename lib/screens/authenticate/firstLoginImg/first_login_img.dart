import 'dart:io';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/avatar_utility.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/services/firebase/storage/storage_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

class FirstLoginImg extends StatefulWidget {
  final String uid;

  FirstLoginImg({required this.uid});

  @override
  _FirstLoginImgState createState() => _FirstLoginImgState();
}

class _FirstLoginImgState extends State<FirstLoginImg> {
  List<String> avatarUrlListRow1 = [
    "assets/images/avatar/cat_avatar.png",
    "assets/images/avatar/guinea_pig_avatar.png",
    "assets/images/avatar/parrot_avatar.png",
  ];
  List<String> avatarUrlListRow2 = [
    "assets/images/avatar/rabbit_avatar.png",
    "assets/images/avatar/shiba_avatar.png",
    "assets/images/avatar/turtle_avatar.png",
  ];

  String? photoPath;
  String defaultImgUrl = "assets/images/avatar/parrot_avatar.png";

  @override
  void initState() {
    photoPath = "assets/images/avatar/parrot_avatar.png";
    super.initState();
  }

  ///Apre la galleria e permette di selezionare una foto
  Future _getImageFromGalley() async {
    final ImagePicker _picker = ImagePicker();
    final pickedFile = await _picker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 50,
        maxWidth: 800,
        maxHeight: 600);
    setState(() => pickedFile != null
        ? photoPath = pickedFile.path
        : photoPath = photoPath);
  }

  ///Elemento grigli per immagini caricate dall'utente
  Widget _photoProfile() {
    return GestureDetector(
      onTap: () {
        _getImageFromGalley();
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.5,
        height: MediaQuery.of(context).size.width * 0.5,
        decoration: BoxDecoration(
          color: customWhite,
          border: Border.all(
            color: customWhite, // Set border color
            width: MediaQuery.of(context).size.width * 0.01,
          ), // Set border width
          borderRadius:
              circularBorderRadius(context), // Set rounded corner radius
        ),
        child: Stack(children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: ClipOval(child: showImage(photoPath!)),
          ),
        ]),
      ),
    );
  }

  Widget _selectableAvatar(String avatarUrl) {
    return GestureDetector(
      onTap: () {
        setState(() => photoPath = avatarUrl);
      },
      child: Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.01),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.29,
          height: MediaQuery.of(context).size.width * 0.29,
          decoration: BoxDecoration(
            color: customWhite,
            border: Border.all(
                color: customWhite, // Set border color
                width: MediaQuery.of(context).size.height *
                    0.002), // Set border width
            borderRadius:
                circularBorderRadius(context), // Set rounded corner radius
          ),
          child: ClipOval(
            child: Image.asset(
              avatarUrl,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }

  Widget _avatarRow() {
    return Container(
      child: SingleChildScrollView(
        child: Column(children: [
          Row(
            children: List.generate(
              avatarUrlListRow1.length,
              (index) {
                return Flexible(
                    child: _selectableAvatar(avatarUrlListRow1[index]));
              },
            ),
          ),
          Row(
            children: List.generate(
              avatarUrlListRow2.length,
              (index) {
                return Flexible(
                    child: _selectableAvatar(avatarUrlListRow2[index]));
              },
            ),
          ),
          _confirmButton(context)
        ]),
      ),
    );
  }

  ///Tasto di conferma
  Widget _confirmButton(BuildContext context) {
    return confirmationButton(context, AppLocalizations.of(context)!.confirm,
        () async {
      if (defaultImgUrl != "assets/images/add_photo.png") {
        final Directory systemTempDir = Directory.systemTemp;
        final byteData = await rootBundle.load(photoPath!);
        final file = File('${systemTempDir.path}/' + this.widget.uid);
        await file.writeAsBytes(byteData.buffer
            .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
        new StorageDp().updateProfileImg(file, this.widget.uid);

        ///inizializzazione della regione di ricerca: valore di default PUGLIA
        new DatabaseServiceUser(uid: this.widget.uid)
            .updateField(DatabaseServiceUser.LOCATION_FILTER_LABEL, "PUGLIA");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //lista dei widget che saranno incolonnati nella pagina
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(Text(
      AppLocalizations.of(context)!.firstAccess,
      style: TextStyle(
          fontSize: MediaQuery.of(context).size.width * 0.045,
          color: customWhite,
          fontWeight: FontWeight.w800),
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(Text(
      AppLocalizations.of(context)!.beforeContinue,
      style: TextStyle(
          fontSize: MediaQuery.of(context).size.width * 0.045,
          color: customWhite,
          fontWeight: FontWeight.bold),
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_photoProfile());
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(Text(
      AppLocalizations.of(context)!.orSelectAvatar,
      style: TextStyle(
          fontSize: MediaQuery.of(context).size.width * 0.045,
          color: customWhite,
          fontWeight: FontWeight.bold),
    ));
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    columnElementList.add(_avatarRow());
    columnElementList.add(SizedBox(
      height: MediaQuery.of(context).size.height * 0.02,
    ));
    return genericPage(context, false,
        columnElementList: columnElementList,
        appbar: false,
        extendBody: true,
        noTitle: true);
  }
}
