import 'package:flutter/material.dart';
import 'package:zampon/screens/authenticate/login_page.dart';
import 'package:zampon/screens/authenticate/signup.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/shared/constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/logo_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';

class WelcomePage extends StatefulWidget {

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  final AuthService _auth = AuthService();
  bool loading = false;
  String error = "";

  //Logo con nome
  Widget _logo() {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.1,
      ),
      child: logoAndName(context),
    );
  }

  //Bottone per accedere con Google
  Widget _googleLoginButton() {
    return loginButtonWithIcon(
      context,
      FaIcon(
        FontAwesomeIcons.google,
        color: Colors.red,
        size: MediaQuery.of(context).size.height * 0.05,
      ),
      AppLocalizations.of(context)!.googleLogin,
      () async {
        setState(() => loading = true);
        dynamic result = await _auth.signInWithGoogle();
        if (result == null) {
          if (mounted) setState(() {
            error = AppLocalizations.of(context)!.incorrectGoogleCredentials;
            loading = false;
          });
        }
      },
    );
  }

  //Bottone per accedere con Facebook
  Widget _facebookLoginButton() {
    return loginButtonWithIcon(
      context,
      FaIcon(
        FontAwesomeIcons.facebook,
        color: Colors.blue,
        size: MediaQuery.of(context).size.height * 0.05,
      ),
      AppLocalizations.of(context)!.facebookLogin,
      () async {
        setState(() => loading = true);
        dynamic result = await _auth.facebookLogin();
        if (result == null) {
          setState(() {
            error = AppLocalizations.of(context)!.incorrectGoogleCredentials;
            loading = false;
          });
        }
      },
    );
  }

  //Bottone per accedere con l'email
  Widget _emailLoginButton() {
    return loginButtonWithIcon(
      context,
      FaIcon(
        FontAwesomeIcons.at,
        color: Colors.black,
        size: MediaQuery.of(context).size.height * 0.05,
      ),
      AppLocalizations.of(context)!.emailLogin,
      () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      },
    );
  }

  Widget _signinButton() {
    return confirmationButton(
      context,
      AppLocalizations.of(context)!.singIn,
      () async {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SignUpPage()));
      },
    );
  }

  //Informativa sulla versione dell'app
  Widget _appVersion() {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.20,
        bottom: MediaQuery.of(context).size.height * 0.004,
      ),
      child: Container(
        child: appVersion(context, MediaQuery.of(context).size.width * 0.035),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //lista dei widget che saranno incolonnati nella pagina
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_logo());
    columnElementList.add(_googleLoginButton());
    columnElementList.add(_facebookLoginButton());
    columnElementList.add(_emailLoginButton());
    columnElementList.add(_signinButton());
    columnElementList.add(_appVersion());
    return genericPage(context, false,
        columnElementList: columnElementList, appbar: false);
  }
}
