import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/logo_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final AuthService _auth = AuthService();

  /*
   *  Mappa contenete la lista degli errori ricevuti da Host e il corrispettivo messaggio di errore da mostrare all'utente 
   */
  final Map<String, String> signUpErrorMessageMap = {
    "email-already-in-use": "Email già registrata",
    "invalid-email": "Email non valida",
    "operation-not-allowed": "Account bloccato",
    "weak-password": "Password debole"
  };

  //serve per effettuare la validazione dei campi in input
  //si associa al widget Form per
  final _formkey = GlobalKey<FormState>();

  //booleano usato per attivare lo spinner
  bool loading = false;

  //text field state
  String username = "";
  String email = "";
  String mobilePhone = "";
  String password = "";
  String confirmPassword = "";
  String error = "";

  //Logo con nome
  Widget _logo() {
    return Padding(
      padding: EdgeInsets.only(),
      child: logoAndNameRegistration(context),
    );
  }

  TextInputType getTextInput(bool? isPhoneNumber, bool? isEmail) {
    if (isPhoneNumber != null && isPhoneNumber) return TextInputType.phone;
    if (isEmail != null && isEmail) return TextInputType.emailAddress;
    return TextInputType.name;
  }

  //Viene costruito il form per la registrazione

  //Costruisce i form  Username, Email, Num telefonico e Password
  Widget _entryField(String title,
      {bool isPassword = false,
      bool isPhoneNumber = false,
      bool isEmail = false,
      bool isConfirmPassword = false}) {
    return noIconTextFormFieldCustom(context,
        keyboardType: getTextInput(isPhoneNumber, isEmail),
        obscureText: (isPassword || isConfirmPassword),
        hintText: title,
        validator: (val) {
      ///validazione password
      if (isPassword) {
        return val!.length < 6
            ? AppLocalizations.of(context)!.passwordLength
            : null;

        ///validazione email
      } else if (isEmail) {
        return validateEmail(val!);

        ///validazione numero telefonico
      } else if (isPhoneNumber) {
        return validateMobile(val!);
      } else if (isConfirmPassword) {
        return validatePassword(val!, password);
      } else {
        return validateUsername(val!);
      }
    }, onChanged: (val) {
      setState(() {
        //non si può usare uno switch case in quanto l'oggetto AppLocalizations è dinamico
        if (title == AppLocalizations.of(context)!.username) {
          username = val!;
        } else if (title == AppLocalizations.of(context)!.email) {
          email = val!;
        } else if (title == AppLocalizations.of(context)!.password) {
          password = val!;
        } else if (title == AppLocalizations.of(context)!.confirmPassword) {
          confirmPassword = val!;
        } else if (title == AppLocalizations.of(context)!.phoneNumber) {
          mobilePhone = val!;
        }
      });
    },);
  }

  /*
   * Tasto di conferma per la registrazione
   */
  Widget _submitButton() {
    return confirmationButton(
      context,
      AppLocalizations.of(context)!.singIn,
      () async {
        if (_formkey.currentState!.validate()) {
          setState(() => loading = true);
          _auth
              .registerWithEmailAndPassword(email, password, mobilePhone, "",
                  username: username)
              .then((result) {
            if (result == null) {
              setState(() {
                error = AppLocalizations.of(context)!.enterValidEmail;
                loading = false;
              });
            }
          }).whenComplete(() {
            //Se non ho ricevuto messaggi di errori la registrazione è andata buon fine
            if (_auth.getErrorMessage().isEmpty) {
              Navigator.pop(context);
            } else {
              print(_auth.getErrorMessage());
              error = signUpErrorMessageMap[_auth.getErrorMessage()]!;
              loading = false;
              //Se ho ricevuto messaggi di errori salvo il tutto nella variabile error e risetto il
              //messaggio di errore statico a "" per gestire errori successivi
              _auth.setErrorMessage("");
            }
          });
        }
      },
    );
  }

  /*
   *  Lista dei campi da compilare la per registrazione:
   *  @param: username
   *  @param: email
   *  @param: numero di telefono
   *  @param: password 
   */
  Widget _userInformationWidget() {
    return Column(
      children: <Widget>[
        _entryField(AppLocalizations.of(context)!.username),
        _entryField(AppLocalizations.of(context)!.email, isEmail: true),
        _entryField(AppLocalizations.of(context)!.phoneNumber,
            isPhoneNumber: true),
        _entryField(AppLocalizations.of(context)!.password, isPassword: true),
        _entryField(AppLocalizations.of(context)!.confirmPassword,
            isConfirmPassword: true),
      ],
    );
  }

  Widget _formCredential() {
    return Form(
        key: _formkey,
        child: Column(children: <Widget>[
          _userInformationWidget(),
          _submitButton(),
        ]));
  }

  /*
   *  Messaggi di errore che appare solo se si riceve un esito negativo dal back-end 
   */
  Widget _singUpErrorMessage() {
    return Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.02,
        ),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.65,
          child: Text(
            error,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.045,
                color: Colors.red,
                fontWeight: FontWeight.bold),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    //lista dei widget che saranno incolonnati nella pagina
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_logo());
    columnElementList.add(_formCredential());
    columnElementList.add(_singUpErrorMessage());

    return loading
        ? Loading()
        : genericPage(context, true,
            backButtonColor: customWhite,
            columnElementList: columnElementList,
            appbar: false,
            trasparent: false,
            noTitle: true);
  }
}
