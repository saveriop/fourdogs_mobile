import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/logo_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'login_page.dart';

class ResetPasswordFeedback extends StatefulWidget {

  @override
  _ResetPasswordFeedbackState createState() => _ResetPasswordFeedbackState();
}

class _ResetPasswordFeedbackState extends State<ResetPasswordFeedback> {
  //Serve per effettuare la validazione dei campi in input
  //Si associa al widget Form per
  final _formkey = GlobalKey<FormState>();

  //Booleano usato per attivare lo spinner
  bool loading = false;

  //Logo con nome
  Widget _logo() {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.17,
      ),
      child: logoAndName(context),
    );
  }

  Widget _confirmationMessage() {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.05,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.85,
        child: Text(AppLocalizations.of(context)!.resetPassConfirm,
            style: informationTitelStyle(context), textAlign: TextAlign.center),
      ),
    );
  }

  ///Costruisce il tasto per inviare le credenziali e loggarsi
  Widget _submitButton() {
    return confirmationButton(
      context,
      AppLocalizations.of(context)!.confirm,
      () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => LoginPage()),
        );
      },
    );
  }

  ///Mette insieme i campi email, password e il tasto per loggare e crea un form
  Widget _formCredential() {
    return Container(
      child: Form(
          key: _formkey,
          child: Column(children: <Widget>[
            _confirmationMessage(),
            _submitButton(),
          ])),
    );
  }

  @override
  Widget build(BuildContext context) {
    ///lista dei widget che saranno incolonnati nella pagina
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_logo());
    columnElementList.add(_formCredential());

    return loading
        ? Loading()
        : genericPage(context, false,
            backButtonColor: customWhite,
            columnElementList: columnElementList,
            appbar: false,
            trasparent: false,
            noTitle: false);
  }
}
