import 'package:flutter/material.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/adsLocation/province.dart';
import 'package:zampon/model/adsLocation/region.dart';

import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/app_bar_model.dart';
import 'package:zampon/shared/widgetModel/title_model.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FirstLoginLocationProviceFilters extends StatefulWidget {
  final Region? region;
  final String? uid;
  final bool selectedAvatar;

  FirstLoginLocationProviceFilters(
      {this.region, this.uid, this.selectedAvatar = false});

  @override
  _FirstLoginLocationProviceFiltersState createState() =>
      _FirstLoginLocationProviceFiltersState();
}

class _FirstLoginLocationProviceFiltersState
    extends State<FirstLoginLocationProviceFilters> {
  @override
  Widget build(BuildContext context) {
    TextStyle subTitleStyle = TextStyle(
        color: customPaleLiliac,
        fontSize: MediaQuery.of(context).size.height * 0.02,
        fontWeight: FontWeight.bold);

    return Scaffold(
      backgroundColor: customWhite,
      body: CustomScrollView(slivers: <Widget>[
        sliverAppBar(
          context,
          true,
        ),
        SliverToBoxAdapter(
            child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        )),
        SliverToBoxAdapter(
          child: titleRow(
              context,
              AppLocalizations.of(context)!.selectYourResearchArea,
              CustomIcons.i6_city_icon),
        ),
        SliverToBoxAdapter(
            child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        )),
        SliverToBoxAdapter(
            child: Padding(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.02,
                    right: MediaQuery.of(context).size.width * 0.02),
                child: Text(AppLocalizations.of(context)!.searchForRegion,
                    style: subTitleStyle))),
        SliverToBoxAdapter(
            child: Padding(
          padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.02,
              right: MediaQuery.of(context).size.width * 0.02),
          child: Card(
            child: ListTile(
              title: Text(this.widget.region?.id ?? "", style: subTitleStyle),
              onTap: () {
                new DatabaseServiceUser(uid: this.widget.uid ?? "").updateField(
                    DatabaseServiceUser.LOCATION_FILTER_LABEL,
                    this.widget.region?.id);
                Navigator.pop(context);
                if (this.widget.selectedAvatar) Navigator.pop(context);
              },
              trailing: Icon(
                CustomIcons.i6_city_icon,
                color: customPaleLiliac,
              ),
            ),
          ),
        )),
        SliverToBoxAdapter(
            child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.02,
        )),
        SliverToBoxAdapter(
            child: Padding(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.02,
                    right: MediaQuery.of(context).size.width * 0.02),
                child: Text(AppLocalizations.of(context)!.searchForProvince,
                    style: subTitleStyle))),
        SliverList(
            delegate: SliverChildListDelegate(
                List.generate(this.widget.region!.provinces.length, (idx) {
          List<Province> provinces = this.widget.region!.provinces;
          return Padding(
            padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.02,
                right: MediaQuery.of(context).size.width * 0.02),
            child: Card(
              child: ListTile(
                leading: Text(
                  provinces[idx].abbreviation ?? "",
                  style: TextStyle(
                    color: customPaleLiliac,
                    fontSize: MediaQuery.of(context).size.height * 0.03,
                  ),
                ),
                title: Text(provinces[idx].id,
                    style: TextStyle(
                        color: customPaleLiliac,
                        fontSize: MediaQuery.of(context).size.height * 0.02,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  new DatabaseServiceUser(uid: this.widget.uid ?? "").updateField(
                      DatabaseServiceUser.LOCATION_FILTER_LABEL,
                      this.widget.region?.id ?? "" +
                          "(" +
                          this.widget.region!.provinces[idx].abbreviation!    +
                          ")");
                  Navigator.pop(context);
                  if (this.widget.selectedAvatar) Navigator.pop(context);
                },
                trailing: Icon(
                  CustomIcons.i6_city_icon,
                  color: customPaleLiliac,
                ),
              ),
            ),
          );
        })))
      ]),
    );
  }
}
