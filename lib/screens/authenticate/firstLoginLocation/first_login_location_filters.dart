import 'package:flutter/material.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/adsLocation/ad_location.dart';
import 'package:zampon/model/adsLocation/region.dart';
import 'package:zampon/screens/authenticate/firstLoginLocation/first_login_location_province_filters.dart';
import 'package:zampon/services/firebase/firestone/location_dp.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/location_populate.dart';
import 'package:zampon/shared/widgetModel/app_bar_model.dart';
import 'package:zampon/shared/widgetModel/title_model.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FirstLoginLocationFilters extends StatefulWidget {
  final AdsLocation? location;
  final String? uid;
  final bool selectedAvatar;

  FirstLoginLocationFilters(
      {this.location, this.uid, this.selectedAvatar = false});

  @override
  _FirstLoginLocationFiltersState createState() =>
      _FirstLoginLocationFiltersState();
}

class _FirstLoginLocationFiltersState extends State<FirstLoginLocationFilters> {
  Widget _regionSliverList(List<Region?>? regions) {
    return SliverList(
        delegate: SliverChildListDelegate(List.generate(regions!.length, (idx) {
      return Padding(
        padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width * 0.02,
          right: MediaQuery.of(context).size.width * 0.02,
        ),
        child: Card(
          child: ListTile(
            title: Text(regions[idx]!.id,
                style: TextStyle(
                    color: customPaleLiliac,
                    fontSize: MediaQuery.of(context).size.height * 0.02,
                    fontWeight: FontWeight.bold)),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FirstLoginLocationProviceFilters(
                          region: regions[idx],
                          uid: this.widget.uid,
                          selectedAvatar: this.widget.selectedAvatar)));
            },
            trailing: Icon(
              Icons.keyboard_arrow_right,
              color: customPaleLiliac,
            ),
          ),
        ),
      );
    })));
  }

  @override
  Widget build(BuildContext context) {
    List<Region>? regions = this.widget.location!.regions;
    return this.widget.location!.regions!.length == 0
        ? Scaffold(
            backgroundColor: Colors.white,
            appBar: customAppBar(context, isBackButton: false),
            bottomNavigationBar: TextButton.icon(
                icon: Icon(
                  Icons.add_photo_alternate,
                  color: customPaleLiliac,
                ),
                label: Text(
                  AppLocalizations.of(context)!.uploadRegionsList,
                ),
                onPressed: () {
                  new DatabaseServiceLocation().addAllLocation(populates());
                }),
          )
        : Scaffold(
            backgroundColor: Colors.white,
            body: CustomScrollView(slivers: <Widget>[
              sliverAppBar(context, false),
              SliverToBoxAdapter(
                  child: SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              )),
              SliverToBoxAdapter(
                  child: titleRow(
                      context,
                      AppLocalizations.of(context)!.selectYourResearchArea,
                      CustomIcons.i6_city_icon)),
              SliverToBoxAdapter(
                  child: SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              )),
              _regionSliverList(regions),
            ]),
          );
  }
}
