import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//import 'package:flutter_icons/flutter_icons.dart';
import 'package:zampon/screens/authenticate/reset_password_feedback.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';
import 'package:zampon/shared/widgetModel/elevated_button_model.dart';
import 'package:zampon/shared/widgetModel/loading_model.dart';
import 'package:zampon/shared/widgetModel/logo_model.dart';
import 'package:zampon/shared/widgetModel/scaffold_model.dart';
import 'package:zampon/shared/widgetModel/text_form_field_model.dart';

class ForgotPassword extends StatefulWidget {

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  //Serve per effettuare la validazione dei campi in input
  //Si associa al widget Form per
  final _formkey = GlobalKey<FormState>();

  //Booleano usato per attivare lo spinner
  bool loading = false;

  //Text field state
  String email = "";
  String password = "";
  String error = "";

  //Logo con nome
  Widget _logo() {
    return Padding(
      padding: EdgeInsets.only(),
      child: logoAndName(context),
    );
  }

  //Viene costruito il form per il login

  //Costruisce il form Email
  Widget _entryField() {
    return noIconTextFormFieldCustom(
      context,
      obscureText: false,
      hintText: AppLocalizations.of(context)!.insertHereYourEmail,
      onChanged: (val) {
        setState(() {
          email = val!;
        });
      },
    );
  }

  ///Costruisce il tasto per inviare le credenziali e loggarsi
  Widget _submitButton() {
    return resetPassButtonWithIcon(
      context,
      Icon(
        //TODO: ICONA NON NULL SAFE(IMPOSTATA UNA A CASO) -> MaterialIcons.autorenew,
        Icons.error,
        color: customWhite,
        size: MediaQuery.of(context).size.height * 0.05,
      ),
      AppLocalizations.of(context)!.resetPassword,
      () {
        if (email.isNotEmpty && null == validateEmail(email)) {
          AuthService().resetPassword(email);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ResetPasswordFeedback()),
          );
        }
      },
    );
  }

  ///Mette insieme i campi email, password e il tasto per loggare e crea un form
  Widget _formCredential() {
    return Container(
      child: Form(
          key: _formkey,
          child: Column(children: <Widget>[
            _entryField(),
            _submitButton(),
          ])),
    );
  }

  //Label di inserimento mail per reset password
  Widget _advertiseLabel() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.65,
      child: Text(
        AppLocalizations.of(context)!.insertMailToResetPass,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: MediaQuery.of(context).size.width * 0.045,
            color: customWhite,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    ///lista dei widget che saranno incolonnati nella pagina
    List<Widget> columnElementList = <Widget>[];
    columnElementList.add(_logo());
    columnElementList.add(_formCredential());
    columnElementList.add(_advertiseLabel());

    return loading
        ? Loading()
        : genericPage(context, true,
            backButtonColor: customWhite,
            columnElementList: columnElementList,
            appbar: false,
            trasparent: false,
            noTitle: false);
  }
}
