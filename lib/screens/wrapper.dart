import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:zampon/screens/first_login_wrapper.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';
import 'package:provider/provider.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/authenticate/welcome_page.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final UserData? user = Provider.of<UserData?>(context);
            return StreamProvider<UserData>.value(
              value: DatabaseServiceUser(uid: snapshot.data!.uid).userData,
              initialData: user == null 
              ? UserData(uid: snapshot.data!.uid) 
              : user,
              child: FirstLoginWrapper(tabIndex: 3),
            );
          }
          return WelcomePage();
        });
  }
}
