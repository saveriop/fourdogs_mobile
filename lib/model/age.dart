class Age {
  int? months;
  int? years;

  Age({this.months, this.years});

  static String petYears(Age? age) {
    if (age!.years != null && age.years! > 0) {
      return age.years.toString();
    } else {
      return "0";
    }
  }

  static String petMonths(Age? age) {
    if (age!.months != null && age.months! > 0) {
      return age.months.toString();
    } else {
      return "0";
    }
  }

  //Conversione in JSON per il salvataggio su firebase
  static Map<String, dynamic> toJson(Age age) => {
        'months': age.months,
        'years': age.years,
      };
}
