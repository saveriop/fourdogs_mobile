class Contact {
  String? userUid;
  String? emailForAd;
  String? mobilePhoneForAd;

  Contact({this.userUid, this.emailForAd, this.mobilePhoneForAd});
}
