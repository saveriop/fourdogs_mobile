///Questa classe rappresenta l'oggetto utilizzato per segnalare un annuncio
class ReportedAd {
  String? id;
  bool? saleAd;
  bool? inappropriateContent;
  
  ReportedAd({
    this.id,
    this.saleAd,
    this.inappropriateContent,
  });

  ///Conversione dell'oggetto in JSON
  Map toJson() => {
        'id': id,
        'saleAd': saleAd,
        'inappropriateContent': inappropriateContent,
      };  
}
