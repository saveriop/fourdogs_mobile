import 'package:zampon/model/age.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/enum/gender.dart';
import 'package:zampon/model/enum/pet_category.dart';
import 'package:zampon/model/location.dart';

class AdClosedElements {
  String? id;
  String? uid;
  AdCategory? adCategory;
  String? adDate;
  Location? adLocation;
  Age? petAge;
  PetCategory? petCategory;
  PetGender? petGender;
  String? petName;
  String? reason;
  String? closedDate;

  AdClosedElements({
    this.id,
    this.uid,
    this.adCategory,
    this.adDate,
    this.adLocation,
    this.petAge,
    this.petCategory,
    this.petGender,
    this.petName,
    this.reason,
    this.closedDate,
  });
}
