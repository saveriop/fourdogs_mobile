import 'package:zampon/model/filter/filter_gender.dart';
import 'package:zampon/model/filter/filter_size.dart';
import 'package:zampon/model/filter/filter_species.dart';

class Filter {
  FilterGender filterGender;
  FilterSize filterSize;
  FilterSpecies filterSpecies;

  Filter({required this.filterGender, required this.filterSize, required this.filterSpecies});

  ///Restituisce true se è stato applicato il filtro
  bool isFilterApplied() {
    if (filterGender.isFilterApplied() ||
        filterSize.isFilterApplied() ||
        filterSpecies.isFilterApplied()) {
      return true;
    }
    return false;
  }
}
