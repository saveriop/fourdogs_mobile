import 'package:zampon/model/adsLocation/province.dart';

class Region {
  //nome della regione
  String id;
  //numero degli annunci della regione delle adozioni
  int adsAdoptionsNumber;
  //numero degli annunci della regione delle adozioni
  int adsDisappearsNumber;
  //lista delle province
  List<Province> provinces;

  Region(
      {required this.id,
      required this.adsAdoptionsNumber,
      required this.adsDisappearsNumber,
      required this.provinces});

  Map toJson() {
    List<Map<String, dynamic>>? provinces = this.provinces.map((i) => i.toJson()).toList();

    return {
      "id": id,
      "adsAdoptionsNumber": adsAdoptionsNumber,
      "adsDisappearsNumber": adsDisappearsNumber,
      "provinces": provinces
    };
  }
}
