class Province {
  //nome della provincia
  String id;
  //numero degli annunci della provincia delle adozioni
  int adsAdoptionsNumber;
  //numero degli annunci della provincia delle scomparse
  int adsDisappearsNumber;
  //abbreviazione delle regione
  String? abbreviation;

  Province({required this.id, required this.adsAdoptionsNumber, required this.adsDisappearsNumber, this.abbreviation});

      Map<String, dynamic> toJson() => 
    {
    'id': id,
    'adsAdoptionsNumber':adsAdoptionsNumber,
    'adsDisappearsNumber':adsDisappearsNumber,
    'abbreviation':abbreviation
  }; 


}