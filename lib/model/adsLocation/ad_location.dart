import 'package:zampon/model/adsLocation/region.dart';

class AdsLocation {
  List<Region>? regions;
  AdsLocation({required this.regions});
  Map toJson() {
    List<Map>? regions = this.regions!.map((i) => i.toJson()).toList();

    return {"regions": regions};
  }
}
