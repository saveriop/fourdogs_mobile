class UserModel {
  final String? uid;

  UserModel({this.uid});
}

class UserData {
  final String uid;
  final String? username;
  final String? email;
  final String? mobilePhone;
  final String? profileImgUrl;
  late final String? locationFilter;
  final bool? sso;
  final List<String>? favoriteAds;
  final String? bio;

  UserData(
      {required this.uid,
      this.username,
      this.email,
      this.mobilePhone,
      this.profileImgUrl,
      this.locationFilter,
      this.sso,
      this.favoriteAds, 
      this.bio});

  /*
   * se almeno un campo dell'oggetto è null restituisce TRUE
   */
  bool checkIfAnyIsNull() {
    return [uid, username, email, mobilePhone, locationFilter, sso, favoriteAds, bio].contains(null);
  }

    /*
   * se almeno un campo dell'oggetto è null restituisce TRUE
   */
  bool checkIflocationFilterIsNull() {
    return [locationFilter].contains(null);
  }
}
