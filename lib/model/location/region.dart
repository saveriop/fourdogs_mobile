class Region {
    String? name;
    int? adoption;
    int? disappear;
    int? found;

    Region({ this.name,this.adoption, this.disappear, this.found});
}