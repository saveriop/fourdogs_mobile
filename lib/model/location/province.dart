class Province {
    String? province;
    String? name;
    String? region;
    int? adoption;
    int? disappear;
    int? found;

    Province({this.province, this.name, this.region, this.adoption, this.disappear, this.found});
}