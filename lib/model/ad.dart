import 'package:zampon/model/contact.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/enum/gender.dart';
import 'package:zampon/model/enum/pet_category.dart';
import 'package:zampon/model/enum/pet_size.dart';

import 'location.dart';
import 'age.dart';

class AdElements {
  String? id;
  String petName;
  Location? adLocation;
  PetGender? petGender;
  PetCategory? petCategory;
  Age? petAge;
  String? adDate;
  PetSize? petSize;
  int? photoNumber;
  String? adDescription;
  AdCategory? adCategory;
  Contact? contact;
  bool? isClosed;
  int? views;

  AdElements({
    this.id,
    required this.petName,
    this.adLocation,
    this.petGender,
    this.petCategory,
    this.petAge,
    this.adDate,
    this.petSize,
    this.photoNumber,
    this.adDescription,
    this.adCategory,
    this.contact,
    this.isClosed,
    this.views,
  });

  ///Conversione dell'oggetto privo dell'ID
  Map toJson() => {
        'petName': petName,
        'petGender': petGender.toString().split('.').last,
        'petCategory': petCategory.toString().split('.').last,
        'petSize': petSize.toString().split('.').last,
        'photoNumber': photoNumber,
        'adCategory': adCategory.toString().split('.').last,
        'adDate': adDate,
        'adDescription': adDescription,
        'contact': {
          'emailForAd': contact?.emailForAd,
          'mobilePhoneForAd': contact?.mobilePhoneForAd,
          'userUid': contact?.userUid
        },
        'age': {
          'months': petAge?.months,
          'years': petAge?.years,
        },
        'adLocation': {
          'city': adLocation?.city,
          'province': adLocation?.province,
          'region': adLocation?.region
        },
        'closed':isClosed,
        'views':views
      };

  @override
  String toString() {
    return "AdElements(" +
        " id: $id, petName: $petName," +
        " adLocation.city: " +
        adLocation!.city! +
        " adLocation.province: " +
        adLocation!.province! +
        " adLocation.region: " +
        adLocation!.region! +
        " petGender: " +
        petGender.toString() +
        " petCategory: " +
        petCategory.toString() +
        " petAge.months: " +
        petAge!.months.toString() +
        " petAge.years: " +
        petAge!.years.toString() +
        " adDate: " +
        adDate! +
        " petSize: " +
        petSize.toString() +
        " photoNumber: " +
        photoNumber.toString() +
        " adDescription: " +
        adDescription! +
        " adCategory: " +
        adCategory.toString() +
        " contact.emailForAd : " +
        contact!.emailForAd! +
        " contact.mobilePhoneForAd : " +
        contact!.mobilePhoneForAd! +
        " contact.userUid : " +
        contact!.userUid! +
        ")";
  }
}

AdElements initAdElement() {
  return AdElements(
      adCategory: AdCategory.Adoption,
      petName: "",
      adLocation: null,
      petGender: PetGender.MALE,
      petCategory: PetCategory.DOG,
      adDate: "",
      petSize: PetSize.SMALL,
      photoNumber: 0,
      adDescription: "",
      contact: Contact(emailForAd: "", mobilePhoneForAd: "", userUid: ""));
}
