class Location {
  String? city;
  String? province;
  String? region;
  String? country;

  Location({this.city, this.province, this.region, this.country});

  static String? locationStr(Location? location){
    if(location!.city!.isEmpty) return "";
    return location.region! + " , " + location.province! + " , " + location.city!;
  }

    static String? locationStrOnTwoLine(Location? location){
    if(location!.city!.isEmpty) return "";
    return location.region! + " , " + location.province! + " \n " + location.city!;
  }

   static String initialValue(Location location){
    if(location.city!.isEmpty) return "";
    return location.city!+ " , " +  location.province! + " , " + location.region!;
  }

  static Location getLocation(String location){
    var arr = location.split(' , ');
    return Location(city: arr[0], province: arr[1], region: arr[2]);
  }

  /*
     *  Conversione in JSON per il salvataggio su firebase 
     */
   static Map<String, dynamic> toJson(Location location) =>
  {
    'city': location.city,
    'province': location.province,
    'region': location.region,
  };

}