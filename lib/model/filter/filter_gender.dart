class FilterGender {
  
  bool isMale;
  bool isFemale;
  bool isOtherGen;

FilterGender({this.isMale = false, this.isFemale = false, this.isOtherGen = false});

  ///Restituisce true se è stato applicato il filtro
  bool isFilterApplied() {
    if ((isMale && isFemale && isOtherGen) || (!isMale && !isFemale && !isOtherGen) ) {
      return false;
    }
    return true;
  }

}