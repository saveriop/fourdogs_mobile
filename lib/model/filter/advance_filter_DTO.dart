import 'package:zampon/main.dart';

class AdvanceFilterDTO {
  List<String>? petcategoryList;
  List<String>? petgenderList;
  List<String>? petsizeList;

  ///label rappresentanti i campi del DTO
  static const String FEMALE = "FEMALE";
  static const String MALE = "MALE";
  static const String OTHER = "OTHER";
  static const String CAT = "CAT";
  static const String DOG = "DOG";
  static const String SMALL = "SMALL";
  static const String MEDIUM = "MEDIUM";
  static const String BIG = "BIG";

  AdvanceFilterDTO(
      { this.petcategoryList,  this.petgenderList,  this.petsizeList});

  ///converte l'oggetto in JSON
  Map toJson() => {
        "petcategoryList": petcategoryList,
        "petgenderList": petgenderList,
        "petsizeList": petsizeList
      };

  ///Questo metodo serve per comporre il DTO necessario al BACK-END per erogare i risultati filtrati
  AdvanceFilterDTO? populateAdvaceFilter() {
    ///se il filtro non è stato applicato il metodo ritorna null
    if (!MyApp.filter.isFilterApplied()) {
      return null;
    }

    ///Verifica lo stato di ogni singolo filtro
    ///Solo se il filtro è stato applicato allora alimenta la corrispettiva lista necessaria al Back-End
    AdvanceFilterDTO advanceFilter = AdvanceFilterDTO();
    advanceFilter.petcategoryList = [];
    advanceFilter.petgenderList = [];
    advanceFilter.petsizeList = [];

    ///Filtro specifico del genere
    if (MyApp.filter.filterGender.isFilterApplied()) {
      if (MyApp.filter.filterGender.isFemale)
        advanceFilter.petgenderList!.add(FEMALE);
      if (MyApp.filter.filterGender.isMale)
        advanceFilter.petgenderList!.add(MALE);
      if (MyApp.filter.filterGender.isOtherGen)
        advanceFilter.petgenderList!.add(OTHER);
    }

    ///Filtro specifico della specie animale
    if (MyApp.filter.filterSpecies.isFilterApplied()) {
      if (MyApp.filter.filterSpecies.isCat)
        advanceFilter.petcategoryList!.add(CAT);
      if (MyApp.filter.filterSpecies.isDog)
        advanceFilter.petcategoryList!.add(DOG);
      if (MyApp.filter.filterSpecies.isOther)
        advanceFilter.petcategoryList!.add(OTHER);
    }

    ///Filtro specifico della taglia dell'animale
    if (MyApp.filter.filterSize.isFilterApplied()) {
      if (MyApp.filter.filterSize.isSmall) advanceFilter.petsizeList!.add(SMALL);
      if (MyApp.filter.filterSize.isMedium)
        advanceFilter.petsizeList!.add(MEDIUM);
      if (MyApp.filter.filterSize.isLarge) advanceFilter.petsizeList!.add(BIG);
    }
    return advanceFilter;
  }
}
