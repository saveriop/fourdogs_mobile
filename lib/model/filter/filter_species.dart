class FilterSpecies {
  
  bool isDog;
  bool isCat;
  bool isOther;

FilterSpecies({this.isDog = false, this.isCat = false, this.isOther = false});

  ///Restituisce true se è stato applicato il filtro
  bool isFilterApplied() {
    if ((isDog && isCat && isOther) || (!isDog && !isCat && !isOther)) {
      return false;
    }
    return true;
  }

}