class FilterSize {
  
  bool isSmall;
  bool isMedium;
  bool isLarge;

FilterSize({this.isSmall = false, this.isMedium = false, this.isLarge = false});

  ///Restituisce true se è stato applicato il filtro
  bool isFilterApplied() {
    if ((isSmall && isMedium && isLarge) || (!isSmall && !isMedium && !isLarge)) {
      return false;
    }
    return true;
  }

}