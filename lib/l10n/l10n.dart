import 'package:flutter/material.dart';

class L10n {
  static final all = [
    const Locale('it'),
    const Locale('en'),
  ];

  static String getFlag(String code) {
    switch (code) {
      case 'en':
        return '🇺🇸';
      case 'it':
      default:
        return '🇮🇹';
    }
  }
}
