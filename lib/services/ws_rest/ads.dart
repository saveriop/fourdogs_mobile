import 'dart:convert';
import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:intl/intl.dart';

import 'package:zampon/model/ad.dart';
import 'package:zampon/model/age.dart';
import 'package:zampon/model/contact.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/enum/gender.dart';
import 'package:zampon/model/enum/pet_category.dart';
import 'package:zampon/model/enum/pet_size.dart';
import 'package:zampon/model/filter/advance_filter_DTO.dart';
import 'package:zampon/model/location.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/services/ws_rest/http_handler.dart';
import 'package:zampon/shared/validate.dart';

///Classe dedicata alla comunicazione con il WS rest /ads
///Tutti i campi del JSON in risposta sono stati censiti e labellati con le apposite costanti
///Viene utilizzata la libreria Dio per effettuare le chiamate HTTP verso il WS rest
class Ads extends HttpHandler {
  /// Costruttore standard
  Ads();

  ///label rappresentanti i campi del DTO
  static const String ID_LABEL = "id";
  static const String PET_NAME_LABEL = "petName";
  static const String PET_GENDER_LABEL = "petGender";
  static const String PET_CATEGORY_LABEL = "petCategory";
  static const String PET_SIZE_LABEL = "petSize";
  static const String PHOTO_NUMBER_LABEL = "photoNumber";
  static const String AD_CATEGORY_LABEL = "adCategory";
  static const String AD_DATE_LABEL = "adDate";
  static const String AD_DESCRIPTION_LABEL = "adDescription";
  static const String CONTACT_LABEL = "contact";
  static const String USER_UID_LABEL = "userUid";
  static const String AD_EMAIL_LABEL = "emailForAd";
  static const String AD_MOBILE_PHONE_LABEL = "mobilePhoneForAd";
  static const String PET_AGE_LABEL = "age";
  static const String PET_AGE_MONTHS_LABEL = "months";
  static const String PET_AGE_YEARS_LABEL = "years";
  static const String AD_LOCATION_LABEL = "adLocation";
  static const String AD_LOCATION_CITY_LABEL = "city";
  static const String AD_LOCATION_PROVINCE_LABEL = "province";
  static const String AD_LOCATION_REGION_LABEL = "region";
  static const String AD_VIEWS_LABEL = "views";
  static const String AD_IS_CLOSED_LABEL = "closed";

  ///Booleana che indica che è stata letta l'ultima pagina
  ///Se è true eviterà di effettuare nuove chiamate HTTP a vuoto
  ///Se è false l'applicativa effettuerà nuove chiamate verso il back-end ricevendo nuove info in maniera paginata
  ///Ad ogni init viene inizializzata a false
  ///In ogni nextPage, quando si riceve una lista vuota dal back-end viene valorizzata con true
  static bool isLastPage = false;

  String _uri = "/ads";

  ///Esempio di URL filtrando per provincia:
  ///http://167.86.98.159:8081/ads/PUGLIA/BA/0/5?adCategory=Disappear
  ///Esempio di URL filtrando per regione:
  ///http://167.86.98.159:8081/ads/PUGLIA/0/5?adCategory=Adoption
  ///Path parameters richiesti: province, region, pageIndex, pageSize
  void _makeUri(
      {String? province, String? region, int? pageIndex, int? pageSize}) {
    if (province != null && province.isNotEmpty) {
      _uri = '/ads/$region/$province/$pageIndex/$pageSize';
    } else {
      _uri = '/ads/$region/$pageIndex/$pageSize';
    }
  }

  ///Questo metodo viene richiamato solo tramite il deeplink dello share
  ///pertanto questo metodo gestisce la descrizione
  AdElements? _parseSingleAdElement(dynamic data) {
    return AdElements(
      id: data[ID_LABEL],
      petName: data[PET_NAME_LABEL],
      petGender:
          EnumToString.fromString(PetGender.values, data[PET_GENDER_LABEL]),
      petCategory: EnumToString.fromString(
        PetCategory.values,
        data[PET_CATEGORY_LABEL],
      ),
      petSize: EnumToString.fromString(
        PetSize.values,
        data[PET_SIZE_LABEL],
      ),
      photoNumber: data[PHOTO_NUMBER_LABEL],
      adCategory: EnumToString.fromString(
        AdCategory.values,
        data[AD_CATEGORY_LABEL],
      ),
      adDate: data[AD_DATE_LABEL].toString().substring(0, 10),
      adDescription: data[AD_DESCRIPTION_LABEL],
      contact: Contact(
        userUid: data[CONTACT_LABEL][USER_UID_LABEL],
        emailForAd: data[CONTACT_LABEL][AD_EMAIL_LABEL],
        mobilePhoneForAd: data[CONTACT_LABEL][AD_MOBILE_PHONE_LABEL],
      ),
      petAge: Age(
        months: data[PET_AGE_LABEL][PET_AGE_MONTHS_LABEL],
        years: data[PET_AGE_LABEL][PET_AGE_YEARS_LABEL],
      ),
      adLocation: Location(
        city: data[AD_LOCATION_LABEL][AD_LOCATION_CITY_LABEL],
        province: data[AD_LOCATION_LABEL][AD_LOCATION_PROVINCE_LABEL],
        region: data[AD_LOCATION_LABEL][AD_LOCATION_REGION_LABEL],
      ),
      isClosed: data[AD_IS_CLOSED_LABEL],
      views: data[AD_VIEWS_LABEL],
    );
  }

  /// Parsa un singolo oggetto ricevuto dal WS Rest per convertirlo in un oggetto AdElement
  AdElements _parseAdElement(dynamic data, int i) {
    return AdElements(
      id: data[i][ID_LABEL],
      petName: data[i][PET_NAME_LABEL],
      petGender:
          EnumToString.fromString(PetGender.values, data[i][PET_GENDER_LABEL]),
      petCategory: EnumToString.fromString(
        PetCategory.values,
        data[i][PET_CATEGORY_LABEL],
      ),
      petSize: EnumToString.fromString(
        PetSize.values,
        data[i][PET_SIZE_LABEL],
      ),
      photoNumber: data[i][PHOTO_NUMBER_LABEL],
      adCategory: EnumToString.fromString(
        AdCategory.values,
        data[i][AD_CATEGORY_LABEL],
      ),
      adDate: data[i][AD_DATE_LABEL].toString().substring(0, 10),
      //adDescription: data[i][AD_DESCRIPTION_LABEL],
      contact: Contact(
        userUid: data[i][CONTACT_LABEL][USER_UID_LABEL],
        emailForAd: data[i][CONTACT_LABEL][AD_EMAIL_LABEL],
        mobilePhoneForAd: data[i][CONTACT_LABEL][AD_MOBILE_PHONE_LABEL],
      ),
      petAge: Age(
        months: data[i][PET_AGE_LABEL][PET_AGE_MONTHS_LABEL],
        years: data[i][PET_AGE_LABEL][PET_AGE_YEARS_LABEL],
      ),
      adLocation: Location(
        city: data[i][AD_LOCATION_LABEL][AD_LOCATION_CITY_LABEL],
        province: data[i][AD_LOCATION_LABEL][AD_LOCATION_PROVINCE_LABEL],
        region: data[i][AD_LOCATION_LABEL][AD_LOCATION_REGION_LABEL],
      ),
      isClosed: data[i][AD_IS_CLOSED_LABEL] ?? false,
      views: data[i][AD_VIEWS_LABEL] ?? 0,
    );
  }

  /// Permette di ottenere la lista di tutti i nomi degli annunci
  /// Questa ricerca ignorerà i filtri regionali
  Future<List<String>> getAllPetName() async {
    List<String> allPetName = [];
    _uri = "/ads/allname";
    try {
      AuthService().initJwt();
      Response response = await HttpHandler.dio.get(_uri,
          options: Options(
            headers: HttpHandler.requestHeaders,
          ));
      final data = response.data;
      if (data is List && data.isNotEmpty) {
        for (int i = 0; i < data.length; i++) {
          allPetName.add(data[i]);
        }
      }
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return allPetName;
  }

  /// Permette di effettuare una ricerca di annunci per nome
  /// La ricerca sarà effettuata sul DB tramite la clausola %LIKE% e non ci sarà discriminante tra UPPER e LOWER CASE
  /// Questa ricerca ignorerà i filtri regionali
  Future<List<AdElements>> findByName(String petName) async {
    List<AdElements> listAds = [];
    _uri = "/ads/name/$petName";
    try {
      AuthService().initJwt();
      Response response = await HttpHandler.dio.get(_uri,
          options: Options(
            headers: HttpHandler.requestHeaders,
          ));
      final data = response.data;
      if (data is List && data.isNotEmpty) {
        for (int i = 0; i < data.length; i++) {
          listAds.add(_parseAdElement(data, i));
        }
      }
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }

  Future<List<AdElements>> initPageAdsCity(
      {String? city,
      int? pageIndex,
      int? pageSize,
      String? adCategory,
      List<String>? favoriteList}) async {

      city = city!.replaceAll("[", "").replaceAll("]", "");
    ///Init gestione ultima pagina
    isLastPage = false;
    List<AdElements> listAds = [];
    //compone l'uri in base ai pathparam ricevuti in input
    _uri = '/ads/city/$city/$pageIndex/$pageSize';
    //aggiungi i queryParameters
    _uri = _uri + '?adCategory=' + adCategory!;
    if (favoriteList != null) {
      favoriteList.forEach((element) {
        _uri = _uri + '&list=' + element;
      });
    }
    //_uri= _uri + '?adCategory='+adCategory + '&list='+'123e4567-e89b-42d3-a456-556642440001'+ '&list='+'123e4567-e89b-42d3-a456-556642440002';
    try {
      ///GESTIONE chiamata HTTP con cache
      ///La chiave della cache è date da "initPageAds" + la categoria della sezione
      var data = await cachedGetHttpRequest(
          uri: _uri, cacheKey: "initPageAds" + adCategory);
      if (data is List && data.isNotEmpty) {
        for (int i = 0; i < data.length; i++) {
          listAds.add(_parseAdElement(data, i));
        }
      }
      return listAds;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }

  Future<List<AdElements>?> nextPageAdsCity(
      {String? city,
      int? pageIndex,
      int? pageSize,
      String? adCategory,
      List<AdElements>? listAds,
      List<String>? favoriteList}) async {
      
      city = city!.replaceAll("[", "").replaceAll("]", "");
    //compone l'uri in base ai pathparam ricevuti in input
    _uri = '/ads/city/$city/$pageIndex/$pageSize';
    //aggiungi i queryParameters
    _uri = _uri + '?adCategory=' + adCategory!;
    if (favoriteList != null) {
      favoriteList.forEach((element) {
        _uri = _uri + '&list=' + element;
      });
    }
    try {
      ///se l'applicativo ha letto l'ultima pagina eviterà di effettuare chiamate HTTP superflue
      if (!isLastPage) {
        AuthService().initJwt();
        Response response = await HttpHandler.dio.get(_uri,
            options: Options(
              headers: HttpHandler.requestHeaders,
            ));
        final data = response.data;
        if (data is List && data.isNotEmpty) {
          for (int i = 0; i < data.length; i++) {
            listAds!.add(_parseAdElement(data, i));
          }
        } else {
          ///se si riceve una lista vuota significa che non ci saranno più annunci da richiedere
          ///pertanto significa che è stata letta l'ultima pagina di annunci disponibile
          isLastPage = true;
        }
      }

      return listAds;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }

  ///Questo metodo deve essere chiamato solo nella init state del Widget dedicato alla paginazione
  ///Serve ad inizializzare la lista degli elementi paginati per la prima volta
  ///Esempio di URL filtrando per provincia:
  ///http://167.86.98.159:8081/ads/PUGLIA/BA/0/5?adCategory=Disappear
  ///Esempio di URL filtrando per regione:
  ///http://167.86.98.159:8081/ads/PUGLIA/0/5?adCategory=Adoption
  ///Path parameters richiesti: province, region, pageIndex, pageSize
  ///Query parameters richiesti: adCategory (Adoption or Disappear)
  Future<List<AdElements>> initPageAds(
      {String? province,
      String? region,
      int? pageIndex,
      int? pageSize,
      String? adCategory,
      List<String>? favoriteList}) async {
    ///Init gestione ultima pagina
    isLastPage = false;
    List<AdElements> listAds = [];
    //compone l'uri in base ai pathparam ricevuti in input
    _makeUri(
        province: province,
        region: region,
        pageIndex: pageIndex,
        pageSize: pageSize);
    //aggiungi i queryParameters
    _uri = _uri + '?adCategory=' + adCategory!;
    if (favoriteList != null) {
      favoriteList.forEach((element) {
        _uri = _uri + '&list=' + element;
      });
    }
    //_uri= _uri + '?adCategory='+adCategory + '&list='+'123e4567-e89b-42d3-a456-556642440001'+ '&list='+'123e4567-e89b-42d3-a456-556642440002';
    try {
      ///GESTIONE chiamata HTTP con cache
      ///La chiave della cache è date da "initPageAds" + la categoria della sezione
      var data = await cachedGetHttpRequest(
          uri: _uri, cacheKey: "initPageAds" + adCategory);
      if (data is List && data.isNotEmpty) {
        for (int i = 0; i < data.length; i++) {
          listAds.add(_parseAdElement(data, i));
        }
      }
      return listAds;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }

  ///Questo metodo deve essere chiamato solo per aggiornare la lista degli elementi ottenendo una nuova pagina
  ///Viene richiamato per far scorrere la paginazione
  ///Esempio di URL filtrando per provincia:
  ///http://167.86.98.159:8081/ads/PUGLIA/BA/0/5?adCategory=Disappear
  ///Esempio di URL filtrando per regione:
  ///http://167.86.98.159:8081/ads/PUGLIA/0/5?adCategory=Adoption
  ///Path parameters richiesti: province, region, pageIndex, pageSize
  ///Query parameters richiesti: adCategory (Adoption or Disappear)
  Future<List<AdElements>?> nextPageAds(
      {String? province,
      String? region,
      int? pageIndex,
      int? pageSize,
      String? adCategory,
      List<AdElements>? listAds,
      List<String>? favoriteList}) async {
    //compone l'uri in base ai pathparam ricevuti in input
    _makeUri(
        province: province,
        region: region,
        pageIndex: pageIndex,
        pageSize: pageSize);
    //aggiungi i queryParameters
    _uri = _uri + '?adCategory=' + adCategory!;
    if (favoriteList != null) {
      favoriteList.forEach((element) {
        _uri = _uri + '&list=' + element;
      });
    }
    try {
      ///se l'applicativo ha letto l'ultima pagina eviterà di effettuare chiamate HTTP superflue
      if (!isLastPage) {
        AuthService().initJwt();
        Response response = await HttpHandler.dio.get(_uri,
            options: Options(
              headers: HttpHandler.requestHeaders,
            ));
        final data = response.data;
        if (data is List && data.isNotEmpty) {
          for (int i = 0; i < data.length; i++) {
            listAds!.add(_parseAdElement(data, i));
          }
        } else {
          ///se si riceve una lista vuota significa che non ci saranno più annunci da richiedere
          ///pertanto significa che è stata letta l'ultima pagina di annunci disponibile
          isLastPage = true;
        }
      }

      return listAds;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }

  ///Questo metodo deve essere chiamato solo nella init state del Widget dedicato alla paginazione
  ///degli annunci di uso specifico utente
  ///Serve ad inizializzare la lista degli elementi paginati per la prima volta
  ///Esempio di URL filtrando per provincia:
  ///http://167.86.98.159:8081/ads/myads/BA/0/5/RoHVOKQz5zfQILYsdnVjwgPYhw63
  ///Esempio di URL filtrando per regione:
  ///http://167.86.98.159:8081/ads/myads/0/5/RoHVOKQz5zfQILYsdnVjwgPYhw63
  ///Path parameters richiesti: province, region, pageIndex, pageSize, useruid
  Future<List<AdElements>> initPageMyAds({
    int? pageIndex,
    int? pageSize,
    String? useruid,
  }) async {
    ///Init gestione ultima pagina
    isLastPage = false;
    List<AdElements> listAds = [];
    _uri = '/ads/myads/$pageIndex/$pageSize/$useruid';
    try {
      AuthService().initJwt();
      Response response = await HttpHandler.dio.get(_uri,
          options: Options(
            headers: HttpHandler.requestHeaders,
          ));
      if (response.statusCode == 200) {
        final data = response.data;
        if (data is List && data.isNotEmpty) {
          for (int i = 0; i < data.length; i++) {
            listAds.add(_parseAdElement(data, i));
          }
        }
      }
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }

  ///Questo metodo deve essere chiamato solo per aggiornare la lista degli elementi ottenendo una nuova pagina
  ///Viene richiamato per far scorrere la paginazione
  ///Esempio di URL filtrando per provincia:
  ///http://167.86.98.159:8081/ads/myads/PUGLIA/BA/1/5/RoHVOKQz5zfQILYsdnVjwgPYhw63
  ///Esempio di URL filtrando per regione:
  ///http://167.86.98.159:8081/ads/myads/PUGLIA/1/5/RoHVOKQz5zfQILYsdnVjwgPYhw63
  ///Path parameters richiesti: province, region, pageIndex, pageSize , useruid
  Future<List<AdElements>?> nextPageMyAds({
    int? pageIndex,
    int? pageSize,
    String? useruid,
    List<AdElements>? listAds,
  }) async {
    _uri = '/ads/myads/$pageIndex/$pageSize/$useruid';

    ///se l'applicativo ha letto l'ultima pagina eviterà di effettuare chiamate HTTP superflue
    if (!isLastPage) {
      AuthService().initJwt();
      Response response = await HttpHandler.dio.get(_uri,
          options: Options(
            headers: HttpHandler.requestHeaders,
          ));
      if (response.statusCode == 200) {
        final data = response.data;
        if (data is List && data.isNotEmpty) {
          for (int i = 0; i < data.length; i++) {
            listAds!.add(_parseAdElement(data, i));
          }
        } else {
          ///se si riceve una lista vuota significa che non ci saranno più annunci da richiedere
          ///pertanto significa che è stata letta l'ultima pagina di annunci disponibile
          isLastPage = true;
        }
      }
    }
    return listAds;
  }

  ///Questo metodo deve essere chiamato solo nella init state del Widget dedicato alla paginazione
  ///Viene richiamato solo è stato applicato un filtro di ricerca
  ///Serve ad inizializzare la lista degli elementi paginati per la prima volta
  ///Esempio di URL filtrando per provincia:
  ///http://167.86.98.159:8081/ads/advance/PUGLIA/BA/0/5?adCategory=Adoption
  ///Esempio di URL filtrando per regione:
  ///http://167.86.98.159:8081/ads/advance/PUGLIA/0/5?adCategory=Adoption
  ///Path parameters richiesti: province, region, pageIndex, pageSize
  ///Si tratta di una chiamata HTTP POST in quanto in input richiede le liste dei valori per quale si
  ///necessita filtrare. L'oggetto presente nel body è definito dall'oggetto AdvanceFilterDTO
  Future<List<AdElements>> initPageAdvanceFilter(
      {String? province,
      String? region,
      int? pageIndex,
      int? pageSize,
      String? adCategory,
      String? city}) async {

    ///Init gestione ultima pagina
    isLastPage = false;
    List<AdElements> listAds = [];
    //compone l'uri in base ai pathparam ricevuti in input
    if (province != null && province.isNotEmpty) {
      _uri = '/ads/advance/$region/$province/$pageIndex/$pageSize';
    } else {
      _uri = '/ads/advance/$region/$pageIndex/$pageSize';
    }
    //aggiungi i queryParameters
    _uri = _uri + '?adCategory=' + adCategory!;


    if(city != null && city.isNotEmpty){
        city = city.replaceAll("[", "").replaceAll("]", "");
        _uri = _uri + '&city=' + city;
    }

    print("--------------------------------------------------------");
    print(_uri);
    print("--------------------------------------------------------");

    //Valorizzazione dei campi dell'header della richiesta HTTP
    try {
      AuthService().initJwt();
      Response response = await HttpHandler.dio.post(_uri,
          data: jsonEncode(AdvanceFilterDTO().populateAdvaceFilter()!.toJson()),
          options: Options(
            headers: HttpHandler.requestHeaders,
            responseType: ResponseType.json,
          ));
      final data = response.data;
      if (data is List && data.isNotEmpty) {
        for (int i = 0; i < data.length; i++) {
          listAds.add(_parseAdElement(data, i));
        }
      }
      return listAds;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }

  ///Questo metodo deve essere chiamato solo per aggiornare la lista degli elementi ottenendo una nuova pagina
  ///Viene richiamato solo è stato applicato un filtro di ricerca
  ///Serve ad inizializzare la lista degli elementi paginati per la prima volta
  ///Esempio di URL filtrando per provincia:
  ///http://167.86.98.159:8081/ads/advance/PUGLIA/BA/0/5?adCategory=Adoption
  ///Esempio di URL filtrando per regione:
  ///http://167.86.98.159:8081/ads/advance/PUGLIA/0/5?adCategory=Adoption
  ///Path parameters richiesti: province, region, pageIndex, pageSize
  ///Si tratta di una chiamata HTTP POST in quanto in input richiede le liste dei valori per quale si
  ///necessita filtrare. L'oggetto presente nel body è definito dall'oggetto AdvanceFilterDTO
  Future<List<AdElements>?> nextPagePageAdvanceFilter(
      {String? province,
      String? region,
      int? pageIndex,
      int? pageSize,
      String? adCategory,
      List<AdElements>? listAds,
      String? city}) async {
    //compone l'uri in base ai pathparam ricevuti in input
    if (province != null && province.isNotEmpty) {
      _uri = '/ads/advance/$region/$province/$pageIndex/$pageSize';
    } else {
      _uri = '/ads/advance/$region/$pageIndex/$pageSize';
    }
    //aggiungi i queryParameters
    _uri = _uri + '?adCategory=' + adCategory!;

    if(city != null && city.isNotEmpty){
        city = city.replaceAll("[", "").replaceAll("]", "");
        _uri = _uri + '&city=' + city;
    }
    try {
      ///se l'applicativo ha letto l'ultima pagina eviterà di effettuare chiamate HTTP superflue
      if (!isLastPage) {
        AuthService().initJwt();
        Response response = await HttpHandler.dio.post(_uri,
            data:
                jsonEncode(AdvanceFilterDTO().populateAdvaceFilter()!.toJson()),
            options: Options(
              headers: HttpHandler.requestHeaders,
              responseType: ResponseType.json,
            ));
        final data = response.data;
        if (data is List && data.isNotEmpty) {
          for (int i = 0; i < data.length; i++) {
            listAds!.add(_parseAdElement(data, i));
          }
        } else {
          ///se si riceve una lista vuota significa che non ci saranno più annunci da richiedere
          ///pertanto significa che è stata letta l'ultima pagina di annunci disponibile
          isLastPage = true;
        }
      }
      return listAds;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }

  ///Questo metodo permette di ottere un oggetto AdElement
  ///Richiede in input l'id dell'annuncio necessario al back-end per effettuare la query
  Future<AdElements?> getAdById(String id) async {
    AdElements? ad;
    _uri = "/ads/$id";
    AuthService().initJwt();
    Response response = await HttpHandler.dio.get(_uri,
        options: Options(
          headers: HttpHandler.requestHeaders,
        ));
    if (response.statusCode == 200) {
      final data = response.data;
      ad = _parseSingleAdElement(data);
    }
    return ad;
  }

  ///Questo metodo permette di creare un annuncio sul Back-end
  ///Effettua una chiamata POST passando in input l'oggetto AdElements convertito in JSON
  ///La data dell'oggetto da inviare deve seguire il seguente formato: 'yyyy-MM-dd hh:mm:ss'
  ///Restituisce l'id dell'annuncio in formato UUID
  ///In caso di eccezione restituisce ""
  Future<String> createAd(AdElements ad) async {
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd hh:mm:ss');
    String formattedDate = formatter.format(now);
    ad.adDate = formattedDate;
    ad.adLocation!.region = convertNameRegionToLabel(ad.adLocation!.region!);

    try {
      AuthService().initJwt();
      Response response = await HttpHandler.dio.post('/ads',
          data: jsonEncode(ad.toJson()),
          options: Options(
            headers: HttpHandler.requestHeaders,
            responseType: ResponseType.plain,
          ));
      return response.data;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return "";
  }

  ///Permette di richiedere la descrizione di uno specifico annuncio
  ///Inoltre ogni qual volta si richiama questo metodo viene incrementato il contatore delle visualizzazioni di quello specifico annuncio
  ///La richiesta della descrizione e l'incremento delle visualizzazioni sta in cache per 600 sec (10 min)
  Future<String> getDescription(String id, {cached = true}) async {
    String finalResponse = "";
    String cacheKey = id + "_desc";
    try {
      AuthService().initJwt();
      //lettura dei dati dalla cache
      FileInfo? file = await DefaultCacheManager().getFileFromCache(cacheKey);
      //viene letta la cache, se viene trovato un file e se il suo tempo di validità è valido lo usa
      //altrimenti rieffettua la chiamata HTTP
      if (cached && (file != null && file.validTill.isAfter(DateTime.now()))) {
        ///converto il file in stringa
        finalResponse = file.file.readAsStringSync();
      } else {
        Response response = await HttpHandler.dio.get('/ads/description/$id',
            options: Options(
              headers: HttpHandler.requestHeaders,
              responseType: ResponseType.plain,
            ));

        ///Dopo aver effettuato la chiamata HTTP converte la risposta in un JSON
        ///successivamente in Stringa e poi in un array di Int solo per poterlo convertire in binario
        ///una volta convertito in binario è possibile salvere il dato in cache
        List<int> list = utf8.encode(json.encode(response.data).toString());
        Uint8List unit8List = Uint8List.fromList(list);

        ///salvataggio risposta in cache
        DefaultCacheManager().putFile(cacheKey, unit8List,
            key: cacheKey, maxAge: Duration(seconds: 600));

        finalResponse = response.data;
      }
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return finalResponse;
  }

  ///Questo metodo permette di modificare un annuncio sul Back-end
  ///Effettua una chiamata PUT passando in input l'oggetto AdElements convertito in JSON
  Future<int?> modifyAd(AdElements ad) async {
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd hh:mm:ss');
    String formattedDate = formatter.format(now);
    ad.adDate = formattedDate;
    try {
      AuthService().initJwt();
      String? id = ad.id;
      Response response = await HttpHandler.dio.put('/ads/$id',
          data: jsonEncode(ad.toJson()),
          options: Options(
            headers: HttpHandler.requestHeaders,
            responseType: ResponseType.plain,
          ));
      return response.statusCode;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return 999;
  }

  ///Questo metodo permette di modificare un annuncio sul Back-end
  ///Effettua una chiamata PUT passando in input l'oggetto AdElements convertito in JSON
  Future<int?> closeAd(AdElements ad, String reason) async {
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd hh:mm:ss');
    String formattedDate = formatter.format(now);
    ad.adDate = formattedDate;
    try {
      AuthService().initJwt();
      String? id = ad.id;
      String? uid = ad.contact!.userUid;
      Response response = await HttpHandler.dio.delete('/ads/$id/$uid/$reason',
          options: Options(
            headers: HttpHandler.requestHeaders,
            responseType: ResponseType.plain,
          ));
      return response.statusCode;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return 999;
  }

  ///Questo metodo permette l'eliminazione definitiva, da DB di un annuncio
  ///Path di esempio:
  ///http://127.0.0.1:8081/ads/definitiveCancellation/123e4567-e89b-42d3-a456-556642440016/RoHVOKQz5zfQILYsdnVjwgPYhw63
  ///Richiede in input l'id dell'annuncio e l'id dell'utente(uid)
  Future<int?> definitiveCancellation(String id, String uid) async {
    try {
      Response response =
          await HttpHandler.dio.delete('/ads/definitiveCancellation/$id/$uid/',
              options: Options(
                headers: HttpHandler.requestHeaders,
                responseType: ResponseType.plain,
              ));
      return response.statusCode;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return 999;
  }
}
