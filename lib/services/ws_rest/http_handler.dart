import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';

///Classe generica per la gestione della comunicazione HTTP
class HttpHandler {
  /// Base Url del WS Rest
  static Dio dio = Dio(
   BaseOptions(baseUrl: 'http://167.86.98.159:8081'),
   //BaseOptions(baseUrl: 'http://10.0.2.2:8081'), //=> Indizzo LocalHost per i test in locale
  );

  ///Header di tutte le richieste http
  static Map<String, String> requestHeaders = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
    'Authorization': AuthService.jwt!
  };

  ///Metodo generico per la gestione delle chiamate GET con cache a 30 sec
  dynamic cachedGetHttpRequest({String? cacheKey, String? uri}) async {
    var finalResponse;
    //lettura dei dati dalla cache
    FileInfo? file = await DefaultCacheManager().getFileFromCache(cacheKey!);
    //viene letta la cache, se viene trovato un file e se il suo tempo di validità è valido lo usa
    //altrimenti rieffettua la chiamata HTTP
    if (file != null && file.validTill.isAfter(DateTime.now())) {
      ///converto il file in stringa
      finalResponse = file.file.readAsStringSync();

      ///converto la stringa in JSON
      finalResponse = json.decode(finalResponse);
    } else {
      try {
        AuthService().initJwt();
        Response response = await HttpHandler.dio.get(uri!,
            options: Options(
              headers: HttpHandler.requestHeaders,
            ));

        ///Dopo aver effettuato la chiamata HTTP converte la risposta in un JSON
        ///successivamente in Stringa e poi in un array di Int solo per poterlo convertire in binario
        ///una volta convertito in binario è possibile salvere il dato in cache
        List<int> list = utf8.encode(json.encode(response.data).toString());
        Uint8List unit8List = Uint8List.fromList(list);

        ///salvataggio risposta in cache
        DefaultCacheManager().putFile(cacheKey, unit8List,
            key: cacheKey, maxAge: Duration(seconds: 30));
        finalResponse = response.data;
      } on DioError catch (error) {
        print(error.message);
        //in caso di token JWT scaduto effettua il refresh
        if (error.response != null && error.response!.statusCode == 403) {
          print("Token expired. Try to refresh");
        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
        }
      }
    }
    return finalResponse;
  }
}
