import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:zampon/model/reported_ad.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/services/ws_rest/http_handler.dart';

class Reported extends HttpHandler {
  /// Costruttore standard
  Reported();

  ///label rappresentanti i campi del DTO
  static const String ID_LABEL = "id";
  static const String SALE_AD_LABEL = "saleAd";
  static const String INAPPROPRIATE_CONTENT_LABEL = "inappropriateContent";

  String _uri = "/reported";

  ///Questo metodo permette di riportare un annuncio sul Back-end
  ///Effettua una chiamata POST passando in input l'oggetto ReportedAd convertito in JSON
  ///La chiamata di report viene cachata a 5 min. Quindi si può segnalare un report ogni 5 min
  Future<void> createAd(ReportedAd reportedAd) async {
    String cacheKey = reportedAd.id! + "_reported";
    try {
      //lettura dei dati dalla cache
      FileInfo? file = await DefaultCacheManager().getFileFromCache(cacheKey);
      AuthService().initJwt();
            //viene letta la cache, se viene trovato un file e se il suo tempo di validità è valido lo usa
      //altrimenti rieffettua la chiamata HTTP
      if (!(file != null && file.validTill.isAfter(DateTime.now()))) {
      await HttpHandler.dio.post(_uri,
          data: jsonEncode(reportedAd.toJson()),
          options: Options(
            headers: HttpHandler.requestHeaders,
            responseType: ResponseType.plain,
          ));
        ///Dopo aver effettuato la chiamata HTTP converte la risposta in un JSON
        ///successivamente in Stringa e poi in un array di Int solo per poterlo convertire in binario
        ///una volta convertito in binario è possibile salvere il dato in cache
        List<int> list = utf8.encode(json.encode(cacheKey).toString());
        Uint8List unit8List = Uint8List.fromList(list);
        ///salvataggio risposta in cache
        DefaultCacheManager().putFile(cacheKey, unit8List,
            key: cacheKey, maxAge: Duration(seconds: 300));
      } else {
        print("CACHED report");
      }
    }  on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");
        ///se il token JWT dell'utente è scaduto effettua il refresh e riesegue il metodo in ricorsione
        new AuthService().refreshJwt();
      }
    }
  }
}
