import 'package:dio/dio.dart';
import 'package:zampon/model/User/user_counter.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/services/ws_rest/http_handler.dart';

class UserInfo extends HttpHandler {
  UserInfo();

  String _uri = "/userinfo";

  ///label rappresentanti i campi del DTO Region
  static const String USER_UID_LABEL = "userUid";
  static const String ADOPTION_LABEL = "adoption";
  static const String DISAPPEAR_LABEL = "disappear";
  static const String FOUND_LABEL = "found";

  /// Parsa un singolo oggetto ricevuto dal WS Rest per convertirlo in un oggetto UserCounter
  /// Di seguito un JSON di esempio che restituisce il backend:
  /// {"userUid":"oLCUyEKKBlMY6TJteJQcJQN0adl2","adoption":1,"disappear":0,"found":0}
  UserCounter _parseUserCounterElement(dynamic data) {
    return UserCounter(
      userUid: data[USER_UID_LABEL],
      adoption: data[ADOPTION_LABEL],
      disappear: data[DISAPPEAR_LABEL],
      found: data[FOUND_LABEL],
    );
  }

  Future<UserCounter> getUserCounter(String userUid) async {
    UserCounter counters = UserCounter();
    _uri = '/userinfo/counter/$userUid';
    try {
      AuthService().initJwt();
      Response response = await HttpHandler.dio.get(_uri,
          options: Options(
            headers: HttpHandler.requestHeaders,
          ));
      final data = response.data;
      if (data.isNotEmpty) {
        counters = _parseUserCounterElement(data);
      } else {
        ///Nel caso il server non trovi contatori per quell'utente a causa di dati sporchi
        ///si setta un valore di default con tutti i valori a zero
        counters.userUid = userUid;
        counters.adoption = 0;
        counters.disappear = 0;
        counters.found = 0;
        return counters;
      }
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh e riesegue il metodo in ricorsione
        new AuthService().refreshJwt();
      }
    }
    return counters;
  }
}
