import 'package:zampon/model/location/province.dart';
import 'package:zampon/model/location/region.dart';
import 'package:zampon/services/ws_rest/http_handler.dart';

class Locations extends HttpHandler {
  Locations();

  String _uri = "/location";

  ///label rappresentanti i campi del DTO Region
  static const String REGION_NAME_LABEL = "name";
  static const String REGION_ADOPTION_LABEL = "adoption";
  static const String REGION_DISAPPEAR_LABEL = "disappear";
  static const String REGION_FOUND_LABEL = "found";

  ///label rappresentanti i campi del DTO Province
  static const String PROVINCE_PROVINCE_LABEL = "province";
  static const String PROVINCE_NAME_LABEL = "name";
  static const String PROVINCE_REGION_LABEL = "region";
  static const String PROVINCE_ADOPTION_LABEL = "adoption";
  static const String PROVINCE_DISAPPEAR_LABEL = "disappear";
  static const String PROVINCE_FOUND_LABEL = "found";

  /// Parsa un singolo oggetto ricevuto dal WS Rest per convertirlo in un oggetto Region
  Region _parseRegionElement(dynamic data, int i) {
    return Region(
      name: data[i][REGION_NAME_LABEL],
      adoption: data[i][REGION_ADOPTION_LABEL],
      disappear: data[i][REGION_DISAPPEAR_LABEL],
      found: data[i][REGION_FOUND_LABEL],
    );
  }

  /// Parsa un singolo oggetto ricevuto dal WS Rest per convertirlo in un oggetto Province
  Province _parseProvinceElement(dynamic data, int i) {
    return Province(
      province: data[i][PROVINCE_PROVINCE_LABEL],
      name: data[i][PROVINCE_NAME_LABEL],
      region: data[i][PROVINCE_REGION_LABEL],
      adoption: data[i][PROVINCE_ADOPTION_LABEL],
      disappear: data[i][PROVINCE_DISAPPEAR_LABEL],
      found: data[i][PROVINCE_FOUND_LABEL],
    );
  }

  ///richiede al back-end la lista delle regioni
  ///Tale risposta viene inserita nella cache con la durata di 2 min
  Future<List<Region>> getRegions() async {
    List<Region> regions = [];
    ///chiave della cache
    final String _cacheKey = "regions";
    _uri = '/location/regions';
    var body = await cachedGetHttpRequest(uri: _uri, cacheKey: _cacheKey);
    ///viene effettuato il parse del JSON in oggetto
    if (body is List && body.isNotEmpty) {
      for (int i = 0; i < body.length; i++) {
        regions.add(_parseRegionElement(body, i));
      }
    }
    return regions;
  }

  Future<List<Province>> getProvincesByRegion(String region) async {
    List<Province> provinces = [];
    _uri = '/location/provinces/$region';
    ///chiave della cache
    final String _cacheKey = '/location/provinces/$region';
    var body = await cachedGetHttpRequest(uri: _uri, cacheKey: _cacheKey);
      if (body is List && body.isNotEmpty) {
        for (int i = 0; i < body.length; i++) {
          provinces.add(_parseProvinceElement(body, i));
        }
      }
    return provinces;
  }

  ///Richiede al Back-end la lista di tutti i comuni che possiedono almeno un annuncio per una singola regione
  ///questa lista è utilizzata per effettuare l'autocompletamento nella sezione filtro per singolo comune
  ///la lista viene inserita in cache tramite il metodo cachedGetHttpRequest
  Future<List<String>> getCitiesNameByRegion(String region) async {
    List<String> cities = [];
    _uri = '/location/city/$region';
    ///chiave della cache
    final String _cacheKey = '/location/city/$region';
    var body = await cachedGetHttpRequest(uri: _uri, cacheKey: _cacheKey);
      if (body is List && body.isNotEmpty) {
        for (int i = 0; i < body.length; i++) {
          cities.add(body[i]);
        }
      }
    return cities;
  }

}


