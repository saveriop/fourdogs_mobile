import 'package:dio/dio.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:zampon/model/age.dart';
import 'package:zampon/model/closed_ad.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/enum/gender.dart';
import 'package:zampon/model/enum/pet_category.dart';
import 'package:zampon/model/location.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/services/ws_rest/http_handler.dart';

class ClosedAds extends HttpHandler {
  /// Costruttore standard
  ClosedAds();

  ///label rappresentanti i campi del DTO
  static const String ID_LABEL = "id";
  static const String UID_LABEL = "uid";
  static const String REASON_LABEL = "reason";
  static const String PET_NAME_LABEL = "petName";
  static const String PET_GENDER_LABEL = "petGender";
  static const String PET_CATEGORY_LABEL = "petCategory";
  static const String AD_CATEGORY_LABEL = "adCategory";
  static const String AD_DATE_LABEL = "adDate";
  static const String CLOSED_AD_DATE_LABEL = "closedDate";
  static const String PET_AGE_LABEL = "age";
  static const String PET_AGE_MONTHS_LABEL = "months";
  static const String PET_AGE_YEARS_LABEL = "years";
  static const String AD_LOCATION_LABEL = "adLocation";
  static const String AD_LOCATION_CITY_LABEL = "city";
  static const String AD_LOCATION_PROVINCE_LABEL = "province";
  static const String AD_LOCATION_REGION_LABEL = "region";

  ///Booleana che indica che è stata letta l'ultima pagina
  ///Se è true eviterà di effettuare nuove chiamate HTTP a vuoto
  ///Se è false l'applicativa effettuerà nuove chiamate verso il back-end ricevendo nuove info in maniera paginata
  ///Ad ogni init viene inizializzata a false
  ///In ogni nextPage, quando si riceve una lista vuota dal back-end viene valorizzata con true
  static bool isLastPage = false;

  String _uri = "/closedads";

  ///Converte un JSON contenente una lista di AdClosedElements
  AdClosedElements _parseAdClosedElement(dynamic data, int i) {
    return AdClosedElements(
      id: data[i][ID_LABEL],
      uid: data[i][UID_LABEL],
      reason: data[i][REASON_LABEL],
      petName: data[i][PET_NAME_LABEL],
      petGender:
          EnumToString.fromString(PetGender.values, data[i][PET_GENDER_LABEL]),
      petCategory: EnumToString.fromString(
        PetCategory.values,
        data[i][PET_CATEGORY_LABEL],
      ),
      adCategory: EnumToString.fromString(
        AdCategory.values,
        data[i][AD_CATEGORY_LABEL],
      ),
      adDate: data[i][AD_DATE_LABEL].toString().substring(0, 10),
      closedDate: data[i][CLOSED_AD_DATE_LABEL].toString().substring(0, 10),
      petAge: Age(
        months: data[i][PET_AGE_LABEL][PET_AGE_MONTHS_LABEL],
        years: data[i][PET_AGE_LABEL][PET_AGE_YEARS_LABEL],
      ),
      adLocation: Location(
        city: data[i][AD_LOCATION_LABEL][AD_LOCATION_CITY_LABEL],
        province: data[i][AD_LOCATION_LABEL][AD_LOCATION_PROVINCE_LABEL],
        region: data[i][AD_LOCATION_LABEL][AD_LOCATION_REGION_LABEL],
      ),
    );
  }

  ///Questo metodo deve essere chiamato solo nella init state del Widget dedicato alla paginazione
  ///Serve ad inizializzare la lista degli elementi paginati per la prima volta
  ///Esempio di URL filtrando:
  ///http://127.0.0.1:8081/closedads/RoHVOKQz5zfQILYsdnVjwgPYhw63/0/5
  ///Path parameters richiesti: uid, pageIndex, pageSize
  Future<List<AdClosedElements>> initPageAds(
      {String? uid,
      int? pageIndex,
      int? pageSize,}) async {
    ///Init gestione ultima pagina
    isLastPage = false;
    List<AdClosedElements> listAds = [];

    //aggiungi i pathParameters
    _uri = _uri + "/" + uid! + "/"+ pageIndex.toString() + "/" + pageSize.toString();

     try {
      ///GESTIONE chiamata HTTP con cache
      ///La chiave della cache è date da "initPageAds" + la categoria della sezione
      var data = await cachedGetHttpRequest(
          uri: _uri, cacheKey: "MyClosedAds");
      if (data is List && data.isNotEmpty) {
        for (int i = 0; i < data.length; i++) {
          listAds.add(_parseAdClosedElement(data, i));
        }
      }
      return listAds;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");

        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }

  ///Questo metodo deve essere chiamato solo per aggiornare la lista degli elementi ottenendo una nuova pagina
  ///Viene richiamato per far scorrere la paginazione
  ///Esempio di URL filtrando:
  ///http://127.0.0.1:8081/closedads/RoHVOKQz5zfQILYsdnVjwgPYhw63/0/5
  ///Path parameters richiesti: province, region, pageIndex, pageSize
  ///Query parameters richiesti: adCategory (Adoption or Disappear)
  Future<List<AdClosedElements>?> nextPageAds(
      {String? uid,
      int? pageIndex,
      int? pageSize,
      List<AdClosedElements>? listAds}) async {
    //compone l'uri in base ai pathparam ricevuti in input
    _uri = _uri + "/" + uid! + "/"+ pageIndex.toString() + "/" + pageSize.toString();
    try {
      ///se l'applicativo ha letto l'ultima pagina eviterà di effettuare chiamate HTTP superflue
      if (!isLastPage) {
        AuthService().initJwt();
        Response response = await HttpHandler.dio.get(_uri,
            options: Options(
              headers: HttpHandler.requestHeaders,
            ));
        final data = response.data;
        if (data is List && data.isNotEmpty) {
          for (int i = 0; i < data.length; i++) {
            listAds!.add(_parseAdClosedElement(data, i));
          }
        } else {
          ///se si riceve una lista vuota significa che non ci saranno più annunci da richiedere
          ///pertanto significa che è stata letta l'ultima pagina di annunci disponibile
          isLastPage = true;
        }
      }
      return listAds;
    } on DioError catch (error) {
      print("Exception occurred: $error");
      if (error.response!.statusCode == 403) {
        print("Token expired. Try to refresh");
        ///se il token JWT dell'utente è scaduto effettua il refresh
        AuthService.isExpired = true;
      }
    }
    return listAds;
  }



}
