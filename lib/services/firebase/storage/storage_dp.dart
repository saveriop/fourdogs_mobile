import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';
import 'package:async/async.dart';

class StorageDp {
  FirebaseStorage storage = FirebaseStorage.instance;

  //Durante il primo login tramite SSO google viene salvata l'img presente nel token ricevuto da GOOGLE
  Future<void> saveGoogleProfileImg(String imgUrl, String uid) async {
    Uri myUri = Uri.parse(imgUrl);
    String imageName = imgUrl
        .substring(imgUrl.lastIndexOf("/"), imgUrl.length)
        .replaceAll("/", "");

    final Directory systemTempDir = Directory.systemTemp;
    final byteData = await NetworkAssetBundle(myUri).load(imgUrl);

    final file = File('${systemTempDir.path}/$imageName.jpeg');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    await storage.ref().child("profile/$uid").putFile(file);
  }

  //Reperisci l'immagine di profilo
  Future<String> getProfileImgReference(String uid) async {
    try {
      Reference ref = FirebaseStorage.instance.ref();
      // Get reference to the file
      Reference fileRef = ref.child("profile/$uid");
      return await fileRef
          .getDownloadURL()
          .onError((error, stackTrace) => "default");
    } catch (platformException) {
      return "default";
    }
  }

  //Reperisci l'immagine del PET
  Future<String> getPetImgReference(String? uid, String imgNmb) async {
    try {
      Reference ref = FirebaseStorage.instance.ref();
      Reference fileRef = ref.child("images/$uid/$uid$imgNmb");
      return await fileRef
          .getDownloadURL()
          .onError((error, stackTrace) => "default");
    } catch (platformException) {
      return "default";
    }
  }

  //Aggiorna nello store l'immagine di profilo tramite l'uid
  updateProfileImg(File image, String uid) async {
    Reference ref = FirebaseStorage.instance.ref();
    await ref.child("profile/$uid").putFile(image);
  }

  //Inserisce/Aggiorna nello store l'immagine di un annuncio
  insertPetImg(File image, String petUid, String uid) async {
    Reference ref = FirebaseStorage.instance.ref();
    await ref.child("images/$uid/$petUid").putFile(image);
  }

  //cancella tutte le foto di un annuncio
  deleteAd(String petUid, int imgNmb) async {
    Reference ref = FirebaseStorage.instance.ref();
    //ciclo finchè non elimino ogni foto presente nella cartella
    //nella cartella ci sono tante foto pari al valore della variabile "imgNmb"
    String url = "";
    while (imgNmb > 0) {
      imgNmb--;
      url = "images/" + petUid + "/" + petUid + "_" + imgNmb.toString();
      await ref.child(url).delete();
    }
  }

  //Recover Image From Storage
  Future<dynamic> recoverImagesFromDb(String id, String numbImg) {
    StorageDp storageDp = StorageDp();
    final AsyncMemoizer _memoizer = AsyncMemoizer();
    return _memoizer.runOnce(() async {
      return await storageDp.getPetImgReference(id, numbImg);
    });
  }
}
