import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';
import 'package:zampon/model/user.dart';
import 'package:zampon/services/firebase/firestone/user_dp.dart';

///Classe dedicata all'autenticazione con Firebase
///Gestisce gli SSO con Google Facebook
///Gestisce l'accesso via Email, login/logout, il reset password via email
///Permette di reperire il Token JWT dell'utente
class AuthService {
  ///token jwt dell'utente
  static String? jwt;

  static bool isExpired = false;

  ///Istanza di FirebaseAuth
  final FirebaseAuth auth = FirebaseAuth.instance;

  ///Sign in google
  final GoogleSignIn googleSignIn = GoogleSignIn();

  ///Messaggio di errore
  static String errorMessage = "";

  ///Create user obj based on firebaseuser
  UserModel? _userFromFirebaseUser(User? user) {
    return user != null ? UserModel(uid: user.uid) : null;
  }

  ///Auth change user stream
  Stream<UserModel?>? get user {
    return auth.authStateChanges().map(_userFromFirebaseUser);
  }

  ///Create user obj based on firebaseuser
  UserData? _userDataFromFirebaseUser(User? user) {
    return user != null
        ? UserData(
            uid: user.uid,
          )
        : null;
  }

  ///Auth change user stream
  Stream<UserData?> get userData {
    return auth.authStateChanges().map(_userDataFromFirebaseUser);
  }

  ///Inizializzazione del token JWT
  ///Questo metodo viene richiamato nella initState della HOME
  void initJwt() async {
    jwt = await auth.currentUser!.getIdToken();
  }

  Stream<String> getJwt() {
    return auth.currentUser!.getIdToken().asStream();
  }

  ///permette di effettuare il refresh del token JWT
  void refreshJwt() {
    jwt = auth.currentUser!.refreshToken!;
  }

  ///Sign in anon
  Future singInAnon() async {
    try {
      //AuthResult
      UserCredential result = await auth.signInAnonymously();
      User user = result.user!;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  ///CHECK USER STATUS
  bool checkUserStatus() {
    bool result = false;
    FirebaseAuth.instance.idTokenChanges().listen((User? user) {
      if (user == null) {
        print('User is currently signed out!');
      } else {
        print('User is signed in!');
        result = true;
      }
    });
    return result;
  }

  ///Sign in google
  Future signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth =
        await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    final UserCredential authResult =
        await FirebaseAuth.instance.signInWithCredential(credential);

    final User? user = authResult.user;
    //Inserisco il record corrispettivo a questo account nel DB solo se è la prima volta che si collega
    await DatabaseServiceUser(uid: user!.uid).
        findUser(user.uid,
         user.email != null ? user.email! : "",
         user.displayName != null ? user.displayName : "", 
         user.phoneNumber != null ? user.phoneNumber : "",
         user.photoURL != null ? user.photoURL! : "",
          true);
    return _userFromFirebaseUser(user);
  }

  ///Login con Facebook
  Future<UserCredential?> facebookLogin() async {
    try {
      // Trigger the sign-in flow
      final LoginResult loginResult = await FacebookAuth.instance.login(
          permissions: ['email', 'public_profile'],
          loginBehavior: LoginBehavior.webOnly);
      // Create a credential from the access token
      final OAuthCredential facebookAuthCredential =
          FacebookAuthProvider.credential(loginResult.accessToken!.token);

      // Once signed in, return the UserCredential
      //return FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
      final loginFbResult = await FirebaseAuth.instance
          .signInWithCredential(facebookAuthCredential);
      await DatabaseServiceUser(uid: loginFbResult.user!.uid).findUser(
          loginFbResult.user!.uid,
          loginFbResult.user!.email!,
          loginFbResult.user!.displayName!,
          loginFbResult.user!.phoneNumber != null
              ? loginFbResult.user!.phoneNumber!
              : "",
          loginFbResult.user!.photoURL!,
          true);
      return null;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  ///Sign in email&password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      //AuthResult
      UserCredential result = await auth.signInWithEmailAndPassword(
          email: email, password: password);
      User user = result.user!;
      jwt = await auth.currentUser!.getIdToken();
      return _userFromFirebaseUser(user);
    } on FirebaseAuthException catch (e) {
      errorMessage = e.code;
      return null;
    }
  }

  ///Register with email & password
  Future registerWithEmailAndPassword(
      String email, String password, String phoneNumber, String locationFilter,
      {required String username}) async {
    try {
      //AuthResult
      UserCredential result = await auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User user = result.user!;
      //Create a new document(firebase cloudstore) for the user with uid
      await DatabaseServiceUser(uid: user.uid).updateUserData(
          user.email!, username, phoneNumber, locationFilter, false, []);
      return _userFromFirebaseUser(user);
    } on FirebaseAuthException catch (e) {
      errorMessage = e.code;
      return null;
    }
  }

  ///Sing out
  Future singOut() async {
    try {
      await FacebookAuth.instance.logOut();
      signOutGoogle();
      return await auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  ///Permette di cambiare password ad un utente loggato che ha effettuato l'accesso tramite email
  ///richiede la vecchia password e la nuova
  ///Effettua prima un'autenticazione verso Firebase con la vecchia password
  ///se l'accesso è corretto significa che la vecchia password è corretta e pertanto effettua l'aggiornamento
  void changePassword(String oldPassword, String newPassword) async {
    final user = auth.currentUser;
    String email = user!.email!;
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: oldPassword,
      );
      user.updatePassword(newPassword).then((_) {
        print("Successfully changed password");
      }).catchError((error) {
        print("Password can't be changed" + error.toString());
        //This might happen, when the wrong password is in, the user isn't found, or if the user hasn't logged in recently.
      });
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }
    }
  }

  ///Reset password via email
  void resetPassword(String email) async {
    try {
      await auth.sendPasswordResetEmail(email: email);
    } on FirebaseAuthException catch (e) {
      print(e.code);
      print(e.message);
    }
  }

  ///Sign out google
  void signOutGoogle() async {
    await googleSignIn.signOut();
  }

  String getErrorMessage() {
    return errorMessage;
  }

  String setErrorMessage(String message) {
    return errorMessage = message;
  }
}
