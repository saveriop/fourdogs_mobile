import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zampon/model/adsLocation/ad_location.dart';
import 'package:zampon/model/adsLocation/province.dart';
import 'package:zampon/model/adsLocation/region.dart';
import 'package:zampon/model/enum/ad_category.dart';

class DatabaseServiceLocation {
  final CollectionReference locationCollection =
      FirebaseFirestore.instance.collection('zampon.location');

  late QuerySnapshot<Object?> response;

  void getRegionStatus() async {
    return locationCollection.limit(1).get().then((value) => value.toString());
  }

  //Inserisci il JSON in DB
  Future addAllLocation(AdsLocation adsLocation) async {
    DocumentReference result = await locationCollection.add({
      'regions': adsLocation.toJson(),
    });
    return result.id;
  }

  //Riduci il contatore delle adozioni/scomparsi di una specifica provincia
  remuveAdsCounter(
      String region, String province, AdCategory adCategory) async {
    AdsLocation? location = await readAllLocation();
    location!.regions!.forEach((element) {
      if (element.id == region) {
        adCategory == AdCategory.Disappear
            ? element.adsDisappearsNumber--
            : element.adsAdoptionsNumber--;
        element.provinces.forEach((element) {
          if (element.abbreviation == province) {
            adCategory == AdCategory.Disappear
                ? element.adsDisappearsNumber--
                : element.adsAdoptionsNumber--;
          }
        });
      }
    });
    //Update sul DB
    Map<String, dynamic> data = {'regions': location.toJson()};
    var batch = FirebaseFirestore.instance.batch();
    response.docs.forEach((element) {
      var docRef = locationCollection.doc(element.id);
      batch.update(docRef, data);
    });
    batch.commit().then((a) {
      print('updated all documents inside Collection');
    });
  }

  //Incrementa il contatore delle adozioni/scomparsi di una specifica provincia
  addAdsCounter(String region, String province, AdCategory adCategory) async {
    AdsLocation? location = await readAllLocation();
    location!.regions!.forEach((element) {
      if (element.id == region) {
        adCategory == AdCategory.Disappear
            ? element.adsDisappearsNumber++
            : element.adsAdoptionsNumber++;
        element.provinces.forEach((element) {
          if (element.abbreviation == province) {
            adCategory == AdCategory.Disappear
                ? element.adsDisappearsNumber++
                : element.adsAdoptionsNumber++;
          }
        });
      }
    });
    //Update sul DB
    Map<String, dynamic> data = {'regions': location.toJson()};
    var batch = FirebaseFirestore.instance.batch();
    response.docs.forEach((element) {
      var docRef = locationCollection.doc(element.id);
      batch.update(docRef, data);
    });
    batch.commit();
  }

  //esempio di JSON letto da DB:
  /*{regions: [{provinces: [{adsDisappearsNumber: 0, adsAdoptionsNumber: 3, id: BARI, abbreviation: BA}, 
  {adsDisappearsNumber: 0, adsAdoptionsNumber: 0, id: LECCE, abbreviation: LE}], adsDisappearsNumber: 0, adsAdoptionsNumber: 3, id: PUGLIA}]}*/
  Future<AdsLocation?> readAllLocation() async {
    List<Region> finalRegions = [];
    //Sarà presente sempre un solo record per questa query
    QuerySnapshot<Object?>? allRegions = await locationCollection.limit(1).get();
    response = allRegions;
    List<dynamic> list = allRegions.docs;
    list.forEach((result) {
      //Leggo tramite una lista dynamic la lista delle regioni
      //Successivamente converto la lista generica in un oggetto definito di tipo AdsLocation
      Map<dynamic, dynamic> map = result["regions"];
      List<dynamic> regions = map["regions"];
      //Ciclo per ogni regione
      regions.forEach((element) {
        List<Province> finalProvinces = [];
        Region regionEle = new Region(provinces: [], id: '', adsAdoptionsNumber: 0, adsDisappearsNumber: 0);
        regionEle.adsAdoptionsNumber = element["adsAdoptionsNumber"];
        regionEle.adsDisappearsNumber = element["adsDisappearsNumber"];
        regionEle.id = element["id"];
        List<dynamic> provinces = element["provinces"];
        //Ciclo per ogni provincia appartenente alla regione
        provinces.forEach((element) {
          finalProvinces.add(Province(
              id: element["id"],
              adsAdoptionsNumber: element["adsAdoptionsNumber"],
              adsDisappearsNumber: element["adsDisappearsNumber"],
              abbreviation: element["abbreviation"]));
        });
        //Aggiungo la provincia all'elemento della regione
        regionEle.provinces = finalProvinces;
        //Aggiungo la singola regione alla lista in quanto ho terminato la sua elaborazione
        finalRegions.add(regionEle);
      });
    });
    return AdsLocation(regions: finalRegions);
  }
}
