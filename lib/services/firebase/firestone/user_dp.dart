import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/services/firebase/storage/storage_dp.dart';

//Gestisce il document users del db NoSQL CLOUD FIRESTORE
class DatabaseServiceUser {
  final String? uid;
  DatabaseServiceUser({this.uid});

  ///Collection reference
  ///Se la collezione alla quale si sta richiedendo l'istanza non esiste verrà creata
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('zampon.users');

  ///label rappresentanti i campi del document
  static const String EMAIL_LABEL = "email";
  static const String MOBILE_PHONE_LABEL = "mobilePhone";
  static const String USERNAME_LABEL = "username";
  static const String LOCATION_FILTER_LABEL = "locationFilter";
  static const String SSO_LABEL = "sso";
  static const String FAVORITE_ADS_LABEL = "favoriteAds";
  static const String BIO_LABEL = "bio";

  ///trova un utente
  Future findUser(String uid, String email, String? userDisplay,
      String? mobilePhone, String photoUrl, bool sso) async {
    userCollection.doc(uid).get().then((value) async {
      if (value.data() == null) {
        //registro l'utente nel document se è il suo primo accesso
        await updateUserData(
          email,
          userDisplay ?? email,
          mobilePhone ?? "",
          "",
          sso,
          [],
        );

        ///salvo l'immagine di gmail legata all'utente nello store
        final StorageDp _storageDp = StorageDp();
        await _storageDp.saveGoogleProfileImg(photoUrl, uid);
      }
    });
  }

  ///Reperisci info dell'utente loggato
  Future<UserData> findCurrentUser(String uid) async {
    return userCollection.doc(uid).get().then((value) async {
      //if (value.data() != null) {
        return new UserData(
            email: value[EMAIL_LABEL],
            mobilePhone: value[MOBILE_PHONE_LABEL],
            uid: uid,
            username: value[USERNAME_LABEL],
            locationFilter: value[LOCATION_FILTER_LABEL],
            sso: value[SSO_LABEL],
            favoriteAds: value[FAVORITE_ADS_LABEL].cast<String>(),
            bio: value[BIO_LABEL] ?? "");
      //}
    });
  }

  ///Reperisci l'username dell'utente desiderato
  Future<String> findUserName(String uid) async {
    return userCollection.doc(uid).get().then((value) async {
      if (value.data() != null) {
        UserData user = new UserData(
            email: value[EMAIL_LABEL],
            mobilePhone: value[MOBILE_PHONE_LABEL],
            uid: uid,
            username: value[USERNAME_LABEL],
            locationFilter: value[LOCATION_FILTER_LABEL],
            sso: value[SSO_LABEL],
            favoriteAds: value[FAVORITE_ADS_LABEL].cast<String>(),
            bio: value[BIO_LABEL] ?? "");
        return user.username!;
      } else {
        return '';
      }
    });
  }

  ///Cancella un utente by uid
  void deleteUser(String uid) {
    userCollection.doc(uid).delete();
  }

  /// aggiorna un campo generico del document
  /// deve ricevere in input la label del campo e il valore che si desidera inserire
  /// N.B.: Valorizzare il campo label con una label definita in questa classe
  Future<void> updateField(String label, value) async {
    await userCollection.doc(uid).update({label: value}).whenComplete(() async {
      print("Completed");
    });
  }

  ///Update a user
  Future updateUserData(String email, String username, String mobilePhone,
      String locationFilter, bool sso, List<String> favoriteAds) async {
    return await userCollection.doc(uid).set({
      EMAIL_LABEL: email,
      USERNAME_LABEL: username,
      MOBILE_PHONE_LABEL: mobilePhone,
      LOCATION_FILTER_LABEL: locationFilter,
      SSO_LABEL: sso,
      FAVORITE_ADS_LABEL: favoriteAds,
      BIO_LABEL: "",
    });
  }

  ///UserData from snapshot
  UserData _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return UserData(
        uid: uid!,
        username: snapshot[USERNAME_LABEL],
        email: snapshot[EMAIL_LABEL],
        mobilePhone: snapshot[MOBILE_PHONE_LABEL],
        locationFilter: snapshot[LOCATION_FILTER_LABEL],
        sso: snapshot[SSO_LABEL],
        favoriteAds: snapshot[FAVORITE_ADS_LABEL].cast<String>(),
        bio: snapshot[BIO_LABEL]);
  }

  ///Get user doc stream
  Stream<UserData> get userData {
    return userCollection.doc(uid).snapshots().map(_userDataFromSnapshot);
  }
}
