import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:zampon/l10n/l10n.dart';
import 'package:zampon/model/ad.dart';
import 'package:zampon/screens/authenticate/welcome_page.dart';
import 'package:zampon/screens/first_login_wrapper.dart';
import 'package:zampon/screens/home/adList/adDetail/ad_detail.dart';
import 'package:zampon/services/ws_rest/ads.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:provider/provider.dart';
//import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/services.dart';
import 'package:zampon/model/filter.dart';
import 'package:zampon/model/filter/filter_gender.dart';
import 'package:zampon/model/filter/filter_size.dart';
import 'package:zampon/model/filter/filter_species.dart';
import 'package:zampon/model/user.dart';
import 'package:zampon/screens/wrapper.dart';
import 'package:zampon/services/firebase/authentication/auth.dart';
import 'package:zampon/services/locale_provider.dart';
import 'package:zampon/shared/constants.dart';
import 'package:uni_links/uni_links.dart';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  ///inizializzazione firebase
  await Firebase.initializeApp();

  ///inizializzazione banner AdMob
  //MobileAds.instance.initialize();

  ///Blocco auto-rotate
  await SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp],
  );

  if (kIsWeb) {
    // initialiaze the facebook javascript SDK
    await FacebookAuth.instance.webAndDesktopInitialize(
      appId: "6491760194169212",
      cookie: true,
      xfbml: true,
      version: "v13.0",
    );
  }

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  ///inizializzo i filtri di ricerca avanzata con valori di default
  static Filter filter = Filter(
      filterGender: FilterGender(),
      filterSize: FilterSize(),
      filterSpecies: FilterSpecies());

  ///Variabili necessarie a mantenere lo stato della navigazione tramite deepLink
  static bool isLinked = false;
  static String? link = "";

  ///lista dei nomi di tutti gli annunci
  ///La lista è utilizzata per effettuare la ricerca per nome degli annunci
  static List<String> allPetNames = [];
  static String selectedName = "";

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  
  @override
  void initState() {
    initUniLinks().then((value) => this.setState(() {
          MyApp.isLinked = true;
        }));
    super.initState();
  }

  ///This widget is the root of your application.
  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
      create: (context) => LocaleProvider(),
      builder: (context, child) {
        return StreamProvider<UserData?>.value(
          value: AuthService().userData,
          initialData: null,
          child: GetMaterialApp(
              routes: {
                '/welcome' : (BuildContext context) => WelcomePage(),
                '/wrapper' : (BuildContext context) => Wrapper(),
                '/firstLoginWrapper': (BuildContext context) => FirstLoginWrapper(tabIndex: 0,),
                '/adDetail': (BuildContext context) => AdDetail(
                      adElements: new AdElements(petName: "", ),
                     userData: new UserData(uid: ""),
                   )
              },
              debugShowCheckedModeBanner: false,
              title: "ZampOn",
              theme: ThemeData(
                primaryColor: customStrongLiliac,
                primaryIconTheme: Theme.of(context)
                    .primaryIconTheme
                    .copyWith(color: customPaleLiliac),
              ),

              /*Abilitare per rendere la lingua di defaut uguale alla lingua di sistema dello smartphone
              locale: provider.locale,*/

              locale: Locale('it', 'IT'),
              supportedLocales: L10n.all,
              localizationsDelegates: [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              home: Wrapper()),
        );
      });
}

///Inizializzazione del deeplink
///Questo link è fondamentale per l'apertura dell'app tramite link
///Se sul telefono l'app è chiuso e la si apre tramite link esso viene reperito da questo metodo
Future<String?> initUniLinks() async {
  try {
    final initialLink = await getInitialLink();
    return initialLink;
  } on PlatformException {
    return "";
  }
}

///Una volta avviata l'applicazione essa si mette in ascolto
///in modo tale che se viene aperto un dinamic link l'applicazione naviga nel dettaglio dello specifico annuncio
Future<void> handleIncomingLinks(context, userData) async {
  linkStream.listen((String? link) {
    uniLinksNavigate(context, userData, link);
  }, onError: (err) {
    // Handle exception by warning the user their action did not succeed
  });
}

///Una volta che si accede tramite dinamicLink
///se ecco possiede l'id di un annuncio permette la navigazione nel dettaglio di tale annuncio
void uniLinksNavigate(context, userData, String? link) async {
  ///se il link è diverso da null (caso possibile durante l'avvio)
  ///e se il link possiede l'elementi id definito dopo il simbolo '='
  ///solo allora viene effettuata laquery per reperire l'oggetto adElement e in tal caso la navigate
  if (link != null && link.indexOf("=") > -1) {
    String id = link.substring(link.indexOf("=") + 1, link.length);
    AdElements? adElement = await Ads().getAdById(id);
    if(adElement != null)
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => AdDetail(
                adElements: adElement,
                userData: userData,
              )),
    );
  }
}
