import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:zampon/assets/custom_icons.dart';

//Back button loginPage & singup
Widget backButton(BuildContext context, Color backButtonColor) {
  return IconButton(
    iconSize: MediaQuery.of(context).size.height * 0.08,
    //cambiare colore al tasto indietro nel titolo
    icon: Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
      child: Icon(
        CustomIcons.i8_back_button,
        color: backButtonColor,
        size: MediaQuery.of(context).size.height * 0.04,
      ),
    ),
    onPressed: () {
      Navigator.pop(context);
    },
  );
}

const String ITALY = "ITALIA";
const Color isClosedColor = Colors.grey;
const Color customPaleLiliac = Color(0xffd8b5ff);
const Color customStrongLiliac = Color(0xffa980fe);
const Color customWhite = Color(0xfff2f2f2);
const Color customPaleGrey = Color(0xffd8d8d8);
const Color customStrongGrey = Color(0xff878787);
const Color customGreen = Color(0xff34d546);
const Color grey1 = Color.fromARGB(255, 117, 117, 117);
const Color grey2 = Color.fromARGB(255, 189, 189, 189);
const Color green1 = Color.fromARGB(255, 76, 175, 80);

adDecoration(BuildContext context, {bool isClosed = false}) {
  return BoxDecoration(
      borderRadius: lightBorderRadius(context),
      gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [0.65, 0.9],
          colors: isClosed
              ? [grey1, grey2]
              : [customStrongLiliac, customPaleLiliac,]
              ));
}

//App version stile
appVersion(BuildContext context, double size) {
  return Text(
    AppLocalizations.of(context)!.softwareVersion + " 1.0.5.33",
    style: TextStyle(
        color: customWhite, fontWeight: FontWeight.bold, fontSize: size),
  );
}

// Centralizzazione dei bordi
BorderRadiusGeometry lightBorderRadius(BuildContext context) {
  return BorderRadius.circular(MediaQuery.of(context).size.height * 0.025);
}

BorderRadius mediumBorderRadius(BuildContext context) {
  return BorderRadius.circular(MediaQuery.of(context).size.height * 0.05);
}

BorderRadiusGeometry heavyBorderRadius(BuildContext context) {
  return BorderRadius.circular(MediaQuery.of(context).size.height * 0.075);
}

BorderRadiusGeometry circularBorderRadius(BuildContext context) {
  return BorderRadius.circular(MediaQuery.of(context).size.height * 0.175);
}

Radius lightRadius(BuildContext context) {
  return Radius.circular(MediaQuery.of(context).size.height * 0.025);
}

Radius mediumRadius(BuildContext context) {
  return Radius.circular(MediaQuery.of(context).size.height * 0.05);
}

Radius heavyRadius(BuildContext context) {
  return Radius.circular(MediaQuery.of(context).size.height * 0.075);
}

TextStyle informationTitelStyle(BuildContext context) {
  return TextStyle(
      fontSize: MediaQuery.of(context).size.width * 0.060,
      color: customWhite,
      fontWeight: FontWeight.bold);
}

TextStyle informationSubtitelStyle(BuildContext context) {
  return TextStyle(
      fontSize: MediaQuery.of(context).size.width * 0.040,
      color: customWhite,
      fontWeight: FontWeight.bold);
}

TextStyle errorSubtitelStyle(BuildContext context) {
  return TextStyle(
      fontSize: MediaQuery.of(context).size.width * 0.040,
      color: Colors.red,
      fontWeight: FontWeight.bold);
}

TextStyle informationBodyStyle(BuildContext context) {
  return TextStyle(
    fontSize: MediaQuery.of(context).size.width * 0.035,
    color: customWhite,
  );
}

//Linea che divide la pagina (Corta)
Widget smallDivider(BuildContext context) {
  return Divider(
      thickness: MediaQuery.of(context).size.height * 0.005,
      indent: MediaQuery.of(context).size.width * 0.3,
      endIndent: MediaQuery.of(context).size.width * 0.3,
      color: customWhite);
}

///Linea che divide la pagina (Media)
Widget mediumDivider(BuildContext context) {
  return Divider(
      thickness: MediaQuery.of(context).size.height * 0.005,
      indent: MediaQuery.of(context).size.width * 0.12,
      endIndent: MediaQuery.of(context).size.width * 0.12,
      color: customWhite);
}

///Linea che divide la pagina (Grande)
Widget largeDivider(BuildContext context) {
  return Divider(
      thickness: MediaQuery.of(context).size.height * 0.005,
      indent: MediaQuery.of(context).size.width * 0.04,
      endIndent: MediaQuery.of(context).size.width * 0.04,
      color: customWhite);
}

///Linea che divide la pagina (Grande e sottile)
Widget thinDivider(BuildContext context) {
  return Divider(
      thickness: MediaQuery.of(context).size.height * 0.002,
      endIndent: MediaQuery.of(context).size.width * 0.08,
      color: customWhite);
}

//Testo utilizzabile per i sottotitoli
Widget subTitle(
  BuildContext context,
  String text, {
  bool thereIsIcon = false,
  bool iconOnLeft = true,
  Widget? icon,
  double topPadding = 0.0,
  double bottomPadding = 0.0,
}) {
  bool visibleLeft = false;
  bool visibleRight = false;

  if (thereIsIcon && iconOnLeft) {
    visibleLeft = true;
  } else if (thereIsIcon && !iconOnLeft) {
    visibleRight = true;
  }

  return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
   if(icon != null) Visibility(child: icon, visible: visibleLeft),
    Padding(
      padding: EdgeInsets.only(
        left: MediaQuery.of(context).size.width * 0.02,
        right: MediaQuery.of(context).size.width * 0.02,
        top: topPadding,
        bottom: bottomPadding,
      ),
      child: Text(text,
          style: TextStyle(
              color: customWhite,
              fontSize: MediaQuery.of(context).size.width * 0.06,
              fontWeight: FontWeight.bold)),
    ),
    if(icon != null) Visibility(child: icon, visible: visibleRight),
  ]);
}

//Testo utilizzabile per i test delle paginazioni
Widget textPagination(
  BuildContext context,
  String text,
) {
  return Padding(
    padding: EdgeInsets.only(
      top: MediaQuery.of(context).size.width * 0.04,
    ),
    child: Text(text,
        style: TextStyle(
            color: customWhite,
            fontSize: MediaQuery.of(context).size.width * 0.03,
            fontWeight: FontWeight.bold)),
  );
}


//Testo utilizzabile per i test delle paginazioni
Widget warningTextPagination(
  BuildContext context,
  String text,
) {
  return Padding(
    padding: EdgeInsets.only(
      top: MediaQuery.of(context).size.width * 0.04,
      left: MediaQuery.of(context).size.width * 0.18, 
      right: MediaQuery.of(context).size.width * 0.18 
    ),
    child: Text(text,
    textAlign: TextAlign.center,
        style: TextStyle(
            color: customWhite,
            fontSize: MediaQuery.of(context).size.width * 0.03,
            fontWeight: FontWeight.bold)),
  );
}

  Widget divider(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
      child: largeDivider(context),
    );
  }
