import 'package:zampon/model/italian_city.dart';
import 'package:zampon/shared/badwords.dart';

String? validateBadWords(String value) {
  bool isBadWord = false;
  final textWords = value.split(' ');
  for (final word in textWords) {
    if (badWords.contains(word.toLowerCase())) {
      isBadWord = true;
    }
  }
  return isBadWord ? "Contenuto inopportuno!" : null;
}

///Validazione password in fase di registrazione
String? validatePassword(String value, String password) {
  if (value.isEmpty) return 'Campo obbligatorio.';
  if (value.length < 6) return 'Password troppo corta';
  if (value != password) return 'Le password inserite sono diverse';
  return null;
}

String? validateUsername(String value) {
  if (value.isEmpty) return 'Username obbligatorio.';
  if (value.length > 28) return 'Username troppo lungo.';
  return validateBadWords(value);
}

String? validateDescription(String? value) {
  if (value!.isEmpty) return 'La descrizione è obbligatoria.';
  if (value.length < 10) return 'Descrizione insufficiente.';
  return validateBadWords(value);
}

// Validazione di una stringa tramite regex alfabetica
String? validateName(String? value) {
  if (value!.isEmpty) return 'Il nome è obbligatorio.';
  final RegExp nameExp = RegExp(r'^[A-Za-z ]+$');
  if (!nameExp.hasMatch(value)) {
    return 'Inserisci solo caratteri alfabetici.';
  }
  return validateBadWords(value);
}

//Validazione di una string tramite regex numerica
String? validateAge(String? value) {
  if (value!.isEmpty) return "Obbligatorio.";
  final RegExp nameExp = RegExp(r'^[0-9]+$');
  if (!nameExp.hasMatch(value)) {
    return 'Solo numeri.';
  }
  return null;
}

//validazione mesi: il valore non deve essere maggiore di 12
String? validateMonth(String? value) {
  if (value!.isEmpty) return "Obbligatorio.";
  if (int.parse(value) > 12) return "Non valido.";
  final RegExp nameExp = RegExp(r'^[0-9]+$');
  if (!nameExp.hasMatch(value)) {
    return 'Solo numeri.';
  }
  return null;
}

///Validazione di un numero di telefono inserito
String? validateMobile(String? value) {
// Italian Mobile number are of 10 digit only
  if (value!.length < 9 || value.length > 11)
    return 'Numero non valido';
  else
    return null;
}

//Validazione di una string tramite regex numerica
String? validateEmail(String? value) {
  if (value!.isEmpty) return "Campo obbligatorio.";
  final RegExp nameExp = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  if (!nameExp.hasMatch(value)) {
    return "Inserire un'email valida";
  }
  return null;
}

/// Validazione della locazione selezionata es.: Andria, BT , PUGLIA
String? validateLocation(String? value) {
  if (value!.isEmpty) return 'Selezionare una città.';
  if (getItalianCitiesList().singleWhere((element) => element.city == value, orElse: () => ItalianCity.city("value") ) ==  ItalianCity.city("value")) {
    return 'Seleziona una città esistente.';
  }
  return null;
}

/// Validazione della locazione selezionata. Le uniche città valide sono quelle presenti nella lista validList
String? validateLocationInput(String? value, List<String> validList) {
  if (value!.isEmpty) return 'Selezionare un Comune';
  if (!validList.contains(value)) {
    return 'Seleziona un Comune suggerito';
  }
  return null;
}

/// Validazione della locazione selezionata. Le uniche città valide sono quelle presenti nella lista validList
String? validateNameInput(String? value, List<String> validList) {
  if (value!.isEmpty) return 'Inserire un nome da ricercare';
  if (!validList.contains(value)) {
    return 'Seleziona un Nome suggerito';
  }
  return null;
}

String convertNameRegionToLabel(String region) {
  switch (region) {
    case "EMILIA ROMAGNA":
      region = "EMILIA_ROMAGNA";
      break;
    case "FRIULI VENEZIA GIULIA":
    case "FRIULI":
      region = "FRIULI_VENEZIA_GIULIA";
      break;
    case "TRENTINO ALTO ADIGE":
    case "TRENTINO":
      region = "TRENTINO_ALTO_ADIGE";
      break;
    case "VALLE D'AOSTA":
      region = "VALLE_D_AOSTA";
      break;
  }
  return region;
}

String convertLabelToNameRegion(String? region) {
  int indexComma = region!.indexOf(",");
  int indexBrackets = region.indexOf("(");
  String city = "";
  String regionName = region;
  //gestione virgola
  if (!(indexComma == -1)) {
    city = region.substring(indexComma, region.length);
    regionName = region.substring(0, indexComma);
  } else if (!(indexBrackets == -1)) {
    //gestione parentesi
    city = region.substring(indexBrackets, region.length);
    regionName = region.substring(0, indexBrackets);
  }
  switch (regionName.trim()) {
    case "EMILIA_ROMAGNA":
      regionName = "EMILIA ROMAGNA";
      break;
    case "FRIULI_VENEZIA_GIULIA":
      regionName = "FRIULI";
      break;
    case "TRENTINO_ALTO_ADIGE":
      regionName = "TRENTINO";
      break;
    case "VALLE_D_AOSTA":
      regionName = "VALLE D'AOSTA";
      break;
  }
  /*
  String totalOutput = regionName + city;
  if(totalOutput.length > 20){
    totalOutput = totalOutput.substring(0,20);
    totalOutput = totalOutput + "...";
  }*/
  return regionName + city;
}

//Verifica che una stringa sia un numero
bool isNumeric(String s) {
  return int.tryParse(s) != null;
}
