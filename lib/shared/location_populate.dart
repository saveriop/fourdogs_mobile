import 'package:zampon/model/adsLocation/ad_location.dart';
import 'package:zampon/model/adsLocation/province.dart';
import 'package:zampon/model/adsLocation/region.dart';

AdsLocation populates() {
  AdsLocation location = new AdsLocation(regions: []);
  List<Region> regions = [];
  /**  
        *   ABRUZZO
        *   Aquila        AQ  
        *   Chieti        CH  
        *   Pescara       PE  
        *   Teramo        TE
        */
  Region abruzzo =
      Region(id: "ABRUZZO", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> abruzzoProvinces = [];
  abruzzoProvinces.add(Province(
      id: "Aquila",
      abbreviation: "AQ",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  abruzzoProvinces.add(Province(
      id: "Chieti",
      abbreviation: "CH",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  abruzzoProvinces.add(Province(
      id: "Pescara",
      abbreviation: "PE",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  abruzzoProvinces.add(Province(
      id: "Teramo",
      abbreviation: "TE",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  abruzzo.provinces = abruzzoProvinces;
  regions.add(abruzzo);
  /**  
        *   BASILICATA
        *   Matera        MT  
        *   Potenza       PZ  
        */
  Region basilicata =
      Region(id: "BASILICATA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> basilicataProvinces = [];
  basilicataProvinces.add(Province(
      id: "Matera",
      abbreviation: "MT",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  basilicataProvinces.add(Province(
      id: "Potenza",
      abbreviation: "PZ",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  basilicata.provinces = basilicataProvinces;
  regions.add(basilicata);
  /**  
        *   CALABRIA
        *   Catanzaro         CZ  
        *   Cosenza           CS  
        *   Crotone           KR
        *   Reggio Calabria   RC
        *   Vibo Valentia     VV
        */
  Region calabria =
      Region(id: "CALABRIA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> calabriaProvinces = [];
  calabriaProvinces.add(Province(
      id: "Catanzaro",
      abbreviation: "CZ",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  calabriaProvinces.add(Province(
      id: "Cosenza",
      abbreviation: "CS",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  calabriaProvinces.add(Province(
      id: "Crotone",
      abbreviation: "KR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  calabriaProvinces.add(Province(
      id: "Reggio Calabria",
      abbreviation: "RC",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  calabriaProvinces.add(Province(
      id: "Vibo Valentia",
      abbreviation: "VV",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  calabria.provinces = calabriaProvinces;
  regions.add(calabria);
  /**  
        *   CAMPANIA
        *   Avellino    AV  
        *   Benevento   BN  
        *   Caserta     CE
        *   Napoli      NA
        *   Salerno     SA
        */
  Region campania =
      Region(id: "CAMPANIA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> campaniaProvinces = [];
  campaniaProvinces.add(Province(
      id: "Avellino",
      abbreviation: "AV",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  campaniaProvinces.add(Province(
      id: "Benevento",
      abbreviation: "BN",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  campaniaProvinces.add(Province(
      id: "Caserta",
      abbreviation: "CE",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  campaniaProvinces.add(Province(
      id: "Napoli",
      abbreviation: "NA",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  campaniaProvinces.add(Province(
      id: "Salerno",
      abbreviation: "SA",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  campania.provinces = campaniaProvinces;
  regions.add(campania);
  /**  
        *   EMILIA ROMAGNA
        *   Bologna           BO  
        *   Ferrara           FE  
        *   Forlì-Cesena      FC
        *   Parma             PR
        *   Piacenza          PC
        *   Ravenna           RA
        *   Reggio Emilia     RE
        *   Rimini            RN
        */
  Region emilia = Region(
      id: "EMILIA ROMAGNA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> emiliaProvinces = [];
  emiliaProvinces.add(Province(
      id: "Bologna",
      abbreviation: "BO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  emiliaProvinces.add(Province(
      id: "Ferrara",
      abbreviation: "FE",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  emiliaProvinces.add(Province(
      id: "Forlì-Cesena",
      abbreviation: "FC",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  emiliaProvinces.add(Province(
      id: "Parma",
      abbreviation: "PR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  emiliaProvinces.add(Province(
      id: "Piacenza",
      abbreviation: "PC",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  emiliaProvinces.add(Province(
      id: "Ravenna",
      abbreviation: "RA",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  emiliaProvinces.add(Province(
      id: "Reggio Emilia",
      abbreviation: "RE",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  emiliaProvinces.add(Province(
      id: "Rimini",
      abbreviation: "RN",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  emilia.provinces = emiliaProvinces;
  regions.add(emilia);
  /**  
        *   FRIULI VENEZIA GIULIA
        *   Gorizia           GO  
        *   Pordenone         PN  
        *   Trieste           TS
        *   Udine             UD
        */
  Region friuli = Region(
      id: "FRIULI VENEZIA GIULIA",
      adsAdoptionsNumber: 0,
      adsDisappearsNumber: 0, provinces: []);
  List<Province> friuliProvinces = [];
  friuliProvinces.add(Province(
      id: "Gorizia",
      abbreviation: "GO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  friuliProvinces.add(Province(
      id: "Pordenone",
      abbreviation: "PN",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  friuliProvinces.add(Province(
      id: "Trieste",
      abbreviation: "TS",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  friuliProvinces.add(Province(
      id: "Udine",
      abbreviation: "UD",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  friuli.provinces = friuliProvinces;
  regions.add(friuli);
  /**  
        *   LAZIO
        *   Frosinone           FR  
        *   Latina              LT  
        *   Rieti               RI
        *   Roma                RM
        *   Viterbo             VT
        */
  Region lazio =
      Region(id: "LAZIO", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> lazioProvinces = [];
  lazioProvinces.add(Province(
      id: "Frosinone",
      abbreviation: "FR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lazioProvinces.add(Province(
      id: "Latina",
      abbreviation: "LT",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lazioProvinces.add(Province(
      id: "Rieti",
      abbreviation: "RI",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lazioProvinces.add(Province(
      id: "Roma",
      abbreviation: "RM",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lazioProvinces.add(Province(
      id: "Viterbo",
      abbreviation: "VT",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lazio.provinces = lazioProvinces;
  regions.add(lazio);
  /**  
        *   LIGURIA
        *   Genova            GE  
        *   Imperia           IM  
        *   Spezia            SP
        *   Savona            SV
        */
  Region liguria =
      Region(id: "LIGURIA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> liguriaProvinces = [];
  liguriaProvinces.add(Province(
      id: "Genova",
      abbreviation: "GE",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  liguriaProvinces.add(Province(
      id: "Imperia",
      abbreviation: "IM",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  liguriaProvinces.add(Province(
      id: "Spezia",
      abbreviation: "SP",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  liguriaProvinces.add(Province(
      id: "Savona",
      abbreviation: "SV",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  liguria.provinces = liguriaProvinces;
  regions.add(liguria);
  /**  
        *   LOMBARDIA
        *   Bergamo         BG  
        *   Brescia         BS  
        *   Como            CO
        *   Cremona         CR
        *    Lecco          LC
        *    Lodi           LO
        *    Mantova        MN
        *    Milano         MI
        *    Monza          MB
        *    Pavia          PV
        *    Sondrio        SO
        *    Varese         VA
        */
  Region lombardia =
      Region(id: "LOMBARDIA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> lombardiaProvinces = [];
  lombardiaProvinces.add(Province(
      id: "Bergamo",
      abbreviation: "BG",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Brescia",
      abbreviation: "BS",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Como",
      abbreviation: "CO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Cremona",
      abbreviation: "CR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Lecco",
      abbreviation: "LC",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Lodi",
      abbreviation: "LO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Mantova",
      abbreviation: "MN",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Milano",
      abbreviation: "MI",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Monza",
      abbreviation: "MB",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Pavia",
      abbreviation: "PV",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Sondrio",
      abbreviation: "SO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardiaProvinces.add(Province(
      id: "Varese",
      abbreviation: "VA",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  lombardia.provinces = lombardiaProvinces;
  regions.add(lombardia);
  /**  
        *   MARCHE
        *   Ancona          AN  
        *   Ascoli Piceno   AP  
        *   Fermo           FM
        *   Macerata        MC
        *   Pesaro          PU
        */
  Region marche =
      Region(id: "MARCHE", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> marcheProvinces = [];
  marcheProvinces.add(Province(
      id: "Ancona",
      abbreviation: "AN",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  marcheProvinces.add(Province(
      id: "Ascoli Piceno",
      abbreviation: "AP",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  marcheProvinces.add(Province(
      id: "Fermo",
      abbreviation: "FM",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  marcheProvinces.add(Province(
      id: "Macerata",
      abbreviation: "MC",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  marcheProvinces.add(Province(
      id: "Pesaro",
      abbreviation: "PU",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  marche.provinces = marcheProvinces;
  regions.add(marche);
  /**  
        *   MOLISE
        *   Campobasso          CB  
        *   Isernia             IS  
        */
  Region molise =
      Region(id: "MOLISE", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> moliseProvinces = [];
  moliseProvinces.add(Province(
      id: "Campobasso",
      abbreviation: "CB",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  moliseProvinces.add(Province(
      id: "Isernia",
      abbreviation: "IS",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  molise.provinces = moliseProvinces;
  regions.add(molise);
  /**  
        *   PIEMONTE
        *   Alessandria               AL  
        *   Asti                      AT
        *   Biella                    BI
        *   Cuneo                     CN
        *   Novara                    NO
        *   Torino                    TO
        *   Verbano Cusio Ossola      VB
        *   Vercelli                  VC     
        */
  Region piemonte =
      Region(id: "PIEMONTE", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> piemonteProvinces = [];
  piemonteProvinces.add(Province(
      id: "Alessandria",
      abbreviation: "AL",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  piemonteProvinces.add(Province(
      id: "Asti",
      abbreviation: "AT",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  piemonteProvinces.add(Province(
      id: "Biella",
      abbreviation: "BI",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  piemonteProvinces.add(Province(
      id: "Cuneo",
      abbreviation: "CN",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  piemonteProvinces.add(Province(
      id: "Novara",
      abbreviation: "NO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  piemonteProvinces.add(Province(
      id: "Torino",
      abbreviation: "TO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  piemonteProvinces.add(Province(
      id: "Verbano Cusio Ossola",
      abbreviation: "VB",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  piemonteProvinces.add(Province(
      id: "Vercelli",
      abbreviation: "VC",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  piemonte.provinces = piemonteProvinces;
  regions.add(piemonte);
  /**  
        *   PUGLIA
        *   Bari                        BA  
        *   Barletta-Andria-Trani       BT
        *   Brindisi                    BR
        *   Foggia                      FG
        *   Lecce                       LE
        *   Taranto                     TA   
        */
  Region puglia =
      Region(id: "PUGLIA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> pugliaProvinces = [];
  pugliaProvinces.add(Province(
      id: "Bari",
      abbreviation: "BA",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  pugliaProvinces.add(Province(
      id: "Barletta-Andria-Trani",
      abbreviation: "BT",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  pugliaProvinces.add(Province(
      id: "Brindisi",
      abbreviation: "BR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  pugliaProvinces.add(Province(
      id: "Foggia",
      abbreviation: "FG",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  pugliaProvinces.add(Province(
      id: "Lecce",
      abbreviation: "LE",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  pugliaProvinces.add(Province(
      id: "Taranto",
      abbreviation: "TA",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  puglia.provinces = pugliaProvinces;
  regions.add(puglia);
  /**  
        *   SARDEGNA
        *   Cagliari                            CA  
        *   Carbonia-Iglesias                   CI
        *   Medio Campidano                     VS
        *   Nuoro                               NU
        *   Ogliastra                           OG
        *   Olbia-Tempio                        OT
        *   Oristano                            OR
        *   Sassari                             SS
        */
  Region sardegna =
      Region(id: "SARDEGNA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> sardegnaProvinces = [];
  sardegnaProvinces.add(Province(
      id: "Cagliari",
      abbreviation: "CA",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  sardegnaProvinces.add(Province(
      id: "Carbonia-Iglesias",
      abbreviation: "CI",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  sardegnaProvinces.add(Province(
      id: "Medio Campidano",
      abbreviation: "VS",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  sardegnaProvinces.add(Province(
      id: "Nuoro",
      abbreviation: "NU",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  sardegnaProvinces.add(Province(
      id: "Ogliastra",
      abbreviation: "OG",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  sardegnaProvinces.add(Province(
      id: "Olbia-Tempio",
      abbreviation: "OT",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  sardegnaProvinces.add(Province(
      id: "Oristano",
      abbreviation: "OR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  sardegnaProvinces.add(Province(
      id: "Sassari",
      abbreviation: "SS",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  sardegna.provinces = sardegnaProvinces;
  regions.add(sardegna);
  /**  
        *   SICILIA
        *   Agrigento         AG  
        *   Caltanissetta     CL
        *   Catania           CT
        *   Enna              EN
        *   Messina           ME
        *   Palermo           PA
        *   Ragusa            RG
        *   Siracusa          SR
        *   Trapani           TP
        */
  Region sicilia =
      Region(id: "SICILIA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> siciliaProvinces = [];
  siciliaProvinces.add(Province(
      id: "Agrigento",
      abbreviation: "AG",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  siciliaProvinces.add(Province(
      id: "Caltanissetta",
      abbreviation: "CL",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  siciliaProvinces.add(Province(
      id: "Catania",
      abbreviation: "CT",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  siciliaProvinces.add(Province(
      id: "Enna",
      abbreviation: "EN",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  siciliaProvinces.add(Province(
      id: "Messina",
      abbreviation: "ME",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  siciliaProvinces.add(Province(
      id: "Palermo",
      abbreviation: "PA",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  siciliaProvinces.add(Province(
      id: "Ragusa",
      abbreviation: "RG",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  siciliaProvinces.add(Province(
      id: "Siracusa",
      abbreviation: "SR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  siciliaProvinces.add(Province(
      id: "Trapani",
      abbreviation: "TP",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  sicilia.provinces = siciliaProvinces;
  regions.add(sicilia);
  /**  
        *   TOSCANA
        *   Arezzo          AR  
        *   Firenze         FI
        *   Grosseto        GR
        *   Livorno         LI
        *   Lucca           LU
        *   Massa-Carrara   MS
        *   Pisa            PI
        *   Pistoia         PT
        *   Prato           PO
        *   Siena           SI
        */
  Region toscana =
      Region(id: "TOSCANA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> toscanaProvinces = [];
  toscanaProvinces.add(Province(
      id: "Arezzo",
      abbreviation: "AR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscanaProvinces.add(Province(
      id: "Firenze",
      abbreviation: "FI",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscanaProvinces.add(Province(
      id: "Grosseto",
      abbreviation: "GR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscanaProvinces.add(Province(
      id: "Livorno",
      abbreviation: "LI",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscanaProvinces.add(Province(
      id: "Lucca",
      abbreviation: "LU",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscanaProvinces.add(Province(
      id: "Massa-Carrara",
      abbreviation: "MS",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscanaProvinces.add(Province(
      id: "Pisa",
      abbreviation: "PI",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscanaProvinces.add(Province(
      id: "Pistoia",
      abbreviation: "PT",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscanaProvinces.add(Province(
      id: "Prato",
      abbreviation: "PO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscanaProvinces.add(Province(
      id: "Siena",
      abbreviation: "SI",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  toscana.provinces = toscanaProvinces;
  regions.add(toscana);
  /**  
        *   TRENTINO ALTO ADIGE
        *   Bolzano          BZ  
        *   Trento            TN
        */
  Region trentino = Region(
      id: "TRENTINO ALTO ADIGE", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> trentinoProvinces = [];
  trentinoProvinces.add(Province(
      id: "Bolzano",
      abbreviation: "BZ",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  trentinoProvinces.add(Province(
      id: "Trento",
      abbreviation: "TN",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  trentino.provinces = trentinoProvinces;
  regions.add(trentino);
  /**  
        *   UMBRIA
        *   Perugia          PG  
        *   Terni            TR
        */
  Region umbria =
      Region(id: "UMBRIA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> umbriaProvinces = [];
  umbriaProvinces.add(Province(
      id: "Perugia",
      abbreviation: "PG",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  umbriaProvinces.add(Province(
      id: "Terni",
      abbreviation: "TR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  umbria.provinces = umbriaProvinces;
  regions.add(umbria);
  /**  
        *   VALLE D'AOSTA
        *   Aosta          AO  
        */
  Region valle = Region(
      id: "VALLE D'AOSTA", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> valleProvinces = [];
  valleProvinces.add(Province(
      id: "Aosta",
      abbreviation: "AO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  valle.provinces = valleProvinces;
  regions.add(valle);
  /**  
        *   VENETO
        *   Belluno         BL
        *   Padova          PD
        *   Rovigo          RO
        *   Treviso         TV
        *   Venezia         VE
        *   Verona          VR
        *   Vicenza         VI
        */
  Region veneto =
      Region(id: "VENETO", adsAdoptionsNumber: 0, adsDisappearsNumber: 0, provinces: []);
  List<Province> venetoProvinces = [];
  venetoProvinces.add(Province(
      id: "Belluno",
      abbreviation: "BL",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  venetoProvinces.add(Province(
      id: "Padova",
      abbreviation: "PD",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  venetoProvinces.add(Province(
      id: "Rovigo",
      abbreviation: "RO",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  venetoProvinces.add(Province(
      id: "Treviso",
      abbreviation: "TV",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  venetoProvinces.add(Province(
      id: "Venezia",
      abbreviation: "VE",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  venetoProvinces.add(Province(
      id: "Verona",
      abbreviation: "VR",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  venetoProvinces.add(Province(
      id: "Vicenza",
      abbreviation: "VI",
      adsDisappearsNumber: 0,
      adsAdoptionsNumber: 0));
  veneto.provinces = venetoProvinces;
  regions.add(veneto);

  //AGGIUNGO
  location.regions = regions;

  return location;
}
