import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/model/enum/ad_category.dart';
import 'package:zampon/model/enum/gender.dart';
import 'package:zampon/model/enum/pet_category.dart';
import 'package:zampon/model/enum/pet_size.dart';

///Restituisce la mappa dei tipi di annuncio chiave icona
Map<String, IconData> getAdCategoryMap(BuildContext context) {
  final Map<String, IconData> adCategoryMap = {
    AppLocalizations.of(context)!.adoption: CustomIcons.i1_adoption_icon,
    AppLocalizations.of(context)!.missingPet: CustomIcons.i4_lost_found_pet,
    AppLocalizations.of(context)!.foundCategory: CustomIcons.i3_found_pet_icon,
    AdCategory.Adoption.toString(): CustomIcons.i1_adoption_icon,
    AdCategory.Disappear.toString(): CustomIcons.i4_lost_found_pet,
    AdCategory.Found.toString(): CustomIcons.i3_found_pet_icon,
  };
  return adCategoryMap;
}

///Restituisce la mappa dei tipi di animali chiave icona
Map<String, IconData> getPetTypeMap(BuildContext context) {
  final Map<String, IconData> petTypeMap = {
    AppLocalizations.of(context)!.dog: CustomIcons.i48_dog_type_icon,
    PetCategory.DOG.toString(): CustomIcons.i48_dog_type_icon,
    AppLocalizations.of(context)!.cat: CustomIcons.i47_cat_type_icon,
    PetCategory.CAT.toString(): CustomIcons.i47_cat_type_icon,
    AppLocalizations.of(context)!.otherPet: CustomIcons.i49_other_type_icon,
    PetCategory.OTHER.toString(): CustomIcons.i49_other_type_icon
  };
  return petTypeMap;
}

///Restituisce la mappa dei tipi di animali chiave icona
Map<String, IconData> getPetSizeIconMap(BuildContext context) {
  final Map<String, IconData> petSizeIconMap = {
    "Piccola": Icons.fitness_center,
    PetSize.SMALL.toString(): CustomIcons.i53_small_size_icon,
    "Media": Icons.fitness_center,
    PetSize.MEDIUM.toString(): CustomIcons.i52_medium_size_icon,
    "Grande": Icons.fitness_center,
    PetSize.BIG.toString(): CustomIcons.i51_large_size_icon
  };
  return petSizeIconMap;
}

///Restituisce la mappa dei generi chiave icona
Map<String, IconData> getGenderMap(BuildContext context) {
  final Map<String, IconData> genderMap = {
    AppLocalizations.of(context)!.male: CustomIcons.i24_icona_maschio,
    PetGender.MALE.toString(): CustomIcons.i24_icona_maschio,
    AppLocalizations.of(context)!.female: CustomIcons.i21_icona_femmina,
    PetGender.FEMALE.toString(): CustomIcons.i21_icona_femmina,
    AppLocalizations.of(context)!.unknown:
        CustomIcons.i23_icona_genere_sconosciuto,
    PetGender.OTHER.toString(): CustomIcons.i23_icona_genere_sconosciuto,
  };
  return genderMap;
}

///Restituisce la mappa delle taglie di animali chiave
Map<String, PetSize> _getPetSizeMap(BuildContext context) {
  final Map<String, PetSize> petSizeMap = {
    "Piccola": PetSize.SMALL,
    PetSize.SMALL.toString(): PetSize.SMALL,
    "Media": PetSize.MEDIUM,
    PetSize.MEDIUM.toString(): PetSize.MEDIUM,
    "Grande": PetSize.BIG,
    PetSize.BIG.toString(): PetSize.BIG
  };
  return petSizeMap;
}

///Restituisce il valore dell'enumeratore in base alla taglia ricevuta in input
PetSize? getPetSize(BuildContext context, String value) {
  return _getPetSizeMap(context)[value];
}

///Restituisce la mappa dei tipi di animali chiave PetCategory
Map<String, PetCategory> _getPetCategoryMap(BuildContext context) {
  final Map<String, PetCategory> petCategoryMap = {
    AppLocalizations.of(context)!.dog: PetCategory.DOG,
    PetCategory.DOG.toString(): PetCategory.DOG,
    AppLocalizations.of(context)!.cat: PetCategory.CAT,
    PetCategory.CAT.toString(): PetCategory.CAT,
    AppLocalizations.of(context)!.otherPet: PetCategory.OTHER,
    PetCategory.OTHER.toString(): PetCategory.OTHER
  };
  return petCategoryMap;
}

///Restituisce il valore dell'enumeratore in base alla categoria ricevuta in input
PetCategory? getPetCategory(BuildContext context, String value) {
  return _getPetCategoryMap(context)[value];
}

///Restituisce il valore dell'enumeratore in base al genere ricevuto in input
PetGender getPetGender(BuildContext context, String value) {
  if (value == AppLocalizations.of(context)!.maleLabel) {
    return PetGender.MALE;
  } else if (value == AppLocalizations.of(context)!.femaleLabel) {
    return PetGender.FEMALE;
  } else {
    return PetGender.OTHER;
  }
}

///Effettua la somma di due interi e restituisce una stringa
String adsSum(int adsDisappearsNumber, int adsAdoptionsNumber) {
  return (adsDisappearsNumber + adsAdoptionsNumber).toString();
}

String getPetLabel(BuildContext context, PetCategory? animalCategory) {
  if (animalCategory == PetCategory.DOG) {
    return AppLocalizations.of(context)!.dog;
  } else if (animalCategory == PetCategory.CAT) {
    return AppLocalizations.of(context)!.cat;
  } else if (animalCategory == PetCategory.OTHER) {
    return AppLocalizations.of(context)!.otherPet;
  } else {
    return AppLocalizations.of(context)!.otherPet;
  }
}

String getAdConditionLabel(BuildContext context, AdCategory adCategory) {
  if (adCategory == AdCategory.Adoption) {
    return AppLocalizations.of(context)!.adoption;
  } else if (adCategory == AdCategory.Disappear) {
    return AppLocalizations.of(context)!.missingPet;
  } else if (adCategory == AdCategory.Found) {
    return AppLocalizations.of(context)!.foundCategory;
  } else {
    return AppLocalizations.of(context)!.adoption;
  }
}

PetGender getPetGenderStr(String gender) {
  if (gender == "PetGender.MALE") {
    return PetGender.MALE;
  } else if (gender == "PetGender.FEMALE") {
    return PetGender.FEMALE;
  } else {
    return PetGender.OTHER;
  }
}

String getGenderLabel(BuildContext context, PetGender? petGender) {
  if (petGender == PetGender.MALE) {
    return AppLocalizations.of(context)!.male;
  } else if (petGender == PetGender.FEMALE) {
    return AppLocalizations.of(context)!.female;
  } else {
    return AppLocalizations.of(context)!.unknown;
  }
}

String getSizeLabel(BuildContext context, PetSize? petSize) {
  if (petSize == PetSize.SMALL) {
    return "Piccola";
  } else if (petSize == PetSize.MEDIUM) {
    return "Media";
  } else {
    return "Grande";
  }
}

///Restituisce la mappa della categoria dell'annuncio
Map<String, AdCategory>? _getAdCategory(BuildContext context) {
  final Map<String, AdCategory> adCategoryMap = {
    "Adoption": AdCategory.Adoption,
    AdCategory.Adoption.toString(): AdCategory.Adoption,
    "Disappear": AdCategory.Disappear,
    AdCategory.Disappear.toString(): AdCategory.Disappear,
    "Found": AdCategory.Found,
    AdCategory.Found.toString(): AdCategory.Found
  };
  return adCategoryMap;
}

///Restituisce il valore dell'enumeratore in base alla categoria dell'annuncio ricevuta in input
AdCategory? getAdCategory(BuildContext context, String value) {
  return _getAdCategory(context)![value];
}

///permette di loggare stringhe di lunghezza superiore al 1800(lunghezza massima del Print)
void printWrapped(String text) {
  final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
  pattern.allMatches(text).forEach((match) => print(match.group(0)));
}
