import 'package:flutter/material.dart';
import '../constants.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

//Checkbox per accettare i termini d'uso
Widget conditionCheckbox(BuildContext context, bool value, Function(bool?) onChanged) {
  return Container(
    width: double.infinity,
    child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      Theme(
        data: Theme.of(context).copyWith(unselectedWidgetColor: customWhite),
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.04,
          width: MediaQuery.of(context).size.width * 0.06,
          child: Checkbox(
            activeColor: customPaleLiliac,
            value: value,
            onChanged: onChanged,
            tristate: false,
          ),
        ),
      ),
      SizedBox(
        width: MediaQuery.of(context).size.width * 0.02,
      ),
      Flexible(
        child: Text(
          AppLocalizations.of(context)!.termsConditions,
          style: TextStyle(
            color: customWhite,
            fontWeight: FontWeight.bold,
            fontSize: MediaQuery.of(context).size.width * 0.035,
          ),
        ),
      )
    ]),
  );
}

//Checkbox per la sezione creazione annuncio
Widget newAdCheckbox(
  BuildContext context,
  String text,
  bool value,
  Function(bool?) onChanged, {
  therIsIcon = false,
  Widget? icon,
  String? subtext,
  double topPadding = 0.0,
  double bottomPadding = 0.0,
}) {
  return Container(
    padding: EdgeInsets.only(
      left: MediaQuery.of(context).size.width * 0.05,
      right: MediaQuery.of(context).size.width * 0.025,
      top: topPadding,
      bottom: bottomPadding,
    ),
    child: Row(children: [
      therIsIcon == true
          ? Container(
              width: MediaQuery.of(context).size.width * 0.15,
              child: icon,
            )
          : Container(),
      Container(
        padding: EdgeInsets.only(),
        width: MediaQuery.of(context).size.width * 0.65,
        alignment: Alignment.centerLeft,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            text,
            style: TextStyle(
              color: customWhite,
              fontWeight: FontWeight.bold,
              fontSize: MediaQuery.of(context).size.width * 0.045,
            ),
          ),
          subtext == null
              ? Container()
              : Text(
                  subtext,
                  style: TextStyle(
                    color: customWhite,
                    fontWeight: FontWeight.bold,
                    fontSize: MediaQuery.of(context).size.width * 0.03,
                  ),
                ),
        ]),
      ),
      Theme(
        data: Theme.of(context).copyWith(unselectedWidgetColor: customWhite),
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.04,
          width: MediaQuery.of(context).size.width * 0.06,
          child: Checkbox(
            activeColor: customPaleLiliac,
            value: value,
            onChanged: onChanged,
            tristate: false,
          ),
        ),
      ),
    ]),
  );
}
