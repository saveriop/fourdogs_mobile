import 'package:flutter/material.dart';
import 'package:zampon/l10n/l10n.dart';
import 'package:zampon/services/locale_provider.dart';
import 'package:provider/provider.dart';

class LanguagePickerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final LocaleProvider? provider = Provider.of<LocaleProvider>(context);
    final Locale? locale = provider!.locale ?? Locale('it');

    return DropdownButtonHideUnderline(
      child: DropdownButton(
        value: locale,
        icon: Container(width: MediaQuery.of(context).size.width * 0.02),
        items: L10n.all.map(
          (locale) {
            final flag = L10n.getFlag(locale.languageCode);

            return DropdownMenuItem(
              child: Center(
                child: Text(
                  flag,
                  style: TextStyle(
                      fontSize: MediaQuery.of(context).size.height * 0.02),
                ),
              ),
              value: locale,
              onTap: () {
                final provider =
                    Provider.of<LocaleProvider>(context, listen: false);

                provider.setLocale(locale);
              },
            );
          },
        ).toList(),
        onChanged: (_) {},
      ),
    );
  }
}
