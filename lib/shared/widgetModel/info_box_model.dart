import 'package:flutter/material.dart';
import 'package:zampon/shared/constants.dart';

Widget petAgeInfoLabel(BuildContext context, String getAdLabel, IconData icon,
    {bool isClosed = false}) {
  return Padding(
    padding: const EdgeInsets.symmetric(),
    child: Container(
      height: MediaQuery.of(context).size.height * 0.035,
      width: MediaQuery.of(context).size.width * 0.3,
      decoration: BoxDecoration(
        color: customWhite,
        borderRadius: mediumBorderRadius(context),
      ),
      child: Stack(alignment: AlignmentDirectional.center, children: <Widget>[
        FractionalTranslation(
          translation: Offset(-0.52, -0.28),
          child: circleContainer(context, icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.035,
              isClosed: isClosed),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              getAdLabel,
              style: TextStyle(
                color: isClosed ? isClosedColor : customStrongLiliac,
                fontWeight: FontWeight.bold,
                fontSize: MediaQuery.of(context).size.height * 0.015,
              ),
            )
          ],
        ),
      ]),
    ),
  );
}

///Return a circle label of pet's category
Widget petInfoLabel(BuildContext context, String getAdLabel, IconData icon,
    {bool isClosed = false}) {
  return Padding(
    padding: const EdgeInsets.symmetric(),
    child: Container(
      height: MediaQuery.of(context).size.height * 0.04,
      width: MediaQuery.of(context).size.width * 0.2,
      decoration: BoxDecoration(
        color: customWhite,
        borderRadius: mediumBorderRadius(context),
      ),
      child: Stack(alignment: AlignmentDirectional.center, children: <Widget>[
        FractionalTranslation(
          translation: Offset(-0.52, -0.28),
          child: circleContainer(context, icon,
              color: customWhite,
              size: MediaQuery.of(context).size.height * 0.035,
              isClosed: isClosed),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              getAdLabel,
              style: TextStyle(
                color: isClosed ? isClosedColor : customStrongLiliac,
                fontWeight: FontWeight.bold,
                fontSize: MediaQuery.of(context).size.height * 0.016,
              ),
            )
          ],
        ),
      ]),
    ),
  );
}

Widget circleContainer(BuildContext context, IconData icon,
    {Color color = Colors.white, double size = 20, bool isClosed = false}) {
  return Container(
      alignment: Alignment.center,
      child: InkWell(
          child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: color,
        ),
        child: Icon(
          icon,
          size: MediaQuery.of(context).size.height * 0.023,
          color: isClosed ? isClosedColor : customStrongLiliac,
        ),
        alignment: Alignment.center,
      )));
}
