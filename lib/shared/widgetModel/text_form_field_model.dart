import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:zampon/model/ad.dart';
import 'package:zampon/model/italian_city.dart';
import 'package:zampon/model/location.dart';
import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/validate.dart';

/*
 * Widget custom per centralizzare tutti le TextFormField. 
 * Tutti i parametri sono opzionali
 * Tutte le definizioni delle decorazioni sono definiti nel metodo @textFormFieldCustomDecoration
 * @param keyboardType: tipo di tastiera. Valore di default "Text"
 * @param initialValue: valore iniziale della InputBox. Se non valorizzato sarà ""
 * @param maxLength: lunghezza massima. Se non valorizzato non ci sarà limitazione
 * @param hintText: Testo che appare nella inputBox quando vuota.
 * @param labelText: Testo della label dell'inputBox
 * @param icon: Logo che apparirà alla sinistra dell'input box.
 * @param onChanged: funzione che si esegue ad ogni evento onChange della inputBox
 * @param validator: funzione che si esegue la validazione del dato inserito dall'utente nel momento della conferma
 */
Widget textFormFieldCustom(
  BuildContext context, {
  TextInputType? keyboardType,
  String? initialValue,
  int? maxLength,
  int? maxLines,
  int? minLines,
  String? hintText,
  String? labelText,
  Widget? icon,
  Function(String?)? onChanged,
  String? Function(String?)? validator,
  TextCapitalization? textCapitalization,
  bool noIcon = false,
  double? fontSize,
  bool readOnly = false,
  bool isResettable = false,
}) {
  return TextFormField(
    inputFormatters: [
      LengthLimitingTextInputFormatter(maxLength),
    ], // for mobile
    key: isResettable ? Key(initialValue!) : null,
    maxLengthEnforcement: MaxLengthEnforcement.truncateAfterCompositionEnds,
    readOnly: readOnly,
    keyboardType: keyboardType == null ? TextInputType.text : keyboardType,
    initialValue: initialValue == null ? "" : initialValue,
    textCapitalization: textCapitalization == null
        ? TextCapitalization.none
        : textCapitalization,
    maxLength: maxLength,
    minLines: minLines,
    maxLines: maxLines,
    style: TextStyle(
      color: customStrongLiliac,
      fontSize: fontSize == null
          ? MediaQuery.of(context).size.width * 0.045
          : fontSize,
    ),
    decoration: noIcon
        ? textFormFieldCustomDecorationNoIcon(
            context,
            hintText: hintText,
            labelText: labelText,
          )
        : textFormFieldCustomDecoration(context,
            hintText: hintText, labelText: labelText, icon: icon),
    onChanged: onChanged,
    validator: validator,
  );
}


/*
 * Decorazione di default del textFormFieldCustomDecoration
 * Gli unici parametri in input sono ereditati dal metodo chiamante.
 * Tutte le opzioni grafiche sono centralizzate in questo metodo
 */
InputDecoration textFormFieldCustomDecoration(BuildContext context,
    {String? hintText, String? labelText, Widget? icon}) {
  return InputDecoration(
    counterStyle: TextStyle(color: customWhite),
    fillColor: customWhite,
    focusColor: customPaleLiliac,
    hoverColor: customPaleLiliac,
    //colore del testo di default
    labelStyle: TextStyle(color: customPaleLiliac),
    border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
    filled: true,
    icon: icon,
    hintText: hintText,
    //rimozione testo sopraelevato nella textbox quando selezionato
    floatingLabelBehavior: FloatingLabelBehavior.never,
    //colore testo sopraelevato nella textbox quando selezionato
    floatingLabelStyle: TextStyle(color: customWhite, ),
    labelText: labelText,
    focusedBorder: OutlineInputBorder(
      borderRadius: mediumBorderRadius(context),
      borderSide: BorderSide(
          width: MediaQuery.of(context).size.width * 0.0001,
          color: customPaleLiliac),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: mediumBorderRadius(context),
      borderSide: BorderSide(
          width: MediaQuery.of(context).size.width * 0.0001,
          color: customPaleLiliac),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: mediumBorderRadius(context),
      borderSide: BorderSide(
          width: MediaQuery.of(context).size.width * 0.0001,
          color: Colors.white),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: mediumBorderRadius(context),
      borderSide: BorderSide(
          width: MediaQuery.of(context).size.width * 0.0001,
          color: Colors.white),
    ),
    contentPadding: EdgeInsets.only(
      left: MediaQuery.of(context).size.width * 0.05,
      right: MediaQuery.of(context).size.width * 0.05,
      top: MediaQuery.of(context).size.height * 0.01,
      bottom: MediaQuery.of(context).size.height * 0.01,
    ),
    //counterText: ""
  );
}

/*
 * Decorazione di default del textFormFieldCustomDecoration
 * Gli unici parametri in input sono ereditati dal metodo chiamante.
 * Tutte le opzioni grafiche sono centralizzate in questo metodo
 */
InputDecoration textFormFieldCustomDecorationNoIcon(BuildContext context,
    {String? hintText, String? labelText}) {
  return InputDecoration(
        //rimozione testo sopraelevato nella textbox quando selezionato
    floatingLabelBehavior: FloatingLabelBehavior.never,
    //colore testo sopraelevato nella textbox quando selezionato
    floatingLabelStyle: TextStyle(color: customWhite, ),
    counterStyle: TextStyle(color: customWhite),
    fillColor: customWhite,
    focusColor: customWhite,
    hoverColor: customWhite,
    labelStyle: TextStyle(
      color: customStrongLiliac,
      fontSize: MediaQuery.of(context).size.width * 0.055,
    ),
    border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
    filled: true,
    hintText: hintText,
    labelText: labelText,
    focusedBorder: OutlineInputBorder(
      borderRadius: mediumBorderRadius(context),
      borderSide: BorderSide(
          width: MediaQuery.of(context).size.width * 0.0001,
          color: customPaleLiliac),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: mediumBorderRadius(context),
      borderSide: BorderSide(
          width: MediaQuery.of(context).size.width * 0.0001,
          color: Colors.white),
    ),
    //counterText: ""
  );
}



//Text form senza ICONA
Widget noIconTextFormFieldCustom(
  BuildContext context, {
  TextInputType? keyboardType,
  String? initialValue,
  int? maxLength,
  required String hintText,
  String labelText = "",
  Function(String?)? onChanged,
  String? Function(String?)? validator,
  bool obscureText = false,
  bool enabled = true,
  TextCapitalization? textCapitalization,
  int maxLines = 1,
  int minLines = 1,
}) {
  return Padding(
    padding:
        EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.025),
    child: Container(
      width: MediaQuery.of(context).size.width * 0.85,
      height: MediaQuery.of(context).size.height * 0.08,
      child: TextFormField(
        maxLines: maxLines,
        minLines: minLines,
        enabled: enabled,
        obscureText: obscureText,
        textAlign: TextAlign.center,
        keyboardType: keyboardType == null ? TextInputType.text : keyboardType,
        initialValue: initialValue == null ? "" : initialValue,
        textCapitalization: textCapitalization == null
            ? TextCapitalization.none
            : textCapitalization,
        maxLength: maxLength,
        style: TextStyle(
          color: customStrongLiliac,
          fontSize: MediaQuery.of(context).size.width * 0.055,
        ),
        decoration:
            _noIconTextFormFieldCustomDecoration(context, hintText, labelText, isEnable: enabled),
        onChanged: onChanged,
        validator: validator,
      ),
    ),
  );
}

/*
 * Decorazione di default del textFormFieldCustomDecoration
 * Gli unici parametri in input sono ereditati dal metodo chiamante.
 * Tutte le opzioni grafiche sono centralizzate in questo metodo
 */
InputDecoration _noIconTextFormFieldCustomDecoration(
    BuildContext context, String hintText, String labelText, {bool isEnable = true}) {
  return InputDecoration(
      counterStyle: TextStyle(color: customWhite),
      fillColor: isEnable ? customWhite : customPaleGrey,
      focusColor: customPaleLiliac,
      hoverColor: customPaleLiliac,
      labelStyle: TextStyle(color: customPaleLiliac),
      border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      filled: true,
      hintText: hintText,
      hintStyle: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.055, color: customPaleLiliac),
      floatingLabelBehavior: FloatingLabelBehavior.always,
      labelText: labelText,
      focusedBorder: OutlineInputBorder(
        borderRadius: mediumBorderRadius(context),
        borderSide: BorderSide(
            width: MediaQuery.of(context).size.width * 0.0001,
            color: customPaleLiliac),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: mediumBorderRadius(context),
        borderSide: BorderSide(
            width: MediaQuery.of(context).size.width * 0.0001,
            color: customPaleLiliac),
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: mediumBorderRadius(context),
        borderSide: BorderSide(
            width: MediaQuery.of(context).size.width * 0.0001,
            color: customPaleLiliac),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: mediumBorderRadius(context),
        borderSide: BorderSide(
            width: MediaQuery.of(context).size.width * 0.0001,
            color: Colors.red),
      ),
      counterText: "");
}

//Autocomplete per la selezione del comune , Provincia, Regione
String _displayStringForOption(ItalianCity option) => option.city!;
List<ItalianCity> _citiesList = getItalianCitiesList();
//List<ItalianCity> _citiesList = getPugliaCitiesList();

Widget textFormFieldForItalianCity(
  BuildContext context,
  AdElements adElements, {
  bool noIcon = false,
  String? hintText,
  String? labelText,
  Widget? icon,
}) {
  return Padding(
    padding:
        EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.00029),
    child: Autocomplete<ItalianCity>(
      initialValue:
          adElements.adLocation != null
          ? TextEditingValue(text: Location.initialValue(adElements.adLocation!))
          : TextEditingValue(text: Location.initialValue(Location(city: "", country: "", province: "", region: ""))),
      optionsBuilder: (TextEditingValue textEditingValue) {
        if (textEditingValue.text == '') {
          return const Iterable<ItalianCity>.empty();
        }
        return _citiesList
            .where((ItalianCity option) => option.city!
                .toLowerCase()
                .startsWith(textEditingValue.text.toLowerCase()))
            .toList();
      },
      displayStringForOption: _displayStringForOption,
      fieldViewBuilder: (BuildContext context,
          TextEditingController fieldTextEditingController,
          FocusNode fieldFocusNode,
          VoidCallback onFieldSubmitted) {
        //fieldTextEditingController = TextEditingController(text: Location.initialValue(this.widget.adElements.adLocation));
        return TextFormField(
          //initialValue: Location.initialValue(this.widget.adElements.adLocation),
          validator: (val) {
            return validateLocation(val!);
          },
          controller: fieldTextEditingController,
          focusNode: fieldFocusNode,
          style: TextStyle(color: customStrongLiliac),
          /*decoration: InputDecoration(
              focusColor: customPaleLiliac,
              hoverColor: customPaleLiliac,
              labelStyle: TextStyle(color: customPaleLiliac),
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.red)),
              filled: true,
              icon: Icon(
                Icons.place,
                color: customPaleLiliac,
              ),
              hintText: AppLocalizations.of(context).city +
                  ", " +
                  AppLocalizations.of(context).province +
                  ", " +
                  AppLocalizations.of(context).region,
              labelText: AppLocalizations.of(context).city + " *",
              focusedBorder: OutlineInputBorder(
                borderRadius: mediumBorderRadius(context),
                borderSide: BorderSide(width: MediaQuery.of(context).size.width * 0.0001, color: customPaleLiliac),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: mediumBorderRadius(context),
                borderSide: BorderSide(width: MediaQuery.of(context).size.width * 0.0001, color: Colors.white),
              ),
            )*/
          decoration: noIcon
              ? textFormFieldCustomDecorationNoIcon(
                  context,
                  hintText: hintText!,
                  labelText: labelText!,
                )
              : textFormFieldCustomDecoration(context,
                  hintText: hintText, labelText: labelText, icon: icon),
        );
      },
      onSelected: (ItalianCity selection) {
        adElements.adLocation = Location.getLocation(selection.city!);
      },
    ),
  );
}



