import 'package:flutter/material.dart';

/*
 * To request more data we need to know when we reach the bottom of the list and call our refresh functionality.
 * To do that we will use the CreationAwareListItem pattern that I shared in this tutorial a few months back.
 * Under the widgets folder create a new file called creation_aware_list_item.dart
 */
class CreationAwareListItem extends StatefulWidget {

  final Function? itemCreated;
  final Widget? child;

   CreationAwareListItem({
    this.itemCreated,
    this.child
  });

  @override
  _CreationAwareListItemState createState() => _CreationAwareListItemState();
}

class _CreationAwareListItemState extends State<CreationAwareListItem> {

  @override
  void initState() {
    super.initState();
    if(widget.itemCreated != null){
      widget.itemCreated!();
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.child!;
  }
}