import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/screens/home/settings/userProfilePage/modifyUserData/modify_user_img.dart';
import 'package:zampon/screens/home/settings/userProfilePage/personal_profile_stream_provider.dart';

import 'package:zampon/services/firebase/storage/storage_dp.dart';
import 'package:zampon/shared/constants.dart';

class UserAvatar extends StatefulWidget {
  final String userUid;
  final bool personalUser;
  final bool isLoggedUser;
  final String? calledUid;
  final bool profileScreen;

  //uid dell'utente per la quale si richiede l'immagine profilo
  UserAvatar(
      {required this.userUid,
      this.personalUser = false,
      this.isLoggedUser = false,
      this.calledUid,
      this.profileScreen = false});

  @override
  _UserAvatarState createState() => _UserAvatarState();
}

class _UserAvatarState extends State<UserAvatar> {
  _fetchData() {
    return new StorageDp().getProfileImgReference(this.widget.userUid);
  }

  /*
   *  Immagine profilo 
   */
  Widget _cachedImgProfile(AsyncSnapshot<dynamic> snapshot) {
    return GestureDetector(
      onTap: () async {
        if (this.widget.personalUser && this.widget.isLoggedUser) {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ModifyUserImg(
                        uid: this.widget.userUid,
                        photoUrl: snapshot.data,
                      )));
          setState(() {});
        } else {
          if (!this.widget.profileScreen) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PersonalProfileStreamProvider(
                        userUid: this.widget.calledUid!,
                        calledUid: this.widget.userUid)));
          }
        }
      },
      child: Stack(children: [
        Positioned.fill(
          child: ClipOval(
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              //nel momento in cui viene aggiunto un nuovo annuncio,
              //prima di scaricarsi l'img utilizza l'ultima in memoria se a TRUE
              useOldImageOnUrlChange: true,
              imageUrl: snapshot.data,
              imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
              )),
              placeholder: (context, url) => Center(
                child: CircularProgressIndicator(),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        ),
        this.widget.personalUser && this.widget.isLoggedUser
            ? Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: customWhite,
                  ),
                  child: IconButton(
                      onPressed: () async {
                        await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ModifyUserImg(
                                      uid: this.widget.userUid,
                                      photoUrl: snapshot.data,
                                    )));
                        setState(() {});
                      },
                      icon: Icon(
                        CustomIcons.i54_icona_plus,
                        color: customStrongLiliac,
                      ),
                      iconSize: MediaQuery.of(context).size.height * 0.05),
                ),
              )
            : Container(),
      ]),
    );
  }

  /*
   * Immagine tonda del profilo
   * Si tratta di un futureBuilder che si collega allo store
   * Nel caso in cui l'immagine non viene trovata sarà mostrato a video un punto esclamativo 
   */
  Widget _image() {
    return FutureBuilder(
        future: this._fetchData(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('none');
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text(
                  '${snapshot.error}',
                  style: TextStyle(color: Colors.red),
                );
              }
          }
          return this.widget.personalUser
              ? Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: MediaQuery.of(context).size.height * 0.25,
                  decoration: BoxDecoration(
                    color: customWhite,
                    shape: BoxShape.circle,
                    border: Border.all(
                        color: customWhite, // Set border color
                        width: MediaQuery.of(context).size.width * 0.01),
                  ),
                  child: _cachedImgProfile(snapshot))
              : GestureDetector(
                  onTap: () {
                    /*    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PersonalProfileStreamProvider(
                    userUid: this.widget.userUid,
                    calledUid: this.widget.userUid)));*/
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.16,
                    height: MediaQuery.of(context).size.height * 0.08,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: customStrongLiliac,
                        width: MediaQuery.of(context).size.width * 0.01,
                      ),
                    ),
                    child: _cachedImgProfile(snapshot),
                  ),
                );
        });
  }

  @override
  Widget build(BuildContext context) {
    //restituisce l'immagine profilo in base l'uid che riceve questa classe nel suo costruttore
    return _image();
  }
}
