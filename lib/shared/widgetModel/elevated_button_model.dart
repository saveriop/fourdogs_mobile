import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zampon/shared/constants.dart';

/*
 *  Restituisce lo stile dell'elevatedButton 
 */
ButtonStyle elevatedButtonStyleFrom(BuildContext context) {
  return ElevatedButton.styleFrom(
    backgroundColor: customPaleLiliac,
    foregroundColor: Colors.white,
    shape: RoundedRectangleBorder(
      borderRadius: mediumBorderRadius(context),
    ),
  );
}

elevationButtonStyleAdDetail(BuildContext context) {
  return ElevatedButton.styleFrom(
    enableFeedback: true,
    shadowColor: Colors.black,
    backgroundColor: customWhite,
    foregroundColor: customStrongLiliac,
    shape: RoundedRectangleBorder(
      side: BorderSide(
        color: customWhite,
      ),
      borderRadius: mediumBorderRadius(context),
    ),
  );
}

_confirmButtibStyleConfirm(BuildContext context) {
  return ElevatedButton.styleFrom(
    alignment: Alignment.center,
    //deactivate color and shadow
    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
    backgroundColor: Colors.transparent,
    shadowColor: Colors.transparent,
    elevation: 0,
    //end
    shape: RoundedRectangleBorder(
        borderRadius: mediumBorderRadius(context),
        side: BorderSide(color: customWhite)),
  );
}

//Bottone per confermare della sezione Login
Widget confirmationButton(
    BuildContext context, String buttonText, VoidCallback onPressed,
    {double? width,
    double? height,
    IconData? icon,
    Color iconColor = Colors.white}) {
  // Per ripristinare l'elevazione del bottone effettuare un refactor del Padding dentro un Material
  // e mettere il valore di elevation nel Material
  return Padding(
    padding: EdgeInsets.only(),
    child: Container(
      width: width == null ? MediaQuery.of(context).size.width * 0.6 : width,
      height:
          height == null ? MediaQuery.of(context).size.height * 0.06 : height,
      decoration: BoxDecoration(
          borderRadius: mediumBorderRadius(context),
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [customPaleLiliac, customStrongLiliac])),
      child: icon != null
          ? ElevatedButton.icon(
              style: _confirmButtibStyleConfirm(context),
              icon: Icon(icon, color: iconColor),
              onPressed: onPressed,
              label: Text(
                " " + buttonText,
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width * 0.04,
                  color: customWhite,
                ),
              ),
            )
          : ElevatedButton(
              style: _confirmButtibStyleConfirm(context),
              onPressed: onPressed,
              child: Text(
                buttonText,
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width * 0.05,
                  color: customWhite,
                ),
              ),
            ),
    ),
  );
}

//Bottone per Accedere nella seizione Login
Widget loginButtonWithIcon(
  BuildContext context,
  FaIcon buttonIcon,
  String buttonText,
  VoidCallback onPressed,
) {
  // Per ripristinare l'elevazione del bottone effettuare un refactor del Padding dentro un Material
  // e mettere il valore di elevation nel Material
  return Padding(
    padding:
        EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.025),
    child: Container(
      width: MediaQuery.of(context).size.width * 0.85,
      height: MediaQuery.of(context).size.height * 0.075,
      decoration: BoxDecoration(
          borderRadius: mediumBorderRadius(context),
          gradient: LinearGradient(colors: [customWhite, customWhite])),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          //deactivate color and shadow
          foregroundColor: customStrongGrey,
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          elevation: 0,
          //end
        ),
        child: Row(children: [
          Container(child: buttonIcon),
          Expanded(
            child: Text(
              buttonText,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.055,
                color: customStrongLiliac,
              ),
            ),
          ),
        ]),
        onPressed: onPressed,
      ),
    ),
  );
}

//Bottone per resettare la Password
Widget resetPassButtonWithIcon(
  BuildContext context,
  Icon buttonIcon,
  String buttonText,
  VoidCallback onPressed,
) {
  // Per ripristinare l'elevazione del bottone effettuare un refactor del Padding dentro un Material
  // e mettere il valore di elevation nel Material
  return Padding(
    padding:
        EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.025),
    child: Container(
      width: MediaQuery.of(context).size.width * 0.85,
      height: MediaQuery.of(context).size.height * 0.08,
      decoration: BoxDecoration(
          border: Border.all(
              color: customWhite,
              width: MediaQuery.of(context).size.width * 0.007),
          borderRadius: mediumBorderRadius(context),
          gradient:
              LinearGradient(colors: [customPaleLiliac, customStrongLiliac])),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          //deactivate color and shadow
          foregroundColor: customStrongGrey,
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          elevation: 0,
          //end
        ),
        child: Row(children: [
          Container(child: buttonIcon),
          Expanded(
            child: Text(
              buttonText,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.055,
                color: customWhite,
              ),
            ),
          ),
        ]),
        onPressed: onPressed,
      ),
    ),
  );
}

Widget accountNavigationButton(
  BuildContext context,
  Icon buttonIcon,
  String buttonText,
  VoidCallback onPressed,
) {
  // Per ripristinare l'elevazione del bottone effettuare un refactor del Padding dentro un Material
  // e mettere il valore di elevation nel Material
  return Padding(
    padding:
        EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.025),
    child: Container(
      width: MediaQuery.of(context).size.width * 0.85,
      height: MediaQuery.of(context).size.height * 0.08,
      decoration: BoxDecoration(
          border: Border.all(
              color: customWhite,
              width: MediaQuery.of(context).size.width * 0.007),
          borderRadius: mediumBorderRadius(context),
          gradient: LinearGradient(colors: [customWhite, customWhite])),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          //deactivate color and shadow
          foregroundColor: customStrongGrey,
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          elevation: 0,
          //end
        ),
        child: Row(children: [
          Container(child: buttonIcon),
          Expanded(
            child: Text(
              buttonText,
              textAlign: TextAlign.center,
              style: TextStyle(
                //fontFamily: 'InUse',
                fontSize: MediaQuery.of(context).size.width * 0.055,
                color: customStrongLiliac,
              ),
            ),
          ),
        ]),
        onPressed: onPressed,
      ),
    ),
  );
}

Widget watchVideoButton(
  BuildContext context,
  Icon buttonIcon,
  String buttonText,
  VoidCallback onPressed,
) {
  // Per ripristinare l'elevazione del bottone effettuare un refactor del Padding dentro un Material
  // e mettere il valore di elevation nel Material
  return Padding(
    padding:
        EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.025),
    child: Container(
      width: MediaQuery.of(context).size.width * 0.85,
      height: MediaQuery.of(context).size.height * 0.08,
      decoration: BoxDecoration(
          border: Border.all(
              color: customWhite,
              width: MediaQuery.of(context).size.width * 0.007),
          borderRadius: mediumBorderRadius(context),
          gradient: LinearGradient(colors: [green1, green1])),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          //deactivate color and shadow
          foregroundColor: customWhite,
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          elevation: 0,
          //end
        ),
        child: Row(children: [
          Expanded(
            child: Text(
              buttonText,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.055,
                color: customWhite,
              ),
            ),
          ),
          Container(child: buttonIcon),
        ]),
        onPressed: onPressed,
      ),
    ),
  );
}

Widget buttonWithRightIcon(
  BuildContext context,
  String buttonText,
  Icon buttonIcon,
  VoidCallback onPressed, {
  double? width,
  double? height,
}) {
  // Per ripristinare l'elevazione del bottone effettuare un refactor del Padding dentro un Material
  // e mettere il valore di elevation nel Material
  return Padding(
    padding: EdgeInsets.only(),
    child: Container(
      width: width == null ? MediaQuery.of(context).size.width * 0.6 : width,
      height:
          height == null ? MediaQuery.of(context).size.height * 0.06 : height,
      decoration: BoxDecoration(
          borderRadius: mediumBorderRadius(context),
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [customPaleLiliac, customStrongLiliac])),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          //deactivate color and shadow
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          elevation: 0,
          //end
          shape: RoundedRectangleBorder(
              borderRadius: mediumBorderRadius(context),
              side: BorderSide(color: customWhite)),
        ),
        onPressed: onPressed,
        child: Row(children: [
          Expanded(
            child: Text(
              buttonText,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.05,
                color: customWhite,
              ),
            ),
          ),
          Container(child: buttonIcon),
        ]),
      ),
    ),
  );
}
