import 'package:flutter/material.dart';
//import 'package:zampon/shared/constants.dart';

Widget logoAndName(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05),
    child: Container(
      child: Image(image: AssetImage('assets/logo/medium_logo_name.png')),
    ),
  );
}

Widget noPaddingLogoAndName(BuildContext context) {
  return Container(
    child: Image(image: AssetImage('assets/logo/medium_logo_name.png')),
  );
}

Widget logoAndNameRegistration(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.0000009,
        bottom: MediaQuery.of(context).size.height * 0.02),
    child: Container(
      child: Image(image: AssetImage('assets/logo/medium_logo_name.png')),
    ),
  );
}
