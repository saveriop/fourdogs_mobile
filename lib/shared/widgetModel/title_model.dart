import 'package:flutter/material.dart';
import 'package:zampon/shared/constants.dart';

//Titolo centrale composto da LOGO + TESTO
Widget titleRow(BuildContext context, String title, IconData icon) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Icon(icon,
          color: customWhite, size: MediaQuery.of(context).size.height * 0.02),
      Text(
        title,
        style: TextStyle(
            color: customWhite,
            fontSize: MediaQuery.of(context).size.height * 0.02,
            fontWeight: FontWeight.bold),
      )
    ],
  );
}

Widget onlyTextTitle(context) {
  return Container(
    padding: EdgeInsets.only(
      top: MediaQuery.of(context).size.height * 0.02,
    ),
    child: Image(
      image: AssetImage('assets/images/title/ZampOnTitle.png'),
      height: MediaQuery.of(context).size.height * 0.55,
      width: MediaQuery.of(context).size.width * 0.55,
    ),
  );
}

Widget homeTitle(context,
    {Animation<double>? animation,
    bool? isPageNameText,
    String? pageNameText,
    AssetImage? pageNameImg}) {
  if (animation == null) animation = AlwaysStoppedAnimation<double>(1);
  return Row(
    children: [
      Container(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.025,
          bottom: MediaQuery.of(context).size.height * 0.0125,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topRight: mediumRadius(context),
            bottomRight: mediumRadius(context),
          ),
          color: customWhite,
        ),
        child: Row(children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.02,
          ),
          Image(
              image: AssetImage('assets/logo/small_logo_no_name.png'),
              height: MediaQuery.of(context).size.height * 0.1,
              width: MediaQuery.of(context).size.width * 0.1,
              color: customStrongLiliac),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.00,
          ),
          Image(
            image: AssetImage('assets/images/title/ZampOnTitle.png'),
            height: MediaQuery.of(context).size.height * 0.50,
            width: MediaQuery.of(context).size.width * 0.50,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.08,
          ),
          pageNameImg != null
              ? Image(
                  image: pageNameImg,
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.2,
                )
              : SizedBox(
                  width: MediaQuery.of(context).size.width * 0.0,
                ),
        ]),
      ),
    ],
  );
}

///Stile titolo utilizzato nei pop-up
Widget titleStyle(String title, {double fontSize = 24.0}) {
  return Text(title,
      style: TextStyle(
          color: customWhite, fontSize: fontSize, fontWeight: FontWeight.bold),
      textAlign: TextAlign.center);
}

Widget pageName(context, String pageName) {
  return Container(
    color: customStrongLiliac,
    width: MediaQuery.of(context).size.width,
    child: Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.01,
        bottom: MediaQuery.of(context).size.height * 0.01,
      ),
      child: Text(
        pageName,
        textAlign: TextAlign.center,
        style: TextStyle(
            color: customWhite,
            fontSize: MediaQuery.of(context).size.height * 0.035,
            fontWeight: FontWeight.bold),
      ),
    ),
  );
}
