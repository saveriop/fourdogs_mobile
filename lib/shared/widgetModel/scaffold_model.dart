import 'package:flutter/material.dart';
import 'package:zampon/shared/widgetModel/app_bar_model.dart';
import '../constants.dart';

/// Scaffold generico per una pagina non sliver
/// Richiede in input una lista di widget che saranno posizionati in una Column
/// @param back: indica se mostrare il tasto indietro sulla destra dell'appbar
/// @param columnElementList: contiene la lista dei widget che saranno incolonnati
///  @param appbar: se settato a false la pagina sarà priva di appbar. Valore di default true
Widget genericPage(BuildContext context, bool isBackButton,
    {List<Widget>? columnElementList,
    bool appbar = true,
    bool trasparent = false,
    bool noTitle = false,
    bool neverScroll = false,
    bool extendBody = false,
    Color backButtonColor = customStrongLiliac}) {
  //se è richiesta la  presenza del tasto back ma non quella dell'appbar, tale tasto sarà inserito nel body
  bool backButtonOutOfAppbar = (!appbar && isBackButton) ? true : false;
  if (columnElementList == null) columnElementList = <Widget>[];
  return Scaffold(
    extendBody: extendBody,
    appBar: appbar
        ? customAppBar(context,
            isBackButton: isBackButton,
            trasparent: trasparent,
            noTitle: noTitle)
        : null,
    body: SingleChildScrollView(
      physics: neverScroll ? const NeverScrollableScrollPhysics() : null,
      child: Container(
        padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.01),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: _linearGradient(),
        child: SingleChildScrollView(
          child: Column(children: [
            backButtonOutOfAppbar
                ? Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.03),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: backButton(context, backButtonColor),
                    ),
                  )
                : Container(),
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: List.generate(columnElementList.length, (index) {
                  return columnElementList![index];
                }),
              ),
            ),
          ]),
        ),
      ),
    ),
  );
}

/// Scaffold generico per una pagina sliver
/// Richiede in input una lista di widget che dovranno essere compatibili con le slivers che saranno posizionati in una Column
/// N.B. Le liste di immagini devono essere wrappate in un Expanded
Widget genericSliversPage(BuildContext context,
    {List<Widget>? columnElementList,
    bool wrapInSingleChildScrollView = false,
    Color backgroundcolor = Colors.transparent}) {
  if (columnElementList == null) columnElementList = <Widget>[];
  return Scaffold(
    backgroundColor: backgroundcolor,
    body: Stack(children: [
      Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: backgroundcolor == Colors.transparent
            ? _linearGradient()
            : _monochromeBackground(backgroundcolor),
        child: NestedScrollView(
          headerSliverBuilder: (context, value) {
            return [
              sliverAppBar(context, true),
            ];
          },
          body: wrapInSingleChildScrollView
              ? SingleChildScrollView(
                  child: Column(
                      children: List.generate(columnElementList.length,
                          (index) {
                    return columnElementList![index];
                  })),
                )
              : Column(
                  children:
                      List.generate(columnElementList.length, (index) {
                  return columnElementList![index];
                })),
        ),
      ),
    ]),
  );
}

BoxDecoration _monochromeBackground(Color color) {
  return BoxDecoration(color: color);
}

///Sfumatura dello scaffold
BoxDecoration _linearGradient() {
  return BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          // Add one stop for each color
          // Values should increase from 0.0 to 1.0
          stops: [
        0.5,
        0.9
      ],
          colors: [
        customStrongLiliac,
        customPaleLiliac,
      ]));
}
