import 'package:flutter/material.dart';
import 'package:zampon/assets/custom_icons.dart';
import 'package:zampon/shared/widgetModel/title_model.dart';
import '../constants.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

//PopUp con due bottoni
Widget popUpCustom(BuildContext context, IconData icon, String titleText,
    String bodyText, ElevatedButton firstButton, ElevatedButton secondButton,
    {Color? iconColor}) {
  return WillPopScope(
    onWillPop: () async {
      return false;
    },
    child: AlertDialog(
        titlePadding: EdgeInsets.only(),
        backgroundColor: Colors.transparent,
        title: Container(
          padding: _popUpPadding(context),
          decoration: BoxDecoration(
            borderRadius: mediumBorderRadius(context),
            color: customStrongLiliac,
            border: Border.all(
              color: customWhite,
            ),
          ),
          child: Column(children: <Widget>[
            Row(children: [
              SizedBox(
                width: MediaQuery.of(context).size.height * 0.02,
              ),
              Icon(
                icon,
                color: iconColor,
                size: MediaQuery.of(context).size.height * 0.035,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.height * 0.042,
              ),
              titleStyle(titleText,
                  fontSize: MediaQuery.of(context).size.height * 0.025),
              Spacer(),
              _popCloseButton(context,
                  size: MediaQuery.of(context).size.height * 0.03)
            ]),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            _bodyText(context, bodyText),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.04,
            ),
            firstButton,
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            secondButton,
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
          ]),
        )),
  );
}

//PopUp di conferma
Widget confirmPopUp(BuildContext context, String titleText, String bodyText) {
  return WillPopScope(
    onWillPop: () async {
      return false;
    },
    child: AlertDialog(
        titlePadding: EdgeInsets.only(),
        backgroundColor: Colors.transparent,
        title: Container(
          padding: _popUpPadding(context),
          decoration: BoxDecoration(
            borderRadius: mediumBorderRadius(context),
            color: customStrongLiliac,
            border: Border.all(
              color: customWhite,
            ),
          ),
          child: Column(children: <Widget>[
            Row(children: [
              SizedBox(
                width: MediaQuery.of(context).size.height * 0.02,
                height: MediaQuery.of(context).size.height * 0.08,
              ),
              titleStyle(titleText, fontSize: 22),
            ]),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            _bodyText(context, bodyText),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            Icon(CustomIcons.i50_icona_spunta, color: customWhite),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            _confirmButton(
              context,
            )
          ]),
        )),
  );
}

//Bottone X che annulla l'operazione
Widget _popCloseButton(BuildContext context, {double? size}) {
  return Align(
    alignment: Alignment.topRight,
    child: IconButton(
      onPressed: () {
        Navigator.of(context).pop();
      },
      icon: Icon(
        CustomIcons.i11_icona_chiudi_pannello,
        size: size == null ? MediaQuery.of(context).size.height * 0.04 : size,
        color: customWhite,
      ),
    ),
  );
}

//Testo del Dialog
Widget _bodyText(BuildContext context, String title) {
  return Padding(
    padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.01),
    child: Text(
      title,
      style: TextStyle(color: customWhite),
      textAlign: TextAlign.center,
    ),
  );
}

//popup padding
EdgeInsets _popUpPadding(BuildContext context) {
  return EdgeInsets.only(
      top: MediaQuery.of(context).size.width * 0.03,
      bottom: MediaQuery.of(context).size.height * 0.03,
      left: MediaQuery.of(context).size.width * 0.015,
      right: MediaQuery.of(context).size.height * 0.015);
}

Widget _confirmButton(
  BuildContext context,
) {
  return ElevatedButton(
    style: ElevatedButton.styleFrom(
      fixedSize: Size(
        MediaQuery.of(context).size.width * 0.8,
        MediaQuery.of(context).size.height * 0.065,
      ),
      backgroundColor : customWhite,
      foregroundColor : customStrongLiliac,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: customWhite,
        ),
        borderRadius: mediumBorderRadius(context),
      ),
    ),
    onPressed: () async {
      Navigator.of(context).pop();
      Navigator.of(context).pop();
    },
    child: Text(AppLocalizations.of(context)!.confirmation,
        style: TextStyle(
            color: customStrongLiliac,
            fontSize: MediaQuery.of(context).size.height * 0.025)),
  );
}
