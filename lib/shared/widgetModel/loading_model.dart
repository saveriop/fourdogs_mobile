import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:zampon/shared/constants.dart';

//Widget centralizzato dedicato al loader
class Loading extends StatelessWidget {
  final bool isOffline;
  Loading({this.isOffline = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: customPaleLiliac,
      child: isOffline
          ? Center(child: Text("SEI OFFLINE"))
          : Center(
              //E' possibile cambiare lo spinnere cambiando l'oggetto e utilizzando un'altro indicato nel seguente sito:
              //https://pub.dev/packages/flutter_spinkit
              child: SpinKitChasingDots(
                color: customStrongLiliac,
                size: MediaQuery.of(context).size.height * 0.1,
              ),
            ),
    );
  }
}
