import 'package:flutter/material.dart';

import 'package:zampon/shared/constants.dart';
import 'package:zampon/shared/widgetModel/title_model.dart';

/// Appbar di default con titolo e tasto per tornare indietro.
///Utilizzata in tutti gli scaffold esclusa la HOME e gli Slivers
PreferredSizeWidget customAppBar(BuildContext context,
    {bool isBackButton = true,
    Color backButtonColor = customStrongLiliac,
    bool trasparent = false,
    bool noTitle = false}) {
  return PreferredSize(
    preferredSize: Size.fromHeight(MediaQuery.of(context).size.height * 0.08),
    child: AppBar(
      titleSpacing: 0,
      automaticallyImplyLeading: false,
      title: noTitle
          ? appbarNoTitle(context, isBackButton,
              backButtonColor: backButtonColor)
          : _appbarTitle(context, isBackButton,
              backButtonColor: backButtonColor),
      backgroundColor: trasparent ? Colors.transparent : customWhite,
      elevation: 0.0,
    ),
  );
}

///  Appbar classica per gli Scaffold Sliver
/// Tramite il bool BackButton si può decidere se far apparire il tasto indietro
/// oppure no
Widget sliverAppBar(
  BuildContext context,
  bool backButton, {
  String? pageNameText,
  AssetImage? pageNameImg,
  //Widget navigatorBar,
  PreferredSizeWidget? navigatorBar,
  bool home = false,
  Color backButtonColor = customStrongLiliac,
  Animation<double>? animation,
}) {
  return SliverAppBar(
      automaticallyImplyLeading: false,
      titleSpacing: 0,
      title: home
          ? homeTitle(context,
              animation: animation,
              pageNameText: pageNameText,
              pageNameImg: pageNameImg)
          : _appbarTitle(context, backButton, backButtonColor: backButtonColor),
      pinned: true,
      snap: true,
      floating: true,
      backgroundColor: customWhite,
      elevation: 0.0,
      bottom: navigatorBar);
}

///Appabar priva di titolo ma con il tasto indietro
Widget appbarNoTitle(
  BuildContext context,
  bool back, {
  Color backButtonColor = customStrongLiliac,
}) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.only(
        topRight: mediumRadius(context),
        bottomRight: mediumRadius(context),
      ),
      color: Colors.transparent,
    ),
    width: MediaQuery.of(context).size.width * 0.02,
    alignment: Alignment.center,
    child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          back ? backButton(context, backButtonColor) : Text("       "),
        ]),
  );
}

/// Titolo dell'appbar
/// @param back: indica se sarà presente o meno il tasto back
Widget _appbarTitle(
  context,
  bool back, {
  Color backButtonColor = customStrongLiliac,
}) {
  return Container(
    padding: EdgeInsets.only(
      bottom: MediaQuery.of(context).size.height * 0.0125,
    ),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.only(
        topRight: mediumRadius(context),
        bottomRight: mediumRadius(context),
      ),
      color: Colors.transparent,
    ),
    //width: MediaQuery.of(context).size.width * 0.01,
    alignment: Alignment.center,
    child: Row(
        //mainAxisAlignment: MainAxisAlignment.start,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          back ? backButton(context, backButtonColor) : Text("       "),
          Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.0),
            child: onlyTextTitle(context),
          ),
        ]),
  );
}
